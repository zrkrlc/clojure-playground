(ns playground.core
    (:require
      [reagent.core :as r]
      [reagent.dom :as d]))

;; -------------------------
;; Views

(defn playground []
  [:div.container
   [:h2 "Load your content below"]
   [:hr]
   [:div.playground {:id "playground"}]]) ;; REPL mount point

;; -------------------------
;; Initialize app

(defn mount-root []
  (d/render [playground] (.getElementById js/document "playground")))

(defn ^:export init! []
  (mount-root))

;; -------------------------
;; Define mount point for REPL
;; Workflow:
;; 1) Upon initialisation, do (ns clojure_playground.core)
;; 2) Define a reagent function f
;; 3) Render function with (load f)

(defn playground-dom [] (.getElementById js/document "playground"))

(defn mount
  ([& elements]
   (let [node (reduce #(conj %1 (%2)) [:div] (vec elements))
         f    (fn [] node)]
    (d/render [f] (playground-dom)))))
