goog.provide('clojure_playground.core');
clojure_playground.core.playground = (function clojure_playground$core$playground(){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.container","div.container",72419955),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2","h2",-372662728),"Load your content below"], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"hr","hr",1377740067)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.playground","div.playground",-439477354),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"id","id",-1388402092),"playground"], null)], null)], null);
});
clojure_playground.core.mount_root = (function clojure_playground$core$mount_root(){
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [clojure_playground.core.playground], null),document.getElementById("app"));
});
clojure_playground.core.init_BANG_ = (function clojure_playground$core$init_BANG_(){
return clojure_playground.core.mount_root();
});
goog.exportSymbol('clojure_playground.core.init_BANG_', clojure_playground.core.init_BANG_);
clojure_playground.core.playground_dom = (function clojure_playground$core$playground_dom(){
return document.getElementById("playground");
});
clojure_playground.core.mount = (function clojure_playground$core$mount(var_args){
var args__4742__auto__ = [];
var len__4736__auto___35987 = arguments.length;
var i__4737__auto___35992 = (0);
while(true){
if((i__4737__auto___35992 < len__4736__auto___35987)){
args__4742__auto__.push((arguments[i__4737__auto___35992]));

var G__35993 = (i__4737__auto___35992 + (1));
i__4737__auto___35992 = G__35993;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return clojure_playground.core.mount.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(clojure_playground.core.mount.cljs$core$IFn$_invoke$arity$variadic = (function (elements){
var node = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (p1__35964_SHARP_,p2__35965_SHARP_){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(p1__35964_SHARP_,(p2__35965_SHARP_.cljs$core$IFn$_invoke$arity$0 ? p2__35965_SHARP_.cljs$core$IFn$_invoke$arity$0() : p2__35965_SHARP_.call(null)));
}),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632)], null),cljs.core.vec(elements));
var f = (function (){
return node;
});
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [f], null),clojure_playground.core.playground_dom());
}));

(clojure_playground.core.mount.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(clojure_playground.core.mount.cljs$lang$applyTo = (function (seq35968){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq35968));
}));


//# sourceMappingURL=clojure_playground.core.js.map
