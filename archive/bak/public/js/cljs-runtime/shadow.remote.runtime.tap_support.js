goog.provide('shadow.remote.runtime.tap_support');
shadow.remote.runtime.tap_support.tap_subscribe = (function shadow$remote$runtime$tap_support$tap_subscribe(p__33947,p__33948){
var map__33958 = p__33947;
var map__33958__$1 = (((((!((map__33958 == null))))?(((((map__33958.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33958.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33958):map__33958);
var svc = map__33958__$1;
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33958__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
var obj_support = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33958__$1,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33958__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var map__33963 = p__33948;
var map__33963__$1 = (((((!((map__33963 == null))))?(((((map__33963.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33963.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33963):map__33963);
var msg = map__33963__$1;
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33963__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var summary = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33963__$1,new cljs.core.Keyword(null,"summary","summary",380847952));
var history = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33963__$1,new cljs.core.Keyword(null,"history","history",-247395220));
var num = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__33963__$1,new cljs.core.Keyword(null,"num","num",1985240673),(10));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(subs_ref,cljs.core.assoc,from,msg);

if(cljs.core.truth_(history)){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap-subscribed","tap-subscribed",-1882247432),new cljs.core.Keyword(null,"history","history",-247395220),cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (oid){
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"oid","oid",-768692334),oid,new cljs.core.Keyword(null,"summary","summary",380847952),shadow.remote.runtime.obj_support.obj_describe_STAR_(obj_support,oid)], null);
}),shadow.remote.runtime.obj_support.get_tap_history(obj_support,num)))], null));
} else {
return null;
}
});
shadow.remote.runtime.tap_support.tap_unsubscribe = (function shadow$remote$runtime$tap_support$tap_unsubscribe(p__33985,p__33986){
var map__33987 = p__33985;
var map__33987__$1 = (((((!((map__33987 == null))))?(((((map__33987.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33987.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33987):map__33987);
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33987__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
var map__33988 = p__33986;
var map__33988__$1 = (((((!((map__33988 == null))))?(((((map__33988.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33988.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33988):map__33988);
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33988__$1,new cljs.core.Keyword(null,"from","from",1815293044));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(subs_ref,cljs.core.dissoc,from);
});
shadow.remote.runtime.tap_support.request_tap_history = (function shadow$remote$runtime$tap_support$request_tap_history(p__34008,p__34009){
var map__34012 = p__34008;
var map__34012__$1 = (((((!((map__34012 == null))))?(((((map__34012.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34012.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34012):map__34012);
var obj_support = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34012__$1,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34012__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var map__34013 = p__34009;
var map__34013__$1 = (((((!((map__34013 == null))))?(((((map__34013.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34013.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34013):map__34013);
var msg = map__34013__$1;
var num = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__34013__$1,new cljs.core.Keyword(null,"num","num",1985240673),(10));
var tap_ids = shadow.remote.runtime.obj_support.get_tap_history(obj_support,num);
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap-history","tap-history",-282803347),new cljs.core.Keyword(null,"oids","oids",-1580877688),tap_ids], null));
});
shadow.remote.runtime.tap_support.tool_disconnect = (function shadow$remote$runtime$tap_support$tool_disconnect(p__34025,tid){
var map__34027 = p__34025;
var map__34027__$1 = (((((!((map__34027 == null))))?(((((map__34027.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34027.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34027):map__34027);
var svc = map__34027__$1;
var subs_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34027__$1,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(subs_ref,cljs.core.dissoc,tid);
});
shadow.remote.runtime.tap_support.start = (function shadow$remote$runtime$tap_support$start(runtime,obj_support){
var subs_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var tap_fn = (function shadow$remote$runtime$tap_support$start_$_runtime_tap(obj){
if((!((obj == null)))){
var oid = shadow.remote.runtime.obj_support.register(obj_support,obj,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"from","from",1815293044),new cljs.core.Keyword(null,"tap","tap",-1086702463)], null));
var seq__34051 = cljs.core.seq(cljs.core.deref(subs_ref));
var chunk__34053 = null;
var count__34054 = (0);
var i__34055 = (0);
while(true){
if((i__34055 < count__34054)){
var vec__34076 = chunk__34053.cljs$core$IIndexed$_nth$arity$2(null,i__34055);
var tid = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34076,(0),null);
var tap_config = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34076,(1),null);
shadow.remote.runtime.api.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap","tap",-1086702463),new cljs.core.Keyword(null,"to","to",192099007),tid,new cljs.core.Keyword(null,"oid","oid",-768692334),oid], null));


var G__34130 = seq__34051;
var G__34131 = chunk__34053;
var G__34132 = count__34054;
var G__34133 = (i__34055 + (1));
seq__34051 = G__34130;
chunk__34053 = G__34131;
count__34054 = G__34132;
i__34055 = G__34133;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34051);
if(temp__5735__auto__){
var seq__34051__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34051__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34051__$1);
var G__34134 = cljs.core.chunk_rest(seq__34051__$1);
var G__34135 = c__4556__auto__;
var G__34136 = cljs.core.count(c__4556__auto__);
var G__34137 = (0);
seq__34051 = G__34134;
chunk__34053 = G__34135;
count__34054 = G__34136;
i__34055 = G__34137;
continue;
} else {
var vec__34083 = cljs.core.first(seq__34051__$1);
var tid = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34083,(0),null);
var tap_config = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34083,(1),null);
shadow.remote.runtime.api.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"tap","tap",-1086702463),new cljs.core.Keyword(null,"to","to",192099007),tid,new cljs.core.Keyword(null,"oid","oid",-768692334),oid], null));


var G__34141 = cljs.core.next(seq__34051__$1);
var G__34142 = null;
var G__34143 = (0);
var G__34144 = (0);
seq__34051 = G__34141;
chunk__34053 = G__34142;
count__34054 = G__34143;
i__34055 = G__34144;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
});
var svc = new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime,new cljs.core.Keyword(null,"obj-support","obj-support",1522559229),obj_support,new cljs.core.Keyword(null,"tap-fn","tap-fn",1573556461),tap_fn,new cljs.core.Keyword(null,"subs-ref","subs-ref",-1355989911),subs_ref], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.tap-support","ext","shadow.remote.runtime.tap-support/ext",1019069674),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"tap-subscribe","tap-subscribe",411179050),(function (p1__34037_SHARP_){
return shadow.remote.runtime.tap_support.tap_subscribe(svc,p1__34037_SHARP_);
}),new cljs.core.Keyword(null,"tap-unsubscribe","tap-unsubscribe",1183890755),(function (p1__34039_SHARP_){
return shadow.remote.runtime.tap_support.tap_unsubscribe(svc,p1__34039_SHARP_);
}),new cljs.core.Keyword(null,"request-tap-history","request-tap-history",-670837812),(function (p1__34040_SHARP_){
return shadow.remote.runtime.tap_support.request_tap_history(svc,p1__34040_SHARP_);
})], null),new cljs.core.Keyword(null,"on-tool-disconnect","on-tool-disconnect",693464366),(function (p1__34041_SHARP_){
return shadow.remote.runtime.tap_support.tool_disconnect(svc,p1__34041_SHARP_);
})], null));

cljs.core.add_tap(tap_fn);

return svc;
});
shadow.remote.runtime.tap_support.stop = (function shadow$remote$runtime$tap_support$stop(p__34096){
var map__34097 = p__34096;
var map__34097__$1 = (((((!((map__34097 == null))))?(((((map__34097.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34097.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34097):map__34097);
var svc = map__34097__$1;
var tap_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34097__$1,new cljs.core.Keyword(null,"tap-fn","tap-fn",1573556461));
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34097__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
cljs.core.remove_tap(tap_fn);

return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.tap-support","ext","shadow.remote.runtime.tap-support/ext",1019069674));
});

//# sourceMappingURL=shadow.remote.runtime.tap_support.js.map
