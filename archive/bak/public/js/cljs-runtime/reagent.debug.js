goog.provide('reagent.debug');
reagent.debug.has_console = (typeof console !== 'undefined');
reagent.debug.tracking = false;
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.warnings !== 'undefined')){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.track_console !== 'undefined')){
} else {
reagent.debug.track_console = (function (){var o = ({});
(o.warn = (function() { 
var G__33807__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__33807 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__33809__i = 0, G__33809__a = new Array(arguments.length -  0);
while (G__33809__i < G__33809__a.length) {G__33809__a[G__33809__i] = arguments[G__33809__i + 0]; ++G__33809__i;}
  args = new cljs.core.IndexedSeq(G__33809__a,0,null);
} 
return G__33807__delegate.call(this,args);};
G__33807.cljs$lang$maxFixedArity = 0;
G__33807.cljs$lang$applyTo = (function (arglist__33810){
var args = cljs.core.seq(arglist__33810);
return G__33807__delegate(args);
});
G__33807.cljs$core$IFn$_invoke$arity$variadic = G__33807__delegate;
return G__33807;
})()
);

(o.error = (function() { 
var G__33811__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__33811 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__33812__i = 0, G__33812__a = new Array(arguments.length -  0);
while (G__33812__i < G__33812__a.length) {G__33812__a[G__33812__i] = arguments[G__33812__i + 0]; ++G__33812__i;}
  args = new cljs.core.IndexedSeq(G__33812__a,0,null);
} 
return G__33811__delegate.call(this,args);};
G__33811.cljs$lang$maxFixedArity = 0;
G__33811.cljs$lang$applyTo = (function (arglist__33813){
var args = cljs.core.seq(arglist__33813);
return G__33811__delegate(args);
});
G__33811.cljs$core$IFn$_invoke$arity$variadic = G__33811__delegate;
return G__33811;
})()
);

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
(reagent.debug.tracking = true);

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

(reagent.debug.tracking = false);

return warns;
});

//# sourceMappingURL=reagent.debug.js.map
