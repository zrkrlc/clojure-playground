goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_35323 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_35323(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_35327 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_35327(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__34432 = coll;
var G__34433 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__34432,G__34433) : shadow.dom.lazy_native_coll_seq.call(null,G__34432,G__34433));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4126__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__34450 = arguments.length;
switch (G__34450) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__34453 = arguments.length;
switch (G__34453) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__34463 = arguments.length;
switch (G__34463) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__34467 = arguments.length;
switch (G__34467) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__34478 = arguments.length;
switch (G__34478) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__34490 = arguments.length;
switch (G__34490) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e34501){if((e34501 instanceof Object)){
var e = e34501;
return console.log("didnt support attachEvent",el,e);
} else {
throw e34501;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__34507 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__34508 = null;
var count__34509 = (0);
var i__34510 = (0);
while(true){
if((i__34510 < count__34509)){
var el = chunk__34508.cljs$core$IIndexed$_nth$arity$2(null,i__34510);
var handler_35361__$1 = ((function (seq__34507,chunk__34508,count__34509,i__34510,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__34507,chunk__34508,count__34509,i__34510,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_35361__$1);


var G__35363 = seq__34507;
var G__35364 = chunk__34508;
var G__35365 = count__34509;
var G__35366 = (i__34510 + (1));
seq__34507 = G__35363;
chunk__34508 = G__35364;
count__34509 = G__35365;
i__34510 = G__35366;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34507);
if(temp__5735__auto__){
var seq__34507__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34507__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34507__$1);
var G__35368 = cljs.core.chunk_rest(seq__34507__$1);
var G__35369 = c__4556__auto__;
var G__35370 = cljs.core.count(c__4556__auto__);
var G__35371 = (0);
seq__34507 = G__35368;
chunk__34508 = G__35369;
count__34509 = G__35370;
i__34510 = G__35371;
continue;
} else {
var el = cljs.core.first(seq__34507__$1);
var handler_35372__$1 = ((function (seq__34507,chunk__34508,count__34509,i__34510,el,seq__34507__$1,temp__5735__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__34507,chunk__34508,count__34509,i__34510,el,seq__34507__$1,temp__5735__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_35372__$1);


var G__35377 = cljs.core.next(seq__34507__$1);
var G__35378 = null;
var G__35379 = (0);
var G__35380 = (0);
seq__34507 = G__35377;
chunk__34508 = G__35378;
count__34509 = G__35379;
i__34510 = G__35380;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__34531 = arguments.length;
switch (G__34531) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__34545 = cljs.core.seq(events);
var chunk__34546 = null;
var count__34547 = (0);
var i__34548 = (0);
while(true){
if((i__34548 < count__34547)){
var vec__34563 = chunk__34546.cljs$core$IIndexed$_nth$arity$2(null,i__34548);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34563,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34563,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__35393 = seq__34545;
var G__35394 = chunk__34546;
var G__35395 = count__34547;
var G__35396 = (i__34548 + (1));
seq__34545 = G__35393;
chunk__34546 = G__35394;
count__34547 = G__35395;
i__34548 = G__35396;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34545);
if(temp__5735__auto__){
var seq__34545__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34545__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34545__$1);
var G__35399 = cljs.core.chunk_rest(seq__34545__$1);
var G__35400 = c__4556__auto__;
var G__35401 = cljs.core.count(c__4556__auto__);
var G__35402 = (0);
seq__34545 = G__35399;
chunk__34546 = G__35400;
count__34547 = G__35401;
i__34548 = G__35402;
continue;
} else {
var vec__34569 = cljs.core.first(seq__34545__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34569,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34569,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__35407 = cljs.core.next(seq__34545__$1);
var G__35408 = null;
var G__35409 = (0);
var G__35410 = (0);
seq__34545 = G__35407;
chunk__34546 = G__35408;
count__34547 = G__35409;
i__34548 = G__35410;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__34579 = cljs.core.seq(styles);
var chunk__34580 = null;
var count__34581 = (0);
var i__34582 = (0);
while(true){
if((i__34582 < count__34581)){
var vec__34597 = chunk__34580.cljs$core$IIndexed$_nth$arity$2(null,i__34582);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34597,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34597,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__35415 = seq__34579;
var G__35416 = chunk__34580;
var G__35417 = count__34581;
var G__35418 = (i__34582 + (1));
seq__34579 = G__35415;
chunk__34580 = G__35416;
count__34581 = G__35417;
i__34582 = G__35418;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34579);
if(temp__5735__auto__){
var seq__34579__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34579__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34579__$1);
var G__35421 = cljs.core.chunk_rest(seq__34579__$1);
var G__35422 = c__4556__auto__;
var G__35423 = cljs.core.count(c__4556__auto__);
var G__35424 = (0);
seq__34579 = G__35421;
chunk__34580 = G__35422;
count__34581 = G__35423;
i__34582 = G__35424;
continue;
} else {
var vec__34606 = cljs.core.first(seq__34579__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34606,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34606,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__35428 = cljs.core.next(seq__34579__$1);
var G__35429 = null;
var G__35430 = (0);
var G__35431 = (0);
seq__34579 = G__35428;
chunk__34580 = G__35429;
count__34581 = G__35430;
i__34582 = G__35431;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__34617_35433 = key;
var G__34617_35434__$1 = (((G__34617_35433 instanceof cljs.core.Keyword))?G__34617_35433.fqn:null);
switch (G__34617_35434__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_35447 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4126__auto__ = goog.string.startsWith(ks_35447,"data-");
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return goog.string.startsWith(ks_35447,"aria-");
}
})())){
el.setAttribute(ks_35447,value);
} else {
(el[ks_35447] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__34665){
var map__34666 = p__34665;
var map__34666__$1 = (((((!((map__34666 == null))))?(((((map__34666.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__34666.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__34666):map__34666);
var props = map__34666__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34666__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__34671 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34671,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34671,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34671,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__34676 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__34676,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__34676;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__34686 = arguments.length;
switch (G__34686) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__34696){
var vec__34698 = p__34696;
var seq__34699 = cljs.core.seq(vec__34698);
var first__34700 = cljs.core.first(seq__34699);
var seq__34699__$1 = cljs.core.next(seq__34699);
var nn = first__34700;
var first__34700__$1 = cljs.core.first(seq__34699__$1);
var seq__34699__$2 = cljs.core.next(seq__34699__$1);
var np = first__34700__$1;
var nc = seq__34699__$2;
var node = vec__34698;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__34703 = nn;
var G__34704 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__34703,G__34704) : create_fn.call(null,G__34703,G__34704));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__34706 = nn;
var G__34707 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__34706,G__34707) : create_fn.call(null,G__34706,G__34707));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__34711 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34711,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34711,(1),null);
var seq__34716_35520 = cljs.core.seq(node_children);
var chunk__34717_35521 = null;
var count__34718_35522 = (0);
var i__34719_35523 = (0);
while(true){
if((i__34719_35523 < count__34718_35522)){
var child_struct_35524 = chunk__34717_35521.cljs$core$IIndexed$_nth$arity$2(null,i__34719_35523);
var children_35525 = shadow.dom.dom_node(child_struct_35524);
if(cljs.core.seq_QMARK_(children_35525)){
var seq__34758_35528 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_35525));
var chunk__34760_35529 = null;
var count__34761_35530 = (0);
var i__34762_35531 = (0);
while(true){
if((i__34762_35531 < count__34761_35530)){
var child_35537 = chunk__34760_35529.cljs$core$IIndexed$_nth$arity$2(null,i__34762_35531);
if(cljs.core.truth_(child_35537)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35537);


var G__35539 = seq__34758_35528;
var G__35540 = chunk__34760_35529;
var G__35541 = count__34761_35530;
var G__35542 = (i__34762_35531 + (1));
seq__34758_35528 = G__35539;
chunk__34760_35529 = G__35540;
count__34761_35530 = G__35541;
i__34762_35531 = G__35542;
continue;
} else {
var G__35544 = seq__34758_35528;
var G__35545 = chunk__34760_35529;
var G__35546 = count__34761_35530;
var G__35547 = (i__34762_35531 + (1));
seq__34758_35528 = G__35544;
chunk__34760_35529 = G__35545;
count__34761_35530 = G__35546;
i__34762_35531 = G__35547;
continue;
}
} else {
var temp__5735__auto___35550 = cljs.core.seq(seq__34758_35528);
if(temp__5735__auto___35550){
var seq__34758_35551__$1 = temp__5735__auto___35550;
if(cljs.core.chunked_seq_QMARK_(seq__34758_35551__$1)){
var c__4556__auto___35552 = cljs.core.chunk_first(seq__34758_35551__$1);
var G__35553 = cljs.core.chunk_rest(seq__34758_35551__$1);
var G__35554 = c__4556__auto___35552;
var G__35555 = cljs.core.count(c__4556__auto___35552);
var G__35556 = (0);
seq__34758_35528 = G__35553;
chunk__34760_35529 = G__35554;
count__34761_35530 = G__35555;
i__34762_35531 = G__35556;
continue;
} else {
var child_35557 = cljs.core.first(seq__34758_35551__$1);
if(cljs.core.truth_(child_35557)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35557);


var G__35559 = cljs.core.next(seq__34758_35551__$1);
var G__35560 = null;
var G__35561 = (0);
var G__35562 = (0);
seq__34758_35528 = G__35559;
chunk__34760_35529 = G__35560;
count__34761_35530 = G__35561;
i__34762_35531 = G__35562;
continue;
} else {
var G__35563 = cljs.core.next(seq__34758_35551__$1);
var G__35564 = null;
var G__35565 = (0);
var G__35566 = (0);
seq__34758_35528 = G__35563;
chunk__34760_35529 = G__35564;
count__34761_35530 = G__35565;
i__34762_35531 = G__35566;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_35525);
}


var G__35568 = seq__34716_35520;
var G__35569 = chunk__34717_35521;
var G__35570 = count__34718_35522;
var G__35571 = (i__34719_35523 + (1));
seq__34716_35520 = G__35568;
chunk__34717_35521 = G__35569;
count__34718_35522 = G__35570;
i__34719_35523 = G__35571;
continue;
} else {
var temp__5735__auto___35576 = cljs.core.seq(seq__34716_35520);
if(temp__5735__auto___35576){
var seq__34716_35577__$1 = temp__5735__auto___35576;
if(cljs.core.chunked_seq_QMARK_(seq__34716_35577__$1)){
var c__4556__auto___35578 = cljs.core.chunk_first(seq__34716_35577__$1);
var G__35581 = cljs.core.chunk_rest(seq__34716_35577__$1);
var G__35582 = c__4556__auto___35578;
var G__35583 = cljs.core.count(c__4556__auto___35578);
var G__35584 = (0);
seq__34716_35520 = G__35581;
chunk__34717_35521 = G__35582;
count__34718_35522 = G__35583;
i__34719_35523 = G__35584;
continue;
} else {
var child_struct_35586 = cljs.core.first(seq__34716_35577__$1);
var children_35588 = shadow.dom.dom_node(child_struct_35586);
if(cljs.core.seq_QMARK_(children_35588)){
var seq__34788_35589 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_35588));
var chunk__34790_35590 = null;
var count__34791_35591 = (0);
var i__34792_35592 = (0);
while(true){
if((i__34792_35592 < count__34791_35591)){
var child_35593 = chunk__34790_35590.cljs$core$IIndexed$_nth$arity$2(null,i__34792_35592);
if(cljs.core.truth_(child_35593)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35593);


var G__35594 = seq__34788_35589;
var G__35595 = chunk__34790_35590;
var G__35596 = count__34791_35591;
var G__35597 = (i__34792_35592 + (1));
seq__34788_35589 = G__35594;
chunk__34790_35590 = G__35595;
count__34791_35591 = G__35596;
i__34792_35592 = G__35597;
continue;
} else {
var G__35598 = seq__34788_35589;
var G__35599 = chunk__34790_35590;
var G__35600 = count__34791_35591;
var G__35601 = (i__34792_35592 + (1));
seq__34788_35589 = G__35598;
chunk__34790_35590 = G__35599;
count__34791_35591 = G__35600;
i__34792_35592 = G__35601;
continue;
}
} else {
var temp__5735__auto___35602__$1 = cljs.core.seq(seq__34788_35589);
if(temp__5735__auto___35602__$1){
var seq__34788_35603__$1 = temp__5735__auto___35602__$1;
if(cljs.core.chunked_seq_QMARK_(seq__34788_35603__$1)){
var c__4556__auto___35604 = cljs.core.chunk_first(seq__34788_35603__$1);
var G__35605 = cljs.core.chunk_rest(seq__34788_35603__$1);
var G__35606 = c__4556__auto___35604;
var G__35607 = cljs.core.count(c__4556__auto___35604);
var G__35608 = (0);
seq__34788_35589 = G__35605;
chunk__34790_35590 = G__35606;
count__34791_35591 = G__35607;
i__34792_35592 = G__35608;
continue;
} else {
var child_35609 = cljs.core.first(seq__34788_35603__$1);
if(cljs.core.truth_(child_35609)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_35609);


var G__35610 = cljs.core.next(seq__34788_35603__$1);
var G__35611 = null;
var G__35612 = (0);
var G__35613 = (0);
seq__34788_35589 = G__35610;
chunk__34790_35590 = G__35611;
count__34791_35591 = G__35612;
i__34792_35592 = G__35613;
continue;
} else {
var G__35614 = cljs.core.next(seq__34788_35603__$1);
var G__35615 = null;
var G__35616 = (0);
var G__35617 = (0);
seq__34788_35589 = G__35614;
chunk__34790_35590 = G__35615;
count__34791_35591 = G__35616;
i__34792_35592 = G__35617;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_35588);
}


var G__35621 = cljs.core.next(seq__34716_35577__$1);
var G__35622 = null;
var G__35623 = (0);
var G__35624 = (0);
seq__34716_35520 = G__35621;
chunk__34717_35521 = G__35622;
count__34718_35522 = G__35623;
i__34719_35523 = G__35624;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__34806 = cljs.core.seq(node);
var chunk__34807 = null;
var count__34808 = (0);
var i__34809 = (0);
while(true){
if((i__34809 < count__34808)){
var n = chunk__34807.cljs$core$IIndexed$_nth$arity$2(null,i__34809);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__35629 = seq__34806;
var G__35630 = chunk__34807;
var G__35631 = count__34808;
var G__35632 = (i__34809 + (1));
seq__34806 = G__35629;
chunk__34807 = G__35630;
count__34808 = G__35631;
i__34809 = G__35632;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34806);
if(temp__5735__auto__){
var seq__34806__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34806__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34806__$1);
var G__35633 = cljs.core.chunk_rest(seq__34806__$1);
var G__35634 = c__4556__auto__;
var G__35635 = cljs.core.count(c__4556__auto__);
var G__35636 = (0);
seq__34806 = G__35633;
chunk__34807 = G__35634;
count__34808 = G__35635;
i__34809 = G__35636;
continue;
} else {
var n = cljs.core.first(seq__34806__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__35637 = cljs.core.next(seq__34806__$1);
var G__35638 = null;
var G__35639 = (0);
var G__35640 = (0);
seq__34806 = G__35637;
chunk__34807 = G__35638;
count__34808 = G__35639;
i__34809 = G__35640;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__34815 = arguments.length;
switch (G__34815) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__34820 = arguments.length;
switch (G__34820) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__34831 = arguments.length;
switch (G__34831) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4126__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4742__auto__ = [];
var len__4736__auto___35648 = arguments.length;
var i__4737__auto___35649 = (0);
while(true){
if((i__4737__auto___35649 < len__4736__auto___35648)){
args__4742__auto__.push((arguments[i__4737__auto___35649]));

var G__35650 = (i__4737__auto___35649 + (1));
i__4737__auto___35649 = G__35650;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__34850_35651 = cljs.core.seq(nodes);
var chunk__34851_35652 = null;
var count__34852_35653 = (0);
var i__34853_35654 = (0);
while(true){
if((i__34853_35654 < count__34852_35653)){
var node_35655 = chunk__34851_35652.cljs$core$IIndexed$_nth$arity$2(null,i__34853_35654);
fragment.appendChild(shadow.dom._to_dom(node_35655));


var G__35657 = seq__34850_35651;
var G__35658 = chunk__34851_35652;
var G__35659 = count__34852_35653;
var G__35660 = (i__34853_35654 + (1));
seq__34850_35651 = G__35657;
chunk__34851_35652 = G__35658;
count__34852_35653 = G__35659;
i__34853_35654 = G__35660;
continue;
} else {
var temp__5735__auto___35661 = cljs.core.seq(seq__34850_35651);
if(temp__5735__auto___35661){
var seq__34850_35662__$1 = temp__5735__auto___35661;
if(cljs.core.chunked_seq_QMARK_(seq__34850_35662__$1)){
var c__4556__auto___35664 = cljs.core.chunk_first(seq__34850_35662__$1);
var G__35665 = cljs.core.chunk_rest(seq__34850_35662__$1);
var G__35666 = c__4556__auto___35664;
var G__35667 = cljs.core.count(c__4556__auto___35664);
var G__35668 = (0);
seq__34850_35651 = G__35665;
chunk__34851_35652 = G__35666;
count__34852_35653 = G__35667;
i__34853_35654 = G__35668;
continue;
} else {
var node_35669 = cljs.core.first(seq__34850_35662__$1);
fragment.appendChild(shadow.dom._to_dom(node_35669));


var G__35670 = cljs.core.next(seq__34850_35662__$1);
var G__35671 = null;
var G__35672 = (0);
var G__35673 = (0);
seq__34850_35651 = G__35670;
chunk__34851_35652 = G__35671;
count__34852_35653 = G__35672;
i__34853_35654 = G__35673;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq34845){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq34845));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__34864_35677 = cljs.core.seq(scripts);
var chunk__34865_35678 = null;
var count__34866_35679 = (0);
var i__34867_35680 = (0);
while(true){
if((i__34867_35680 < count__34866_35679)){
var vec__34884_35681 = chunk__34865_35678.cljs$core$IIndexed$_nth$arity$2(null,i__34867_35680);
var script_tag_35682 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34884_35681,(0),null);
var script_body_35683 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34884_35681,(1),null);
eval(script_body_35683);


var G__35684 = seq__34864_35677;
var G__35685 = chunk__34865_35678;
var G__35686 = count__34866_35679;
var G__35687 = (i__34867_35680 + (1));
seq__34864_35677 = G__35684;
chunk__34865_35678 = G__35685;
count__34866_35679 = G__35686;
i__34867_35680 = G__35687;
continue;
} else {
var temp__5735__auto___35688 = cljs.core.seq(seq__34864_35677);
if(temp__5735__auto___35688){
var seq__34864_35689__$1 = temp__5735__auto___35688;
if(cljs.core.chunked_seq_QMARK_(seq__34864_35689__$1)){
var c__4556__auto___35690 = cljs.core.chunk_first(seq__34864_35689__$1);
var G__35691 = cljs.core.chunk_rest(seq__34864_35689__$1);
var G__35692 = c__4556__auto___35690;
var G__35693 = cljs.core.count(c__4556__auto___35690);
var G__35694 = (0);
seq__34864_35677 = G__35691;
chunk__34865_35678 = G__35692;
count__34866_35679 = G__35693;
i__34867_35680 = G__35694;
continue;
} else {
var vec__34890_35695 = cljs.core.first(seq__34864_35689__$1);
var script_tag_35696 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34890_35695,(0),null);
var script_body_35697 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34890_35695,(1),null);
eval(script_body_35697);


var G__35698 = cljs.core.next(seq__34864_35689__$1);
var G__35699 = null;
var G__35700 = (0);
var G__35701 = (0);
seq__34864_35677 = G__35698;
chunk__34865_35678 = G__35699;
count__34866_35679 = G__35700;
i__34867_35680 = G__35701;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__34899){
var vec__34902 = p__34899;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34902,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34902,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__34917 = arguments.length;
switch (G__34917) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__34930 = cljs.core.seq(style_keys);
var chunk__34931 = null;
var count__34932 = (0);
var i__34933 = (0);
while(true){
if((i__34933 < count__34932)){
var it = chunk__34931.cljs$core$IIndexed$_nth$arity$2(null,i__34933);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__35706 = seq__34930;
var G__35707 = chunk__34931;
var G__35708 = count__34932;
var G__35709 = (i__34933 + (1));
seq__34930 = G__35706;
chunk__34931 = G__35707;
count__34932 = G__35708;
i__34933 = G__35709;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__34930);
if(temp__5735__auto__){
var seq__34930__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34930__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__34930__$1);
var G__35713 = cljs.core.chunk_rest(seq__34930__$1);
var G__35714 = c__4556__auto__;
var G__35715 = cljs.core.count(c__4556__auto__);
var G__35716 = (0);
seq__34930 = G__35713;
chunk__34931 = G__35714;
count__34932 = G__35715;
i__34933 = G__35716;
continue;
} else {
var it = cljs.core.first(seq__34930__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__35717 = cljs.core.next(seq__34930__$1);
var G__35718 = null;
var G__35719 = (0);
var G__35720 = (0);
seq__34930 = G__35717;
chunk__34931 = G__35718;
count__34932 = G__35719;
i__34933 = G__35720;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k34943,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__34951 = k34943;
var G__34951__$1 = (((G__34951 instanceof cljs.core.Keyword))?G__34951.fqn:null);
switch (G__34951__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k34943,else__4383__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__34954){
var vec__34955 = p__34954;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34955,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34955,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__34942){
var self__ = this;
var G__34942__$1 = this;
return (new cljs.core.RecordIter((0),G__34942__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (coll__4377__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
})(this__4376__auto____$1);
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this34944,other34945){
var self__ = this;
var this34944__$1 = this;
return (((!((other34945 == null)))) && ((this34944__$1.constructor === other34945.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34944__$1.x,other34945.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34944__$1.y,other34945.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this34944__$1.__extmap,other34945.__extmap)));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__34942){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__34975 = cljs.core.keyword_identical_QMARK_;
var expr__34976 = k__4388__auto__;
if(cljs.core.truth_((pred__34975.cljs$core$IFn$_invoke$arity$2 ? pred__34975.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__34976) : pred__34975.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__34976)))){
return (new shadow.dom.Coordinate(G__34942,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__34975.cljs$core$IFn$_invoke$arity$2 ? pred__34975.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__34976) : pred__34975.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__34976)))){
return (new shadow.dom.Coordinate(self__.x,G__34942,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__34942),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__34942){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__34942,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4386__auto__,(0)),cljs.core._nth(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__34949){
var extmap__4419__auto__ = (function (){var G__35019 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__34949,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__34949)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__35019);
} else {
return G__35019;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__34949),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__34949),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k35032,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__35067 = k35032;
var G__35067__$1 = (((G__35067 instanceof cljs.core.Keyword))?G__35067.fqn:null);
switch (G__35067__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k35032,else__4383__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__35073){
var vec__35074 = p__35073;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35074,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35074,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Size{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__35031){
var self__ = this;
var G__35031__$1 = this;
return (new cljs.core.RecordIter((0),G__35031__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (coll__4377__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
})(this__4376__auto____$1);
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this35033,other35034){
var self__ = this;
var this35033__$1 = this;
return (((!((other35034 == null)))) && ((this35033__$1.constructor === other35034.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35033__$1.w,other35034.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35033__$1.h,other35034.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35033__$1.__extmap,other35034.__extmap)));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__35031){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__35093 = cljs.core.keyword_identical_QMARK_;
var expr__35094 = k__4388__auto__;
if(cljs.core.truth_((pred__35093.cljs$core$IFn$_invoke$arity$2 ? pred__35093.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__35094) : pred__35093.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__35094)))){
return (new shadow.dom.Size(G__35031,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__35093.cljs$core$IFn$_invoke$arity$2 ? pred__35093.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__35094) : pred__35093.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__35094)))){
return (new shadow.dom.Size(self__.w,G__35031,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__35031),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__35031){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__35031,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4386__auto__,(0)),cljs.core._nth(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__35042){
var extmap__4419__auto__ = (function (){var G__35102 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__35042,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__35042)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__35102);
} else {
return G__35102;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__35042),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__35042),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4610__auto__ = opts;
var l__4611__auto__ = a__4610__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4611__auto__)){
var G__35754 = (i + (1));
var G__35755 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__35754;
ret = G__35755;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__35126){
var vec__35127 = p__35126;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35127,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35127,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__35135 = arguments.length;
switch (G__35135) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5733__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5733__auto__)){
var child = temp__5733__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__35757 = ps;
var G__35758 = (i + (1));
el__$1 = G__35757;
i = G__35758;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__35165 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35165,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35165,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35165,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__35172_35766 = cljs.core.seq(props);
var chunk__35173_35767 = null;
var count__35174_35768 = (0);
var i__35175_35769 = (0);
while(true){
if((i__35175_35769 < count__35174_35768)){
var vec__35193_35770 = chunk__35173_35767.cljs$core$IIndexed$_nth$arity$2(null,i__35175_35769);
var k_35771 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35193_35770,(0),null);
var v_35772 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35193_35770,(1),null);
el.setAttributeNS((function (){var temp__5735__auto__ = cljs.core.namespace(k_35771);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_35771),v_35772);


var G__35773 = seq__35172_35766;
var G__35774 = chunk__35173_35767;
var G__35775 = count__35174_35768;
var G__35776 = (i__35175_35769 + (1));
seq__35172_35766 = G__35773;
chunk__35173_35767 = G__35774;
count__35174_35768 = G__35775;
i__35175_35769 = G__35776;
continue;
} else {
var temp__5735__auto___35777 = cljs.core.seq(seq__35172_35766);
if(temp__5735__auto___35777){
var seq__35172_35778__$1 = temp__5735__auto___35777;
if(cljs.core.chunked_seq_QMARK_(seq__35172_35778__$1)){
var c__4556__auto___35779 = cljs.core.chunk_first(seq__35172_35778__$1);
var G__35780 = cljs.core.chunk_rest(seq__35172_35778__$1);
var G__35781 = c__4556__auto___35779;
var G__35782 = cljs.core.count(c__4556__auto___35779);
var G__35783 = (0);
seq__35172_35766 = G__35780;
chunk__35173_35767 = G__35781;
count__35174_35768 = G__35782;
i__35175_35769 = G__35783;
continue;
} else {
var vec__35201_35784 = cljs.core.first(seq__35172_35778__$1);
var k_35785 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35201_35784,(0),null);
var v_35786 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35201_35784,(1),null);
el.setAttributeNS((function (){var temp__5735__auto____$1 = cljs.core.namespace(k_35785);
if(cljs.core.truth_(temp__5735__auto____$1)){
var ns = temp__5735__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_35785),v_35786);


var G__35788 = cljs.core.next(seq__35172_35778__$1);
var G__35789 = null;
var G__35790 = (0);
var G__35791 = (0);
seq__35172_35766 = G__35788;
chunk__35173_35767 = G__35789;
count__35174_35768 = G__35790;
i__35175_35769 = G__35791;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__35216 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35216,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35216,(1),null);
var seq__35221_35792 = cljs.core.seq(node_children);
var chunk__35223_35793 = null;
var count__35224_35794 = (0);
var i__35225_35795 = (0);
while(true){
if((i__35225_35795 < count__35224_35794)){
var child_struct_35796 = chunk__35223_35793.cljs$core$IIndexed$_nth$arity$2(null,i__35225_35795);
if((!((child_struct_35796 == null)))){
if(typeof child_struct_35796 === 'string'){
var text_35797 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_35797),child_struct_35796].join(''));
} else {
var children_35798 = shadow.dom.svg_node(child_struct_35796);
if(cljs.core.seq_QMARK_(children_35798)){
var seq__35259_35799 = cljs.core.seq(children_35798);
var chunk__35261_35800 = null;
var count__35262_35801 = (0);
var i__35263_35802 = (0);
while(true){
if((i__35263_35802 < count__35262_35801)){
var child_35803 = chunk__35261_35800.cljs$core$IIndexed$_nth$arity$2(null,i__35263_35802);
if(cljs.core.truth_(child_35803)){
node.appendChild(child_35803);


var G__35804 = seq__35259_35799;
var G__35805 = chunk__35261_35800;
var G__35806 = count__35262_35801;
var G__35807 = (i__35263_35802 + (1));
seq__35259_35799 = G__35804;
chunk__35261_35800 = G__35805;
count__35262_35801 = G__35806;
i__35263_35802 = G__35807;
continue;
} else {
var G__35808 = seq__35259_35799;
var G__35809 = chunk__35261_35800;
var G__35810 = count__35262_35801;
var G__35811 = (i__35263_35802 + (1));
seq__35259_35799 = G__35808;
chunk__35261_35800 = G__35809;
count__35262_35801 = G__35810;
i__35263_35802 = G__35811;
continue;
}
} else {
var temp__5735__auto___35812 = cljs.core.seq(seq__35259_35799);
if(temp__5735__auto___35812){
var seq__35259_35813__$1 = temp__5735__auto___35812;
if(cljs.core.chunked_seq_QMARK_(seq__35259_35813__$1)){
var c__4556__auto___35814 = cljs.core.chunk_first(seq__35259_35813__$1);
var G__35815 = cljs.core.chunk_rest(seq__35259_35813__$1);
var G__35816 = c__4556__auto___35814;
var G__35817 = cljs.core.count(c__4556__auto___35814);
var G__35818 = (0);
seq__35259_35799 = G__35815;
chunk__35261_35800 = G__35816;
count__35262_35801 = G__35817;
i__35263_35802 = G__35818;
continue;
} else {
var child_35820 = cljs.core.first(seq__35259_35813__$1);
if(cljs.core.truth_(child_35820)){
node.appendChild(child_35820);


var G__35823 = cljs.core.next(seq__35259_35813__$1);
var G__35824 = null;
var G__35825 = (0);
var G__35826 = (0);
seq__35259_35799 = G__35823;
chunk__35261_35800 = G__35824;
count__35262_35801 = G__35825;
i__35263_35802 = G__35826;
continue;
} else {
var G__35827 = cljs.core.next(seq__35259_35813__$1);
var G__35828 = null;
var G__35829 = (0);
var G__35830 = (0);
seq__35259_35799 = G__35827;
chunk__35261_35800 = G__35828;
count__35262_35801 = G__35829;
i__35263_35802 = G__35830;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_35798);
}
}


var G__35833 = seq__35221_35792;
var G__35834 = chunk__35223_35793;
var G__35835 = count__35224_35794;
var G__35836 = (i__35225_35795 + (1));
seq__35221_35792 = G__35833;
chunk__35223_35793 = G__35834;
count__35224_35794 = G__35835;
i__35225_35795 = G__35836;
continue;
} else {
var G__35838 = seq__35221_35792;
var G__35839 = chunk__35223_35793;
var G__35840 = count__35224_35794;
var G__35841 = (i__35225_35795 + (1));
seq__35221_35792 = G__35838;
chunk__35223_35793 = G__35839;
count__35224_35794 = G__35840;
i__35225_35795 = G__35841;
continue;
}
} else {
var temp__5735__auto___35842 = cljs.core.seq(seq__35221_35792);
if(temp__5735__auto___35842){
var seq__35221_35843__$1 = temp__5735__auto___35842;
if(cljs.core.chunked_seq_QMARK_(seq__35221_35843__$1)){
var c__4556__auto___35844 = cljs.core.chunk_first(seq__35221_35843__$1);
var G__35845 = cljs.core.chunk_rest(seq__35221_35843__$1);
var G__35846 = c__4556__auto___35844;
var G__35847 = cljs.core.count(c__4556__auto___35844);
var G__35848 = (0);
seq__35221_35792 = G__35845;
chunk__35223_35793 = G__35846;
count__35224_35794 = G__35847;
i__35225_35795 = G__35848;
continue;
} else {
var child_struct_35849 = cljs.core.first(seq__35221_35843__$1);
if((!((child_struct_35849 == null)))){
if(typeof child_struct_35849 === 'string'){
var text_35850 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_35850),child_struct_35849].join(''));
} else {
var children_35851 = shadow.dom.svg_node(child_struct_35849);
if(cljs.core.seq_QMARK_(children_35851)){
var seq__35266_35852 = cljs.core.seq(children_35851);
var chunk__35268_35853 = null;
var count__35269_35854 = (0);
var i__35270_35855 = (0);
while(true){
if((i__35270_35855 < count__35269_35854)){
var child_35856 = chunk__35268_35853.cljs$core$IIndexed$_nth$arity$2(null,i__35270_35855);
if(cljs.core.truth_(child_35856)){
node.appendChild(child_35856);


var G__35857 = seq__35266_35852;
var G__35858 = chunk__35268_35853;
var G__35859 = count__35269_35854;
var G__35860 = (i__35270_35855 + (1));
seq__35266_35852 = G__35857;
chunk__35268_35853 = G__35858;
count__35269_35854 = G__35859;
i__35270_35855 = G__35860;
continue;
} else {
var G__35861 = seq__35266_35852;
var G__35862 = chunk__35268_35853;
var G__35863 = count__35269_35854;
var G__35864 = (i__35270_35855 + (1));
seq__35266_35852 = G__35861;
chunk__35268_35853 = G__35862;
count__35269_35854 = G__35863;
i__35270_35855 = G__35864;
continue;
}
} else {
var temp__5735__auto___35866__$1 = cljs.core.seq(seq__35266_35852);
if(temp__5735__auto___35866__$1){
var seq__35266_35867__$1 = temp__5735__auto___35866__$1;
if(cljs.core.chunked_seq_QMARK_(seq__35266_35867__$1)){
var c__4556__auto___35868 = cljs.core.chunk_first(seq__35266_35867__$1);
var G__35869 = cljs.core.chunk_rest(seq__35266_35867__$1);
var G__35870 = c__4556__auto___35868;
var G__35871 = cljs.core.count(c__4556__auto___35868);
var G__35872 = (0);
seq__35266_35852 = G__35869;
chunk__35268_35853 = G__35870;
count__35269_35854 = G__35871;
i__35270_35855 = G__35872;
continue;
} else {
var child_35873 = cljs.core.first(seq__35266_35867__$1);
if(cljs.core.truth_(child_35873)){
node.appendChild(child_35873);


var G__35874 = cljs.core.next(seq__35266_35867__$1);
var G__35875 = null;
var G__35876 = (0);
var G__35877 = (0);
seq__35266_35852 = G__35874;
chunk__35268_35853 = G__35875;
count__35269_35854 = G__35876;
i__35270_35855 = G__35877;
continue;
} else {
var G__35878 = cljs.core.next(seq__35266_35867__$1);
var G__35879 = null;
var G__35880 = (0);
var G__35881 = (0);
seq__35266_35852 = G__35878;
chunk__35268_35853 = G__35879;
count__35269_35854 = G__35880;
i__35270_35855 = G__35881;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_35851);
}
}


var G__35882 = cljs.core.next(seq__35221_35843__$1);
var G__35883 = null;
var G__35884 = (0);
var G__35885 = (0);
seq__35221_35792 = G__35882;
chunk__35223_35793 = G__35883;
count__35224_35794 = G__35884;
i__35225_35795 = G__35885;
continue;
} else {
var G__35890 = cljs.core.next(seq__35221_35843__$1);
var G__35891 = null;
var G__35892 = (0);
var G__35893 = (0);
seq__35221_35792 = G__35890;
chunk__35223_35793 = G__35891;
count__35224_35794 = G__35892;
i__35225_35795 = G__35893;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

goog.object.set(shadow.dom._to_svg,"string",(function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

goog.object.set(shadow.dom.SVGElement,"null",true);

goog.object.set(shadow.dom._to_svg,"null",(function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___35897 = arguments.length;
var i__4737__auto___35898 = (0);
while(true){
if((i__4737__auto___35898 < len__4736__auto___35897)){
args__4742__auto__.push((arguments[i__4737__auto___35898]));

var G__35899 = (i__4737__auto___35898 + (1));
i__4737__auto___35898 = G__35899;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq35276){
var G__35277 = cljs.core.first(seq35276);
var seq35276__$1 = cljs.core.next(seq35276);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35277,seq35276__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__35281 = arguments.length;
switch (G__35281) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__4115__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4115__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4115__auto__;
}
})())){
var c__30723__auto___35908 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_35296){
var state_val_35297 = (state_35296[(1)]);
if((state_val_35297 === (1))){
var state_35296__$1 = state_35296;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_35296__$1,(2),once_or_cleanup);
} else {
if((state_val_35297 === (2))){
var inst_35293 = (state_35296[(2)]);
var inst_35294 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_35296__$1 = (function (){var statearr_35300 = state_35296;
(statearr_35300[(7)] = inst_35293);

return statearr_35300;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_35296__$1,inst_35294);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__30624__auto__ = null;
var shadow$dom$state_machine__30624__auto____0 = (function (){
var statearr_35302 = [null,null,null,null,null,null,null,null];
(statearr_35302[(0)] = shadow$dom$state_machine__30624__auto__);

(statearr_35302[(1)] = (1));

return statearr_35302;
});
var shadow$dom$state_machine__30624__auto____1 = (function (state_35296){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_35296);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e35306){var ex__30627__auto__ = e35306;
var statearr_35307_35909 = state_35296;
(statearr_35307_35909[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_35296[(4)]))){
var statearr_35308_35910 = state_35296;
(statearr_35308_35910[(1)] = cljs.core.first((state_35296[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35911 = state_35296;
state_35296 = G__35911;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
shadow$dom$state_machine__30624__auto__ = function(state_35296){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__30624__auto____0.call(this);
case 1:
return shadow$dom$state_machine__30624__auto____1.call(this,state_35296);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__30624__auto____0;
shadow$dom$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__30624__auto____1;
return shadow$dom$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_35312 = f__30724__auto__();
(statearr_35312[(6)] = c__30723__auto___35908);

return statearr_35312;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
