goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___36881 = arguments.length;
var i__4737__auto___36882 = (0);
while(true){
if((i__4737__auto___36882 < len__4736__auto___36881)){
args__4742__auto__.push((arguments[i__4737__auto___36882]));

var G__36883 = (i__4737__auto___36882 + (1));
i__4737__auto___36882 = G__36883;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq36718){
var G__36719 = cljs.core.first(seq36718);
var seq36718__$1 = cljs.core.next(seq36718);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__36719,seq36718__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__36731 = cljs.core.seq(sources);
var chunk__36732 = null;
var count__36733 = (0);
var i__36734 = (0);
while(true){
if((i__36734 < count__36733)){
var map__36751 = chunk__36732.cljs$core$IIndexed$_nth$arity$2(null,i__36734);
var map__36751__$1 = (((((!((map__36751 == null))))?(((((map__36751.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36751.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36751):map__36751);
var src = map__36751__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36751__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36751__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36751__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36751__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e36753){var e_36884 = e36753;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_36884);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_36884.message)].join('')));
}

var G__36885 = seq__36731;
var G__36886 = chunk__36732;
var G__36887 = count__36733;
var G__36888 = (i__36734 + (1));
seq__36731 = G__36885;
chunk__36732 = G__36886;
count__36733 = G__36887;
i__36734 = G__36888;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__36731);
if(temp__5735__auto__){
var seq__36731__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__36731__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__36731__$1);
var G__36889 = cljs.core.chunk_rest(seq__36731__$1);
var G__36890 = c__4556__auto__;
var G__36891 = cljs.core.count(c__4556__auto__);
var G__36892 = (0);
seq__36731 = G__36889;
chunk__36732 = G__36890;
count__36733 = G__36891;
i__36734 = G__36892;
continue;
} else {
var map__36754 = cljs.core.first(seq__36731__$1);
var map__36754__$1 = (((((!((map__36754 == null))))?(((((map__36754.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36754.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36754):map__36754);
var src = map__36754__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36754__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36754__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36754__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36754__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e36756){var e_36893 = e36756;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_36893);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_36893.message)].join('')));
}

var G__36894 = cljs.core.next(seq__36731__$1);
var G__36895 = null;
var G__36896 = (0);
var G__36897 = (0);
seq__36731 = G__36894;
chunk__36732 = G__36895;
count__36733 = G__36896;
i__36734 = G__36897;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__36760 = cljs.core.seq(js_requires);
var chunk__36761 = null;
var count__36762 = (0);
var i__36763 = (0);
while(true){
if((i__36763 < count__36762)){
var js_ns = chunk__36761.cljs$core$IIndexed$_nth$arity$2(null,i__36763);
var require_str_36898 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_36898);


var G__36899 = seq__36760;
var G__36900 = chunk__36761;
var G__36901 = count__36762;
var G__36902 = (i__36763 + (1));
seq__36760 = G__36899;
chunk__36761 = G__36900;
count__36762 = G__36901;
i__36763 = G__36902;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__36760);
if(temp__5735__auto__){
var seq__36760__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__36760__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__36760__$1);
var G__36903 = cljs.core.chunk_rest(seq__36760__$1);
var G__36904 = c__4556__auto__;
var G__36905 = cljs.core.count(c__4556__auto__);
var G__36906 = (0);
seq__36760 = G__36903;
chunk__36761 = G__36904;
count__36762 = G__36905;
i__36763 = G__36906;
continue;
} else {
var js_ns = cljs.core.first(seq__36760__$1);
var require_str_36907 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_36907);


var G__36908 = cljs.core.next(seq__36760__$1);
var G__36909 = null;
var G__36910 = (0);
var G__36911 = (0);
seq__36760 = G__36908;
chunk__36761 = G__36909;
count__36762 = G__36910;
i__36763 = G__36911;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__36767){
var map__36768 = p__36767;
var map__36768__$1 = (((((!((map__36768 == null))))?(((((map__36768.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36768.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36768):map__36768);
var msg = map__36768__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36768__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36768__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36770(s__36771){
return (new cljs.core.LazySeq(null,(function (){
var s__36771__$1 = s__36771;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__36771__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var map__36776 = cljs.core.first(xs__6292__auto__);
var map__36776__$1 = (((((!((map__36776 == null))))?(((((map__36776.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36776.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36776):map__36776);
var src = map__36776__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36776__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36776__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4525__auto__ = ((function (s__36771__$1,map__36776,map__36776__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__36768,map__36768__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36770_$_iter__36772(s__36773){
return (new cljs.core.LazySeq(null,((function (s__36771__$1,map__36776,map__36776__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__36768,map__36768__$1,msg,info,reload_info){
return (function (){
var s__36773__$1 = s__36773;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__36773__$1);
if(temp__5735__auto____$1){
var s__36773__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__36773__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__36773__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__36775 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__36774 = (0);
while(true){
if((i__36774 < size__4528__auto__)){
var warning = cljs.core._nth(c__4527__auto__,i__36774);
cljs.core.chunk_append(b__36775,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__36912 = (i__36774 + (1));
i__36774 = G__36912;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__36775),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36770_$_iter__36772(cljs.core.chunk_rest(s__36773__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__36775),null);
}
} else {
var warning = cljs.core.first(s__36773__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36770_$_iter__36772(cljs.core.rest(s__36773__$2)));
}
} else {
return null;
}
break;
}
});})(s__36771__$1,map__36776,map__36776__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__36768,map__36768__$1,msg,info,reload_info))
,null,null));
});})(s__36771__$1,map__36776,map__36776__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__36768,map__36768__$1,msg,info,reload_info))
;
var fs__4526__auto__ = cljs.core.seq(iterys__4525__auto__(warnings));
if(fs__4526__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4526__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__36770(cljs.core.rest(s__36771__$1)));
} else {
var G__36913 = cljs.core.rest(s__36771__$1);
s__36771__$1 = G__36913;
continue;
}
} else {
var G__36914 = cljs.core.rest(s__36771__$1);
s__36771__$1 = G__36914;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
var seq__36778_36915 = cljs.core.seq(warnings);
var chunk__36779_36916 = null;
var count__36780_36917 = (0);
var i__36781_36918 = (0);
while(true){
if((i__36781_36918 < count__36780_36917)){
var map__36786_36919 = chunk__36779_36916.cljs$core$IIndexed$_nth$arity$2(null,i__36781_36918);
var map__36786_36920__$1 = (((((!((map__36786_36919 == null))))?(((((map__36786_36919.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36786_36919.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36786_36919):map__36786_36919);
var w_36921 = map__36786_36920__$1;
var msg_36922__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36786_36920__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_36923 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36786_36920__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_36924 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36786_36920__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_36925 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36786_36920__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_36925)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_36923),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_36924),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_36922__$1)].join(''));


var G__36926 = seq__36778_36915;
var G__36927 = chunk__36779_36916;
var G__36928 = count__36780_36917;
var G__36929 = (i__36781_36918 + (1));
seq__36778_36915 = G__36926;
chunk__36779_36916 = G__36927;
count__36780_36917 = G__36928;
i__36781_36918 = G__36929;
continue;
} else {
var temp__5735__auto___36930 = cljs.core.seq(seq__36778_36915);
if(temp__5735__auto___36930){
var seq__36778_36931__$1 = temp__5735__auto___36930;
if(cljs.core.chunked_seq_QMARK_(seq__36778_36931__$1)){
var c__4556__auto___36932 = cljs.core.chunk_first(seq__36778_36931__$1);
var G__36933 = cljs.core.chunk_rest(seq__36778_36931__$1);
var G__36934 = c__4556__auto___36932;
var G__36935 = cljs.core.count(c__4556__auto___36932);
var G__36936 = (0);
seq__36778_36915 = G__36933;
chunk__36779_36916 = G__36934;
count__36780_36917 = G__36935;
i__36781_36918 = G__36936;
continue;
} else {
var map__36788_36937 = cljs.core.first(seq__36778_36931__$1);
var map__36788_36938__$1 = (((((!((map__36788_36937 == null))))?(((((map__36788_36937.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36788_36937.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36788_36937):map__36788_36937);
var w_36939 = map__36788_36938__$1;
var msg_36940__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36788_36938__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_36941 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36788_36938__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_36942 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36788_36938__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_36943 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36788_36938__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_36943)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_36941),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_36942),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_36940__$1)].join(''));


var G__36944 = cljs.core.next(seq__36778_36931__$1);
var G__36945 = null;
var G__36946 = (0);
var G__36947 = (0);
seq__36778_36915 = G__36944;
chunk__36779_36916 = G__36945;
count__36780_36917 = G__36946;
i__36781_36918 = G__36947;
continue;
}
} else {
}
}
break;
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__36766_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__36766_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$)){
return new$;
} else {
return false;
}
} else {
return false;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__36791){
var map__36793 = p__36791;
var map__36793__$1 = (((((!((map__36793 == null))))?(((((map__36793.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36793.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36793):map__36793);
var msg = map__36793__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36793__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__36795 = cljs.core.seq(updates);
var chunk__36797 = null;
var count__36798 = (0);
var i__36799 = (0);
while(true){
if((i__36799 < count__36798)){
var path = chunk__36797.cljs$core$IIndexed$_nth$arity$2(null,i__36799);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__36825_36948 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__36828_36949 = null;
var count__36829_36950 = (0);
var i__36830_36951 = (0);
while(true){
if((i__36830_36951 < count__36829_36950)){
var node_36952 = chunk__36828_36949.cljs$core$IIndexed$_nth$arity$2(null,i__36830_36951);
var path_match_36953 = shadow.cljs.devtools.client.browser.match_paths(node_36952.getAttribute("href"),path);
if(cljs.core.truth_(path_match_36953)){
var new_link_36954 = (function (){var G__36835 = node_36952.cloneNode(true);
G__36835.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_36953),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__36835;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_36953], 0));

goog.dom.insertSiblingAfter(new_link_36954,node_36952);

goog.dom.removeNode(node_36952);


var G__36955 = seq__36825_36948;
var G__36956 = chunk__36828_36949;
var G__36957 = count__36829_36950;
var G__36958 = (i__36830_36951 + (1));
seq__36825_36948 = G__36955;
chunk__36828_36949 = G__36956;
count__36829_36950 = G__36957;
i__36830_36951 = G__36958;
continue;
} else {
var G__36959 = seq__36825_36948;
var G__36960 = chunk__36828_36949;
var G__36961 = count__36829_36950;
var G__36962 = (i__36830_36951 + (1));
seq__36825_36948 = G__36959;
chunk__36828_36949 = G__36960;
count__36829_36950 = G__36961;
i__36830_36951 = G__36962;
continue;
}
} else {
var temp__5735__auto___36963 = cljs.core.seq(seq__36825_36948);
if(temp__5735__auto___36963){
var seq__36825_36964__$1 = temp__5735__auto___36963;
if(cljs.core.chunked_seq_QMARK_(seq__36825_36964__$1)){
var c__4556__auto___36965 = cljs.core.chunk_first(seq__36825_36964__$1);
var G__36966 = cljs.core.chunk_rest(seq__36825_36964__$1);
var G__36967 = c__4556__auto___36965;
var G__36968 = cljs.core.count(c__4556__auto___36965);
var G__36969 = (0);
seq__36825_36948 = G__36966;
chunk__36828_36949 = G__36967;
count__36829_36950 = G__36968;
i__36830_36951 = G__36969;
continue;
} else {
var node_36970 = cljs.core.first(seq__36825_36964__$1);
var path_match_36971 = shadow.cljs.devtools.client.browser.match_paths(node_36970.getAttribute("href"),path);
if(cljs.core.truth_(path_match_36971)){
var new_link_36972 = (function (){var G__36836 = node_36970.cloneNode(true);
G__36836.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_36971),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__36836;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_36971], 0));

goog.dom.insertSiblingAfter(new_link_36972,node_36970);

goog.dom.removeNode(node_36970);


var G__36973 = cljs.core.next(seq__36825_36964__$1);
var G__36974 = null;
var G__36975 = (0);
var G__36976 = (0);
seq__36825_36948 = G__36973;
chunk__36828_36949 = G__36974;
count__36829_36950 = G__36975;
i__36830_36951 = G__36976;
continue;
} else {
var G__36977 = cljs.core.next(seq__36825_36964__$1);
var G__36978 = null;
var G__36979 = (0);
var G__36980 = (0);
seq__36825_36948 = G__36977;
chunk__36828_36949 = G__36978;
count__36829_36950 = G__36979;
i__36830_36951 = G__36980;
continue;
}
}
} else {
}
}
break;
}


var G__36981 = seq__36795;
var G__36982 = chunk__36797;
var G__36983 = count__36798;
var G__36984 = (i__36799 + (1));
seq__36795 = G__36981;
chunk__36797 = G__36982;
count__36798 = G__36983;
i__36799 = G__36984;
continue;
} else {
var G__36985 = seq__36795;
var G__36986 = chunk__36797;
var G__36987 = count__36798;
var G__36988 = (i__36799 + (1));
seq__36795 = G__36985;
chunk__36797 = G__36986;
count__36798 = G__36987;
i__36799 = G__36988;
continue;
}
} else {
var temp__5735__auto__ = cljs.core.seq(seq__36795);
if(temp__5735__auto__){
var seq__36795__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__36795__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__36795__$1);
var G__36989 = cljs.core.chunk_rest(seq__36795__$1);
var G__36990 = c__4556__auto__;
var G__36991 = cljs.core.count(c__4556__auto__);
var G__36992 = (0);
seq__36795 = G__36989;
chunk__36797 = G__36990;
count__36798 = G__36991;
i__36799 = G__36992;
continue;
} else {
var path = cljs.core.first(seq__36795__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__36837_36993 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__36840_36994 = null;
var count__36841_36995 = (0);
var i__36842_36996 = (0);
while(true){
if((i__36842_36996 < count__36841_36995)){
var node_36997 = chunk__36840_36994.cljs$core$IIndexed$_nth$arity$2(null,i__36842_36996);
var path_match_36998 = shadow.cljs.devtools.client.browser.match_paths(node_36997.getAttribute("href"),path);
if(cljs.core.truth_(path_match_36998)){
var new_link_36999 = (function (){var G__36847 = node_36997.cloneNode(true);
G__36847.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_36998),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__36847;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_36998], 0));

goog.dom.insertSiblingAfter(new_link_36999,node_36997);

goog.dom.removeNode(node_36997);


var G__37000 = seq__36837_36993;
var G__37001 = chunk__36840_36994;
var G__37002 = count__36841_36995;
var G__37003 = (i__36842_36996 + (1));
seq__36837_36993 = G__37000;
chunk__36840_36994 = G__37001;
count__36841_36995 = G__37002;
i__36842_36996 = G__37003;
continue;
} else {
var G__37004 = seq__36837_36993;
var G__37005 = chunk__36840_36994;
var G__37006 = count__36841_36995;
var G__37007 = (i__36842_36996 + (1));
seq__36837_36993 = G__37004;
chunk__36840_36994 = G__37005;
count__36841_36995 = G__37006;
i__36842_36996 = G__37007;
continue;
}
} else {
var temp__5735__auto___37008__$1 = cljs.core.seq(seq__36837_36993);
if(temp__5735__auto___37008__$1){
var seq__36837_37009__$1 = temp__5735__auto___37008__$1;
if(cljs.core.chunked_seq_QMARK_(seq__36837_37009__$1)){
var c__4556__auto___37010 = cljs.core.chunk_first(seq__36837_37009__$1);
var G__37011 = cljs.core.chunk_rest(seq__36837_37009__$1);
var G__37012 = c__4556__auto___37010;
var G__37013 = cljs.core.count(c__4556__auto___37010);
var G__37014 = (0);
seq__36837_36993 = G__37011;
chunk__36840_36994 = G__37012;
count__36841_36995 = G__37013;
i__36842_36996 = G__37014;
continue;
} else {
var node_37015 = cljs.core.first(seq__36837_37009__$1);
var path_match_37016 = shadow.cljs.devtools.client.browser.match_paths(node_37015.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37016)){
var new_link_37017 = (function (){var G__36848 = node_37015.cloneNode(true);
G__36848.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37016),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__36848;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37016], 0));

goog.dom.insertSiblingAfter(new_link_37017,node_37015);

goog.dom.removeNode(node_37015);


var G__37018 = cljs.core.next(seq__36837_37009__$1);
var G__37019 = null;
var G__37020 = (0);
var G__37021 = (0);
seq__36837_36993 = G__37018;
chunk__36840_36994 = G__37019;
count__36841_36995 = G__37020;
i__36842_36996 = G__37021;
continue;
} else {
var G__37022 = cljs.core.next(seq__36837_37009__$1);
var G__37023 = null;
var G__37024 = (0);
var G__37025 = (0);
seq__36837_36993 = G__37022;
chunk__36840_36994 = G__37023;
count__36841_36995 = G__37024;
i__36842_36996 = G__37025;
continue;
}
}
} else {
}
}
break;
}


var G__37026 = cljs.core.next(seq__36795__$1);
var G__37027 = null;
var G__37028 = (0);
var G__37029 = (0);
seq__36795 = G__37026;
chunk__36797 = G__37027;
count__36798 = G__37028;
i__36799 = G__37029;
continue;
} else {
var G__37030 = cljs.core.next(seq__36795__$1);
var G__37031 = null;
var G__37032 = (0);
var G__37033 = (0);
seq__36795 = G__37030;
chunk__36797 = G__37031;
count__36798 = G__37032;
i__36799 = G__37033;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(runtime,p__36849){
var map__36850 = p__36849;
var map__36850__$1 = (((((!((map__36850 == null))))?(((((map__36850.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36850.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36850):map__36850);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36850__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
return shadow.cljs.devtools.client.shared.load_sources(runtime,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return shadow.cljs.devtools.client.browser.devtools_msg("ready!");
}));
});
shadow.cljs.devtools.client.browser.client_info = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null);
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$2 = (function (this$,p__36852){
var map__36853 = p__36852;
var map__36853__$1 = (((((!((map__36853 == null))))?(((((map__36853.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36853.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36853):map__36853);
var _ = map__36853__$1;
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36853__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__36855,done,error){
var map__36856 = p__36855;
var map__36856__$1 = (((((!((map__36856 == null))))?(((((map__36856.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36856.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36856):map__36856);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36856__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__36858,done,error){
var map__36859 = p__36858;
var map__36859__$1 = (((((!((map__36859 == null))))?(((((map__36859.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36859.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36859):map__36859);
var msg = map__36859__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36859__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36859__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36859__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__36861){
var map__36862 = p__36861;
var map__36862__$1 = (((((!((map__36862 == null))))?(((((map__36862.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36862.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36862):map__36862);
var src = map__36862__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36862__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4115__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4115__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__36864 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__36864) : done.call(null,G__36864));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__36865){
var map__36866 = p__36865;
var map__36866__$1 = (((((!((map__36866 == null))))?(((((map__36866.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36866.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36866):map__36866);
var msg__$1 = map__36866__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36866__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e36868){var ex = e36868;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__36869){
var map__36870 = p__36869;
var map__36870__$1 = (((((!((map__36870 == null))))?(((((map__36870.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36870.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36870):map__36870);
var env = map__36870__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36870__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-runtime-init","cljs-runtime-init",1305890232),(function (msg){
return shadow.cljs.devtools.client.browser.repl_init(runtime,msg);
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (p__36872){
var map__36873 = p__36872;
var map__36873__$1 = (((((!((map__36873 == null))))?(((((map__36873.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36873.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36873):map__36873);
var msg = map__36873__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36873__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__36875){
var map__36876 = p__36875;
var map__36876__$1 = (((((!((map__36876 == null))))?(((((map__36876.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36876.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36876):map__36876);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36876__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36876__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__36878){
var map__36879 = p__36878;
var map__36879__$1 = (((((!((map__36879 == null))))?(((((map__36879.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__36879.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__36879):map__36879);
var svc = map__36879__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__36879__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
