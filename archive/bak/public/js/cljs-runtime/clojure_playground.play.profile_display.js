goog.provide('clojure_playground.play.profile_display');
clojure_playground.play.profile_display.header = (function clojure_playground$play$profile_display$header(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.header","div.header",1964513620),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"Welcome to Mr. Magorium's Magic Emporium!"], null)], null);
});
clojure_playground.play.profile_display.footer = (function clojure_playground$play$profile_display$footer(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.footer","div.footer",1103856744),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"I am a foot."], null)], null);
});
/**
 * Updates value on change.
 */
clojure_playground.play.profile_display.input_element = (function clojure_playground$play$profile_display$input_element(id,name,value){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"name","name",1843675177),name,new cljs.core.Keyword(null,"class","class",-2030961996),"form-control",new cljs.core.Keyword(null,"type","type",1174270348),cljs.core.type,new cljs.core.Keyword(null,"required","required",1807647006),"",new cljs.core.Keyword(null,"on-change","on-change",-732046149),(function (p1__35954_SHARP_){
return cljs.core.reset_BANG_(value,p1__35954_SHARP_.target.value);
})], null)], null);
});
clojure_playground.play.profile_display.input_first_name = (function clojure_playground$play$profile_display$input_first_name(first_name){
return clojure_playground.play.profile_display.input_element("first-name","firstname",first_name);
});
clojure_playground.play.profile_display.form = (function clojure_playground$play$profile_display$form(){
var first_name = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"form","form",-1624062471),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"label","label",1718410804),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"for","for",-1323786319),"first-name"], null),"First name:"], null),clojure_playground.play.profile_display.input_first_name(first_name),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"input","input",556931961),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),"submit",new cljs.core.Keyword(null,"value","value",305978217),"Release thy name!"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),cljs.core.deref(first_name)], null)], null);
});

//# sourceMappingURL=clojure_playground.play.profile_display.js.map
