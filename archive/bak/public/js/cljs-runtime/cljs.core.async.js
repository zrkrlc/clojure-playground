goog.provide('cljs.core.async');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__30873 = arguments.length;
switch (G__30873) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async30881 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30881 = (function (f,blockable,meta30882){
this.f = f;
this.blockable = blockable;
this.meta30882 = meta30882;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async30881.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30883,meta30882__$1){
var self__ = this;
var _30883__$1 = this;
return (new cljs.core.async.t_cljs$core$async30881(self__.f,self__.blockable,meta30882__$1));
}));

(cljs.core.async.t_cljs$core$async30881.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30883){
var self__ = this;
var _30883__$1 = this;
return self__.meta30882;
}));

(cljs.core.async.t_cljs$core$async30881.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async30881.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async30881.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async30881.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async30881.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta30882","meta30882",222803028,null)], null);
}));

(cljs.core.async.t_cljs$core$async30881.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async30881.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30881");

(cljs.core.async.t_cljs$core$async30881.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async30881");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30881.
 */
cljs.core.async.__GT_t_cljs$core$async30881 = (function cljs$core$async$__GT_t_cljs$core$async30881(f__$1,blockable__$1,meta30882){
return (new cljs.core.async.t_cljs$core$async30881(f__$1,blockable__$1,meta30882));
});

}

return (new cljs.core.async.t_cljs$core$async30881(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__30921 = arguments.length;
switch (G__30921) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__30979 = arguments.length;
switch (G__30979) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__30999 = arguments.length;
switch (G__30999) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_34417 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_34417) : fn1.call(null,val_34417));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_34417) : fn1.call(null,val_34417));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__31029 = arguments.length;
switch (G__31029) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5733__auto__)){
var ret = temp__5733__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5733__auto__)){
var retb = temp__5733__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4613__auto___34430 = n;
var x_34431 = (0);
while(true){
if((x_34431 < n__4613__auto___34430)){
(a[x_34431] = x_34431);

var G__34434 = (x_34431 + (1));
x_34431 = G__34434;
continue;
} else {
}
break;
}

goog.array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async31066 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31066 = (function (flag,meta31067){
this.flag = flag;
this.meta31067 = meta31067;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async31066.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_31068,meta31067__$1){
var self__ = this;
var _31068__$1 = this;
return (new cljs.core.async.t_cljs$core$async31066(self__.flag,meta31067__$1));
}));

(cljs.core.async.t_cljs$core$async31066.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_31068){
var self__ = this;
var _31068__$1 = this;
return self__.meta31067;
}));

(cljs.core.async.t_cljs$core$async31066.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async31066.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async31066.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async31066.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async31066.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta31067","meta31067",-291588158,null)], null);
}));

(cljs.core.async.t_cljs$core$async31066.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async31066.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31066");

(cljs.core.async.t_cljs$core$async31066.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async31066");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async31066.
 */
cljs.core.async.__GT_t_cljs$core$async31066 = (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async31066(flag__$1,meta31067){
return (new cljs.core.async.t_cljs$core$async31066(flag__$1,meta31067));
});

}

return (new cljs.core.async.t_cljs$core$async31066(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async31083 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31083 = (function (flag,cb,meta31084){
this.flag = flag;
this.cb = cb;
this.meta31084 = meta31084;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async31083.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_31085,meta31084__$1){
var self__ = this;
var _31085__$1 = this;
return (new cljs.core.async.t_cljs$core$async31083(self__.flag,self__.cb,meta31084__$1));
}));

(cljs.core.async.t_cljs$core$async31083.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_31085){
var self__ = this;
var _31085__$1 = this;
return self__.meta31084;
}));

(cljs.core.async.t_cljs$core$async31083.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async31083.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async31083.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async31083.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async31083.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta31084","meta31084",-1471794875,null)], null);
}));

(cljs.core.async.t_cljs$core$async31083.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async31083.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31083");

(cljs.core.async.t_cljs$core$async31083.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async31083");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async31083.
 */
cljs.core.async.__GT_t_cljs$core$async31083 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async31083(flag__$1,cb__$1,meta31084){
return (new cljs.core.async.t_cljs$core$async31083(flag__$1,cb__$1,meta31084));
});

}

return (new cljs.core.async.t_cljs$core$async31083(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__31099_SHARP_){
var G__31106 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__31099_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__31106) : fret.call(null,G__31106));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__31100_SHARP_){
var G__31107 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__31100_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__31107) : fret.call(null,G__31107));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4126__auto__ = wport;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return port;
}
})()], null));
} else {
var G__34454 = (i + (1));
i = G__34454;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4126__auto__ = ret;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5735__auto__ = (function (){var and__4115__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4115__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4115__auto__;
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var got = temp__5735__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___34458 = arguments.length;
var i__4737__auto___34462 = (0);
while(true){
if((i__4737__auto___34462 < len__4736__auto___34458)){
args__4742__auto__.push((arguments[i__4737__auto___34462]));

var G__34464 = (i__4737__auto___34462 + (1));
i__4737__auto___34462 = G__34464;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__31121){
var map__31122 = p__31121;
var map__31122__$1 = (((((!((map__31122 == null))))?(((((map__31122.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31122.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31122):map__31122);
var opts = map__31122__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq31117){
var G__31118 = cljs.core.first(seq31117);
var seq31117__$1 = cljs.core.next(seq31117);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__31118,seq31117__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__31133 = arguments.length;
switch (G__31133) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__30723__auto___34472 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_31224){
var state_val_31228 = (state_31224[(1)]);
if((state_val_31228 === (7))){
var inst_31199 = (state_31224[(2)]);
var state_31224__$1 = state_31224;
var statearr_31247_34476 = state_31224__$1;
(statearr_31247_34476[(2)] = inst_31199);

(statearr_31247_34476[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (1))){
var state_31224__$1 = state_31224;
var statearr_31248_34479 = state_31224__$1;
(statearr_31248_34479[(2)] = null);

(statearr_31248_34479[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (4))){
var inst_31166 = (state_31224[(7)]);
var inst_31166__$1 = (state_31224[(2)]);
var inst_31173 = (inst_31166__$1 == null);
var state_31224__$1 = (function (){var statearr_31251 = state_31224;
(statearr_31251[(7)] = inst_31166__$1);

return statearr_31251;
})();
if(cljs.core.truth_(inst_31173)){
var statearr_31252_34481 = state_31224__$1;
(statearr_31252_34481[(1)] = (5));

} else {
var statearr_31253_34483 = state_31224__$1;
(statearr_31253_34483[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (13))){
var state_31224__$1 = state_31224;
var statearr_31257_34485 = state_31224__$1;
(statearr_31257_34485[(2)] = null);

(statearr_31257_34485[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (6))){
var inst_31166 = (state_31224[(7)]);
var state_31224__$1 = state_31224;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31224__$1,(11),to,inst_31166);
} else {
if((state_val_31228 === (3))){
var inst_31209 = (state_31224[(2)]);
var state_31224__$1 = state_31224;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31224__$1,inst_31209);
} else {
if((state_val_31228 === (12))){
var state_31224__$1 = state_31224;
var statearr_31261_34488 = state_31224__$1;
(statearr_31261_34488[(2)] = null);

(statearr_31261_34488[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (2))){
var state_31224__$1 = state_31224;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31224__$1,(4),from);
} else {
if((state_val_31228 === (11))){
var inst_31189 = (state_31224[(2)]);
var state_31224__$1 = state_31224;
if(cljs.core.truth_(inst_31189)){
var statearr_31263_34491 = state_31224__$1;
(statearr_31263_34491[(1)] = (12));

} else {
var statearr_31266_34492 = state_31224__$1;
(statearr_31266_34492[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (9))){
var state_31224__$1 = state_31224;
var statearr_31269_34493 = state_31224__$1;
(statearr_31269_34493[(2)] = null);

(statearr_31269_34493[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (5))){
var state_31224__$1 = state_31224;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31271_34495 = state_31224__$1;
(statearr_31271_34495[(1)] = (8));

} else {
var statearr_31272_34496 = state_31224__$1;
(statearr_31272_34496[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (14))){
var inst_31197 = (state_31224[(2)]);
var state_31224__$1 = state_31224;
var statearr_31273_34497 = state_31224__$1;
(statearr_31273_34497[(2)] = inst_31197);

(statearr_31273_34497[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (10))){
var inst_31185 = (state_31224[(2)]);
var state_31224__$1 = state_31224;
var statearr_31277_34498 = state_31224__$1;
(statearr_31277_34498[(2)] = inst_31185);

(statearr_31277_34498[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31228 === (8))){
var inst_31177 = cljs.core.async.close_BANG_(to);
var state_31224__$1 = state_31224;
var statearr_31279_34499 = state_31224__$1;
(statearr_31279_34499[(2)] = inst_31177);

(statearr_31279_34499[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_31292 = [null,null,null,null,null,null,null,null];
(statearr_31292[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_31292[(1)] = (1));

return statearr_31292;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_31224){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_31224);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e31296){var ex__30627__auto__ = e31296;
var statearr_31297_34504 = state_31224;
(statearr_31297_34504[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_31224[(4)]))){
var statearr_31298_34505 = state_31224;
(statearr_31298_34505[(1)] = cljs.core.first((state_31224[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34506 = state_31224;
state_31224 = G__34506;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_31224){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_31224);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_31299 = f__30724__auto__();
(statearr_31299[(6)] = c__30723__auto___34472);

return statearr_31299;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = (function (p__31309){
var vec__31310 = p__31309;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31310,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31310,(1),null);
var job = vec__31310;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__30723__auto___34512 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_31317){
var state_val_31318 = (state_31317[(1)]);
if((state_val_31318 === (1))){
var state_31317__$1 = state_31317;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31317__$1,(2),res,v);
} else {
if((state_val_31318 === (2))){
var inst_31314 = (state_31317[(2)]);
var inst_31315 = cljs.core.async.close_BANG_(res);
var state_31317__$1 = (function (){var statearr_31324 = state_31317;
(statearr_31324[(7)] = inst_31314);

return statearr_31324;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_31317__$1,inst_31315);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0 = (function (){
var statearr_31333 = [null,null,null,null,null,null,null,null];
(statearr_31333[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__);

(statearr_31333[(1)] = (1));

return statearr_31333;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1 = (function (state_31317){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_31317);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e31337){var ex__30627__auto__ = e31337;
var statearr_31342_34517 = state_31317;
(statearr_31342_34517[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_31317[(4)]))){
var statearr_31346_34518 = state_31317;
(statearr_31346_34518[(1)] = cljs.core.first((state_31317[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34519 = state_31317;
state_31317 = G__34519;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = function(state_31317){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1.call(this,state_31317);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_31355 = f__30724__auto__();
(statearr_31355[(6)] = c__30723__auto___34512);

return statearr_31355;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__31369){
var vec__31372 = p__31369;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31372,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__31372,(1),null);
var job = vec__31372;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__4613__auto___34520 = n;
var __34521 = (0);
while(true){
if((__34521 < n__4613__auto___34520)){
var G__31390_34522 = type;
var G__31390_34523__$1 = (((G__31390_34522 instanceof cljs.core.Keyword))?G__31390_34522.fqn:null);
switch (G__31390_34523__$1) {
case "compute":
var c__30723__auto___34525 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__34521,c__30723__auto___34525,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async){
return (function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = ((function (__34521,c__30723__auto___34525,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async){
return (function (state_31414){
var state_val_31415 = (state_31414[(1)]);
if((state_val_31415 === (1))){
var state_31414__$1 = state_31414;
var statearr_31433_34526 = state_31414__$1;
(statearr_31433_34526[(2)] = null);

(statearr_31433_34526[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31415 === (2))){
var state_31414__$1 = state_31414;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31414__$1,(4),jobs);
} else {
if((state_val_31415 === (3))){
var inst_31409 = (state_31414[(2)]);
var state_31414__$1 = state_31414;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31414__$1,inst_31409);
} else {
if((state_val_31415 === (4))){
var inst_31397 = (state_31414[(2)]);
var inst_31398 = process(inst_31397);
var state_31414__$1 = state_31414;
if(cljs.core.truth_(inst_31398)){
var statearr_31447_34529 = state_31414__$1;
(statearr_31447_34529[(1)] = (5));

} else {
var statearr_31448_34530 = state_31414__$1;
(statearr_31448_34530[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31415 === (5))){
var state_31414__$1 = state_31414;
var statearr_31449_34532 = state_31414__$1;
(statearr_31449_34532[(2)] = null);

(statearr_31449_34532[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31415 === (6))){
var state_31414__$1 = state_31414;
var statearr_31453_34533 = state_31414__$1;
(statearr_31453_34533[(2)] = null);

(statearr_31453_34533[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31415 === (7))){
var inst_31407 = (state_31414[(2)]);
var state_31414__$1 = state_31414;
var statearr_31455_34534 = state_31414__$1;
(statearr_31455_34534[(2)] = inst_31407);

(statearr_31455_34534[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__34521,c__30723__auto___34525,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async))
;
return ((function (__34521,switch__30623__auto__,c__30723__auto___34525,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0 = (function (){
var statearr_31461 = [null,null,null,null,null,null,null];
(statearr_31461[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__);

(statearr_31461[(1)] = (1));

return statearr_31461;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1 = (function (state_31414){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_31414);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e31465){var ex__30627__auto__ = e31465;
var statearr_31466_34538 = state_31414;
(statearr_31466_34538[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_31414[(4)]))){
var statearr_31475_34539 = state_31414;
(statearr_31475_34539[(1)] = cljs.core.first((state_31414[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34540 = state_31414;
state_31414 = G__34540;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = function(state_31414){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1.call(this,state_31414);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__;
})()
;})(__34521,switch__30623__auto__,c__30723__auto___34525,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async))
})();
var state__30725__auto__ = (function (){var statearr_31493 = f__30724__auto__();
(statearr_31493[(6)] = c__30723__auto___34525);

return statearr_31493;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
});})(__34521,c__30723__auto___34525,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async))
);


break;
case "async":
var c__30723__auto___34543 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__34521,c__30723__auto___34543,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async){
return (function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = ((function (__34521,c__30723__auto___34543,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async){
return (function (state_31521){
var state_val_31522 = (state_31521[(1)]);
if((state_val_31522 === (1))){
var state_31521__$1 = state_31521;
var statearr_31526_34544 = state_31521__$1;
(statearr_31526_34544[(2)] = null);

(statearr_31526_34544[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31522 === (2))){
var state_31521__$1 = state_31521;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31521__$1,(4),jobs);
} else {
if((state_val_31522 === (3))){
var inst_31517 = (state_31521[(2)]);
var state_31521__$1 = state_31521;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31521__$1,inst_31517);
} else {
if((state_val_31522 === (4))){
var inst_31505 = (state_31521[(2)]);
var inst_31510 = async(inst_31505);
var state_31521__$1 = state_31521;
if(cljs.core.truth_(inst_31510)){
var statearr_31530_34551 = state_31521__$1;
(statearr_31530_34551[(1)] = (5));

} else {
var statearr_31531_34555 = state_31521__$1;
(statearr_31531_34555[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31522 === (5))){
var state_31521__$1 = state_31521;
var statearr_31534_34556 = state_31521__$1;
(statearr_31534_34556[(2)] = null);

(statearr_31534_34556[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31522 === (6))){
var state_31521__$1 = state_31521;
var statearr_31536_34557 = state_31521__$1;
(statearr_31536_34557[(2)] = null);

(statearr_31536_34557[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31522 === (7))){
var inst_31515 = (state_31521[(2)]);
var state_31521__$1 = state_31521;
var statearr_31539_34558 = state_31521__$1;
(statearr_31539_34558[(2)] = inst_31515);

(statearr_31539_34558[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__34521,c__30723__auto___34543,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async))
;
return ((function (__34521,switch__30623__auto__,c__30723__auto___34543,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0 = (function (){
var statearr_31542 = [null,null,null,null,null,null,null];
(statearr_31542[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__);

(statearr_31542[(1)] = (1));

return statearr_31542;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1 = (function (state_31521){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_31521);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e31545){var ex__30627__auto__ = e31545;
var statearr_31549_34566 = state_31521;
(statearr_31549_34566[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_31521[(4)]))){
var statearr_31550_34567 = state_31521;
(statearr_31550_34567[(1)] = cljs.core.first((state_31521[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34568 = state_31521;
state_31521 = G__34568;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = function(state_31521){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1.call(this,state_31521);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__;
})()
;})(__34521,switch__30623__auto__,c__30723__auto___34543,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async))
})();
var state__30725__auto__ = (function (){var statearr_31556 = f__30724__auto__();
(statearr_31556[(6)] = c__30723__auto___34543);

return statearr_31556;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
});})(__34521,c__30723__auto___34543,G__31390_34522,G__31390_34523__$1,n__4613__auto___34520,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__31390_34523__$1)].join('')));

}

var G__34572 = (__34521 + (1));
__34521 = G__34572;
continue;
} else {
}
break;
}

var c__30723__auto___34573 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_31603){
var state_val_31604 = (state_31603[(1)]);
if((state_val_31604 === (7))){
var inst_31598 = (state_31603[(2)]);
var state_31603__$1 = state_31603;
var statearr_31622_34574 = state_31603__$1;
(statearr_31622_34574[(2)] = inst_31598);

(statearr_31622_34574[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31604 === (1))){
var state_31603__$1 = state_31603;
var statearr_31623_34578 = state_31603__$1;
(statearr_31623_34578[(2)] = null);

(statearr_31623_34578[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31604 === (4))){
var inst_31565 = (state_31603[(7)]);
var inst_31565__$1 = (state_31603[(2)]);
var inst_31567 = (inst_31565__$1 == null);
var state_31603__$1 = (function (){var statearr_31626 = state_31603;
(statearr_31626[(7)] = inst_31565__$1);

return statearr_31626;
})();
if(cljs.core.truth_(inst_31567)){
var statearr_31629_34583 = state_31603__$1;
(statearr_31629_34583[(1)] = (5));

} else {
var statearr_31630_34584 = state_31603__$1;
(statearr_31630_34584[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31604 === (6))){
var inst_31572 = (state_31603[(8)]);
var inst_31565 = (state_31603[(7)]);
var inst_31572__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_31575 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_31589 = [inst_31565,inst_31572__$1];
var inst_31590 = (new cljs.core.PersistentVector(null,2,(5),inst_31575,inst_31589,null));
var state_31603__$1 = (function (){var statearr_31631 = state_31603;
(statearr_31631[(8)] = inst_31572__$1);

return statearr_31631;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31603__$1,(8),jobs,inst_31590);
} else {
if((state_val_31604 === (3))){
var inst_31600 = (state_31603[(2)]);
var state_31603__$1 = state_31603;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31603__$1,inst_31600);
} else {
if((state_val_31604 === (2))){
var state_31603__$1 = state_31603;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31603__$1,(4),from);
} else {
if((state_val_31604 === (9))){
var inst_31594 = (state_31603[(2)]);
var state_31603__$1 = (function (){var statearr_31647 = state_31603;
(statearr_31647[(9)] = inst_31594);

return statearr_31647;
})();
var statearr_31648_34594 = state_31603__$1;
(statearr_31648_34594[(2)] = null);

(statearr_31648_34594[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31604 === (5))){
var inst_31570 = cljs.core.async.close_BANG_(jobs);
var state_31603__$1 = state_31603;
var statearr_31667_34595 = state_31603__$1;
(statearr_31667_34595[(2)] = inst_31570);

(statearr_31667_34595[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31604 === (8))){
var inst_31572 = (state_31603[(8)]);
var inst_31592 = (state_31603[(2)]);
var state_31603__$1 = (function (){var statearr_31669 = state_31603;
(statearr_31669[(10)] = inst_31592);

return statearr_31669;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31603__$1,(9),results,inst_31572);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0 = (function (){
var statearr_31672 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31672[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__);

(statearr_31672[(1)] = (1));

return statearr_31672;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1 = (function (state_31603){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_31603);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e31674){var ex__30627__auto__ = e31674;
var statearr_31675_34604 = state_31603;
(statearr_31675_34604[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_31603[(4)]))){
var statearr_31684_34605 = state_31603;
(statearr_31684_34605[(1)] = cljs.core.first((state_31603[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34609 = state_31603;
state_31603 = G__34609;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = function(state_31603){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1.call(this,state_31603);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_31687 = f__30724__auto__();
(statearr_31687[(6)] = c__30723__auto___34573);

return statearr_31687;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


var c__30723__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_31733){
var state_val_31734 = (state_31733[(1)]);
if((state_val_31734 === (7))){
var inst_31729 = (state_31733[(2)]);
var state_31733__$1 = state_31733;
var statearr_31740_34610 = state_31733__$1;
(statearr_31740_34610[(2)] = inst_31729);

(statearr_31740_34610[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (20))){
var state_31733__$1 = state_31733;
var statearr_31747_34611 = state_31733__$1;
(statearr_31747_34611[(2)] = null);

(statearr_31747_34611[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (1))){
var state_31733__$1 = state_31733;
var statearr_31748_34612 = state_31733__$1;
(statearr_31748_34612[(2)] = null);

(statearr_31748_34612[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (4))){
var inst_31693 = (state_31733[(7)]);
var inst_31693__$1 = (state_31733[(2)]);
var inst_31695 = (inst_31693__$1 == null);
var state_31733__$1 = (function (){var statearr_31749 = state_31733;
(statearr_31749[(7)] = inst_31693__$1);

return statearr_31749;
})();
if(cljs.core.truth_(inst_31695)){
var statearr_31750_34618 = state_31733__$1;
(statearr_31750_34618[(1)] = (5));

} else {
var statearr_31752_34619 = state_31733__$1;
(statearr_31752_34619[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (15))){
var inst_31711 = (state_31733[(8)]);
var state_31733__$1 = state_31733;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31733__$1,(18),to,inst_31711);
} else {
if((state_val_31734 === (21))){
var inst_31724 = (state_31733[(2)]);
var state_31733__$1 = state_31733;
var statearr_31785_34620 = state_31733__$1;
(statearr_31785_34620[(2)] = inst_31724);

(statearr_31785_34620[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (13))){
var inst_31726 = (state_31733[(2)]);
var state_31733__$1 = (function (){var statearr_31786 = state_31733;
(statearr_31786[(9)] = inst_31726);

return statearr_31786;
})();
var statearr_31788_34622 = state_31733__$1;
(statearr_31788_34622[(2)] = null);

(statearr_31788_34622[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (6))){
var inst_31693 = (state_31733[(7)]);
var state_31733__$1 = state_31733;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31733__$1,(11),inst_31693);
} else {
if((state_val_31734 === (17))){
var inst_31719 = (state_31733[(2)]);
var state_31733__$1 = state_31733;
if(cljs.core.truth_(inst_31719)){
var statearr_31790_34625 = state_31733__$1;
(statearr_31790_34625[(1)] = (19));

} else {
var statearr_31791_34626 = state_31733__$1;
(statearr_31791_34626[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (3))){
var inst_31731 = (state_31733[(2)]);
var state_31733__$1 = state_31733;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31733__$1,inst_31731);
} else {
if((state_val_31734 === (12))){
var inst_31708 = (state_31733[(10)]);
var state_31733__$1 = state_31733;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31733__$1,(14),inst_31708);
} else {
if((state_val_31734 === (2))){
var state_31733__$1 = state_31733;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31733__$1,(4),results);
} else {
if((state_val_31734 === (19))){
var state_31733__$1 = state_31733;
var statearr_31799_34628 = state_31733__$1;
(statearr_31799_34628[(2)] = null);

(statearr_31799_34628[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (11))){
var inst_31708 = (state_31733[(2)]);
var state_31733__$1 = (function (){var statearr_31802 = state_31733;
(statearr_31802[(10)] = inst_31708);

return statearr_31802;
})();
var statearr_31803_34634 = state_31733__$1;
(statearr_31803_34634[(2)] = null);

(statearr_31803_34634[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (9))){
var state_31733__$1 = state_31733;
var statearr_31805_34637 = state_31733__$1;
(statearr_31805_34637[(2)] = null);

(statearr_31805_34637[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (5))){
var state_31733__$1 = state_31733;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31809_34638 = state_31733__$1;
(statearr_31809_34638[(1)] = (8));

} else {
var statearr_31810_34639 = state_31733__$1;
(statearr_31810_34639[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (14))){
var inst_31711 = (state_31733[(8)]);
var inst_31711__$1 = (state_31733[(2)]);
var inst_31712 = (inst_31711__$1 == null);
var inst_31713 = cljs.core.not(inst_31712);
var state_31733__$1 = (function (){var statearr_31812 = state_31733;
(statearr_31812[(8)] = inst_31711__$1);

return statearr_31812;
})();
if(inst_31713){
var statearr_31814_34640 = state_31733__$1;
(statearr_31814_34640[(1)] = (15));

} else {
var statearr_31816_34641 = state_31733__$1;
(statearr_31816_34641[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (16))){
var state_31733__$1 = state_31733;
var statearr_31817_34642 = state_31733__$1;
(statearr_31817_34642[(2)] = false);

(statearr_31817_34642[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (10))){
var inst_31705 = (state_31733[(2)]);
var state_31733__$1 = state_31733;
var statearr_31822_34643 = state_31733__$1;
(statearr_31822_34643[(2)] = inst_31705);

(statearr_31822_34643[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (18))){
var inst_31716 = (state_31733[(2)]);
var state_31733__$1 = state_31733;
var statearr_31824_34645 = state_31733__$1;
(statearr_31824_34645[(2)] = inst_31716);

(statearr_31824_34645[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31734 === (8))){
var inst_31699 = cljs.core.async.close_BANG_(to);
var state_31733__$1 = state_31733;
var statearr_31825_34648 = state_31733__$1;
(statearr_31825_34648[(2)] = inst_31699);

(statearr_31825_34648[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0 = (function (){
var statearr_31829 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_31829[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__);

(statearr_31829[(1)] = (1));

return statearr_31829;
});
var cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1 = (function (state_31733){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_31733);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e31831){var ex__30627__auto__ = e31831;
var statearr_31832_34649 = state_31733;
(statearr_31832_34649[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_31733[(4)]))){
var statearr_31833_34650 = state_31733;
(statearr_31833_34650[(1)] = cljs.core.first((state_31733[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34652 = state_31733;
state_31733 = G__34652;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__ = function(state_31733){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1.call(this,state_31733);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__30624__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_31834 = f__30724__auto__();
(statearr_31834[(6)] = c__30723__auto__);

return statearr_31834;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));

return c__30723__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__31837 = arguments.length;
switch (G__31837) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__31849 = arguments.length;
switch (G__31849) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__31872 = arguments.length;
switch (G__31872) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__30723__auto___34668 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_31905){
var state_val_31906 = (state_31905[(1)]);
if((state_val_31906 === (7))){
var inst_31901 = (state_31905[(2)]);
var state_31905__$1 = state_31905;
var statearr_31907_34669 = state_31905__$1;
(statearr_31907_34669[(2)] = inst_31901);

(statearr_31907_34669[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (1))){
var state_31905__$1 = state_31905;
var statearr_31908_34670 = state_31905__$1;
(statearr_31908_34670[(2)] = null);

(statearr_31908_34670[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (4))){
var inst_31882 = (state_31905[(7)]);
var inst_31882__$1 = (state_31905[(2)]);
var inst_31883 = (inst_31882__$1 == null);
var state_31905__$1 = (function (){var statearr_31914 = state_31905;
(statearr_31914[(7)] = inst_31882__$1);

return statearr_31914;
})();
if(cljs.core.truth_(inst_31883)){
var statearr_31915_34674 = state_31905__$1;
(statearr_31915_34674[(1)] = (5));

} else {
var statearr_31917_34675 = state_31905__$1;
(statearr_31917_34675[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (13))){
var state_31905__$1 = state_31905;
var statearr_31922_34677 = state_31905__$1;
(statearr_31922_34677[(2)] = null);

(statearr_31922_34677[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (6))){
var inst_31882 = (state_31905[(7)]);
var inst_31888 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_31882) : p.call(null,inst_31882));
var state_31905__$1 = state_31905;
if(cljs.core.truth_(inst_31888)){
var statearr_31923_34682 = state_31905__$1;
(statearr_31923_34682[(1)] = (9));

} else {
var statearr_31924_34683 = state_31905__$1;
(statearr_31924_34683[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (3))){
var inst_31903 = (state_31905[(2)]);
var state_31905__$1 = state_31905;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31905__$1,inst_31903);
} else {
if((state_val_31906 === (12))){
var state_31905__$1 = state_31905;
var statearr_31925_34685 = state_31905__$1;
(statearr_31925_34685[(2)] = null);

(statearr_31925_34685[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (2))){
var state_31905__$1 = state_31905;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31905__$1,(4),ch);
} else {
if((state_val_31906 === (11))){
var inst_31882 = (state_31905[(7)]);
var inst_31892 = (state_31905[(2)]);
var state_31905__$1 = state_31905;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31905__$1,(8),inst_31892,inst_31882);
} else {
if((state_val_31906 === (9))){
var state_31905__$1 = state_31905;
var statearr_31929_34687 = state_31905__$1;
(statearr_31929_34687[(2)] = tc);

(statearr_31929_34687[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (5))){
var inst_31885 = cljs.core.async.close_BANG_(tc);
var inst_31886 = cljs.core.async.close_BANG_(fc);
var state_31905__$1 = (function (){var statearr_31930 = state_31905;
(statearr_31930[(8)] = inst_31885);

return statearr_31930;
})();
var statearr_31931_34688 = state_31905__$1;
(statearr_31931_34688[(2)] = inst_31886);

(statearr_31931_34688[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (14))){
var inst_31899 = (state_31905[(2)]);
var state_31905__$1 = state_31905;
var statearr_31932_34689 = state_31905__$1;
(statearr_31932_34689[(2)] = inst_31899);

(statearr_31932_34689[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (10))){
var state_31905__$1 = state_31905;
var statearr_31933_34690 = state_31905__$1;
(statearr_31933_34690[(2)] = fc);

(statearr_31933_34690[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31906 === (8))){
var inst_31894 = (state_31905[(2)]);
var state_31905__$1 = state_31905;
if(cljs.core.truth_(inst_31894)){
var statearr_31934_34692 = state_31905__$1;
(statearr_31934_34692[(1)] = (12));

} else {
var statearr_31936_34693 = state_31905__$1;
(statearr_31936_34693[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_31938 = [null,null,null,null,null,null,null,null,null];
(statearr_31938[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_31938[(1)] = (1));

return statearr_31938;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_31905){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_31905);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e31939){var ex__30627__auto__ = e31939;
var statearr_31940_34694 = state_31905;
(statearr_31940_34694[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_31905[(4)]))){
var statearr_31941_34695 = state_31905;
(statearr_31941_34695[(1)] = cljs.core.first((state_31905[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34701 = state_31905;
state_31905 = G__34701;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_31905){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_31905);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_31944 = f__30724__auto__();
(statearr_31944[(6)] = c__30723__auto___34668);

return statearr_31944;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__30723__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_31970){
var state_val_31971 = (state_31970[(1)]);
if((state_val_31971 === (7))){
var inst_31966 = (state_31970[(2)]);
var state_31970__$1 = state_31970;
var statearr_31973_34705 = state_31970__$1;
(statearr_31973_34705[(2)] = inst_31966);

(statearr_31973_34705[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31971 === (1))){
var inst_31949 = init;
var inst_31950 = inst_31949;
var state_31970__$1 = (function (){var statearr_31974 = state_31970;
(statearr_31974[(7)] = inst_31950);

return statearr_31974;
})();
var statearr_31975_34708 = state_31970__$1;
(statearr_31975_34708[(2)] = null);

(statearr_31975_34708[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31971 === (4))){
var inst_31953 = (state_31970[(8)]);
var inst_31953__$1 = (state_31970[(2)]);
var inst_31954 = (inst_31953__$1 == null);
var state_31970__$1 = (function (){var statearr_31976 = state_31970;
(statearr_31976[(8)] = inst_31953__$1);

return statearr_31976;
})();
if(cljs.core.truth_(inst_31954)){
var statearr_31977_34710 = state_31970__$1;
(statearr_31977_34710[(1)] = (5));

} else {
var statearr_31978_34714 = state_31970__$1;
(statearr_31978_34714[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31971 === (6))){
var inst_31953 = (state_31970[(8)]);
var inst_31950 = (state_31970[(7)]);
var inst_31957 = (state_31970[(9)]);
var inst_31957__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_31950,inst_31953) : f.call(null,inst_31950,inst_31953));
var inst_31958 = cljs.core.reduced_QMARK_(inst_31957__$1);
var state_31970__$1 = (function (){var statearr_31983 = state_31970;
(statearr_31983[(9)] = inst_31957__$1);

return statearr_31983;
})();
if(inst_31958){
var statearr_31984_34720 = state_31970__$1;
(statearr_31984_34720[(1)] = (8));

} else {
var statearr_31990_34729 = state_31970__$1;
(statearr_31990_34729[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31971 === (3))){
var inst_31968 = (state_31970[(2)]);
var state_31970__$1 = state_31970;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31970__$1,inst_31968);
} else {
if((state_val_31971 === (2))){
var state_31970__$1 = state_31970;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31970__$1,(4),ch);
} else {
if((state_val_31971 === (9))){
var inst_31957 = (state_31970[(9)]);
var inst_31950 = inst_31957;
var state_31970__$1 = (function (){var statearr_32019 = state_31970;
(statearr_32019[(7)] = inst_31950);

return statearr_32019;
})();
var statearr_32024_34734 = state_31970__$1;
(statearr_32024_34734[(2)] = null);

(statearr_32024_34734[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31971 === (5))){
var inst_31950 = (state_31970[(7)]);
var state_31970__$1 = state_31970;
var statearr_32032_34735 = state_31970__$1;
(statearr_32032_34735[(2)] = inst_31950);

(statearr_32032_34735[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31971 === (10))){
var inst_31964 = (state_31970[(2)]);
var state_31970__$1 = state_31970;
var statearr_32047_34736 = state_31970__$1;
(statearr_32047_34736[(2)] = inst_31964);

(statearr_32047_34736[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31971 === (8))){
var inst_31957 = (state_31970[(9)]);
var inst_31960 = cljs.core.deref(inst_31957);
var state_31970__$1 = state_31970;
var statearr_32048_34738 = state_31970__$1;
(statearr_32048_34738[(2)] = inst_31960);

(statearr_32048_34738[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__30624__auto__ = null;
var cljs$core$async$reduce_$_state_machine__30624__auto____0 = (function (){
var statearr_32054 = [null,null,null,null,null,null,null,null,null,null];
(statearr_32054[(0)] = cljs$core$async$reduce_$_state_machine__30624__auto__);

(statearr_32054[(1)] = (1));

return statearr_32054;
});
var cljs$core$async$reduce_$_state_machine__30624__auto____1 = (function (state_31970){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_31970);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e32056){var ex__30627__auto__ = e32056;
var statearr_32057_34739 = state_31970;
(statearr_32057_34739[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_31970[(4)]))){
var statearr_32058_34740 = state_31970;
(statearr_32058_34740[(1)] = cljs.core.first((state_31970[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34741 = state_31970;
state_31970 = G__34741;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__30624__auto__ = function(state_31970){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__30624__auto____1.call(this,state_31970);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__30624__auto____0;
cljs$core$async$reduce_$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__30624__auto____1;
return cljs$core$async$reduce_$_state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_32059 = f__30724__auto__();
(statearr_32059[(6)] = c__30723__auto__);

return statearr_32059;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));

return c__30723__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__30723__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_32068){
var state_val_32069 = (state_32068[(1)]);
if((state_val_32069 === (1))){
var inst_32063 = cljs.core.async.reduce(f__$1,init,ch);
var state_32068__$1 = state_32068;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32068__$1,(2),inst_32063);
} else {
if((state_val_32069 === (2))){
var inst_32065 = (state_32068[(2)]);
var inst_32066 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_32065) : f__$1.call(null,inst_32065));
var state_32068__$1 = state_32068;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32068__$1,inst_32066);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__30624__auto__ = null;
var cljs$core$async$transduce_$_state_machine__30624__auto____0 = (function (){
var statearr_32070 = [null,null,null,null,null,null,null];
(statearr_32070[(0)] = cljs$core$async$transduce_$_state_machine__30624__auto__);

(statearr_32070[(1)] = (1));

return statearr_32070;
});
var cljs$core$async$transduce_$_state_machine__30624__auto____1 = (function (state_32068){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_32068);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e32072){var ex__30627__auto__ = e32072;
var statearr_32073_34750 = state_32068;
(statearr_32073_34750[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_32068[(4)]))){
var statearr_32074_34751 = state_32068;
(statearr_32074_34751[(1)] = cljs.core.first((state_32068[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34752 = state_32068;
state_32068 = G__34752;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__30624__auto__ = function(state_32068){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__30624__auto____1.call(this,state_32068);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__30624__auto____0;
cljs$core$async$transduce_$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__30624__auto____1;
return cljs$core$async$transduce_$_state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_32075 = f__30724__auto__();
(statearr_32075[(6)] = c__30723__auto__);

return statearr_32075;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));

return c__30723__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__32077 = arguments.length;
switch (G__32077) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__30723__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_32114){
var state_val_32117 = (state_32114[(1)]);
if((state_val_32117 === (7))){
var inst_32094 = (state_32114[(2)]);
var state_32114__$1 = state_32114;
var statearr_32124_34756 = state_32114__$1;
(statearr_32124_34756[(2)] = inst_32094);

(statearr_32124_34756[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (1))){
var inst_32084 = cljs.core.seq(coll);
var inst_32088 = inst_32084;
var state_32114__$1 = (function (){var statearr_32126 = state_32114;
(statearr_32126[(7)] = inst_32088);

return statearr_32126;
})();
var statearr_32127_34757 = state_32114__$1;
(statearr_32127_34757[(2)] = null);

(statearr_32127_34757[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (4))){
var inst_32088 = (state_32114[(7)]);
var inst_32092 = cljs.core.first(inst_32088);
var state_32114__$1 = state_32114;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32114__$1,(7),ch,inst_32092);
} else {
if((state_val_32117 === (13))){
var inst_32108 = (state_32114[(2)]);
var state_32114__$1 = state_32114;
var statearr_32128_34764 = state_32114__$1;
(statearr_32128_34764[(2)] = inst_32108);

(statearr_32128_34764[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (6))){
var inst_32097 = (state_32114[(2)]);
var state_32114__$1 = state_32114;
if(cljs.core.truth_(inst_32097)){
var statearr_32129_34767 = state_32114__$1;
(statearr_32129_34767[(1)] = (8));

} else {
var statearr_32130_34769 = state_32114__$1;
(statearr_32130_34769[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (3))){
var inst_32112 = (state_32114[(2)]);
var state_32114__$1 = state_32114;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32114__$1,inst_32112);
} else {
if((state_val_32117 === (12))){
var state_32114__$1 = state_32114;
var statearr_32136_34770 = state_32114__$1;
(statearr_32136_34770[(2)] = null);

(statearr_32136_34770[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (2))){
var inst_32088 = (state_32114[(7)]);
var state_32114__$1 = state_32114;
if(cljs.core.truth_(inst_32088)){
var statearr_32143_34772 = state_32114__$1;
(statearr_32143_34772[(1)] = (4));

} else {
var statearr_32144_34773 = state_32114__$1;
(statearr_32144_34773[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (11))){
var inst_32105 = cljs.core.async.close_BANG_(ch);
var state_32114__$1 = state_32114;
var statearr_32150_34774 = state_32114__$1;
(statearr_32150_34774[(2)] = inst_32105);

(statearr_32150_34774[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (9))){
var state_32114__$1 = state_32114;
if(cljs.core.truth_(close_QMARK_)){
var statearr_32151_34775 = state_32114__$1;
(statearr_32151_34775[(1)] = (11));

} else {
var statearr_32152_34776 = state_32114__$1;
(statearr_32152_34776[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (5))){
var inst_32088 = (state_32114[(7)]);
var state_32114__$1 = state_32114;
var statearr_32153_34777 = state_32114__$1;
(statearr_32153_34777[(2)] = inst_32088);

(statearr_32153_34777[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (10))){
var inst_32110 = (state_32114[(2)]);
var state_32114__$1 = state_32114;
var statearr_32161_34778 = state_32114__$1;
(statearr_32161_34778[(2)] = inst_32110);

(statearr_32161_34778[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32117 === (8))){
var inst_32088 = (state_32114[(7)]);
var inst_32100 = cljs.core.next(inst_32088);
var inst_32088__$1 = inst_32100;
var state_32114__$1 = (function (){var statearr_32162 = state_32114;
(statearr_32162[(7)] = inst_32088__$1);

return statearr_32162;
})();
var statearr_32163_34782 = state_32114__$1;
(statearr_32163_34782[(2)] = null);

(statearr_32163_34782[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_32164 = [null,null,null,null,null,null,null,null];
(statearr_32164[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_32164[(1)] = (1));

return statearr_32164;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_32114){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_32114);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e32165){var ex__30627__auto__ = e32165;
var statearr_32168_34794 = state_32114;
(statearr_32168_34794[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_32114[(4)]))){
var statearr_32171_34795 = state_32114;
(statearr_32171_34795[(1)] = cljs.core.first((state_32114[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34798 = state_32114;
state_32114 = G__34798;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_32114){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_32114);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_32173 = f__30724__auto__();
(statearr_32173[(6)] = c__30723__auto__);

return statearr_32173;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));

return c__30723__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__32180 = arguments.length;
switch (G__32180) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_34800 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_34800(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_34801 = (function (m,ch,close_QMARK_){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4429__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4426__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_34801(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_34803 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_34803(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_34804 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_34804(m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32215 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32215 = (function (ch,cs,meta32216){
this.ch = ch;
this.cs = cs;
this.meta32216 = meta32216;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32215.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32217,meta32216__$1){
var self__ = this;
var _32217__$1 = this;
return (new cljs.core.async.t_cljs$core$async32215(self__.ch,self__.cs,meta32216__$1));
}));

(cljs.core.async.t_cljs$core$async32215.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32217){
var self__ = this;
var _32217__$1 = this;
return self__.meta32216;
}));

(cljs.core.async.t_cljs$core$async32215.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32215.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async32215.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32215.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async32215.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async32215.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async32215.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta32216","meta32216",-1757355173,null)], null);
}));

(cljs.core.async.t_cljs$core$async32215.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32215.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32215");

(cljs.core.async.t_cljs$core$async32215.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async32215");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32215.
 */
cljs.core.async.__GT_t_cljs$core$async32215 = (function cljs$core$async$mult_$___GT_t_cljs$core$async32215(ch__$1,cs__$1,meta32216){
return (new cljs.core.async.t_cljs$core$async32215(ch__$1,cs__$1,meta32216));
});

}

return (new cljs.core.async.t_cljs$core$async32215(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__30723__auto___34810 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_32406){
var state_val_32409 = (state_32406[(1)]);
if((state_val_32409 === (7))){
var inst_32396 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32413_34811 = state_32406__$1;
(statearr_32413_34811[(2)] = inst_32396);

(statearr_32413_34811[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (20))){
var inst_32270 = (state_32406[(7)]);
var inst_32290 = cljs.core.first(inst_32270);
var inst_32291 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32290,(0),null);
var inst_32292 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32290,(1),null);
var state_32406__$1 = (function (){var statearr_32414 = state_32406;
(statearr_32414[(8)] = inst_32291);

return statearr_32414;
})();
if(cljs.core.truth_(inst_32292)){
var statearr_32415_34813 = state_32406__$1;
(statearr_32415_34813[(1)] = (22));

} else {
var statearr_32420_34814 = state_32406__$1;
(statearr_32420_34814[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (27))){
var inst_32337 = (state_32406[(9)]);
var inst_32326 = (state_32406[(10)]);
var inst_32324 = (state_32406[(11)]);
var inst_32239 = (state_32406[(12)]);
var inst_32337__$1 = cljs.core._nth(inst_32324,inst_32326);
var inst_32338 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_32337__$1,inst_32239,done);
var state_32406__$1 = (function (){var statearr_32425 = state_32406;
(statearr_32425[(9)] = inst_32337__$1);

return statearr_32425;
})();
if(cljs.core.truth_(inst_32338)){
var statearr_32426_34816 = state_32406__$1;
(statearr_32426_34816[(1)] = (30));

} else {
var statearr_32428_34817 = state_32406__$1;
(statearr_32428_34817[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (1))){
var state_32406__$1 = state_32406;
var statearr_32431_34818 = state_32406__$1;
(statearr_32431_34818[(2)] = null);

(statearr_32431_34818[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (24))){
var inst_32270 = (state_32406[(7)]);
var inst_32299 = (state_32406[(2)]);
var inst_32300 = cljs.core.next(inst_32270);
var inst_32248 = inst_32300;
var inst_32249 = null;
var inst_32250 = (0);
var inst_32251 = (0);
var state_32406__$1 = (function (){var statearr_32432 = state_32406;
(statearr_32432[(13)] = inst_32248);

(statearr_32432[(14)] = inst_32299);

(statearr_32432[(15)] = inst_32249);

(statearr_32432[(16)] = inst_32251);

(statearr_32432[(17)] = inst_32250);

return statearr_32432;
})();
var statearr_32433_34821 = state_32406__$1;
(statearr_32433_34821[(2)] = null);

(statearr_32433_34821[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (39))){
var state_32406__$1 = state_32406;
var statearr_32440_34822 = state_32406__$1;
(statearr_32440_34822[(2)] = null);

(statearr_32440_34822[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (4))){
var inst_32239 = (state_32406[(12)]);
var inst_32239__$1 = (state_32406[(2)]);
var inst_32240 = (inst_32239__$1 == null);
var state_32406__$1 = (function (){var statearr_32441 = state_32406;
(statearr_32441[(12)] = inst_32239__$1);

return statearr_32441;
})();
if(cljs.core.truth_(inst_32240)){
var statearr_32442_34823 = state_32406__$1;
(statearr_32442_34823[(1)] = (5));

} else {
var statearr_32443_34825 = state_32406__$1;
(statearr_32443_34825[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (15))){
var inst_32248 = (state_32406[(13)]);
var inst_32249 = (state_32406[(15)]);
var inst_32251 = (state_32406[(16)]);
var inst_32250 = (state_32406[(17)]);
var inst_32266 = (state_32406[(2)]);
var inst_32267 = (inst_32251 + (1));
var tmp32434 = inst_32248;
var tmp32435 = inst_32249;
var tmp32436 = inst_32250;
var inst_32248__$1 = tmp32434;
var inst_32249__$1 = tmp32435;
var inst_32250__$1 = tmp32436;
var inst_32251__$1 = inst_32267;
var state_32406__$1 = (function (){var statearr_32450 = state_32406;
(statearr_32450[(13)] = inst_32248__$1);

(statearr_32450[(18)] = inst_32266);

(statearr_32450[(15)] = inst_32249__$1);

(statearr_32450[(16)] = inst_32251__$1);

(statearr_32450[(17)] = inst_32250__$1);

return statearr_32450;
})();
var statearr_32453_34826 = state_32406__$1;
(statearr_32453_34826[(2)] = null);

(statearr_32453_34826[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (21))){
var inst_32303 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32465_34827 = state_32406__$1;
(statearr_32465_34827[(2)] = inst_32303);

(statearr_32465_34827[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (31))){
var inst_32337 = (state_32406[(9)]);
var inst_32343 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_32337);
var state_32406__$1 = state_32406;
var statearr_32474_34829 = state_32406__$1;
(statearr_32474_34829[(2)] = inst_32343);

(statearr_32474_34829[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (32))){
var inst_32325 = (state_32406[(19)]);
var inst_32326 = (state_32406[(10)]);
var inst_32324 = (state_32406[(11)]);
var inst_32323 = (state_32406[(20)]);
var inst_32345 = (state_32406[(2)]);
var inst_32346 = (inst_32326 + (1));
var tmp32456 = inst_32325;
var tmp32457 = inst_32324;
var tmp32458 = inst_32323;
var inst_32323__$1 = tmp32458;
var inst_32324__$1 = tmp32457;
var inst_32325__$1 = tmp32456;
var inst_32326__$1 = inst_32346;
var state_32406__$1 = (function (){var statearr_32485 = state_32406;
(statearr_32485[(21)] = inst_32345);

(statearr_32485[(19)] = inst_32325__$1);

(statearr_32485[(10)] = inst_32326__$1);

(statearr_32485[(11)] = inst_32324__$1);

(statearr_32485[(20)] = inst_32323__$1);

return statearr_32485;
})();
var statearr_32488_34834 = state_32406__$1;
(statearr_32488_34834[(2)] = null);

(statearr_32488_34834[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (40))){
var inst_32365 = (state_32406[(22)]);
var inst_32369 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_32365);
var state_32406__$1 = state_32406;
var statearr_32493_34836 = state_32406__$1;
(statearr_32493_34836[(2)] = inst_32369);

(statearr_32493_34836[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (33))){
var inst_32349 = (state_32406[(23)]);
var inst_32354 = cljs.core.chunked_seq_QMARK_(inst_32349);
var state_32406__$1 = state_32406;
if(inst_32354){
var statearr_32497_34837 = state_32406__$1;
(statearr_32497_34837[(1)] = (36));

} else {
var statearr_32499_34838 = state_32406__$1;
(statearr_32499_34838[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (13))){
var inst_32260 = (state_32406[(24)]);
var inst_32263 = cljs.core.async.close_BANG_(inst_32260);
var state_32406__$1 = state_32406;
var statearr_32502_34839 = state_32406__$1;
(statearr_32502_34839[(2)] = inst_32263);

(statearr_32502_34839[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (22))){
var inst_32291 = (state_32406[(8)]);
var inst_32296 = cljs.core.async.close_BANG_(inst_32291);
var state_32406__$1 = state_32406;
var statearr_32512_34842 = state_32406__$1;
(statearr_32512_34842[(2)] = inst_32296);

(statearr_32512_34842[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (36))){
var inst_32349 = (state_32406[(23)]);
var inst_32356 = cljs.core.chunk_first(inst_32349);
var inst_32357 = cljs.core.chunk_rest(inst_32349);
var inst_32358 = cljs.core.count(inst_32356);
var inst_32323 = inst_32357;
var inst_32324 = inst_32356;
var inst_32325 = inst_32358;
var inst_32326 = (0);
var state_32406__$1 = (function (){var statearr_32517 = state_32406;
(statearr_32517[(19)] = inst_32325);

(statearr_32517[(10)] = inst_32326);

(statearr_32517[(11)] = inst_32324);

(statearr_32517[(20)] = inst_32323);

return statearr_32517;
})();
var statearr_32521_34844 = state_32406__$1;
(statearr_32521_34844[(2)] = null);

(statearr_32521_34844[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (41))){
var inst_32349 = (state_32406[(23)]);
var inst_32371 = (state_32406[(2)]);
var inst_32372 = cljs.core.next(inst_32349);
var inst_32323 = inst_32372;
var inst_32324 = null;
var inst_32325 = (0);
var inst_32326 = (0);
var state_32406__$1 = (function (){var statearr_32526 = state_32406;
(statearr_32526[(19)] = inst_32325);

(statearr_32526[(10)] = inst_32326);

(statearr_32526[(11)] = inst_32324);

(statearr_32526[(25)] = inst_32371);

(statearr_32526[(20)] = inst_32323);

return statearr_32526;
})();
var statearr_32534_34846 = state_32406__$1;
(statearr_32534_34846[(2)] = null);

(statearr_32534_34846[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (43))){
var state_32406__$1 = state_32406;
var statearr_32539_34847 = state_32406__$1;
(statearr_32539_34847[(2)] = null);

(statearr_32539_34847[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (29))){
var inst_32380 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32545_34848 = state_32406__$1;
(statearr_32545_34848[(2)] = inst_32380);

(statearr_32545_34848[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (44))){
var inst_32393 = (state_32406[(2)]);
var state_32406__$1 = (function (){var statearr_32555 = state_32406;
(statearr_32555[(26)] = inst_32393);

return statearr_32555;
})();
var statearr_32556_34849 = state_32406__$1;
(statearr_32556_34849[(2)] = null);

(statearr_32556_34849[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (6))){
var inst_32315 = (state_32406[(27)]);
var inst_32314 = cljs.core.deref(cs);
var inst_32315__$1 = cljs.core.keys(inst_32314);
var inst_32316 = cljs.core.count(inst_32315__$1);
var inst_32317 = cljs.core.reset_BANG_(dctr,inst_32316);
var inst_32322 = cljs.core.seq(inst_32315__$1);
var inst_32323 = inst_32322;
var inst_32324 = null;
var inst_32325 = (0);
var inst_32326 = (0);
var state_32406__$1 = (function (){var statearr_32572 = state_32406;
(statearr_32572[(28)] = inst_32317);

(statearr_32572[(19)] = inst_32325);

(statearr_32572[(10)] = inst_32326);

(statearr_32572[(27)] = inst_32315__$1);

(statearr_32572[(11)] = inst_32324);

(statearr_32572[(20)] = inst_32323);

return statearr_32572;
})();
var statearr_32581_34854 = state_32406__$1;
(statearr_32581_34854[(2)] = null);

(statearr_32581_34854[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (28))){
var inst_32349 = (state_32406[(23)]);
var inst_32323 = (state_32406[(20)]);
var inst_32349__$1 = cljs.core.seq(inst_32323);
var state_32406__$1 = (function (){var statearr_32590 = state_32406;
(statearr_32590[(23)] = inst_32349__$1);

return statearr_32590;
})();
if(inst_32349__$1){
var statearr_32594_34855 = state_32406__$1;
(statearr_32594_34855[(1)] = (33));

} else {
var statearr_32596_34856 = state_32406__$1;
(statearr_32596_34856[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (25))){
var inst_32325 = (state_32406[(19)]);
var inst_32326 = (state_32406[(10)]);
var inst_32332 = (inst_32326 < inst_32325);
var inst_32333 = inst_32332;
var state_32406__$1 = state_32406;
if(cljs.core.truth_(inst_32333)){
var statearr_32601_34857 = state_32406__$1;
(statearr_32601_34857[(1)] = (27));

} else {
var statearr_32604_34858 = state_32406__$1;
(statearr_32604_34858[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (34))){
var state_32406__$1 = state_32406;
var statearr_32621_34859 = state_32406__$1;
(statearr_32621_34859[(2)] = null);

(statearr_32621_34859[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (17))){
var state_32406__$1 = state_32406;
var statearr_32625_34860 = state_32406__$1;
(statearr_32625_34860[(2)] = null);

(statearr_32625_34860[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (3))){
var inst_32398 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32406__$1,inst_32398);
} else {
if((state_val_32409 === (12))){
var inst_32308 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32629_34861 = state_32406__$1;
(statearr_32629_34861[(2)] = inst_32308);

(statearr_32629_34861[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (2))){
var state_32406__$1 = state_32406;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32406__$1,(4),ch);
} else {
if((state_val_32409 === (23))){
var state_32406__$1 = state_32406;
var statearr_32636_34862 = state_32406__$1;
(statearr_32636_34862[(2)] = null);

(statearr_32636_34862[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (35))){
var inst_32378 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32642_34863 = state_32406__$1;
(statearr_32642_34863[(2)] = inst_32378);

(statearr_32642_34863[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (19))){
var inst_32270 = (state_32406[(7)]);
var inst_32278 = cljs.core.chunk_first(inst_32270);
var inst_32279 = cljs.core.chunk_rest(inst_32270);
var inst_32280 = cljs.core.count(inst_32278);
var inst_32248 = inst_32279;
var inst_32249 = inst_32278;
var inst_32250 = inst_32280;
var inst_32251 = (0);
var state_32406__$1 = (function (){var statearr_32660 = state_32406;
(statearr_32660[(13)] = inst_32248);

(statearr_32660[(15)] = inst_32249);

(statearr_32660[(16)] = inst_32251);

(statearr_32660[(17)] = inst_32250);

return statearr_32660;
})();
var statearr_32669_34874 = state_32406__$1;
(statearr_32669_34874[(2)] = null);

(statearr_32669_34874[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (11))){
var inst_32248 = (state_32406[(13)]);
var inst_32270 = (state_32406[(7)]);
var inst_32270__$1 = cljs.core.seq(inst_32248);
var state_32406__$1 = (function (){var statearr_32679 = state_32406;
(statearr_32679[(7)] = inst_32270__$1);

return statearr_32679;
})();
if(inst_32270__$1){
var statearr_32681_34876 = state_32406__$1;
(statearr_32681_34876[(1)] = (16));

} else {
var statearr_32682_34877 = state_32406__$1;
(statearr_32682_34877[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (9))){
var inst_32310 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32688_34878 = state_32406__$1;
(statearr_32688_34878[(2)] = inst_32310);

(statearr_32688_34878[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (5))){
var inst_32246 = cljs.core.deref(cs);
var inst_32247 = cljs.core.seq(inst_32246);
var inst_32248 = inst_32247;
var inst_32249 = null;
var inst_32250 = (0);
var inst_32251 = (0);
var state_32406__$1 = (function (){var statearr_32695 = state_32406;
(statearr_32695[(13)] = inst_32248);

(statearr_32695[(15)] = inst_32249);

(statearr_32695[(16)] = inst_32251);

(statearr_32695[(17)] = inst_32250);

return statearr_32695;
})();
var statearr_32700_34882 = state_32406__$1;
(statearr_32700_34882[(2)] = null);

(statearr_32700_34882[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (14))){
var state_32406__$1 = state_32406;
var statearr_32704_34883 = state_32406__$1;
(statearr_32704_34883[(2)] = null);

(statearr_32704_34883[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (45))){
var inst_32390 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32709_34887 = state_32406__$1;
(statearr_32709_34887[(2)] = inst_32390);

(statearr_32709_34887[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (26))){
var inst_32315 = (state_32406[(27)]);
var inst_32382 = (state_32406[(2)]);
var inst_32383 = cljs.core.seq(inst_32315);
var state_32406__$1 = (function (){var statearr_32717 = state_32406;
(statearr_32717[(29)] = inst_32382);

return statearr_32717;
})();
if(inst_32383){
var statearr_32718_34888 = state_32406__$1;
(statearr_32718_34888[(1)] = (42));

} else {
var statearr_32719_34889 = state_32406__$1;
(statearr_32719_34889[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (16))){
var inst_32270 = (state_32406[(7)]);
var inst_32276 = cljs.core.chunked_seq_QMARK_(inst_32270);
var state_32406__$1 = state_32406;
if(inst_32276){
var statearr_32723_34893 = state_32406__$1;
(statearr_32723_34893[(1)] = (19));

} else {
var statearr_32724_34894 = state_32406__$1;
(statearr_32724_34894[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (38))){
var inst_32375 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32728_34898 = state_32406__$1;
(statearr_32728_34898[(2)] = inst_32375);

(statearr_32728_34898[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (30))){
var state_32406__$1 = state_32406;
var statearr_32735_34901 = state_32406__$1;
(statearr_32735_34901[(2)] = null);

(statearr_32735_34901[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (10))){
var inst_32249 = (state_32406[(15)]);
var inst_32251 = (state_32406[(16)]);
var inst_32259 = cljs.core._nth(inst_32249,inst_32251);
var inst_32260 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32259,(0),null);
var inst_32261 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32259,(1),null);
var state_32406__$1 = (function (){var statearr_32744 = state_32406;
(statearr_32744[(24)] = inst_32260);

return statearr_32744;
})();
if(cljs.core.truth_(inst_32261)){
var statearr_32745_34907 = state_32406__$1;
(statearr_32745_34907[(1)] = (13));

} else {
var statearr_32748_34908 = state_32406__$1;
(statearr_32748_34908[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (18))){
var inst_32306 = (state_32406[(2)]);
var state_32406__$1 = state_32406;
var statearr_32753_34909 = state_32406__$1;
(statearr_32753_34909[(2)] = inst_32306);

(statearr_32753_34909[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (42))){
var state_32406__$1 = state_32406;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32406__$1,(45),dchan);
} else {
if((state_val_32409 === (37))){
var inst_32365 = (state_32406[(22)]);
var inst_32349 = (state_32406[(23)]);
var inst_32239 = (state_32406[(12)]);
var inst_32365__$1 = cljs.core.first(inst_32349);
var inst_32366 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_32365__$1,inst_32239,done);
var state_32406__$1 = (function (){var statearr_32758 = state_32406;
(statearr_32758[(22)] = inst_32365__$1);

return statearr_32758;
})();
if(cljs.core.truth_(inst_32366)){
var statearr_32759_34911 = state_32406__$1;
(statearr_32759_34911[(1)] = (39));

} else {
var statearr_32764_34912 = state_32406__$1;
(statearr_32764_34912[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32409 === (8))){
var inst_32251 = (state_32406[(16)]);
var inst_32250 = (state_32406[(17)]);
var inst_32253 = (inst_32251 < inst_32250);
var inst_32254 = inst_32253;
var state_32406__$1 = state_32406;
if(cljs.core.truth_(inst_32254)){
var statearr_32766_34914 = state_32406__$1;
(statearr_32766_34914[(1)] = (10));

} else {
var statearr_32768_34915 = state_32406__$1;
(statearr_32768_34915[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__30624__auto__ = null;
var cljs$core$async$mult_$_state_machine__30624__auto____0 = (function (){
var statearr_32780 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32780[(0)] = cljs$core$async$mult_$_state_machine__30624__auto__);

(statearr_32780[(1)] = (1));

return statearr_32780;
});
var cljs$core$async$mult_$_state_machine__30624__auto____1 = (function (state_32406){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_32406);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e32783){var ex__30627__auto__ = e32783;
var statearr_32784_34918 = state_32406;
(statearr_32784_34918[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_32406[(4)]))){
var statearr_32785_34919 = state_32406;
(statearr_32785_34919[(1)] = cljs.core.first((state_32406[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34920 = state_32406;
state_32406 = G__34920;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__30624__auto__ = function(state_32406){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__30624__auto____1.call(this,state_32406);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__30624__auto____0;
cljs$core$async$mult_$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__30624__auto____1;
return cljs$core$async$mult_$_state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_32788 = f__30724__auto__();
(statearr_32788[(6)] = c__30723__auto___34810);

return statearr_32788;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__32798 = arguments.length;
switch (G__32798) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_34925 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_34925(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_34929 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_34929(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_34934 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_34934(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_34935 = (function (m,state_map){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4429__auto__.call(null,m,state_map));
} else {
var m__4426__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4426__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_34935(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_34936 = (function (m,mode){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4429__auto__.call(null,m,mode));
} else {
var m__4426__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4426__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_34936(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___34938 = arguments.length;
var i__4737__auto___34939 = (0);
while(true){
if((i__4737__auto___34939 < len__4736__auto___34938)){
args__4742__auto__.push((arguments[i__4737__auto___34939]));

var G__34940 = (i__4737__auto___34939 + (1));
i__4737__auto___34939 = G__34940;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__32867){
var map__32868 = p__32867;
var map__32868__$1 = (((((!((map__32868 == null))))?(((((map__32868.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__32868.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__32868):map__32868);
var opts = map__32868__$1;
var statearr_32870_34941 = state;
(statearr_32870_34941[(1)] = cont_block);


var temp__5735__auto__ = cljs.core.async.do_alts((function (val){
var statearr_32871_34946 = state;
(statearr_32871_34946[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5735__auto__)){
var cb = temp__5735__auto__;
var statearr_32882_34948 = state;
(statearr_32882_34948[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq32860){
var G__32861 = cljs.core.first(seq32860);
var seq32860__$1 = cljs.core.next(seq32860);
var G__32862 = cljs.core.first(seq32860__$1);
var seq32860__$2 = cljs.core.next(seq32860__$1);
var G__32863 = cljs.core.first(seq32860__$2);
var seq32860__$3 = cljs.core.next(seq32860__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__32861,G__32862,G__32863,seq32860__$3);
}));

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async32932 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32932 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta32933){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta32933 = meta32933;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32934,meta32933__$1){
var self__ = this;
var _32934__$1 = this;
return (new cljs.core.async.t_cljs$core$async32932(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta32933__$1));
}));

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32934){
var self__ = this;
var _32934__$1 = this;
return self__.meta32933;
}));

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32932.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32932.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta32933","meta32933",2044412986,null)], null);
}));

(cljs.core.async.t_cljs$core$async32932.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32932.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32932");

(cljs.core.async.t_cljs$core$async32932.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async32932");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32932.
 */
cljs.core.async.__GT_t_cljs$core$async32932 = (function cljs$core$async$mix_$___GT_t_cljs$core$async32932(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta32933){
return (new cljs.core.async.t_cljs$core$async32932(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta32933));
});

}

return (new cljs.core.async.t_cljs$core$async32932(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__30723__auto___34962 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_33083){
var state_val_33084 = (state_33083[(1)]);
if((state_val_33084 === (7))){
var inst_32987 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
var statearr_33086_34963 = state_33083__$1;
(statearr_33086_34963[(2)] = inst_32987);

(statearr_33086_34963[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (20))){
var inst_33003 = (state_33083[(7)]);
var state_33083__$1 = state_33083;
var statearr_33087_34964 = state_33083__$1;
(statearr_33087_34964[(2)] = inst_33003);

(statearr_33087_34964[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (27))){
var state_33083__$1 = state_33083;
var statearr_33088_34965 = state_33083__$1;
(statearr_33088_34965[(2)] = null);

(statearr_33088_34965[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (1))){
var inst_32969 = (state_33083[(8)]);
var inst_32969__$1 = calc_state();
var inst_32971 = (inst_32969__$1 == null);
var inst_32972 = cljs.core.not(inst_32971);
var state_33083__$1 = (function (){var statearr_33090 = state_33083;
(statearr_33090[(8)] = inst_32969__$1);

return statearr_33090;
})();
if(inst_32972){
var statearr_33091_34966 = state_33083__$1;
(statearr_33091_34966[(1)] = (2));

} else {
var statearr_33092_34967 = state_33083__$1;
(statearr_33092_34967[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (24))){
var inst_33032 = (state_33083[(9)]);
var inst_33042 = (state_33083[(10)]);
var inst_33057 = (state_33083[(11)]);
var inst_33057__$1 = (inst_33032.cljs$core$IFn$_invoke$arity$1 ? inst_33032.cljs$core$IFn$_invoke$arity$1(inst_33042) : inst_33032.call(null,inst_33042));
var state_33083__$1 = (function (){var statearr_33094 = state_33083;
(statearr_33094[(11)] = inst_33057__$1);

return statearr_33094;
})();
if(cljs.core.truth_(inst_33057__$1)){
var statearr_33095_34972 = state_33083__$1;
(statearr_33095_34972[(1)] = (29));

} else {
var statearr_33096_34974 = state_33083__$1;
(statearr_33096_34974[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (4))){
var inst_32990 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
if(cljs.core.truth_(inst_32990)){
var statearr_33098_34982 = state_33083__$1;
(statearr_33098_34982[(1)] = (8));

} else {
var statearr_33099_34984 = state_33083__$1;
(statearr_33099_34984[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (15))){
var inst_33025 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
if(cljs.core.truth_(inst_33025)){
var statearr_33101_34991 = state_33083__$1;
(statearr_33101_34991[(1)] = (19));

} else {
var statearr_33102_34994 = state_33083__$1;
(statearr_33102_34994[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (21))){
var inst_33031 = (state_33083[(12)]);
var inst_33031__$1 = (state_33083[(2)]);
var inst_33032 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33031__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_33033 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33031__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_33034 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33031__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_33083__$1 = (function (){var statearr_33104 = state_33083;
(statearr_33104[(9)] = inst_33032);

(statearr_33104[(12)] = inst_33031__$1);

(statearr_33104[(13)] = inst_33033);

return statearr_33104;
})();
return cljs.core.async.ioc_alts_BANG_(state_33083__$1,(22),inst_33034);
} else {
if((state_val_33084 === (31))){
var inst_33065 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
if(cljs.core.truth_(inst_33065)){
var statearr_33105_35004 = state_33083__$1;
(statearr_33105_35004[(1)] = (32));

} else {
var statearr_33106_35005 = state_33083__$1;
(statearr_33106_35005[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (32))){
var inst_33041 = (state_33083[(14)]);
var state_33083__$1 = state_33083;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33083__$1,(35),out,inst_33041);
} else {
if((state_val_33084 === (33))){
var inst_33031 = (state_33083[(12)]);
var inst_33003 = inst_33031;
var state_33083__$1 = (function (){var statearr_33107 = state_33083;
(statearr_33107[(7)] = inst_33003);

return statearr_33107;
})();
var statearr_33108_35010 = state_33083__$1;
(statearr_33108_35010[(2)] = null);

(statearr_33108_35010[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (13))){
var inst_33003 = (state_33083[(7)]);
var inst_33010 = inst_33003.cljs$lang$protocol_mask$partition0$;
var inst_33015 = (inst_33010 & (64));
var inst_33016 = inst_33003.cljs$core$ISeq$;
var inst_33017 = (cljs.core.PROTOCOL_SENTINEL === inst_33016);
var inst_33018 = ((inst_33015) || (inst_33017));
var state_33083__$1 = state_33083;
if(cljs.core.truth_(inst_33018)){
var statearr_33109_35015 = state_33083__$1;
(statearr_33109_35015[(1)] = (16));

} else {
var statearr_33110_35016 = state_33083__$1;
(statearr_33110_35016[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (22))){
var inst_33042 = (state_33083[(10)]);
var inst_33041 = (state_33083[(14)]);
var inst_33040 = (state_33083[(2)]);
var inst_33041__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33040,(0),null);
var inst_33042__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33040,(1),null);
var inst_33044 = (inst_33041__$1 == null);
var inst_33045 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_33042__$1,change);
var inst_33046 = ((inst_33044) || (inst_33045));
var state_33083__$1 = (function (){var statearr_33111 = state_33083;
(statearr_33111[(10)] = inst_33042__$1);

(statearr_33111[(14)] = inst_33041__$1);

return statearr_33111;
})();
if(cljs.core.truth_(inst_33046)){
var statearr_33112_35017 = state_33083__$1;
(statearr_33112_35017[(1)] = (23));

} else {
var statearr_33113_35018 = state_33083__$1;
(statearr_33113_35018[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (36))){
var inst_33031 = (state_33083[(12)]);
var inst_33003 = inst_33031;
var state_33083__$1 = (function (){var statearr_33114 = state_33083;
(statearr_33114[(7)] = inst_33003);

return statearr_33114;
})();
var statearr_33115_35020 = state_33083__$1;
(statearr_33115_35020[(2)] = null);

(statearr_33115_35020[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (29))){
var inst_33057 = (state_33083[(11)]);
var state_33083__$1 = state_33083;
var statearr_33116_35021 = state_33083__$1;
(statearr_33116_35021[(2)] = inst_33057);

(statearr_33116_35021[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (6))){
var state_33083__$1 = state_33083;
var statearr_33117_35022 = state_33083__$1;
(statearr_33117_35022[(2)] = false);

(statearr_33117_35022[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (28))){
var inst_33053 = (state_33083[(2)]);
var inst_33054 = calc_state();
var inst_33003 = inst_33054;
var state_33083__$1 = (function (){var statearr_33118 = state_33083;
(statearr_33118[(7)] = inst_33003);

(statearr_33118[(15)] = inst_33053);

return statearr_33118;
})();
var statearr_33120_35023 = state_33083__$1;
(statearr_33120_35023[(2)] = null);

(statearr_33120_35023[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (25))){
var inst_33079 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
var statearr_33123_35024 = state_33083__$1;
(statearr_33123_35024[(2)] = inst_33079);

(statearr_33123_35024[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (34))){
var inst_33077 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
var statearr_33124_35025 = state_33083__$1;
(statearr_33124_35025[(2)] = inst_33077);

(statearr_33124_35025[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (17))){
var state_33083__$1 = state_33083;
var statearr_33126_35026 = state_33083__$1;
(statearr_33126_35026[(2)] = false);

(statearr_33126_35026[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (3))){
var state_33083__$1 = state_33083;
var statearr_33127_35028 = state_33083__$1;
(statearr_33127_35028[(2)] = false);

(statearr_33127_35028[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (12))){
var inst_33081 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33083__$1,inst_33081);
} else {
if((state_val_33084 === (2))){
var inst_32969 = (state_33083[(8)]);
var inst_32975 = inst_32969.cljs$lang$protocol_mask$partition0$;
var inst_32976 = (inst_32975 & (64));
var inst_32977 = inst_32969.cljs$core$ISeq$;
var inst_32978 = (cljs.core.PROTOCOL_SENTINEL === inst_32977);
var inst_32979 = ((inst_32976) || (inst_32978));
var state_33083__$1 = state_33083;
if(cljs.core.truth_(inst_32979)){
var statearr_33129_35029 = state_33083__$1;
(statearr_33129_35029[(1)] = (5));

} else {
var statearr_33130_35030 = state_33083__$1;
(statearr_33130_35030[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (23))){
var inst_33041 = (state_33083[(14)]);
var inst_33048 = (inst_33041 == null);
var state_33083__$1 = state_33083;
if(cljs.core.truth_(inst_33048)){
var statearr_33132_35035 = state_33083__$1;
(statearr_33132_35035[(1)] = (26));

} else {
var statearr_33133_35036 = state_33083__$1;
(statearr_33133_35036[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (35))){
var inst_33068 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
if(cljs.core.truth_(inst_33068)){
var statearr_33134_35041 = state_33083__$1;
(statearr_33134_35041[(1)] = (36));

} else {
var statearr_33135_35043 = state_33083__$1;
(statearr_33135_35043[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (19))){
var inst_33003 = (state_33083[(7)]);
var inst_33028 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_33003);
var state_33083__$1 = state_33083;
var statearr_33136_35045 = state_33083__$1;
(statearr_33136_35045[(2)] = inst_33028);

(statearr_33136_35045[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (11))){
var inst_33003 = (state_33083[(7)]);
var inst_33007 = (inst_33003 == null);
var inst_33008 = cljs.core.not(inst_33007);
var state_33083__$1 = state_33083;
if(inst_33008){
var statearr_33140_35052 = state_33083__$1;
(statearr_33140_35052[(1)] = (13));

} else {
var statearr_33141_35054 = state_33083__$1;
(statearr_33141_35054[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (9))){
var inst_32969 = (state_33083[(8)]);
var state_33083__$1 = state_33083;
var statearr_33142_35059 = state_33083__$1;
(statearr_33142_35059[(2)] = inst_32969);

(statearr_33142_35059[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (5))){
var state_33083__$1 = state_33083;
var statearr_33143_35061 = state_33083__$1;
(statearr_33143_35061[(2)] = true);

(statearr_33143_35061[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (14))){
var state_33083__$1 = state_33083;
var statearr_33144_35066 = state_33083__$1;
(statearr_33144_35066[(2)] = false);

(statearr_33144_35066[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (26))){
var inst_33042 = (state_33083[(10)]);
var inst_33050 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_33042);
var state_33083__$1 = state_33083;
var statearr_33145_35068 = state_33083__$1;
(statearr_33145_35068[(2)] = inst_33050);

(statearr_33145_35068[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (16))){
var state_33083__$1 = state_33083;
var statearr_33149_35069 = state_33083__$1;
(statearr_33149_35069[(2)] = true);

(statearr_33149_35069[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (38))){
var inst_33073 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
var statearr_33150_35071 = state_33083__$1;
(statearr_33150_35071[(2)] = inst_33073);

(statearr_33150_35071[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (30))){
var inst_33032 = (state_33083[(9)]);
var inst_33042 = (state_33083[(10)]);
var inst_33033 = (state_33083[(13)]);
var inst_33060 = cljs.core.empty_QMARK_(inst_33032);
var inst_33061 = (inst_33033.cljs$core$IFn$_invoke$arity$1 ? inst_33033.cljs$core$IFn$_invoke$arity$1(inst_33042) : inst_33033.call(null,inst_33042));
var inst_33062 = cljs.core.not(inst_33061);
var inst_33063 = ((inst_33060) && (inst_33062));
var state_33083__$1 = state_33083;
var statearr_33154_35077 = state_33083__$1;
(statearr_33154_35077[(2)] = inst_33063);

(statearr_33154_35077[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (10))){
var inst_32969 = (state_33083[(8)]);
var inst_32995 = (state_33083[(2)]);
var inst_32996 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32995,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_32997 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32995,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32998 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32995,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_33003 = inst_32969;
var state_33083__$1 = (function (){var statearr_33164 = state_33083;
(statearr_33164[(7)] = inst_33003);

(statearr_33164[(16)] = inst_32996);

(statearr_33164[(17)] = inst_32997);

(statearr_33164[(18)] = inst_32998);

return statearr_33164;
})();
var statearr_33165_35078 = state_33083__$1;
(statearr_33165_35078[(2)] = null);

(statearr_33165_35078[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (18))){
var inst_33022 = (state_33083[(2)]);
var state_33083__$1 = state_33083;
var statearr_33166_35079 = state_33083__$1;
(statearr_33166_35079[(2)] = inst_33022);

(statearr_33166_35079[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (37))){
var state_33083__$1 = state_33083;
var statearr_33167_35080 = state_33083__$1;
(statearr_33167_35080[(2)] = null);

(statearr_33167_35080[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33084 === (8))){
var inst_32969 = (state_33083[(8)]);
var inst_32992 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_32969);
var state_33083__$1 = state_33083;
var statearr_33168_35081 = state_33083__$1;
(statearr_33168_35081[(2)] = inst_32992);

(statearr_33168_35081[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__30624__auto__ = null;
var cljs$core$async$mix_$_state_machine__30624__auto____0 = (function (){
var statearr_33169 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33169[(0)] = cljs$core$async$mix_$_state_machine__30624__auto__);

(statearr_33169[(1)] = (1));

return statearr_33169;
});
var cljs$core$async$mix_$_state_machine__30624__auto____1 = (function (state_33083){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_33083);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e33170){var ex__30627__auto__ = e33170;
var statearr_33171_35082 = state_33083;
(statearr_33171_35082[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_33083[(4)]))){
var statearr_33172_35083 = state_33083;
(statearr_33172_35083[(1)] = cljs.core.first((state_33083[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35084 = state_33083;
state_33083 = G__35084;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__30624__auto__ = function(state_33083){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__30624__auto____1.call(this,state_33083);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__30624__auto____0;
cljs$core$async$mix_$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__30624__auto____1;
return cljs$core$async$mix_$_state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_33178 = f__30724__auto__();
(statearr_33178[(6)] = c__30723__auto___34962);

return statearr_33178;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_35085 = (function (p,v,ch,close_QMARK_){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4429__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4426__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_35085(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_35090 = (function (p,v,ch){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4429__auto__.call(null,p,v,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4426__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_35090(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_35091 = (function() {
var G__35092 = null;
var G__35092__1 = (function (p){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4429__auto__.call(null,p));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4426__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__35092__2 = (function (p,v){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4429__auto__.call(null,p,v));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4426__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__35092 = function(p,v){
switch(arguments.length){
case 1:
return G__35092__1.call(this,p);
case 2:
return G__35092__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__35092.cljs$core$IFn$_invoke$arity$1 = G__35092__1;
G__35092.cljs$core$IFn$_invoke$arity$2 = G__35092__2;
return G__35092;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__33198 = arguments.length;
switch (G__33198) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_35091(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_35091(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__33215 = arguments.length;
switch (G__33215) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__4126__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__33207_SHARP_){
if(cljs.core.truth_((p1__33207_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__33207_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__33207_SHARP_.call(null,topic)))){
return p1__33207_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__33207_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33219 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33219 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta33220){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta33220 = meta33220;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33221,meta33220__$1){
var self__ = this;
var _33221__$1 = this;
return (new cljs.core.async.t_cljs$core$async33219(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta33220__$1));
}));

(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33221){
var self__ = this;
var _33221__$1 = this;
return self__.meta33220;
}));

(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async33219.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async33219.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta33220","meta33220",595336880,null)], null);
}));

(cljs.core.async.t_cljs$core$async33219.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33219.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33219");

(cljs.core.async.t_cljs$core$async33219.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33219");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33219.
 */
cljs.core.async.__GT_t_cljs$core$async33219 = (function cljs$core$async$__GT_t_cljs$core$async33219(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta33220){
return (new cljs.core.async.t_cljs$core$async33219(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta33220));
});

}

return (new cljs.core.async.t_cljs$core$async33219(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__30723__auto___35103 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_33321){
var state_val_33322 = (state_33321[(1)]);
if((state_val_33322 === (7))){
var inst_33317 = (state_33321[(2)]);
var state_33321__$1 = state_33321;
var statearr_33323_35104 = state_33321__$1;
(statearr_33323_35104[(2)] = inst_33317);

(statearr_33323_35104[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (20))){
var state_33321__$1 = state_33321;
var statearr_33325_35105 = state_33321__$1;
(statearr_33325_35105[(2)] = null);

(statearr_33325_35105[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (1))){
var state_33321__$1 = state_33321;
var statearr_33326_35106 = state_33321__$1;
(statearr_33326_35106[(2)] = null);

(statearr_33326_35106[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (24))){
var inst_33300 = (state_33321[(7)]);
var inst_33309 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_33300);
var state_33321__$1 = state_33321;
var statearr_33327_35107 = state_33321__$1;
(statearr_33327_35107[(2)] = inst_33309);

(statearr_33327_35107[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (4))){
var inst_33248 = (state_33321[(8)]);
var inst_33248__$1 = (state_33321[(2)]);
var inst_33249 = (inst_33248__$1 == null);
var state_33321__$1 = (function (){var statearr_33328 = state_33321;
(statearr_33328[(8)] = inst_33248__$1);

return statearr_33328;
})();
if(cljs.core.truth_(inst_33249)){
var statearr_33329_35108 = state_33321__$1;
(statearr_33329_35108[(1)] = (5));

} else {
var statearr_33331_35109 = state_33321__$1;
(statearr_33331_35109[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (15))){
var inst_33294 = (state_33321[(2)]);
var state_33321__$1 = state_33321;
var statearr_33334_35110 = state_33321__$1;
(statearr_33334_35110[(2)] = inst_33294);

(statearr_33334_35110[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (21))){
var inst_33314 = (state_33321[(2)]);
var state_33321__$1 = (function (){var statearr_33339 = state_33321;
(statearr_33339[(9)] = inst_33314);

return statearr_33339;
})();
var statearr_33340_35111 = state_33321__$1;
(statearr_33340_35111[(2)] = null);

(statearr_33340_35111[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (13))){
var inst_33274 = (state_33321[(10)]);
var inst_33276 = cljs.core.chunked_seq_QMARK_(inst_33274);
var state_33321__$1 = state_33321;
if(inst_33276){
var statearr_33346_35112 = state_33321__$1;
(statearr_33346_35112[(1)] = (16));

} else {
var statearr_33351_35113 = state_33321__$1;
(statearr_33351_35113[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (22))){
var inst_33306 = (state_33321[(2)]);
var state_33321__$1 = state_33321;
if(cljs.core.truth_(inst_33306)){
var statearr_33356_35114 = state_33321__$1;
(statearr_33356_35114[(1)] = (23));

} else {
var statearr_33357_35115 = state_33321__$1;
(statearr_33357_35115[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (6))){
var inst_33302 = (state_33321[(11)]);
var inst_33248 = (state_33321[(8)]);
var inst_33300 = (state_33321[(7)]);
var inst_33300__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_33248) : topic_fn.call(null,inst_33248));
var inst_33301 = cljs.core.deref(mults);
var inst_33302__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_33301,inst_33300__$1);
var state_33321__$1 = (function (){var statearr_33367 = state_33321;
(statearr_33367[(11)] = inst_33302__$1);

(statearr_33367[(7)] = inst_33300__$1);

return statearr_33367;
})();
if(cljs.core.truth_(inst_33302__$1)){
var statearr_33368_35116 = state_33321__$1;
(statearr_33368_35116[(1)] = (19));

} else {
var statearr_33369_35117 = state_33321__$1;
(statearr_33369_35117[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (25))){
var inst_33311 = (state_33321[(2)]);
var state_33321__$1 = state_33321;
var statearr_33370_35118 = state_33321__$1;
(statearr_33370_35118[(2)] = inst_33311);

(statearr_33370_35118[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (17))){
var inst_33274 = (state_33321[(10)]);
var inst_33285 = cljs.core.first(inst_33274);
var inst_33286 = cljs.core.async.muxch_STAR_(inst_33285);
var inst_33287 = cljs.core.async.close_BANG_(inst_33286);
var inst_33288 = cljs.core.next(inst_33274);
var inst_33258 = inst_33288;
var inst_33259 = null;
var inst_33260 = (0);
var inst_33261 = (0);
var state_33321__$1 = (function (){var statearr_33375 = state_33321;
(statearr_33375[(12)] = inst_33287);

(statearr_33375[(13)] = inst_33259);

(statearr_33375[(14)] = inst_33258);

(statearr_33375[(15)] = inst_33260);

(statearr_33375[(16)] = inst_33261);

return statearr_33375;
})();
var statearr_33376_35119 = state_33321__$1;
(statearr_33376_35119[(2)] = null);

(statearr_33376_35119[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (3))){
var inst_33319 = (state_33321[(2)]);
var state_33321__$1 = state_33321;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33321__$1,inst_33319);
} else {
if((state_val_33322 === (12))){
var inst_33296 = (state_33321[(2)]);
var state_33321__$1 = state_33321;
var statearr_33377_35124 = state_33321__$1;
(statearr_33377_35124[(2)] = inst_33296);

(statearr_33377_35124[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (2))){
var state_33321__$1 = state_33321;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33321__$1,(4),ch);
} else {
if((state_val_33322 === (23))){
var state_33321__$1 = state_33321;
var statearr_33378_35125 = state_33321__$1;
(statearr_33378_35125[(2)] = null);

(statearr_33378_35125[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (19))){
var inst_33302 = (state_33321[(11)]);
var inst_33248 = (state_33321[(8)]);
var inst_33304 = cljs.core.async.muxch_STAR_(inst_33302);
var state_33321__$1 = state_33321;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33321__$1,(22),inst_33304,inst_33248);
} else {
if((state_val_33322 === (11))){
var inst_33274 = (state_33321[(10)]);
var inst_33258 = (state_33321[(14)]);
var inst_33274__$1 = cljs.core.seq(inst_33258);
var state_33321__$1 = (function (){var statearr_33379 = state_33321;
(statearr_33379[(10)] = inst_33274__$1);

return statearr_33379;
})();
if(inst_33274__$1){
var statearr_33380_35130 = state_33321__$1;
(statearr_33380_35130[(1)] = (13));

} else {
var statearr_33381_35131 = state_33321__$1;
(statearr_33381_35131[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (9))){
var inst_33298 = (state_33321[(2)]);
var state_33321__$1 = state_33321;
var statearr_33382_35132 = state_33321__$1;
(statearr_33382_35132[(2)] = inst_33298);

(statearr_33382_35132[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (5))){
var inst_33255 = cljs.core.deref(mults);
var inst_33256 = cljs.core.vals(inst_33255);
var inst_33257 = cljs.core.seq(inst_33256);
var inst_33258 = inst_33257;
var inst_33259 = null;
var inst_33260 = (0);
var inst_33261 = (0);
var state_33321__$1 = (function (){var statearr_33383 = state_33321;
(statearr_33383[(13)] = inst_33259);

(statearr_33383[(14)] = inst_33258);

(statearr_33383[(15)] = inst_33260);

(statearr_33383[(16)] = inst_33261);

return statearr_33383;
})();
var statearr_33384_35134 = state_33321__$1;
(statearr_33384_35134[(2)] = null);

(statearr_33384_35134[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (14))){
var state_33321__$1 = state_33321;
var statearr_33388_35136 = state_33321__$1;
(statearr_33388_35136[(2)] = null);

(statearr_33388_35136[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (16))){
var inst_33274 = (state_33321[(10)]);
var inst_33278 = cljs.core.chunk_first(inst_33274);
var inst_33279 = cljs.core.chunk_rest(inst_33274);
var inst_33281 = cljs.core.count(inst_33278);
var inst_33258 = inst_33279;
var inst_33259 = inst_33278;
var inst_33260 = inst_33281;
var inst_33261 = (0);
var state_33321__$1 = (function (){var statearr_33389 = state_33321;
(statearr_33389[(13)] = inst_33259);

(statearr_33389[(14)] = inst_33258);

(statearr_33389[(15)] = inst_33260);

(statearr_33389[(16)] = inst_33261);

return statearr_33389;
})();
var statearr_33393_35137 = state_33321__$1;
(statearr_33393_35137[(2)] = null);

(statearr_33393_35137[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (10))){
var inst_33259 = (state_33321[(13)]);
var inst_33258 = (state_33321[(14)]);
var inst_33260 = (state_33321[(15)]);
var inst_33261 = (state_33321[(16)]);
var inst_33268 = cljs.core._nth(inst_33259,inst_33261);
var inst_33269 = cljs.core.async.muxch_STAR_(inst_33268);
var inst_33270 = cljs.core.async.close_BANG_(inst_33269);
var inst_33271 = (inst_33261 + (1));
var tmp33385 = inst_33259;
var tmp33386 = inst_33258;
var tmp33387 = inst_33260;
var inst_33258__$1 = tmp33386;
var inst_33259__$1 = tmp33385;
var inst_33260__$1 = tmp33387;
var inst_33261__$1 = inst_33271;
var state_33321__$1 = (function (){var statearr_33394 = state_33321;
(statearr_33394[(17)] = inst_33270);

(statearr_33394[(13)] = inst_33259__$1);

(statearr_33394[(14)] = inst_33258__$1);

(statearr_33394[(15)] = inst_33260__$1);

(statearr_33394[(16)] = inst_33261__$1);

return statearr_33394;
})();
var statearr_33395_35138 = state_33321__$1;
(statearr_33395_35138[(2)] = null);

(statearr_33395_35138[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (18))){
var inst_33291 = (state_33321[(2)]);
var state_33321__$1 = state_33321;
var statearr_33396_35139 = state_33321__$1;
(statearr_33396_35139[(2)] = inst_33291);

(statearr_33396_35139[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33322 === (8))){
var inst_33260 = (state_33321[(15)]);
var inst_33261 = (state_33321[(16)]);
var inst_33264 = (inst_33261 < inst_33260);
var inst_33265 = inst_33264;
var state_33321__$1 = state_33321;
if(cljs.core.truth_(inst_33265)){
var statearr_33397_35140 = state_33321__$1;
(statearr_33397_35140[(1)] = (10));

} else {
var statearr_33398_35141 = state_33321__$1;
(statearr_33398_35141[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_33404 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33404[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_33404[(1)] = (1));

return statearr_33404;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_33321){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_33321);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e33406){var ex__30627__auto__ = e33406;
var statearr_33407_35142 = state_33321;
(statearr_33407_35142[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_33321[(4)]))){
var statearr_33408_35143 = state_33321;
(statearr_33408_35143[(1)] = cljs.core.first((state_33321[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35144 = state_33321;
state_33321 = G__35144;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_33321){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_33321);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_33413 = f__30724__auto__();
(statearr_33413[(6)] = c__30723__auto___35103);

return statearr_33413;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__33427 = arguments.length;
switch (G__33427) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__33433 = arguments.length;
switch (G__33433) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__33444 = arguments.length;
switch (G__33444) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__30723__auto___35150 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_33499){
var state_val_33501 = (state_33499[(1)]);
if((state_val_33501 === (7))){
var state_33499__$1 = state_33499;
var statearr_33505_35151 = state_33499__$1;
(statearr_33505_35151[(2)] = null);

(statearr_33505_35151[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (1))){
var state_33499__$1 = state_33499;
var statearr_33506_35152 = state_33499__$1;
(statearr_33506_35152[(2)] = null);

(statearr_33506_35152[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (4))){
var inst_33455 = (state_33499[(7)]);
var inst_33456 = (state_33499[(8)]);
var inst_33458 = (inst_33456 < inst_33455);
var state_33499__$1 = state_33499;
if(cljs.core.truth_(inst_33458)){
var statearr_33510_35153 = state_33499__$1;
(statearr_33510_35153[(1)] = (6));

} else {
var statearr_33511_35154 = state_33499__$1;
(statearr_33511_35154[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (15))){
var inst_33485 = (state_33499[(9)]);
var inst_33490 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_33485);
var state_33499__$1 = state_33499;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33499__$1,(17),out,inst_33490);
} else {
if((state_val_33501 === (13))){
var inst_33485 = (state_33499[(9)]);
var inst_33485__$1 = (state_33499[(2)]);
var inst_33486 = cljs.core.some(cljs.core.nil_QMARK_,inst_33485__$1);
var state_33499__$1 = (function (){var statearr_33513 = state_33499;
(statearr_33513[(9)] = inst_33485__$1);

return statearr_33513;
})();
if(cljs.core.truth_(inst_33486)){
var statearr_33516_35155 = state_33499__$1;
(statearr_33516_35155[(1)] = (14));

} else {
var statearr_33517_35156 = state_33499__$1;
(statearr_33517_35156[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (6))){
var state_33499__$1 = state_33499;
var statearr_33518_35157 = state_33499__$1;
(statearr_33518_35157[(2)] = null);

(statearr_33518_35157[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (17))){
var inst_33492 = (state_33499[(2)]);
var state_33499__$1 = (function (){var statearr_33522 = state_33499;
(statearr_33522[(10)] = inst_33492);

return statearr_33522;
})();
var statearr_33523_35158 = state_33499__$1;
(statearr_33523_35158[(2)] = null);

(statearr_33523_35158[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (3))){
var inst_33497 = (state_33499[(2)]);
var state_33499__$1 = state_33499;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33499__$1,inst_33497);
} else {
if((state_val_33501 === (12))){
var _ = (function (){var statearr_33527 = state_33499;
(statearr_33527[(4)] = cljs.core.rest((state_33499[(4)])));

return statearr_33527;
})();
var state_33499__$1 = state_33499;
var ex33521 = (state_33499__$1[(2)]);
var statearr_33528_35160 = state_33499__$1;
(statearr_33528_35160[(5)] = ex33521);


if((ex33521 instanceof Object)){
var statearr_33532_35161 = state_33499__$1;
(statearr_33532_35161[(1)] = (11));

(statearr_33532_35161[(5)] = null);

} else {
throw ex33521;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (2))){
var inst_33454 = cljs.core.reset_BANG_(dctr,cnt);
var inst_33455 = cnt;
var inst_33456 = (0);
var state_33499__$1 = (function (){var statearr_33538 = state_33499;
(statearr_33538[(11)] = inst_33454);

(statearr_33538[(7)] = inst_33455);

(statearr_33538[(8)] = inst_33456);

return statearr_33538;
})();
var statearr_33539_35162 = state_33499__$1;
(statearr_33539_35162[(2)] = null);

(statearr_33539_35162[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (11))){
var inst_33463 = (state_33499[(2)]);
var inst_33464 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_33499__$1 = (function (){var statearr_33542 = state_33499;
(statearr_33542[(12)] = inst_33463);

return statearr_33542;
})();
var statearr_33543_35163 = state_33499__$1;
(statearr_33543_35163[(2)] = inst_33464);

(statearr_33543_35163[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (9))){
var inst_33456 = (state_33499[(8)]);
var _ = (function (){var statearr_33546 = state_33499;
(statearr_33546[(4)] = cljs.core.cons((12),(state_33499[(4)])));

return statearr_33546;
})();
var inst_33471 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_33456) : chs__$1.call(null,inst_33456));
var inst_33472 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_33456) : done.call(null,inst_33456));
var inst_33473 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_33471,inst_33472);
var ___$1 = (function (){var statearr_33549 = state_33499;
(statearr_33549[(4)] = cljs.core.rest((state_33499[(4)])));

return statearr_33549;
})();
var state_33499__$1 = state_33499;
var statearr_33551_35170 = state_33499__$1;
(statearr_33551_35170[(2)] = inst_33473);

(statearr_33551_35170[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (5))){
var inst_33483 = (state_33499[(2)]);
var state_33499__$1 = (function (){var statearr_33552 = state_33499;
(statearr_33552[(13)] = inst_33483);

return statearr_33552;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33499__$1,(13),dchan);
} else {
if((state_val_33501 === (14))){
var inst_33488 = cljs.core.async.close_BANG_(out);
var state_33499__$1 = state_33499;
var statearr_33556_35176 = state_33499__$1;
(statearr_33556_35176[(2)] = inst_33488);

(statearr_33556_35176[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (16))){
var inst_33495 = (state_33499[(2)]);
var state_33499__$1 = state_33499;
var statearr_33559_35177 = state_33499__$1;
(statearr_33559_35177[(2)] = inst_33495);

(statearr_33559_35177[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (10))){
var inst_33456 = (state_33499[(8)]);
var inst_33476 = (state_33499[(2)]);
var inst_33477 = (inst_33456 + (1));
var inst_33456__$1 = inst_33477;
var state_33499__$1 = (function (){var statearr_33562 = state_33499;
(statearr_33562[(8)] = inst_33456__$1);

(statearr_33562[(14)] = inst_33476);

return statearr_33562;
})();
var statearr_33565_35181 = state_33499__$1;
(statearr_33565_35181[(2)] = null);

(statearr_33565_35181[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33501 === (8))){
var inst_33481 = (state_33499[(2)]);
var state_33499__$1 = state_33499;
var statearr_33567_35182 = state_33499__$1;
(statearr_33567_35182[(2)] = inst_33481);

(statearr_33567_35182[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_33569 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33569[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_33569[(1)] = (1));

return statearr_33569;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_33499){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_33499);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e33572){var ex__30627__auto__ = e33572;
var statearr_33573_35184 = state_33499;
(statearr_33573_35184[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_33499[(4)]))){
var statearr_33576_35185 = state_33499;
(statearr_33576_35185[(1)] = cljs.core.first((state_33499[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35186 = state_33499;
state_33499 = G__35186;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_33499){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_33499);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_33578 = f__30724__auto__();
(statearr_33578[(6)] = c__30723__auto___35150);

return statearr_33578;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__33592 = arguments.length;
switch (G__33592) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30723__auto___35192 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_33649){
var state_val_33650 = (state_33649[(1)]);
if((state_val_33650 === (7))){
var inst_33618 = (state_33649[(7)]);
var inst_33620 = (state_33649[(8)]);
var inst_33618__$1 = (state_33649[(2)]);
var inst_33620__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33618__$1,(0),null);
var inst_33621 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_33618__$1,(1),null);
var inst_33622 = (inst_33620__$1 == null);
var state_33649__$1 = (function (){var statearr_33656 = state_33649;
(statearr_33656[(7)] = inst_33618__$1);

(statearr_33656[(8)] = inst_33620__$1);

(statearr_33656[(9)] = inst_33621);

return statearr_33656;
})();
if(cljs.core.truth_(inst_33622)){
var statearr_33657_35196 = state_33649__$1;
(statearr_33657_35196[(1)] = (8));

} else {
var statearr_33658_35197 = state_33649__$1;
(statearr_33658_35197[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33650 === (1))){
var inst_33601 = cljs.core.vec(chs);
var inst_33603 = inst_33601;
var state_33649__$1 = (function (){var statearr_33660 = state_33649;
(statearr_33660[(10)] = inst_33603);

return statearr_33660;
})();
var statearr_33661_35198 = state_33649__$1;
(statearr_33661_35198[(2)] = null);

(statearr_33661_35198[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33650 === (4))){
var inst_33603 = (state_33649[(10)]);
var state_33649__$1 = state_33649;
return cljs.core.async.ioc_alts_BANG_(state_33649__$1,(7),inst_33603);
} else {
if((state_val_33650 === (6))){
var inst_33645 = (state_33649[(2)]);
var state_33649__$1 = state_33649;
var statearr_33666_35200 = state_33649__$1;
(statearr_33666_35200[(2)] = inst_33645);

(statearr_33666_35200[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33650 === (3))){
var inst_33647 = (state_33649[(2)]);
var state_33649__$1 = state_33649;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33649__$1,inst_33647);
} else {
if((state_val_33650 === (2))){
var inst_33603 = (state_33649[(10)]);
var inst_33607 = cljs.core.count(inst_33603);
var inst_33608 = (inst_33607 > (0));
var state_33649__$1 = state_33649;
if(cljs.core.truth_(inst_33608)){
var statearr_33670_35204 = state_33649__$1;
(statearr_33670_35204[(1)] = (4));

} else {
var statearr_33671_35205 = state_33649__$1;
(statearr_33671_35205[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33650 === (11))){
var inst_33603 = (state_33649[(10)]);
var inst_33636 = (state_33649[(2)]);
var tmp33668 = inst_33603;
var inst_33603__$1 = tmp33668;
var state_33649__$1 = (function (){var statearr_33675 = state_33649;
(statearr_33675[(10)] = inst_33603__$1);

(statearr_33675[(11)] = inst_33636);

return statearr_33675;
})();
var statearr_33678_35206 = state_33649__$1;
(statearr_33678_35206[(2)] = null);

(statearr_33678_35206[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33650 === (9))){
var inst_33620 = (state_33649[(8)]);
var state_33649__$1 = state_33649;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33649__$1,(11),out,inst_33620);
} else {
if((state_val_33650 === (5))){
var inst_33641 = cljs.core.async.close_BANG_(out);
var state_33649__$1 = state_33649;
var statearr_33686_35208 = state_33649__$1;
(statearr_33686_35208[(2)] = inst_33641);

(statearr_33686_35208[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33650 === (10))){
var inst_33639 = (state_33649[(2)]);
var state_33649__$1 = state_33649;
var statearr_33689_35210 = state_33649__$1;
(statearr_33689_35210[(2)] = inst_33639);

(statearr_33689_35210[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33650 === (8))){
var inst_33603 = (state_33649[(10)]);
var inst_33618 = (state_33649[(7)]);
var inst_33620 = (state_33649[(8)]);
var inst_33621 = (state_33649[(9)]);
var inst_33625 = (function (){var cs = inst_33603;
var vec__33613 = inst_33618;
var v = inst_33620;
var c = inst_33621;
return (function (p1__33587_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__33587_SHARP_);
});
})();
var inst_33632 = cljs.core.filterv(inst_33625,inst_33603);
var inst_33603__$1 = inst_33632;
var state_33649__$1 = (function (){var statearr_33691 = state_33649;
(statearr_33691[(10)] = inst_33603__$1);

return statearr_33691;
})();
var statearr_33692_35212 = state_33649__$1;
(statearr_33692_35212[(2)] = null);

(statearr_33692_35212[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_33697 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33697[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_33697[(1)] = (1));

return statearr_33697;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_33649){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_33649);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e33698){var ex__30627__auto__ = e33698;
var statearr_33699_35219 = state_33649;
(statearr_33699_35219[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_33649[(4)]))){
var statearr_33700_35220 = state_33649;
(statearr_33700_35220[(1)] = cljs.core.first((state_33649[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35227 = state_33649;
state_33649 = G__35227;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_33649){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_33649);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_33704 = f__30724__auto__();
(statearr_33704[(6)] = c__30723__auto___35192);

return statearr_33704;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__33712 = arguments.length;
switch (G__33712) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30723__auto___35237 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_33747){
var state_val_33748 = (state_33747[(1)]);
if((state_val_33748 === (7))){
var inst_33727 = (state_33747[(7)]);
var inst_33727__$1 = (state_33747[(2)]);
var inst_33728 = (inst_33727__$1 == null);
var inst_33729 = cljs.core.not(inst_33728);
var state_33747__$1 = (function (){var statearr_33751 = state_33747;
(statearr_33751[(7)] = inst_33727__$1);

return statearr_33751;
})();
if(inst_33729){
var statearr_33752_35239 = state_33747__$1;
(statearr_33752_35239[(1)] = (8));

} else {
var statearr_33754_35240 = state_33747__$1;
(statearr_33754_35240[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33748 === (1))){
var inst_33721 = (0);
var state_33747__$1 = (function (){var statearr_33755 = state_33747;
(statearr_33755[(8)] = inst_33721);

return statearr_33755;
})();
var statearr_33756_35241 = state_33747__$1;
(statearr_33756_35241[(2)] = null);

(statearr_33756_35241[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33748 === (4))){
var state_33747__$1 = state_33747;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33747__$1,(7),ch);
} else {
if((state_val_33748 === (6))){
var inst_33740 = (state_33747[(2)]);
var state_33747__$1 = state_33747;
var statearr_33759_35242 = state_33747__$1;
(statearr_33759_35242[(2)] = inst_33740);

(statearr_33759_35242[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33748 === (3))){
var inst_33742 = (state_33747[(2)]);
var inst_33743 = cljs.core.async.close_BANG_(out);
var state_33747__$1 = (function (){var statearr_33765 = state_33747;
(statearr_33765[(9)] = inst_33742);

return statearr_33765;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_33747__$1,inst_33743);
} else {
if((state_val_33748 === (2))){
var inst_33721 = (state_33747[(8)]);
var inst_33724 = (inst_33721 < n);
var state_33747__$1 = state_33747;
if(cljs.core.truth_(inst_33724)){
var statearr_33766_35243 = state_33747__$1;
(statearr_33766_35243[(1)] = (4));

} else {
var statearr_33767_35244 = state_33747__$1;
(statearr_33767_35244[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33748 === (11))){
var inst_33721 = (state_33747[(8)]);
var inst_33732 = (state_33747[(2)]);
var inst_33733 = (inst_33721 + (1));
var inst_33721__$1 = inst_33733;
var state_33747__$1 = (function (){var statearr_33769 = state_33747;
(statearr_33769[(8)] = inst_33721__$1);

(statearr_33769[(10)] = inst_33732);

return statearr_33769;
})();
var statearr_33770_35245 = state_33747__$1;
(statearr_33770_35245[(2)] = null);

(statearr_33770_35245[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33748 === (9))){
var state_33747__$1 = state_33747;
var statearr_33771_35246 = state_33747__$1;
(statearr_33771_35246[(2)] = null);

(statearr_33771_35246[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33748 === (5))){
var state_33747__$1 = state_33747;
var statearr_33775_35253 = state_33747__$1;
(statearr_33775_35253[(2)] = null);

(statearr_33775_35253[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33748 === (10))){
var inst_33737 = (state_33747[(2)]);
var state_33747__$1 = state_33747;
var statearr_33780_35254 = state_33747__$1;
(statearr_33780_35254[(2)] = inst_33737);

(statearr_33780_35254[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33748 === (8))){
var inst_33727 = (state_33747[(7)]);
var state_33747__$1 = state_33747;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33747__$1,(11),out,inst_33727);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_33781 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33781[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_33781[(1)] = (1));

return statearr_33781;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_33747){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_33747);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e33782){var ex__30627__auto__ = e33782;
var statearr_33783_35256 = state_33747;
(statearr_33783_35256[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_33747[(4)]))){
var statearr_33786_35257 = state_33747;
(statearr_33786_35257[(1)] = cljs.core.first((state_33747[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35258 = state_33747;
state_33747 = G__35258;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_33747){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_33747);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_33789 = f__30724__auto__();
(statearr_33789[(6)] = c__30723__auto___35237);

return statearr_33789;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33792 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33792 = (function (f,ch,meta33793){
this.f = f;
this.ch = ch;
this.meta33793 = meta33793;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33794,meta33793__$1){
var self__ = this;
var _33794__$1 = this;
return (new cljs.core.async.t_cljs$core$async33792(self__.f,self__.ch,meta33793__$1));
}));

(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33794){
var self__ = this;
var _33794__$1 = this;
return self__.meta33793;
}));

(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33798 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33798 = (function (f,ch,meta33793,_,fn1,meta33799){
this.f = f;
this.ch = ch;
this.meta33793 = meta33793;
this._ = _;
this.fn1 = fn1;
this.meta33799 = meta33799;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33798.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33800,meta33799__$1){
var self__ = this;
var _33800__$1 = this;
return (new cljs.core.async.t_cljs$core$async33798(self__.f,self__.ch,self__.meta33793,self__._,self__.fn1,meta33799__$1));
}));

(cljs.core.async.t_cljs$core$async33798.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33800){
var self__ = this;
var _33800__$1 = this;
return self__.meta33799;
}));

(cljs.core.async.t_cljs$core$async33798.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33798.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async33798.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async33798.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__33791_SHARP_){
var G__33808 = (((p1__33791_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__33791_SHARP_) : self__.f.call(null,p1__33791_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__33808) : f1.call(null,G__33808));
});
}));

(cljs.core.async.t_cljs$core$async33798.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33793","meta33793",-889932596,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async33792","cljs.core.async/t_cljs$core$async33792",624119752,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta33799","meta33799",-970235967,null)], null);
}));

(cljs.core.async.t_cljs$core$async33798.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33798.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33798");

(cljs.core.async.t_cljs$core$async33798.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33798");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33798.
 */
cljs.core.async.__GT_t_cljs$core$async33798 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async33798(f__$1,ch__$1,meta33793__$1,___$2,fn1__$1,meta33799){
return (new cljs.core.async.t_cljs$core$async33798(f__$1,ch__$1,meta33793__$1,___$2,fn1__$1,meta33799));
});

}

return (new cljs.core.async.t_cljs$core$async33798(self__.f,self__.ch,self__.meta33793,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4115__auto__ = ret;
if(cljs.core.truth_(and__4115__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4115__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__33814 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__33814) : self__.f.call(null,G__33814));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33792.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async33792.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33793","meta33793",-889932596,null)], null);
}));

(cljs.core.async.t_cljs$core$async33792.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33792.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33792");

(cljs.core.async.t_cljs$core$async33792.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33792");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33792.
 */
cljs.core.async.__GT_t_cljs$core$async33792 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async33792(f__$1,ch__$1,meta33793){
return (new cljs.core.async.t_cljs$core$async33792(f__$1,ch__$1,meta33793));
});

}

return (new cljs.core.async.t_cljs$core$async33792(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33821 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33821 = (function (f,ch,meta33822){
this.f = f;
this.ch = ch;
this.meta33822 = meta33822;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33821.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33823,meta33822__$1){
var self__ = this;
var _33823__$1 = this;
return (new cljs.core.async.t_cljs$core$async33821(self__.f,self__.ch,meta33822__$1));
}));

(cljs.core.async.t_cljs$core$async33821.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33823){
var self__ = this;
var _33823__$1 = this;
return self__.meta33822;
}));

(cljs.core.async.t_cljs$core$async33821.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33821.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33821.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33821.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async33821.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33821.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async33821.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33822","meta33822",334043475,null)], null);
}));

(cljs.core.async.t_cljs$core$async33821.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33821.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33821");

(cljs.core.async.t_cljs$core$async33821.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33821");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33821.
 */
cljs.core.async.__GT_t_cljs$core$async33821 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async33821(f__$1,ch__$1,meta33822){
return (new cljs.core.async.t_cljs$core$async33821(f__$1,ch__$1,meta33822));
});

}

return (new cljs.core.async.t_cljs$core$async33821(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async33834 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33834 = (function (p,ch,meta33835){
this.p = p;
this.ch = ch;
this.meta33835 = meta33835;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33836,meta33835__$1){
var self__ = this;
var _33836__$1 = this;
return (new cljs.core.async.t_cljs$core$async33834(self__.p,self__.ch,meta33835__$1));
}));

(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33836){
var self__ = this;
var _33836__$1 = this;
return self__.meta33835;
}));

(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33834.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async33834.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33835","meta33835",-1084869707,null)], null);
}));

(cljs.core.async.t_cljs$core$async33834.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33834.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33834");

(cljs.core.async.t_cljs$core$async33834.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async33834");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33834.
 */
cljs.core.async.__GT_t_cljs$core$async33834 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async33834(p__$1,ch__$1,meta33835){
return (new cljs.core.async.t_cljs$core$async33834(p__$1,ch__$1,meta33835));
});

}

return (new cljs.core.async.t_cljs$core$async33834(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__33846 = arguments.length;
switch (G__33846) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30723__auto___35279 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_33870){
var state_val_33871 = (state_33870[(1)]);
if((state_val_33871 === (7))){
var inst_33866 = (state_33870[(2)]);
var state_33870__$1 = state_33870;
var statearr_33873_35282 = state_33870__$1;
(statearr_33873_35282[(2)] = inst_33866);

(statearr_33873_35282[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33871 === (1))){
var state_33870__$1 = state_33870;
var statearr_33875_35283 = state_33870__$1;
(statearr_33875_35283[(2)] = null);

(statearr_33875_35283[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33871 === (4))){
var inst_33852 = (state_33870[(7)]);
var inst_33852__$1 = (state_33870[(2)]);
var inst_33853 = (inst_33852__$1 == null);
var state_33870__$1 = (function (){var statearr_33878 = state_33870;
(statearr_33878[(7)] = inst_33852__$1);

return statearr_33878;
})();
if(cljs.core.truth_(inst_33853)){
var statearr_33879_35284 = state_33870__$1;
(statearr_33879_35284[(1)] = (5));

} else {
var statearr_33880_35285 = state_33870__$1;
(statearr_33880_35285[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33871 === (6))){
var inst_33852 = (state_33870[(7)]);
var inst_33857 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_33852) : p.call(null,inst_33852));
var state_33870__$1 = state_33870;
if(cljs.core.truth_(inst_33857)){
var statearr_33881_35286 = state_33870__$1;
(statearr_33881_35286[(1)] = (8));

} else {
var statearr_33882_35287 = state_33870__$1;
(statearr_33882_35287[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33871 === (3))){
var inst_33868 = (state_33870[(2)]);
var state_33870__$1 = state_33870;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33870__$1,inst_33868);
} else {
if((state_val_33871 === (2))){
var state_33870__$1 = state_33870;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33870__$1,(4),ch);
} else {
if((state_val_33871 === (11))){
var inst_33860 = (state_33870[(2)]);
var state_33870__$1 = state_33870;
var statearr_33886_35289 = state_33870__$1;
(statearr_33886_35289[(2)] = inst_33860);

(statearr_33886_35289[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33871 === (9))){
var state_33870__$1 = state_33870;
var statearr_33887_35290 = state_33870__$1;
(statearr_33887_35290[(2)] = null);

(statearr_33887_35290[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33871 === (5))){
var inst_33855 = cljs.core.async.close_BANG_(out);
var state_33870__$1 = state_33870;
var statearr_33888_35291 = state_33870__$1;
(statearr_33888_35291[(2)] = inst_33855);

(statearr_33888_35291[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33871 === (10))){
var inst_33863 = (state_33870[(2)]);
var state_33870__$1 = (function (){var statearr_33889 = state_33870;
(statearr_33889[(8)] = inst_33863);

return statearr_33889;
})();
var statearr_33890_35298 = state_33870__$1;
(statearr_33890_35298[(2)] = null);

(statearr_33890_35298[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33871 === (8))){
var inst_33852 = (state_33870[(7)]);
var state_33870__$1 = state_33870;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33870__$1,(11),out,inst_33852);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_33891 = [null,null,null,null,null,null,null,null,null];
(statearr_33891[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_33891[(1)] = (1));

return statearr_33891;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_33870){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_33870);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e33892){var ex__30627__auto__ = e33892;
var statearr_33893_35301 = state_33870;
(statearr_33893_35301[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_33870[(4)]))){
var statearr_33894_35303 = state_33870;
(statearr_33894_35303[(1)] = cljs.core.first((state_33870[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35305 = state_33870;
state_33870 = G__35305;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_33870){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_33870);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_33895 = f__30724__auto__();
(statearr_33895[(6)] = c__30723__auto___35279);

return statearr_33895;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__33898 = arguments.length;
switch (G__33898) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__30723__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_33967){
var state_val_33968 = (state_33967[(1)]);
if((state_val_33968 === (7))){
var inst_33959 = (state_33967[(2)]);
var state_33967__$1 = state_33967;
var statearr_33978_35313 = state_33967__$1;
(statearr_33978_35313[(2)] = inst_33959);

(statearr_33978_35313[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (20))){
var inst_33926 = (state_33967[(7)]);
var inst_33937 = (state_33967[(2)]);
var inst_33938 = cljs.core.next(inst_33926);
var inst_33912 = inst_33938;
var inst_33913 = null;
var inst_33914 = (0);
var inst_33915 = (0);
var state_33967__$1 = (function (){var statearr_33982 = state_33967;
(statearr_33982[(8)] = inst_33913);

(statearr_33982[(9)] = inst_33937);

(statearr_33982[(10)] = inst_33914);

(statearr_33982[(11)] = inst_33915);

(statearr_33982[(12)] = inst_33912);

return statearr_33982;
})();
var statearr_33983_35314 = state_33967__$1;
(statearr_33983_35314[(2)] = null);

(statearr_33983_35314[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (1))){
var state_33967__$1 = state_33967;
var statearr_33990_35315 = state_33967__$1;
(statearr_33990_35315[(2)] = null);

(statearr_33990_35315[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (4))){
var inst_33901 = (state_33967[(13)]);
var inst_33901__$1 = (state_33967[(2)]);
var inst_33902 = (inst_33901__$1 == null);
var state_33967__$1 = (function (){var statearr_33995 = state_33967;
(statearr_33995[(13)] = inst_33901__$1);

return statearr_33995;
})();
if(cljs.core.truth_(inst_33902)){
var statearr_33997_35316 = state_33967__$1;
(statearr_33997_35316[(1)] = (5));

} else {
var statearr_33998_35317 = state_33967__$1;
(statearr_33998_35317[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (15))){
var state_33967__$1 = state_33967;
var statearr_34006_35318 = state_33967__$1;
(statearr_34006_35318[(2)] = null);

(statearr_34006_35318[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (21))){
var state_33967__$1 = state_33967;
var statearr_34010_35319 = state_33967__$1;
(statearr_34010_35319[(2)] = null);

(statearr_34010_35319[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (13))){
var inst_33913 = (state_33967[(8)]);
var inst_33914 = (state_33967[(10)]);
var inst_33915 = (state_33967[(11)]);
var inst_33912 = (state_33967[(12)]);
var inst_33922 = (state_33967[(2)]);
var inst_33923 = (inst_33915 + (1));
var tmp34001 = inst_33913;
var tmp34002 = inst_33914;
var tmp34003 = inst_33912;
var inst_33912__$1 = tmp34003;
var inst_33913__$1 = tmp34001;
var inst_33914__$1 = tmp34002;
var inst_33915__$1 = inst_33923;
var state_33967__$1 = (function (){var statearr_34017 = state_33967;
(statearr_34017[(8)] = inst_33913__$1);

(statearr_34017[(10)] = inst_33914__$1);

(statearr_34017[(14)] = inst_33922);

(statearr_34017[(11)] = inst_33915__$1);

(statearr_34017[(12)] = inst_33912__$1);

return statearr_34017;
})();
var statearr_34018_35321 = state_33967__$1;
(statearr_34018_35321[(2)] = null);

(statearr_34018_35321[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (22))){
var state_33967__$1 = state_33967;
var statearr_34020_35322 = state_33967__$1;
(statearr_34020_35322[(2)] = null);

(statearr_34020_35322[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (6))){
var inst_33901 = (state_33967[(13)]);
var inst_33910 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_33901) : f.call(null,inst_33901));
var inst_33911 = cljs.core.seq(inst_33910);
var inst_33912 = inst_33911;
var inst_33913 = null;
var inst_33914 = (0);
var inst_33915 = (0);
var state_33967__$1 = (function (){var statearr_34023 = state_33967;
(statearr_34023[(8)] = inst_33913);

(statearr_34023[(10)] = inst_33914);

(statearr_34023[(11)] = inst_33915);

(statearr_34023[(12)] = inst_33912);

return statearr_34023;
})();
var statearr_34024_35324 = state_33967__$1;
(statearr_34024_35324[(2)] = null);

(statearr_34024_35324[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (17))){
var inst_33926 = (state_33967[(7)]);
var inst_33930 = cljs.core.chunk_first(inst_33926);
var inst_33931 = cljs.core.chunk_rest(inst_33926);
var inst_33932 = cljs.core.count(inst_33930);
var inst_33912 = inst_33931;
var inst_33913 = inst_33930;
var inst_33914 = inst_33932;
var inst_33915 = (0);
var state_33967__$1 = (function (){var statearr_34026 = state_33967;
(statearr_34026[(8)] = inst_33913);

(statearr_34026[(10)] = inst_33914);

(statearr_34026[(11)] = inst_33915);

(statearr_34026[(12)] = inst_33912);

return statearr_34026;
})();
var statearr_34031_35326 = state_33967__$1;
(statearr_34031_35326[(2)] = null);

(statearr_34031_35326[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (3))){
var inst_33961 = (state_33967[(2)]);
var state_33967__$1 = state_33967;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33967__$1,inst_33961);
} else {
if((state_val_33968 === (12))){
var inst_33946 = (state_33967[(2)]);
var state_33967__$1 = state_33967;
var statearr_34033_35328 = state_33967__$1;
(statearr_34033_35328[(2)] = inst_33946);

(statearr_34033_35328[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (2))){
var state_33967__$1 = state_33967;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33967__$1,(4),in$);
} else {
if((state_val_33968 === (23))){
var inst_33956 = (state_33967[(2)]);
var state_33967__$1 = state_33967;
var statearr_34044_35329 = state_33967__$1;
(statearr_34044_35329[(2)] = inst_33956);

(statearr_34044_35329[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (19))){
var inst_33941 = (state_33967[(2)]);
var state_33967__$1 = state_33967;
var statearr_34047_35330 = state_33967__$1;
(statearr_34047_35330[(2)] = inst_33941);

(statearr_34047_35330[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (11))){
var inst_33926 = (state_33967[(7)]);
var inst_33912 = (state_33967[(12)]);
var inst_33926__$1 = cljs.core.seq(inst_33912);
var state_33967__$1 = (function (){var statearr_34049 = state_33967;
(statearr_34049[(7)] = inst_33926__$1);

return statearr_34049;
})();
if(inst_33926__$1){
var statearr_34050_35331 = state_33967__$1;
(statearr_34050_35331[(1)] = (14));

} else {
var statearr_34052_35332 = state_33967__$1;
(statearr_34052_35332[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (9))){
var inst_33950 = (state_33967[(2)]);
var inst_33951 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_33967__$1 = (function (){var statearr_34060 = state_33967;
(statearr_34060[(15)] = inst_33950);

return statearr_34060;
})();
if(cljs.core.truth_(inst_33951)){
var statearr_34062_35333 = state_33967__$1;
(statearr_34062_35333[(1)] = (21));

} else {
var statearr_34064_35334 = state_33967__$1;
(statearr_34064_35334[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (5))){
var inst_33904 = cljs.core.async.close_BANG_(out);
var state_33967__$1 = state_33967;
var statearr_34069_35335 = state_33967__$1;
(statearr_34069_35335[(2)] = inst_33904);

(statearr_34069_35335[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (14))){
var inst_33926 = (state_33967[(7)]);
var inst_33928 = cljs.core.chunked_seq_QMARK_(inst_33926);
var state_33967__$1 = state_33967;
if(inst_33928){
var statearr_34073_35339 = state_33967__$1;
(statearr_34073_35339[(1)] = (17));

} else {
var statearr_34074_35340 = state_33967__$1;
(statearr_34074_35340[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (16))){
var inst_33944 = (state_33967[(2)]);
var state_33967__$1 = state_33967;
var statearr_34075_35341 = state_33967__$1;
(statearr_34075_35341[(2)] = inst_33944);

(statearr_34075_35341[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33968 === (10))){
var inst_33913 = (state_33967[(8)]);
var inst_33915 = (state_33967[(11)]);
var inst_33920 = cljs.core._nth(inst_33913,inst_33915);
var state_33967__$1 = state_33967;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33967__$1,(13),out,inst_33920);
} else {
if((state_val_33968 === (18))){
var inst_33926 = (state_33967[(7)]);
var inst_33935 = cljs.core.first(inst_33926);
var state_33967__$1 = state_33967;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33967__$1,(20),out,inst_33935);
} else {
if((state_val_33968 === (8))){
var inst_33914 = (state_33967[(10)]);
var inst_33915 = (state_33967[(11)]);
var inst_33917 = (inst_33915 < inst_33914);
var inst_33918 = inst_33917;
var state_33967__$1 = state_33967;
if(cljs.core.truth_(inst_33918)){
var statearr_34086_35344 = state_33967__$1;
(statearr_34086_35344[(1)] = (10));

} else {
var statearr_34087_35345 = state_33967__$1;
(statearr_34087_35345[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__30624__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__30624__auto____0 = (function (){
var statearr_34092 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34092[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__30624__auto__);

(statearr_34092[(1)] = (1));

return statearr_34092;
});
var cljs$core$async$mapcat_STAR__$_state_machine__30624__auto____1 = (function (state_33967){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_33967);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e34093){var ex__30627__auto__ = e34093;
var statearr_34094_35348 = state_33967;
(statearr_34094_35348[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_33967[(4)]))){
var statearr_34095_35349 = state_33967;
(statearr_34095_35349[(1)] = cljs.core.first((state_33967[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35351 = state_33967;
state_33967 = G__35351;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__30624__auto__ = function(state_33967){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__30624__auto____1.call(this,state_33967);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__30624__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__30624__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_34100 = f__30724__auto__();
(statearr_34100[(6)] = c__30723__auto__);

return statearr_34100;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));

return c__30723__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__34105 = arguments.length;
switch (G__34105) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__34117 = arguments.length;
switch (G__34117) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__34127 = arguments.length;
switch (G__34127) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30723__auto___35357 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_34173){
var state_val_34174 = (state_34173[(1)]);
if((state_val_34174 === (7))){
var inst_34168 = (state_34173[(2)]);
var state_34173__$1 = state_34173;
var statearr_34175_35359 = state_34173__$1;
(statearr_34175_35359[(2)] = inst_34168);

(statearr_34175_35359[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34174 === (1))){
var inst_34145 = null;
var state_34173__$1 = (function (){var statearr_34176 = state_34173;
(statearr_34176[(7)] = inst_34145);

return statearr_34176;
})();
var statearr_34178_35367 = state_34173__$1;
(statearr_34178_35367[(2)] = null);

(statearr_34178_35367[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34174 === (4))){
var inst_34151 = (state_34173[(8)]);
var inst_34151__$1 = (state_34173[(2)]);
var inst_34153 = (inst_34151__$1 == null);
var inst_34154 = cljs.core.not(inst_34153);
var state_34173__$1 = (function (){var statearr_34180 = state_34173;
(statearr_34180[(8)] = inst_34151__$1);

return statearr_34180;
})();
if(inst_34154){
var statearr_34181_35373 = state_34173__$1;
(statearr_34181_35373[(1)] = (5));

} else {
var statearr_34182_35374 = state_34173__$1;
(statearr_34182_35374[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34174 === (6))){
var state_34173__$1 = state_34173;
var statearr_34183_35375 = state_34173__$1;
(statearr_34183_35375[(2)] = null);

(statearr_34183_35375[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34174 === (3))){
var inst_34170 = (state_34173[(2)]);
var inst_34171 = cljs.core.async.close_BANG_(out);
var state_34173__$1 = (function (){var statearr_34184 = state_34173;
(statearr_34184[(9)] = inst_34170);

return statearr_34184;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_34173__$1,inst_34171);
} else {
if((state_val_34174 === (2))){
var state_34173__$1 = state_34173;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34173__$1,(4),ch);
} else {
if((state_val_34174 === (11))){
var inst_34151 = (state_34173[(8)]);
var inst_34162 = (state_34173[(2)]);
var inst_34145 = inst_34151;
var state_34173__$1 = (function (){var statearr_34185 = state_34173;
(statearr_34185[(7)] = inst_34145);

(statearr_34185[(10)] = inst_34162);

return statearr_34185;
})();
var statearr_34186_35384 = state_34173__$1;
(statearr_34186_35384[(2)] = null);

(statearr_34186_35384[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34174 === (9))){
var inst_34151 = (state_34173[(8)]);
var state_34173__$1 = state_34173;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34173__$1,(11),out,inst_34151);
} else {
if((state_val_34174 === (5))){
var inst_34145 = (state_34173[(7)]);
var inst_34151 = (state_34173[(8)]);
var inst_34156 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_34151,inst_34145);
var state_34173__$1 = state_34173;
if(inst_34156){
var statearr_34190_35385 = state_34173__$1;
(statearr_34190_35385[(1)] = (8));

} else {
var statearr_34191_35386 = state_34173__$1;
(statearr_34191_35386[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34174 === (10))){
var inst_34165 = (state_34173[(2)]);
var state_34173__$1 = state_34173;
var statearr_34192_35387 = state_34173__$1;
(statearr_34192_35387[(2)] = inst_34165);

(statearr_34192_35387[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34174 === (8))){
var inst_34145 = (state_34173[(7)]);
var tmp34189 = inst_34145;
var inst_34145__$1 = tmp34189;
var state_34173__$1 = (function (){var statearr_34195 = state_34173;
(statearr_34195[(7)] = inst_34145__$1);

return statearr_34195;
})();
var statearr_34196_35390 = state_34173__$1;
(statearr_34196_35390[(2)] = null);

(statearr_34196_35390[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_34197 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_34197[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_34197[(1)] = (1));

return statearr_34197;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_34173){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_34173);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e34198){var ex__30627__auto__ = e34198;
var statearr_34199_35397 = state_34173;
(statearr_34199_35397[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_34173[(4)]))){
var statearr_34200_35398 = state_34173;
(statearr_34200_35398[(1)] = cljs.core.first((state_34173[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35403 = state_34173;
state_34173 = G__35403;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_34173){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_34173);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_34201 = f__30724__auto__();
(statearr_34201[(6)] = c__30723__auto___35357);

return statearr_34201;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__34203 = arguments.length;
switch (G__34203) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30723__auto___35413 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_34241){
var state_val_34242 = (state_34241[(1)]);
if((state_val_34242 === (7))){
var inst_34237 = (state_34241[(2)]);
var state_34241__$1 = state_34241;
var statearr_34245_35414 = state_34241__$1;
(statearr_34245_35414[(2)] = inst_34237);

(statearr_34245_35414[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (1))){
var inst_34204 = (new Array(n));
var inst_34205 = inst_34204;
var inst_34206 = (0);
var state_34241__$1 = (function (){var statearr_34246 = state_34241;
(statearr_34246[(7)] = inst_34205);

(statearr_34246[(8)] = inst_34206);

return statearr_34246;
})();
var statearr_34247_35420 = state_34241__$1;
(statearr_34247_35420[(2)] = null);

(statearr_34247_35420[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (4))){
var inst_34209 = (state_34241[(9)]);
var inst_34209__$1 = (state_34241[(2)]);
var inst_34210 = (inst_34209__$1 == null);
var inst_34211 = cljs.core.not(inst_34210);
var state_34241__$1 = (function (){var statearr_34248 = state_34241;
(statearr_34248[(9)] = inst_34209__$1);

return statearr_34248;
})();
if(inst_34211){
var statearr_34249_35425 = state_34241__$1;
(statearr_34249_35425[(1)] = (5));

} else {
var statearr_34250_35426 = state_34241__$1;
(statearr_34250_35426[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (15))){
var inst_34231 = (state_34241[(2)]);
var state_34241__$1 = state_34241;
var statearr_34251_35427 = state_34241__$1;
(statearr_34251_35427[(2)] = inst_34231);

(statearr_34251_35427[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (13))){
var state_34241__$1 = state_34241;
var statearr_34252_35432 = state_34241__$1;
(statearr_34252_35432[(2)] = null);

(statearr_34252_35432[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (6))){
var inst_34206 = (state_34241[(8)]);
var inst_34227 = (inst_34206 > (0));
var state_34241__$1 = state_34241;
if(cljs.core.truth_(inst_34227)){
var statearr_34253_35435 = state_34241__$1;
(statearr_34253_35435[(1)] = (12));

} else {
var statearr_34254_35437 = state_34241__$1;
(statearr_34254_35437[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (3))){
var inst_34239 = (state_34241[(2)]);
var state_34241__$1 = state_34241;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34241__$1,inst_34239);
} else {
if((state_val_34242 === (12))){
var inst_34205 = (state_34241[(7)]);
var inst_34229 = cljs.core.vec(inst_34205);
var state_34241__$1 = state_34241;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34241__$1,(15),out,inst_34229);
} else {
if((state_val_34242 === (2))){
var state_34241__$1 = state_34241;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34241__$1,(4),ch);
} else {
if((state_val_34242 === (11))){
var inst_34221 = (state_34241[(2)]);
var inst_34222 = (new Array(n));
var inst_34205 = inst_34222;
var inst_34206 = (0);
var state_34241__$1 = (function (){var statearr_34255 = state_34241;
(statearr_34255[(10)] = inst_34221);

(statearr_34255[(7)] = inst_34205);

(statearr_34255[(8)] = inst_34206);

return statearr_34255;
})();
var statearr_34256_35443 = state_34241__$1;
(statearr_34256_35443[(2)] = null);

(statearr_34256_35443[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (9))){
var inst_34205 = (state_34241[(7)]);
var inst_34219 = cljs.core.vec(inst_34205);
var state_34241__$1 = state_34241;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34241__$1,(11),out,inst_34219);
} else {
if((state_val_34242 === (5))){
var inst_34205 = (state_34241[(7)]);
var inst_34206 = (state_34241[(8)]);
var inst_34209 = (state_34241[(9)]);
var inst_34214 = (state_34241[(11)]);
var inst_34213 = (inst_34205[inst_34206] = inst_34209);
var inst_34214__$1 = (inst_34206 + (1));
var inst_34215 = (inst_34214__$1 < n);
var state_34241__$1 = (function (){var statearr_34260 = state_34241;
(statearr_34260[(12)] = inst_34213);

(statearr_34260[(11)] = inst_34214__$1);

return statearr_34260;
})();
if(cljs.core.truth_(inst_34215)){
var statearr_34261_35449 = state_34241__$1;
(statearr_34261_35449[(1)] = (8));

} else {
var statearr_34262_35451 = state_34241__$1;
(statearr_34262_35451[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (14))){
var inst_34234 = (state_34241[(2)]);
var inst_34235 = cljs.core.async.close_BANG_(out);
var state_34241__$1 = (function (){var statearr_34264 = state_34241;
(statearr_34264[(13)] = inst_34234);

return statearr_34264;
})();
var statearr_34266_35453 = state_34241__$1;
(statearr_34266_35453[(2)] = inst_34235);

(statearr_34266_35453[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (10))){
var inst_34225 = (state_34241[(2)]);
var state_34241__$1 = state_34241;
var statearr_34267_35454 = state_34241__$1;
(statearr_34267_35454[(2)] = inst_34225);

(statearr_34267_35454[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34242 === (8))){
var inst_34205 = (state_34241[(7)]);
var inst_34214 = (state_34241[(11)]);
var tmp34263 = inst_34205;
var inst_34205__$1 = tmp34263;
var inst_34206 = inst_34214;
var state_34241__$1 = (function (){var statearr_34268 = state_34241;
(statearr_34268[(7)] = inst_34205__$1);

(statearr_34268[(8)] = inst_34206);

return statearr_34268;
})();
var statearr_34269_35456 = state_34241__$1;
(statearr_34269_35456[(2)] = null);

(statearr_34269_35456[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_34270 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34270[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_34270[(1)] = (1));

return statearr_34270;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_34241){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_34241);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e34273){var ex__30627__auto__ = e34273;
var statearr_34274_35462 = state_34241;
(statearr_34274_35462[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_34241[(4)]))){
var statearr_34275_35464 = state_34241;
(statearr_34275_35464[(1)] = cljs.core.first((state_34241[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35465 = state_34241;
state_34241 = G__35465;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_34241){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_34241);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_34276 = f__30724__auto__();
(statearr_34276[(6)] = c__30723__auto___35413);

return statearr_34276;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__34278 = arguments.length;
switch (G__34278) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30723__auto___35494 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30724__auto__ = (function (){var switch__30623__auto__ = (function (state_34323){
var state_val_34324 = (state_34323[(1)]);
if((state_val_34324 === (7))){
var inst_34319 = (state_34323[(2)]);
var state_34323__$1 = state_34323;
var statearr_34326_35498 = state_34323__$1;
(statearr_34326_35498[(2)] = inst_34319);

(statearr_34326_35498[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (1))){
var inst_34281 = [];
var inst_34283 = inst_34281;
var inst_34284 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_34323__$1 = (function (){var statearr_34329 = state_34323;
(statearr_34329[(7)] = inst_34284);

(statearr_34329[(8)] = inst_34283);

return statearr_34329;
})();
var statearr_34330_35502 = state_34323__$1;
(statearr_34330_35502[(2)] = null);

(statearr_34330_35502[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (4))){
var inst_34287 = (state_34323[(9)]);
var inst_34287__$1 = (state_34323[(2)]);
var inst_34288 = (inst_34287__$1 == null);
var inst_34289 = cljs.core.not(inst_34288);
var state_34323__$1 = (function (){var statearr_34331 = state_34323;
(statearr_34331[(9)] = inst_34287__$1);

return statearr_34331;
})();
if(inst_34289){
var statearr_34332_35503 = state_34323__$1;
(statearr_34332_35503[(1)] = (5));

} else {
var statearr_34333_35504 = state_34323__$1;
(statearr_34333_35504[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (15))){
var inst_34313 = (state_34323[(2)]);
var state_34323__$1 = state_34323;
var statearr_34334_35505 = state_34323__$1;
(statearr_34334_35505[(2)] = inst_34313);

(statearr_34334_35505[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (13))){
var state_34323__$1 = state_34323;
var statearr_34336_35507 = state_34323__$1;
(statearr_34336_35507[(2)] = null);

(statearr_34336_35507[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (6))){
var inst_34283 = (state_34323[(8)]);
var inst_34308 = inst_34283.length;
var inst_34309 = (inst_34308 > (0));
var state_34323__$1 = state_34323;
if(cljs.core.truth_(inst_34309)){
var statearr_34338_35508 = state_34323__$1;
(statearr_34338_35508[(1)] = (12));

} else {
var statearr_34339_35509 = state_34323__$1;
(statearr_34339_35509[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (3))){
var inst_34321 = (state_34323[(2)]);
var state_34323__$1 = state_34323;
return cljs.core.async.impl.ioc_helpers.return_chan(state_34323__$1,inst_34321);
} else {
if((state_val_34324 === (12))){
var inst_34283 = (state_34323[(8)]);
var inst_34311 = cljs.core.vec(inst_34283);
var state_34323__$1 = state_34323;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34323__$1,(15),out,inst_34311);
} else {
if((state_val_34324 === (2))){
var state_34323__$1 = state_34323;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_34323__$1,(4),ch);
} else {
if((state_val_34324 === (11))){
var inst_34291 = (state_34323[(10)]);
var inst_34287 = (state_34323[(9)]);
var inst_34301 = (state_34323[(2)]);
var inst_34302 = [];
var inst_34303 = inst_34302.push(inst_34287);
var inst_34283 = inst_34302;
var inst_34284 = inst_34291;
var state_34323__$1 = (function (){var statearr_34340 = state_34323;
(statearr_34340[(11)] = inst_34301);

(statearr_34340[(7)] = inst_34284);

(statearr_34340[(8)] = inst_34283);

(statearr_34340[(12)] = inst_34303);

return statearr_34340;
})();
var statearr_34341_35518 = state_34323__$1;
(statearr_34341_35518[(2)] = null);

(statearr_34341_35518[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (9))){
var inst_34283 = (state_34323[(8)]);
var inst_34299 = cljs.core.vec(inst_34283);
var state_34323__$1 = state_34323;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_34323__$1,(11),out,inst_34299);
} else {
if((state_val_34324 === (5))){
var inst_34284 = (state_34323[(7)]);
var inst_34291 = (state_34323[(10)]);
var inst_34287 = (state_34323[(9)]);
var inst_34291__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_34287) : f.call(null,inst_34287));
var inst_34292 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_34291__$1,inst_34284);
var inst_34293 = cljs.core.keyword_identical_QMARK_(inst_34284,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_34294 = ((inst_34292) || (inst_34293));
var state_34323__$1 = (function (){var statearr_34344 = state_34323;
(statearr_34344[(10)] = inst_34291__$1);

return statearr_34344;
})();
if(cljs.core.truth_(inst_34294)){
var statearr_34345_35534 = state_34323__$1;
(statearr_34345_35534[(1)] = (8));

} else {
var statearr_34347_35536 = state_34323__$1;
(statearr_34347_35536[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (14))){
var inst_34316 = (state_34323[(2)]);
var inst_34317 = cljs.core.async.close_BANG_(out);
var state_34323__$1 = (function (){var statearr_34352 = state_34323;
(statearr_34352[(13)] = inst_34316);

return statearr_34352;
})();
var statearr_34353_35543 = state_34323__$1;
(statearr_34353_35543[(2)] = inst_34317);

(statearr_34353_35543[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (10))){
var inst_34306 = (state_34323[(2)]);
var state_34323__$1 = state_34323;
var statearr_34354_35549 = state_34323__$1;
(statearr_34354_35549[(2)] = inst_34306);

(statearr_34354_35549[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_34324 === (8))){
var inst_34283 = (state_34323[(8)]);
var inst_34291 = (state_34323[(10)]);
var inst_34287 = (state_34323[(9)]);
var inst_34296 = inst_34283.push(inst_34287);
var tmp34350 = inst_34283;
var inst_34283__$1 = tmp34350;
var inst_34284 = inst_34291;
var state_34323__$1 = (function (){var statearr_34355 = state_34323;
(statearr_34355[(7)] = inst_34284);

(statearr_34355[(8)] = inst_34283__$1);

(statearr_34355[(14)] = inst_34296);

return statearr_34355;
})();
var statearr_34356_35558 = state_34323__$1;
(statearr_34356_35558[(2)] = null);

(statearr_34356_35558[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__30624__auto__ = null;
var cljs$core$async$state_machine__30624__auto____0 = (function (){
var statearr_34360 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_34360[(0)] = cljs$core$async$state_machine__30624__auto__);

(statearr_34360[(1)] = (1));

return statearr_34360;
});
var cljs$core$async$state_machine__30624__auto____1 = (function (state_34323){
while(true){
var ret_value__30625__auto__ = (function (){try{while(true){
var result__30626__auto__ = switch__30623__auto__(state_34323);
if(cljs.core.keyword_identical_QMARK_(result__30626__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__30626__auto__;
}
break;
}
}catch (e34363){var ex__30627__auto__ = e34363;
var statearr_34364_35579 = state_34323;
(statearr_34364_35579[(2)] = ex__30627__auto__);


if(cljs.core.seq((state_34323[(4)]))){
var statearr_34365_35585 = state_34323;
(statearr_34365_35585[(1)] = cljs.core.first((state_34323[(4)])));

} else {
throw ex__30627__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__30625__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35587 = state_34323;
state_34323 = G__35587;
continue;
} else {
return ret_value__30625__auto__;
}
break;
}
});
cljs$core$async$state_machine__30624__auto__ = function(state_34323){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__30624__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__30624__auto____1.call(this,state_34323);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__30624__auto____0;
cljs$core$async$state_machine__30624__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__30624__auto____1;
return cljs$core$async$state_machine__30624__auto__;
})()
})();
var state__30725__auto__ = (function (){var statearr_34366 = f__30724__auto__();
(statearr_34366[(6)] = c__30723__auto___35494);

return statearr_34366;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30725__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
