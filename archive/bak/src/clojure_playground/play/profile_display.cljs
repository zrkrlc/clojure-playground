(ns clojure-playground.play.profile-display
  (:require [reagent.core :as r]
            [reagent.dom  :as d]))
            ;;[clojure-playground.play.profile-display.state :as state]))

(defn header []
  [:div.header
   [:h3 "Welcome to Mr. Magorium's Magic Emporium!"]])

(defn footer []
  [:div.footer
   [:h3 "I am a foot."]])

(defn input-element
  "Updates value on change." 
  [id name value]
  [:input {:id        id
           :name      name
           :class     "form-control"
           :type      type
           :required  ""
           :on-change #(reset! value (-> % .-target .-value))}])

(defn input-first-name
  [first-name]
  (input-element "first-name" "firstname" first-name))

(defn form []
  (let [first-name (atom nil)] 
    [:form  
     [:label {:for "first-name"} "First name:"]
     (input-first-name first-name)
     [:input {:type  "submit"
              :value "Release thy name!"}]
     [:p @first-name]]
   ))

