# How to run
```
npx shadow-cljs clj-repl ;; runs server with REPL
npx shadow-cljs watch app ;; runs shadow-cljs
```

# How to run (with REPL)
```
npx shadow-cljs watch app ;; yes, this must go first
npx shadow-cljs cljs-repl app 
```
