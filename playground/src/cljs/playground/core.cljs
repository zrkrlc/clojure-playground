;; How to add new pages:
;; * Grep "User" and add routes and corresponding
;;   elements accordingly
(ns playground.core
  (:require
   [reagent.core :as reagent :refer [atom]]
   [reagent.dom :as rdom]
   [reagent.session :as session]
   [re-frame.core :as rf]
   [reitit.frontend :as reitit]
   [clerk.core :as clerk]
   [accountant.core :as accountant]

   ;; JS libraries
   [inspirational-quotes :as js-quote]

   ;; User-defined components 
   [playground.toys.hello-world :refer [hello-world]]
   [playground.toys.an-image :refer [an-image]]
   [playground.toys.nameless-company :refer [nameless-company]]
   [playground.toys.tic-tac-toe :refer [tic-tac-toe]]
   [playground.toys.query-me :refer [query-me]]

   ;; Blackbelt
   [playground.blackbelt.state]
   [playground.blackbelt.table-belt.core :refer [table-belt]]
   [playground.blackbelt.cheffy-belt.core :refer [cheffy-belt]]
   [playground.blackbelt.write-belt.core :refer [write-belt]]
   ))

;; -------------------------
;; Routes

(def router
  (reitit/router
   [["/"                           :index]
    ["/items"
     [""                            :items]
     ["/:item-id"                   :item]]
    ["/about"                      :about]

    ;; User-defined
    ["/hello-world"                :hello-world]
    ["/an-image"                   :an-image]
    ["/nameless-company"           :nameless-company]
    ["/tic-tac-toe"                :tic-tac-toe]
    ["/query-me"                   :query-me]

    ;; Blackbelt
    ["/table-belt"                 :table-belt]
    ["/cheffy-belt"                :cheffy-belt]
    ["/write-belt"                 :write-belt]
    ]))


(defn path-for [route & [params]]
  (if params
    (:path (reitit/match-by-name router route params))
    (:path (reitit/match-by-name router route))))

;; -------------------------
;; Page components

(defn home-page []
  (fn []
    [:div#main.container.mx-auto.flex.flex-col
     [:h1.text-3xl.py-4.leading-tight "Welcome to <playground>"]

     ;; User-defined
     [:h2.pt-4.text-2xl "From the user"]
     [:ol.p-4
      [:li [:a {:href (path-for :hello-world)} "Hello World"]]
      [:li [:a {:href (path-for :an-image)} "An image"]]
      [:li [:a {:href (path-for :nameless-company)} "Nameless Co."]]
      [:li [:a {:href (path-for :tic-tac-toe)} "(Obligatory) Tic Tac Toe"]]
      [:li [:a {:href (path-for :query-me)} "Query me"]]
      ]

     ;; Blackbelt
     [:h2.pt-4.text-2xl "Blackbelt experiments"]
     [:ol.p-4
      [:li [:a {:href (path-for :table-belt)} "Table-belt"]]
      [:li [:a {:href (path-for :cheffy-belt)} "Cheffy-belt"]]
      [:li [:a {:href (path-for :write-belt)} "Write-belt"]]
      ]

     [:h2.pt-4.text-2xl "From the system"]
     [:ul.p-4
      [:li [:a {:href (path-for :items)} "Items of playground"]]
      [:li [:a {:href "/broken/link"} "Broken link"]]]

     ]))



(defn items-page []
  (fn []
    [:span.main
     [:h1 "The items of playground"]
     [:ul (map (fn [item-id]
                 [:li {:name (str "item-" item-id) :key (str "item-" item-id)}
                  [:a {:href (path-for :item {:item-id item-id})} "Item: " item-id]])
               (range 1 60))]]))


(defn item-page []
  (fn []
    (let [routing-data (session/get :route)
          item (get-in routing-data [:route-params :item-id])
          query (get-in routing-data [:query-params :t])]
      [:span.main
       [:h1 (str "Item " item " of playground")]
       [:p (str query)]
       [:p [:a {:href (str (path-for :items) "?t=293847")} "Back to the list of items"]]])))


(defn about-page []
  (fn [] [:span.main
          [:h1 "About playground"]]))


;; -------------------------
;; Translate routes -> page components

(defn page-for [route]
  (case route
    :index                      #'home-page
    :about                      #'about-page
    :items                      #'items-page
    :item                       #'item-page
    
    ;; User-defined
    :hello-world                #'hello-world
    :an-image                   #'an-image
    :nameless-company           #'nameless-company
    :tic-tac-toe                #'tic-tac-toe
    :query-me                   #'query-me

    ;; Blackbelt
    :table-belt                 #'table-belt
    :cheffy-belt                #'cheffy-belt
    :write-belt                 #'write-belt
    ))


;; -------------------------
;; Page mounting component

(defn current-page []
  "Renders the current page. Also inserts the correct component
  header (unless you're in the home page). Also checks for :full-page
  to see if nav elements should be rendered in full."
  (let [routing-data         (session/get :route)
        page                 (:current-page routing-data)
        page-name            (name (symbol page))
        page-name-no-hyphens (clojure.string/capitalize 
                               (clojure.string/replace page-name #"-" " "))
        inspirational-quote  (into {} (for [[k v] (js->clj (.. js-quote getQuote))]
                                        [(keyword k) v]))]
    [:div#body-container.container.mx-auto.flex.justify-center
     (if (:full-page (meta page))
       [:div#full-page-container.absolute.top-0.left-0
        {:style {:background-color "white"
                 :width "100vw"
                 :height "100%"
                 :overflow-x "hidden"}}
        [page]
        [:div {:style {:display "flex"
                       :justify-content "flex-end"
                       :padding "2em"}}
         [:a {:href (path-for :index)} "<- Go back"]]]

       ;; TODO: fix issue where too much content in [page] results in nav being pushed
       ;; over the top of the page 
       [:div.app-container 
        [:header
         [:p [:a {:href (path-for :index)} "Home"] " | "
          [:a {:href (path-for :about)} "About <playground>"]]]
        [:hr {:style {:width "100%"}}]
        (when-not (= #'home-page page) [:h1.py-6 page-name-no-hyphens])
        [page]
        [:hr {:style {:width "100%"}}]
        [:footer
         [:q {:style {:font-style "italic"}} (:text inspirational-quote)]
         [:p {:style {:text-align "right"}} "—" (:author inspirational-quote)]]]

       )]))

;; -------------------------
;; Initialize app

(defn ^:dev/after-load mount-root []
  (rdom/render [current-page] (.getElementById js/document "app")))

(defn ^:export init! []
  (clerk/initialize!)
  (accountant/configure-navigation!
   {:nav-handler
    (fn [path]
      (let [match (reitit/match-by-path router path)
            current-page (:name (:data  match))
            route-params (:path-params match)
            query-params (:query-params match)]
        (reagent/after-render clerk/after-render!)
        (session/put! :route {:current-page (page-for current-page)
                              :route-params route-params
                              :query-params query-params})
        (clerk/navigate-page! path)
        ))
    :path-exists?
    (fn [path]
      (boolean (reitit/match-by-path router path)))})
  (accountant/dispatch-current!)
  (rf/dispatch-sync [:initialize-blackbelt-db])
  (mount-root))
