(ns playground.blackbelt.cheffy-belt.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [playground.blackbelt.state]
            [playground.blackbelt.cheffy-belt.subs]
            [playground.blackbelt.cheffy-belt.events]
            [playground.blackbelt.cheffy-belt.views :refer [home
                                                            nav]]))
(defn pages
  [page-name]
  (case page-name
    :home        [home]
    [home]))

;; Main
(defn ^:full-page cheffy-belt
  []
  (let [active-nav @(rf/subscribe [:active-nav])]
    [:div.cheffy-belt
     [nav]
     [pages active-nav]]))
