(ns playground.blackbelt.state
  (:require [clojure.string :as string]
            [clojure.pprint :refer [pprint]] ;; TODO: remove in production
            [reagent.core :as r]
            [re-frame.core :as rf]
            [faker :as js-faker]))


;; -- Forms on users --
; Helper functions
(defn get-id [n]
  (let [alphabet (map char (concat (range 48 58) (range 97 123)))]
    (apply str (repeatedly n #(rand-nth (map char alphabet))))))

(defn get-user []
  (let [user-id    (get-id 8)
        first-name (.. js-faker -name firstName)
        last-name  (.. js-faker -name lastName)]
    {user-id {:user-id             user-id
              :first-name          first-name
              :last-name           last-name
              :email               (string/lower-case
                                     (str first-name "." last-name "@gmail.com"))
              :password            "password"
              :date-joined         (- (. js/Date now) (rand-int (Math/pow 2 34)))
              :image-url           (.. js-faker -image avatar)
              ;;:subscriptions       #{}
              :authored-tasks      #{}
              :authored-task-trees #{}
              }}))

; Main functions
(def *users
  (r/atom (->> #'get-user
               repeatedly
               (take 10)
               (apply merge))))

;; TODO: write mock for *user-data
(def *user-data
  "When a user opens a task, the task components will look for data here.
  <user>.<tasks>.<task-01> keys should mirror <task-01>.<components>;
  <user>.<tasks>.<task-01> should give USER data, not task data."
  (r/atom {:user-01 {:user-id           :user-01
                     :task-trees        {:task-tree-01 {}}
                     :tasks             {:task-01 {:repetition {}
                                                   :bounty     {}}
                                         :task-02 {:repetition {}
                                                   :bounty     {}}
                                         }}}))


;; -- Forms on tasks --
; Helper functions
(defn get-task [] 
  (let [task-id (get-id 16)]
    {task-id {:task-id        task-id
              :version        (rand-int 5)
              ;; TODO: limit :title to 64 characters
              :title          (str (string/capitalize (.. js-faker -hacker verb))
                                   " "
                                   (.. js-faker -hacker noun)
                                   (rand-nth [" with " " using " " via "])
                                   (.. js-faker -hacker adjective)
                                   " "
                                   (.. js-faker -hacker noun))
              ;; TODO: limit :subtitle to 128 characters
              :subtitle       (string/capitalize (.. js-faker -hacker phrase))
              ;; TODO: limit :description to 2^16 characters
              ;; TODO: decide if you should break description down into a map of steps
              :description    (.. js-faker -lorem paragraph)
              :image-url      (.. js-faker -image technics)
              :author         (get-id 8)
              :date-created   (- (. js/Date now) (rand-int (Math/pow 2 32)))
              :date-modified  (- (. js/Date now) (rand-int (Math/pow 2 30)))
              :materials      (vec (for [x (range (rand-int 5))]
                                     (list (.. js-faker -commerce product)
                                           (rand-int 10))))
              :components     [:repetition
                               :bounty]
              }
     }))

; Main functions
(def *tasks
  (r/atom (->> get-task
               repeatedly
               (take 20)
               (apply merge))))



;; -- Forms on task trees --
; Helper functions
(defn get-task-tree [*tasks]
  (let [tasks           @*tasks
        task-tree-id    (get-id 12)
        task-keys       (keys tasks)
        root            (rand-nth (keys tasks))]
    {task-tree-id {:task-tree-id        task-tree-id
                   :version             (rand-int 5)
                   :title               (str (string/capitalize (.. js-faker -hacker ingverb))
                                             " "
                                             (.. js-faker -hacker noun))
                   :subtitle            (string/capitalize (.. js-faker -hacker phrase))
                   :description         (string/capitalize (.. js-faker -lorem paragraph))
                   :image-url           (.. js-faker -image technics)
                   :author              (get-id 8)
                   :date-created        (- (. js/Date now) (rand-int (Math/pow 2 32)))
                   :date-modified       (- (. js/Date now) (rand-int (Math/pow 2 30)))
                   :root                root
                   ;; TODO: figure out how to ensure that task trees don't have cycles
                   :children            (set (remove #(= % root) 
                                                     (take 5 task-keys)))
                   }
     }))

; Main functions
(def *task-trees
  (r/atom (->> #(get-task-tree *tasks)
               repeatedly
               (take 4)
               (apply merge))))
;;(def *task-trees
;;  (r/atom {:task-tree-01 {:task-tree-id        :task-tree-01
;;                          :version             0
;;                          :title               "<128 characters>"
;;                          :subtitle            "<512 characters>"
;;                          :description         "<2^16 characters>"
;;                          :image-url           "<url>"
;;                          :author              :user-04
;;                          :date-created        "<date>"
;;                          :date-modified       "<date>"
;;                          :root                :task-01
;;                          :children            #{"<set of task IDs>"}
;;                          :subscribers         #{"<set of user IDs>"}}
;;           :task-tree-02 {:task-tree-id        :task-tree-02
;;                          :version             0
;;                          :title               "<128 characters>"
;;                          :subtitle            "<512 characters>"
;;                          :description         "<2^16 characters>"
;;                          :image-url           "<url>"
;;                          :author              :user-04
;;                          :date-created        "<date>"
;;                          :date-modified       "<date>"
;;                          :root                :task-02
;;                          :children            #{"<set of task IDs>"}
;;                          :subscribers         #{"<set of user IDs>"}}
;;          }))


;; -- Global state --
; Defines the global state
(def app-db
  {:authenticated {:user-id nil}
   :errors        {}
   :nav           {:active-page        :home
                   :active-nav         :home
                   :active-modal       nil
                   :active-profile     nil
                   :active-task        nil
                   :active-task-tree   nil}
   ;; TODO: change *<var>s to non-Atoms
   :users          @*users
   :tasks          @*tasks
   :task-trees     @*task-trees
   })

(rf/reg-event-db 
  :initialize-blackbelt-db
  (fn [_ _]
    app-db))

