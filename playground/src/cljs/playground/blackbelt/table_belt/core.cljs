(ns playground.blackbelt.table-belt.core
  (:require [reagent.core :as r]
            [reagent.dom :as d]
            [faker :as js-faker]
            [clojure.string :as string]
            [goog.string :as gstring]
            [goog.string.format]
            ))

(def style-table
  {:font-family "monospace"
   :vertical-align "top"})

(defn ^:full-page table-belt []
  (fn []
    [:div.table-belt-main {:style {:display "flex"
                                   :justify-content "space-around"
                                   :padding-top "2em"}}
     [:table {:style style-table}
      [:tr
       [:th "ID"] 
       [:th "Task"]
       [:th "Metric (Ideal)"]
       [:th "Successes/Attempts (best)"]]
      (for [i (range 3)]
        [:tr 
         [:td (-> (.. js-faker -random uuid)
                  (#(string/split % "-"))
                  first
                  (#(subs % 0 8)))]
         [:td (str (-> (.. js-faker -hacker verb)
                       string/capitalize)
                   " "
                   (.. js-faker -hacker noun)
                   " using "
                   (.. js-faker -hacker adjective)
                   " "
                   (.. js-faker -hacker noun))]
         (let [ideal-value  (rand 100)
               ideal-unit   (.. js-faker -hacker noun)
               ideal        (str (gstring/format "%.2d" ideal-value)
                                 " "
                                 ideal-unit)
               best-attempt-value (- ideal-value (rand ideal-value))
               attempts    (rand-int 50)]
           (list 
             [:td (str ideal-unit " (" ideal "s)")]       
             [:td (str (- attempts (rand-int attempts))
                       "/"
                       attempts
                       " ("
                       (gstring/format "%.2d" best-attempt-value)
                       " "
                       ideal-unit
                       "s)")])
           )])]]))


