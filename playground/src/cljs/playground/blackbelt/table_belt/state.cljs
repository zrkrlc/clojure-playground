(ns playground.blackbelt.table_belt.state
  (:require [reagent.core :as r]))

(def users 
  (r/atom {:user-01 {:id                :user-01
                     :firstname         "Scott"
                     :lastname          "Alexander"
                     :date-joined       "1597049138476" ;; Epoch time, like JS
                     :image-url         "https://i.imgur.com/zdygDDF.jpg"}
           :user-02 {:id                :user-02
                     :firstname         "Paul"
                     :lastname          "Christiano"
                     :date-joined       "1597049140476"
                     :image-url         "https://i.imgur.com/R1jinyE.jpg"}
           :user-03 {:id                :user-03
                     :firstname         "Hedy"
                     :lastname          "Lamarr"
                     :date-joined       "1597049144476"
                     :image-url         "https://i.imgur.com/fobJ2Ep.png"}
           :user-04 {:id                :user-04
                     :firstname         "Jessica"
                     :lastname          "Livingston"
                     :date-joined       "1597049159476"
                     :image-url         "https://i.imgur.com/7nvy8OF.png"}
           }))


(def tasks
  "The goal here is: a task must be purely computed. Essentially:"
  (comment {:task-00 {:id                :task-00
                     :version           0
                     :title             "<128 characters>"
                     :blurb             "<512 characters>"
                     :description       "<2^16 characters, Markdown>"
                     :image-url         "<url>"
                     :author            :user-04
                     :date-created      "<date>"
                     :date-modified     "<date>" 
                     :materials         ["<vector of item IDs>"] 
                     :components        ["<vector of component IDs>"]
                     :children          ["<vector of task IDs>"] 
                     :subscribers       #{"<set of user IDs>"}}})

  (r/atom {:task-01 {:id                :task-01
                     :version           0
                     :title             "Cut onions"
                     :blurb             "Master the art of crying on demand!"
                     :description       "Lorem ipsum dolor sit amet..."
                     :image-url         "https://i.imgur.com/9Y5Bj3p.png"
                     :author            :user-04
                     :date-created      "1597049159478"
                     :date-modified     "1597049159576" 
                     :materials         ['("knife" 1) '("onion" 5)] 
                     :components        ["<vector of component IDs>"]
                     :children          ["<vector of task IDs>"] 
                     :subscribers       #{"<set of user IDs>"}
                     }
           :task-01 {:id                :task-00
                     :version           0
                     :title             "<128 characters>"
                     :blurb             "<512 characters>"
                     :description       "<2^16 characters, Markdown>"
                     :image-url         "<url>"
                     :author            :user-04
                     :date-created      "<date>"
                     :date-modified     "<date>" 
                     :materials         ["<vector of item IDs>"] 
                     :components        ["<vector of component IDs>"]
                     :children          ["<vector of task IDs>"] 
                     :subscribers       #{"<set of user IDs>"}
                     }
           }))


(def user-data
  "When a user opens a task, the task components will look for data here.
  <user>.<tasks>.<task-01> keys should mirror <task-01>.<components>;
  <user>.<tasks>.<task-01> should give USER data, not task data."
  (r/atom {:user-01 {:id                :user-01
                     :tasks             {:task-01 {:repetition `("<arbitrary user data>")
                                                   :bounty `("<arbitrary user data>")}
                                         :task-02 {:repetition `("<arbitrary user data>")
                                                   :bounty `("<arbitrary user data>")}
                                         }}}))


