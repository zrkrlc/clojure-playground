(ns playground.blackbelt.write-belt.subs
  (:require [re-frame.core :refer [reg-sub]]))

(reg-sub ::active-nave
         (fn [db _]
           (get-in db [:nav :active-nav])))

(reg-sub ::active-user
         (fn [db _]
           (get-in db [:authenticated :user-id])))

(reg-sub ::users
         (fn [db _]
           (get-in db [:users])))

(reg-sub ::tasks
         (fn [db _]
           (get-in db [:tasks])))

(reg-sub ::task-trees
         (fn [db _]
           (get-in db [:task-trees])))
