(ns playground.blackbelt.write-belt.views
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [inspirational-quotes :as js.quotes]
            [playground.blackbelt.write-belt.events]
            [playground.blackbelt.write-belt.subs]))

;; Components
; TODO: make the nav work
(defn nav []
  (let [active-user @(rf/subscribe [:active-user])]
    (fn []
      [:div.nav {:style {:display "flex"
                         :justify-content "flex-end"
                         :padding "1em"}}
       [:a {:href "/write-belt" :style {:padding-left "1em"}} "Home"]
       (when (nil? active-user)
         [:a {:href "/write-belt#log-in" :style {:padding-left "1em"}} "Log in"])
       [:a {:href "/write-belt#dashboard" :style {:padding-left "1em"}} "Dashboard"]
       ])))

(defn task-card [task]
  (let [task-id      (:task-id task)
        title        (:title task)
        subtitle     (:subtitle task)
        description  (:description task)
        image-url    (:image-url task)]
    (fn ^{:key task-id} []
      [:a {:href (str "/write-belt?t=" task-id)
           :style {:text-decoration "none"
                   :color "var(--fg-normal)"}}
       [:button.text-left.hover:bg-blue-100
        {:on-click #(.. js/window location href (str "/write-belt?=t" task-id))}
        [:div#user-card.flex.flex-col.p-4.rounded.overflow-hidden.shadow-lg
         {:style {:width "20em"
                  :height "40em"}}
         [:img.w-full {:src (str image-url "?t=" (.. js/Date now))}]
         [:div.px-6.py-4
          [:p.text-base.font-bold.pb-4.text-justify title]
          [:p.text-sm.text-justify subtitle]
          [:p.text-xs [:span.font-semibold "ID: "] task-id]]
         [:div.px-6.pb-2.text-justify.self-end.overflow-y-auto
          [:p.text-sm {:style {:hyphens "auto"}}
           description]]
         ]]])))

(defn user-card [user]
  (let [user-id      (:user-id user)
        first-name   (:first-name user)
        last-name    (:last-name user)
        email        (:email user)
        date-joined  (.. (new js/Date (:date-joined user)) toString)
        image-url    (:image-url user)]
    (fn ^{:key user-id} []
      [:div#user-card.flex.flex-col.p-4.rounded.overflow-hidden.shadow-lg
       {:style {:width "20em"}}
       [:img.w-full {:src image-url}]
       [:div.px-6.py-4
        [:h3 first-name " " last-name]]
       [:div.px-6.pt-4.pb-2.self-end
        [:p.text-xs [:span.font-semibold "ID: "] user-id]
        [:p.text-sm [:span.font-semibold "E-mail address: "] email]
        [:p.text-sm [:span.font-semibold "Date joined: "] date-joined]]
       ])))

;; Pages
(defn log-in []
  (fn []
    [nav]
    [:div "Log in"]))

(defn home []
  (let [users        (vals @(rf/subscribe [:users]))
        tasks        (vals @(rf/subscribe [:tasks]))]
    (fn []
      [:div#home
       [:div#home-tasks.mx-auto.p-8
        [:h2.pb-4 "Tasks"]
        [:div.flex.flex-row.flex-wrap.justify-between
         (for [task tasks]
           [task-card task])]]

       [:div#home-users.mx-auto.p-8
        [:h2.pb-4 "Our users"]
        [:div.flex.flex-row.flex-wrap.justify-between
         (for [user users]
           [user-card user])]
       ]])))


