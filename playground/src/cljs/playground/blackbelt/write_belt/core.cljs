(ns playground.blackbelt.write-belt.core
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [playground.blackbelt.state]
            [playground.blackbelt.write-belt.subs]
            [playground.blackbelt.write-belt.events]
            [playground.blackbelt.write-belt.views :refer [home
                                                            nav]]))
(defn pages
  [page-name]
  (case page-name
    :home        [home]
    [home]))

;; Main
(defn ^:full-page write-belt
  []
  (let [active-nav @(rf/subscribe [:active-nav])]
    [:div.write-belt
     [pages active-nav]]))
