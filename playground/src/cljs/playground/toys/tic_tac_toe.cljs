(ns playground.toys.tic-tac-toe
  (:require [reagent.core :as r]
            [reagent.dom :as d]))

(def style 
  {:display                "grid"
   :grid-template-columns  "1fr 1fr 1fr"
   :grid-template-rows     "1fr 1fr 1fr"
   :border                 "2px solid black"
   })

(def style-grid-items
  {:border        "1px solid black"
   })

(def style-button-items
  {:background-color        "none"
   :border                  "none"
   :width                   "100%"
   :height                  "2em"
   :color                   "black"
   :font-size               "4em"
   :font-family             "Roboto"
   })

(def style-button-reset
  {:background-color        "none"
   :border                  "none"
   :width                   "20vw"
   :font-size               "2em"
   :font-family             "Roboto"
   })

(def board-state
  (r/atom (into (sorted-map)
            (map vector [[0 0] [1 0] [2 0]
                         [0 1] [1 1] [2 1]
                         [0 2] [1 2] [2 2]];; \a to \i
                  (repeat "")))))

;; When 0 mod 2, it's O's turn
(def turn-state
  (r/atom 0))

(def win-state
  (r/atom false))

(def draw-state
  (r/atom false))

(defn translate [coords delta-x delta-y]
  [(+ delta-x (first coords))
   (+ delta-y (second coords))])

(defn check-board-state [coords board-state]
  (let [source (get @board-state coords)
        f #(get @board-state (translate coords %1 %2))]
    (or (= source (f 1 0)   (f 2 0))
        (= source (f 1 1)   (f 2 2))
        (= source (f 0 -1)  (f 0 -2))
        (= source (f -1 -1) (f -2 -2))
        (= source (f -1 0)  (f -2 0))
        (= source (f -1 1)  (f -2 2))
        (= source (f 0 1)   (f 0 2))
        (= source (f 1 -1)  (f 2 -2)))))

(defn player-win [turn-state win-state]
  (if (and @win-state
           (= 0 (mod (int @turn-state) 2)))
    "X"
    "O"))

(defn player-draw [turn-state]
  (>= @turn-state 9))

(defn handle-turn! [coords board-state turn-state]
  (when (empty? (get @board-state coords))
    ((swap! turn-state inc)
     (if (= 0 (mod (int @turn-state) 2))
       (swap! board-state #(assoc % coords "O"))
       (swap! board-state #(assoc % coords "X")))
     (reset! win-state (check-board-state coords board-state)))))

(defn handle-reset! [board-state turn-state win-state draw-state]
  (doall (for [[k v] @board-state]
           (swap! board-state #(assoc % k ""))))
  (reset! turn-state 0)
  (reset! win-state false)
  (reset! draw-state false))

(defn ^:export tic-tac-toe []
  (if-not @win-state
    [:div.tic-tac-toe {:style style}
     (doall (for [[k v] @board-state]
              [:div.grid-item {:id (str (first k) "-" (second k)) 
                               :style style-grid-items}
               [:button {:style style-button-items
                         :on-click (fn [] (handle-turn! k board-state turn-state))}
                v]]))]
     [:div {:style {:display "flex"
                    :flex-direction "column"
                    :place-items "center"}}
      [:img {:src "https://media1.giphy.com/media/5kq0GCjHA8Rwc/giphy.gif"
             :style {:height "auto"}}]
      [:h3 "Player " (player-win turn-state win-state) " wins!"]
      [:button {:style style-button-reset
                :on-click (fn [] (handle-reset! board-state turn-state win-state draw-state))}
       "Play again?"]
      ]))
