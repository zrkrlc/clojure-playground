(ns playground.toys.nameless-company
  (:require [reagent.core :as r]
            [reagent.dom :as d]
            [playground.toys.nameless-company-state :as state]
            [faker :as js-fake]))

(defn nameless-company []
  (let [users @state/users]
    [:div.main
     [:table {:style {:border "1px solid red"
                      :table-layout "fixed"
                      :width "100%"}}
      [:tr {:style {:word-wrap "break-word"}}
       [:th ""]
       [:th "Name"]
       [:th "Nickname"]
       [:th "E-mail"]
       [:th "Date modified"]]
      (for [[k v] users]
        [:tr
         [:td [:img {:src (:image-url v)
                :width "80px"}]]
         [:td [:p (:firstname v) " " (:lastname v)]]
         [:td [:p (clojure.string/capitalize (.. js-fake -hacker adjective)) " " (:firstname v)]]
         [:td [:p (.. js-fake -internet userName) "\n" "@" (.. js-fake -internet domainName) ]]
         [:td [:p (str (.. js-fake -date recent))]]
         ])
      ]]))
