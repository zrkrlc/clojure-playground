(ns playground.toys.query-me
  (:require [reagent.core :as r]
            [reagent.session :as session]))

(defn query-me []
  (fn []
    (let [routing-data (session/get :route)
          query-value (get-in routing-data [:query-params :query])]
      [:div.main
       [:form {:action #(session/assoc-in! [:route :query-params] {:query 293847})
               :method ""}
        [:label {:for "name"} "Enter a query: "]
        [:input.border.rounded 
         {:type "text"
          :name "name"
          :required ""}]
        [:input {:type "submit"
                 :value "Submit query"}]]
       [:a {:href (str "/query-me?query=" query-value)}
        "Click me"]])))
