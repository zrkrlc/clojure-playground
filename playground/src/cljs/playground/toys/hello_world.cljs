(ns playground.toys.hello-world
  (:require [reagent.core :as r]
            [reagent.dom :as d]))

(defn ^:export hello-world []
  (let [style-normal-text {:style {:font-size "100%"}}
        style-small-text {:style {:font-size "80%" :line-height "0.8em"}}
        style-x-small-text {:style {:font-size "60%" :line-height "0.6em"}}
        style-xx-small-text {:style {:font-size "40%" :line-height "0.4em"}}
        style-xxx-small-text {:style {:font-size "20%" :line-height "0.2em"}}] 
    (fn [] [:span.main
            [:div
             [:span style-normal-text 
              "Oh god, this shit again. Why does everyone have to start their 
              bullshit coding projects "]
             [:span style-small-text
              "with such an unoriginal, unfunny reference to some piece of shit
              programming "]
             [:span style-x-small-text
              "folklore written by some old halfwit who probably can't tell
              their maps from "]
             [:span style-xx-small-text
              "their folds. What has the world turned into that we can't inject
              some freshness into the shitty 80x20 terminals "]
             [:span style-xxx-small-text
              "we violate our throats with every day, from 9 to 5, exchanging
              our precious youth for meaningless jobs..."]]])))

