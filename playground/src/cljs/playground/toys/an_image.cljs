(ns playground.toys.an-image
  (:require [reagent.core :as r]
            [reagent.dom :as d]))

(defn ^:export an-image 
  "Displays a single image."
  ;; TODO: implement a timer that replaces the image with a rickroll GIF
  []
  [:div {:style {:display "flex"
                 :justify-content "center"}}
   [:img {:src "https://slatestarcodex.com/blog_images/bluetwirl.gif"}]])
