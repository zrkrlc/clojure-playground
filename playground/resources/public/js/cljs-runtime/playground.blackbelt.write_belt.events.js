goog.provide('playground.blackbelt.write_belt.events');
goog.require('cljs.core');
goog.require('re_frame.core');
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("playground.blackbelt.write-belt.events","set-active-nav","playground.blackbelt.write-belt.events/set-active-nav",-4003268),(function (db,p__55018){
var vec__55019 = p__55018;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55019,(0),null);
var active_nav = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55019,(1),null);
return cljs.core.assoc_in(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"nav","nav",719540477),new cljs.core.Keyword(null,"active-nav","active-nav",-275650057)], null),active_nav);
}));

//# sourceMappingURL=playground.blackbelt.write_belt.events.js.map
