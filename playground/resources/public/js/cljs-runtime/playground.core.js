goog.provide('playground.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('reagent.dom');
goog.require('reagent.session');
goog.require('re_frame.core');
goog.require('reitit.frontend');
goog.require('clerk.core');
goog.require('accountant.core');
var module$node_modules$inspirational_quotes$index=shadow.js.require("module$node_modules$inspirational_quotes$index", {});
goog.require('playground.toys.hello_world');
goog.require('playground.toys.an_image');
goog.require('playground.toys.nameless_company');
goog.require('playground.toys.tic_tac_toe');
goog.require('playground.toys.query_me');
goog.require('playground.blackbelt.state');
goog.require('playground.blackbelt.table_belt.core');
goog.require('playground.blackbelt.cheffy_belt.core');
goog.require('playground.blackbelt.write_belt.core');
playground.core.router = reitit.frontend.router.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 11, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/",new cljs.core.Keyword(null,"index","index",-1531685915)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/items",new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["",new cljs.core.Keyword(null,"items","items",1031954938)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/:item-id",new cljs.core.Keyword(null,"item","item",249373802)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/about",new cljs.core.Keyword(null,"about","about",1423892543)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/hello-world",new cljs.core.Keyword(null,"hello-world","hello-world",-788431463)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/an-image",new cljs.core.Keyword(null,"an-image","an-image",-778841902)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/nameless-company",new cljs.core.Keyword(null,"nameless-company","nameless-company",483780832)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/tic-tac-toe",new cljs.core.Keyword(null,"tic-tac-toe","tic-tac-toe",-2027526810)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/query-me",new cljs.core.Keyword(null,"query-me","query-me",-2077792193)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/table-belt",new cljs.core.Keyword(null,"table-belt","table-belt",437537574)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/cheffy-belt",new cljs.core.Keyword(null,"cheffy-belt","cheffy-belt",392591822)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, ["/write-belt",new cljs.core.Keyword(null,"write-belt","write-belt",-1268614507)], null)], null));
playground.core.path_for = (function playground$core$path_for(var_args){
var args__4742__auto__ = [];
var len__4736__auto___54967 = arguments.length;
var i__4737__auto___54968 = (0);
while(true){
if((i__4737__auto___54968 < len__4736__auto___54967)){
args__4742__auto__.push((arguments[i__4737__auto___54968]));

var G__54969 = (i__4737__auto___54968 + (1));
i__4737__auto___54968 = G__54969;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return playground.core.path_for.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(playground.core.path_for.cljs$core$IFn$_invoke$arity$variadic = (function (route,p__54952){
var vec__54953 = p__54952;
var params = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__54953,(0),null);
if(cljs.core.truth_(params)){
return new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(reitit.frontend.match_by_name.cljs$core$IFn$_invoke$arity$3(playground.core.router,route,params));
} else {
return new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(reitit.frontend.match_by_name.cljs$core$IFn$_invoke$arity$2(playground.core.router,route));
}
}));

(playground.core.path_for.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(playground.core.path_for.cljs$lang$applyTo = (function (seq54950){
var G__54951 = cljs.core.first(seq54950);
var seq54950__$1 = cljs.core.next(seq54950);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__54951,seq54950__$1);
}));

playground.core.home_page = (function playground$core$home_page(){
return (function (){
return new cljs.core.PersistentVector(null, 8, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#main.container.mx-auto.flex.flex-col","div#main.container.mx-auto.flex.flex-col",1560440653),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1.text-3xl.py-4.leading-tight","h1.text-3xl.py-4.leading-tight",2094944286),"Welcome to <playground>"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2.pt-4.text-2xl","h2.pt-4.text-2xl",-1751202759),"From the user"], null),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol.p-4","ol.p-4",1275021633),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"hello-world","hello-world",-788431463))], null),"Hello World"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"an-image","an-image",-778841902))], null),"An image"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"nameless-company","nameless-company",483780832))], null),"Nameless Co."], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"tic-tac-toe","tic-tac-toe",-2027526810))], null),"(Obligatory) Tic Tac Toe"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"query-me","query-me",-2077792193))], null),"Query me"], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2.pt-4.text-2xl","h2.pt-4.text-2xl",-1751202759),"Blackbelt experiments"], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ol.p-4","ol.p-4",1275021633),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"table-belt","table-belt",437537574))], null),"Table-belt"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"cheffy-belt","cheffy-belt",392591822))], null),"Cheffy-belt"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"write-belt","write-belt",-1268614507))], null),"Write-belt"], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2.pt-4.text-2xl","h2.pt-4.text-2xl",-1751202759),"From the system"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul.p-4","ul.p-4",-52876092),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"items","items",1031954938))], null),"Items of playground"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),"/broken/link"], null),"Broken link"], null)], null)], null)], null);
});
});
playground.core.items_page = (function playground$core$items_page(){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.main","span.main",-358707614),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"The items of playground"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ul","ul",-1349521403),cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (item_id){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),["item-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(item_id)].join(''),new cljs.core.Keyword(null,"key","key",-1516042587),["item-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(item_id)].join('')], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"item","item",249373802),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"item-id","item-id",-1804511607),item_id], null)], 0))], null),"Item: ",item_id], null)], null);
}),cljs.core.range.cljs$core$IFn$_invoke$arity$2((1),(60)))], null)], null);
});
});
playground.core.item_page = (function playground$core$item_page(){
return (function (){
var routing_data = reagent.session.get(new cljs.core.Keyword(null,"route","route",329891309));
var item = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(routing_data,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"route-params","route-params",2111411055),new cljs.core.Keyword(null,"item-id","item-id",-1804511607)], null));
var query = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(routing_data,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"query-params","query-params",900640534),new cljs.core.Keyword(null,"t","t",-1397832519)], null));
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.main","span.main",-358707614),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),["Item ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(item)," of playground"].join('')], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),cljs.core.str.cljs$core$IFn$_invoke$arity$1(query)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(playground.core.path_for(new cljs.core.Keyword(null,"items","items",1031954938))),"?t=293847"].join('')], null),"Back to the list of items"], null)], null)], null);
});
});
playground.core.about_page = (function playground$core$about_page(){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.main","span.main",-358707614),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"About playground"], null)], null);
});
});
playground.core.page_for = (function playground$core$page_for(route){
var G__54956 = route;
var G__54956__$1 = (((G__54956 instanceof cljs.core.Keyword))?G__54956.fqn:null);
switch (G__54956__$1) {
case "index":
return new cljs.core.Var(function(){return playground.core.home_page;},new cljs.core.Symbol("playground.core","home-page","playground.core/home-page",1167928155,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.core","playground.core",779088631,null),new cljs.core.Symbol(null,"home-page","home-page",-850279575,null),"playground/core.cljs",16,1,64,64,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.core.home_page)?playground.core.home_page.cljs$lang$test:null)]));

break;
case "about":
return new cljs.core.Var(function(){return playground.core.about_page;},new cljs.core.Symbol("playground.core","about-page","playground.core/about-page",-110565153,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.core","playground.core",779088631,null),new cljs.core.Symbol(null,"about-page","about-page",2116788009,null),"playground/core.cljs",17,1,117,117,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.core.about_page)?playground.core.about_page.cljs$lang$test:null)]));

break;
case "items":
return new cljs.core.Var(function(){return playground.core.items_page;},new cljs.core.Symbol("playground.core","items-page","playground.core/items-page",723920916,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.core","playground.core",779088631,null),new cljs.core.Symbol(null,"items-page","items-page",-1402798074,null),"playground/core.cljs",17,1,96,96,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.core.items_page)?playground.core.items_page.cljs$lang$test:null)]));

break;
case "item":
return new cljs.core.Var(function(){return playground.core.item_page;},new cljs.core.Symbol("playground.core","item-page","playground.core/item-page",-987219646,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.core","playground.core",779088631,null),new cljs.core.Symbol(null,"item-page","item-page",1282130364,null),"playground/core.cljs",16,1,106,106,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.core.item_page)?playground.core.item_page.cljs$lang$test:null)]));

break;
case "hello-world":
return new cljs.core.Var(function(){return playground.toys.hello_world.hello_world;},new cljs.core.Symbol("playground.toys.hello-world","hello-world","playground.toys.hello-world/hello-world",-214945039,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"export","export",214356590),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.toys.hello-world","playground.toys.hello-world",-735265299,null),new cljs.core.Symbol(null,"hello-world","hello-world",852100064,null),"playground/toys/hello_world.cljs",(27),true,(1),(5),(5),cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.toys.hello_world.hello_world)?playground.toys.hello_world.hello_world.cljs$lang$test:null)]));

break;
case "an-image":
return new cljs.core.Var(function(){return playground.toys.an_image.an_image;},new cljs.core.Symbol("playground.toys.an-image","an-image","playground.toys.an-image/an-image",-1076119720,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"export","export",214356590),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.toys.an-image","playground.toys.an-image",1379961015,null),new cljs.core.Symbol(null,"an-image","an-image",861689625,null),"playground/toys/an_image.cljs",(24),true,(1),(5),(5),cljs.core.list(cljs.core.PersistentVector.EMPTY),"Displays a single image.",(cljs.core.truth_(playground.toys.an_image.an_image)?playground.toys.an_image.an_image.cljs$lang$test:null)]));

break;
case "nameless-company":
return new cljs.core.Var(function(){return playground.toys.nameless_company.nameless_company;},new cljs.core.Symbol("playground.toys.nameless-company","nameless-company","playground.toys.nameless-company/nameless-company",-173912099,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.toys.nameless-company","playground.toys.nameless-company",-824647856,null),new cljs.core.Symbol(null,"nameless-company","nameless-company",2124312359,null),"playground/toys/nameless_company.cljs",(23),(1),(7),(7),cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.toys.nameless_company.nameless_company)?playground.toys.nameless_company.nameless_company.cljs$lang$test:null)]));

break;
case "tic-tac-toe":
return new cljs.core.Var(function(){return playground.toys.tic_tac_toe.tic_tac_toe;},new cljs.core.Symbol("playground.toys.tic-tac-toe","tic-tac-toe","playground.toys.tic-tac-toe/tic-tac-toe",-1247188449,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"export","export",214356590),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.toys.tic-tac-toe","playground.toys.tic-tac-toe",1485124028,null),new cljs.core.Symbol(null,"tic-tac-toe","tic-tac-toe",-386995283,null),"playground/toys/tic_tac_toe.cljs",(27),true,(1),(91),(91),cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.toys.tic_tac_toe.tic_tac_toe)?playground.toys.tic_tac_toe.tic_tac_toe.cljs$lang$test:null)]));

break;
case "query-me":
return new cljs.core.Var(function(){return playground.toys.query_me.query_me;},new cljs.core.Symbol("playground.toys.query-me","query-me","playground.toys.query-me/query-me",-1194189883,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.toys.query-me","playground.toys.query-me",293275042,null),new cljs.core.Symbol(null,"query-me","query-me",-437260666,null),"playground/toys/query_me.cljs",(15),(1),(5),(5),cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.toys.query_me.query_me)?playground.toys.query_me.query_me.cljs$lang$test:null)]));

break;
case "table-belt":
return new cljs.core.Var(function(){return playground.blackbelt.table_belt.core.table_belt;},new cljs.core.Symbol("playground.blackbelt.table-belt.core","table-belt","playground.blackbelt.table-belt.core/table-belt",1169717273,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"full-page","full-page",941808500),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.blackbelt.table-belt.core","playground.blackbelt.table-belt.core",1713454178,null),new cljs.core.Symbol(null,"table-belt","table-belt",2078069101,null),"playground/blackbelt/table_belt/core.cljs",(29),(1),(14),true,(14),cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.blackbelt.table_belt.core.table_belt)?playground.blackbelt.table_belt.core.table_belt.cljs$lang$test:null)]));

break;
case "cheffy-belt":
return new cljs.core.Var(function(){return playground.blackbelt.cheffy_belt.core.cheffy_belt;},new cljs.core.Symbol("playground.blackbelt.cheffy-belt.core","cheffy-belt","playground.blackbelt.cheffy-belt.core/cheffy-belt",-2061632208,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"full-page","full-page",941808500),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.blackbelt.cheffy-belt.core","playground.blackbelt.cheffy-belt.core",188065298,null),new cljs.core.Symbol(null,"cheffy-belt","cheffy-belt",2033123349,null),"playground/blackbelt/cheffy_belt/core.cljs",(30),(1),(16),true,(16),cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.blackbelt.cheffy_belt.core.cheffy_belt)?playground.blackbelt.cheffy_belt.core.cheffy_belt.cljs$lang$test:null)]));

break;
case "write-belt":
return new cljs.core.Var(function(){return playground.blackbelt.write_belt.core.write_belt;},new cljs.core.Symbol("playground.blackbelt.write-belt.core","write-belt","playground.blackbelt.write-belt.core/write-belt",1219617585,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"full-page","full-page",941808500),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.blackbelt.write-belt.core","playground.blackbelt.write-belt.core",86459832,null),new cljs.core.Symbol(null,"write-belt","write-belt",371917020,null),"playground/blackbelt/write_belt/core.cljs",(29),(1),(16),true,(16),cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.blackbelt.write_belt.core.write_belt)?playground.blackbelt.write_belt.core.write_belt.cljs$lang$test:null)]));

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__54956__$1)].join('')));

}
});
playground.core.current_page = (function playground$core$current_page(){

var routing_data = reagent.session.get(new cljs.core.Keyword(null,"route","route",329891309));
var page = new cljs.core.Keyword(null,"current-page","current-page",-101294180).cljs$core$IFn$_invoke$arity$1(routing_data);
var page_name = cljs.core.name(cljs.core.symbol.cljs$core$IFn$_invoke$arity$1(page));
var page_name_no_hyphens = clojure.string.capitalize(clojure.string.replace(page_name,/-/," "));
var inspirational_quote = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,(function (){var iter__4529__auto__ = (function playground$core$current_page_$_iter__54957(s__54958){
return (new cljs.core.LazySeq(null,(function (){
var s__54958__$1 = s__54958;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__54958__$1);
if(temp__5735__auto__){
var s__54958__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__54958__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__54958__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__54960 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__54959 = (0);
while(true){
if((i__54959 < size__4528__auto__)){
var vec__54961 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__54959);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__54961,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__54961,(1),null);
cljs.core.chunk_append(b__54960,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(k),v], null));

var G__54971 = (i__54959 + (1));
i__54959 = G__54971;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__54960),playground$core$current_page_$_iter__54957(cljs.core.chunk_rest(s__54958__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__54960),null);
}
} else {
var vec__54964 = cljs.core.first(s__54958__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__54964,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__54964,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(k),v], null),playground$core$current_page_$_iter__54957(cljs.core.rest(s__54958__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(cljs.core.js__GT_clj.cljs$core$IFn$_invoke$arity$1(module$node_modules$inspirational_quotes$index.getQuote()));
})());
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#body-container.container.mx-auto.flex.justify-center","div#body-container.container.mx-auto.flex.justify-center",356120505),(cljs.core.truth_(new cljs.core.Keyword(null,"full-page","full-page",941808500).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(page)))?new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#full-page-container.absolute.top-0.left-0","div#full-page-container.absolute.top-0.left-0",1641800050),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"background-color","background-color",570434026),"white",new cljs.core.Keyword(null,"width","width",-384071477),"100vw",new cljs.core.Keyword(null,"height","height",1025178622),"100%",new cljs.core.Keyword(null,"overflow-x","overflow-x",-26547754),"hidden"], null)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [page], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"display","display",242065432),"flex",new cljs.core.Keyword(null,"justify-content","justify-content",-1990475787),"flex-end",new cljs.core.Keyword(null,"padding","padding",1660304693),"2em"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"index","index",-1531685915))], null),"<- Go back"], null)], null)], null):new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.app-container","div.app-container",-164087897),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"header","header",119441134),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"index","index",-1531685915))], null),"Home"], null)," | ",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"href","href",-793805698),playground.core.path_for(new cljs.core.Keyword(null,"about","about",1423892543))], null),"About <playground>"], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"hr","hr",1377740067),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"width","width",-384071477),"100%"], null)], null)], null),((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Var(function(){return playground.core.home_page;},new cljs.core.Symbol("playground.core","home-page","playground.core/home-page",1167928155,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.core","playground.core",779088631,null),new cljs.core.Symbol(null,"home-page","home-page",-850279575,null),"playground/core.cljs",16,1,64,64,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.core.home_page)?playground.core.home_page.cljs$lang$test:null)])),page))?null:new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1.py-6","h1.py-6",-922147412),page_name_no_hyphens], null)),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [page], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"hr","hr",1377740067),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"width","width",-384071477),"100%"], null)], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"footer","footer",1606445390),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"q","q",689001697),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"font-style","font-style",-773672352),"italic"], null)], null),new cljs.core.Keyword(null,"text","text",-1790561697).cljs$core$IFn$_invoke$arity$1(inspirational_quote)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"text-align","text-align",1786091845),"right"], null)], null),"\u2014",new cljs.core.Keyword(null,"author","author",2111686192).cljs$core$IFn$_invoke$arity$1(inspirational_quote)], null)], null)], null))], null);
});
playground.core.mount_root = (function playground$core$mount_root(){
return reagent.dom.render.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.core.current_page], null),document.getElementById("app"));
});
playground.core.init_BANG_ = (function playground$core$init_BANG_(){
clerk.core.initialize_BANG_();

accountant.core.configure_navigation_BANG_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"nav-handler","nav-handler",2039495484),(function (path){
var match = reitit.frontend.match_by_path(playground.core.router,path);
var current_page = new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(match));
var route_params = new cljs.core.Keyword(null,"path-params","path-params",-48130597).cljs$core$IFn$_invoke$arity$1(match);
var query_params = new cljs.core.Keyword(null,"query-params","query-params",900640534).cljs$core$IFn$_invoke$arity$1(match);
reagent.core.after_render(clerk.core.after_render_BANG_);

reagent.session.put_BANG_(new cljs.core.Keyword(null,"route","route",329891309),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"current-page","current-page",-101294180),playground.core.page_for(current_page),new cljs.core.Keyword(null,"route-params","route-params",2111411055),route_params,new cljs.core.Keyword(null,"query-params","query-params",900640534),query_params], null));

return clerk.core.navigate_page_BANG_(path);
}),new cljs.core.Keyword(null,"path-exists?","path-exists?",1473384514),(function (path){
return cljs.core.boolean$(reitit.frontend.match_by_path(playground.core.router,path));
})], null));

accountant.core.dispatch_current_BANG_();

re_frame.core.dispatch_sync(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"initialize-blackbelt-db","initialize-blackbelt-db",1389266178)], null));

return playground.core.mount_root();
});
goog.exportSymbol('playground.core.init_BANG_', playground.core.init_BANG_);

//# sourceMappingURL=playground.core.js.map
