goog.provide('rewrite_clj.node.protocols');
goog.require('cljs.core');
goog.require('clojure.string');

/**
 * Protocol for EDN/Clojure nodes.
 * @interface
 */
rewrite_clj.node.protocols.Node = function(){};

var rewrite_clj$node$protocols$Node$tag$dyn_56407 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.tag[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.tag["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Node.tag",_);
}
}
});
/**
 * Keyword representing the type of the node.
 */
rewrite_clj.node.protocols.tag = (function rewrite_clj$node$protocols$tag(_){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$Node$tag$arity$1 == null)))))){
return _.rewrite_clj$node$protocols$Node$tag$arity$1(_);
} else {
return rewrite_clj$node$protocols$Node$tag$dyn_56407(_);
}
});

var rewrite_clj$node$protocols$Node$printable_only_QMARK_$dyn_56413 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.printable_only_QMARK_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.printable_only_QMARK_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Node.printable-only?",_);
}
}
});
/**
 * Return true if the node cannot be converted to an s-expression
 *   element.
 */
rewrite_clj.node.protocols.printable_only_QMARK_ = (function rewrite_clj$node$protocols$printable_only_QMARK_(_){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$Node$printable_only_QMARK_$arity$1 == null)))))){
return _.rewrite_clj$node$protocols$Node$printable_only_QMARK_$arity$1(_);
} else {
return rewrite_clj$node$protocols$Node$printable_only_QMARK_$dyn_56413(_);
}
});

var rewrite_clj$node$protocols$Node$sexpr$dyn_56424 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.sexpr[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.sexpr["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Node.sexpr",_);
}
}
});
/**
 * Convert node to s-expression.
 */
rewrite_clj.node.protocols.sexpr = (function rewrite_clj$node$protocols$sexpr(_){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$Node$sexpr$arity$1 == null)))))){
return _.rewrite_clj$node$protocols$Node$sexpr$arity$1(_);
} else {
return rewrite_clj$node$protocols$Node$sexpr$dyn_56424(_);
}
});

var rewrite_clj$node$protocols$Node$length$dyn_56432 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.length[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.length["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Node.length",_);
}
}
});
/**
 * Get number of characters for the string version of this node.
 */
rewrite_clj.node.protocols.length = (function rewrite_clj$node$protocols$length(_){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$Node$length$arity$1 == null)))))){
return _.rewrite_clj$node$protocols$Node$length$arity$1(_);
} else {
return rewrite_clj$node$protocols$Node$length$dyn_56432(_);
}
});

var rewrite_clj$node$protocols$Node$string$dyn_56441 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.string[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.string["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Node.string",_);
}
}
});
/**
 * Convert node to printable string.
 */
rewrite_clj.node.protocols.string = (function rewrite_clj$node$protocols$string(_){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$Node$string$arity$1 == null)))))){
return _.rewrite_clj$node$protocols$Node$string$arity$1(_);
} else {
return rewrite_clj$node$protocols$Node$string$dyn_56441(_);
}
});

goog.object.set(rewrite_clj.node.protocols.Node,"object",true);

var G__56290_56453 = rewrite_clj.node.protocols.tag;
var G__56291_56454 = "object";
var G__56292_56455 = (function (_){
return new cljs.core.Keyword(null,"unknown","unknown",-935977881);
});
goog.object.set(G__56290_56453,G__56291_56454,G__56292_56455);

var G__56295_56458 = rewrite_clj.node.protocols.printable_only_QMARK_;
var G__56296_56459 = "object";
var G__56297_56460 = (function (_){
return false;
});
goog.object.set(G__56295_56458,G__56296_56459,G__56297_56460);

var G__56302_56463 = rewrite_clj.node.protocols.sexpr;
var G__56303_56464 = "object";
var G__56304_56465 = (function (this$){
return this$;
});
goog.object.set(G__56302_56463,G__56303_56464,G__56304_56465);

var G__56313_56466 = rewrite_clj.node.protocols.length;
var G__56314_56467 = "object";
var G__56315_56468 = (function (this$){
return cljs.core.count(rewrite_clj.node.protocols.string(this$));
});
goog.object.set(G__56313_56466,G__56314_56467,G__56315_56468);

var G__56319_56469 = rewrite_clj.node.protocols.string;
var G__56320_56470 = "object";
var G__56321_56471 = (function (this$){
return cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([this$], 0));
});
goog.object.set(G__56319_56469,G__56320_56470,G__56321_56471);
/**
 * Given a seq of nodes, convert those that represent s-expressions
 * to the respective forms.
 */
rewrite_clj.node.protocols.sexprs = (function rewrite_clj$node$protocols$sexprs(nodes){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(rewrite_clj.node.protocols.sexpr,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(rewrite_clj.node.protocols.printable_only_QMARK_,nodes));
});
/**
 * Sum up lengths of the given nodes.
 */
rewrite_clj.node.protocols.sum_lengths = (function rewrite_clj$node$protocols$sum_lengths(nodes){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,cljs.core.map.cljs$core$IFn$_invoke$arity$2(rewrite_clj.node.protocols.length,nodes));
});
/**
 * Convert nodes to strings and concatenate them.
 */
rewrite_clj.node.protocols.concat_strings = (function rewrite_clj$node$protocols$concat_strings(nodes){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.map.cljs$core$IFn$_invoke$arity$2(rewrite_clj.node.protocols.string,nodes));
});

/**
 * Protocol for non-leaf EDN/Clojure nodes.
 * @interface
 */
rewrite_clj.node.protocols.InnerNode = function(){};

var rewrite_clj$node$protocols$InnerNode$inner_QMARK_$dyn_56484 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.inner_QMARK_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.inner_QMARK_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("InnerNode.inner?",_);
}
}
});
/**
 * Check whether the node can contain children.
 */
rewrite_clj.node.protocols.inner_QMARK_ = (function rewrite_clj$node$protocols$inner_QMARK_(_){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$InnerNode$inner_QMARK_$arity$1 == null)))))){
return _.rewrite_clj$node$protocols$InnerNode$inner_QMARK_$arity$1(_);
} else {
return rewrite_clj$node$protocols$InnerNode$inner_QMARK_$dyn_56484(_);
}
});

var rewrite_clj$node$protocols$InnerNode$children$dyn_56495 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.children[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.children["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("InnerNode.children",_);
}
}
});
/**
 * Get child nodes.
 */
rewrite_clj.node.protocols.children = (function rewrite_clj$node$protocols$children(_){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$InnerNode$children$arity$1 == null)))))){
return _.rewrite_clj$node$protocols$InnerNode$children$arity$1(_);
} else {
return rewrite_clj$node$protocols$InnerNode$children$dyn_56495(_);
}
});

var rewrite_clj$node$protocols$InnerNode$replace_children$dyn_56500 = (function (_,children){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.replace_children[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(_,children) : m__4429__auto__.call(null,_,children));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.replace_children["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(_,children) : m__4426__auto__.call(null,_,children));
} else {
throw cljs.core.missing_protocol("InnerNode.replace-children",_);
}
}
});
/**
 * Replace the node's children.
 */
rewrite_clj.node.protocols.replace_children = (function rewrite_clj$node$protocols$replace_children(_,children){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$InnerNode$replace_children$arity$2 == null)))))){
return _.rewrite_clj$node$protocols$InnerNode$replace_children$arity$2(_,children);
} else {
return rewrite_clj$node$protocols$InnerNode$replace_children$dyn_56500(_,children);
}
});

goog.object.set(rewrite_clj.node.protocols.InnerNode,"object",true);

var G__56370_56506 = rewrite_clj.node.protocols.inner_QMARK_;
var G__56371_56507 = "object";
var G__56372_56508 = (function (_){
return false;
});
goog.object.set(G__56370_56506,G__56371_56507,G__56372_56508);

var G__56374_56511 = rewrite_clj.node.protocols.children;
var G__56375_56512 = "object";
var G__56376_56513 = (function (_){
throw (new Error("UnsupportedOperationException"));
});
goog.object.set(G__56374_56511,G__56375_56512,G__56376_56513);

var G__56377_56515 = rewrite_clj.node.protocols.replace_children;
var G__56378_56516 = "object";
var G__56379_56517 = (function (_,___$1){
throw (new Error("UnsupportedOperationException"));
});
goog.object.set(G__56377_56515,G__56378_56516,G__56379_56517);
/**
 * Get all child s-expressions for the given node.
 */
rewrite_clj.node.protocols.child_sexprs = (function rewrite_clj$node$protocols$child_sexprs(node){
if(cljs.core.truth_(rewrite_clj.node.protocols.inner_QMARK_(node))){
return rewrite_clj.node.protocols.sexprs(rewrite_clj.node.protocols.children(node));
} else {
return null;
}
});

/**
 * Protocol for values that can be coerced to nodes.
 * @interface
 */
rewrite_clj.node.protocols.NodeCoerceable = function(){};

var rewrite_clj$node$protocols$NodeCoerceable$coerce$dyn_56521 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (rewrite_clj.node.protocols.coerce[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (rewrite_clj.node.protocols.coerce["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("NodeCoerceable.coerce",_);
}
}
});
rewrite_clj.node.protocols.coerce = (function rewrite_clj$node$protocols$coerce(_){
if((((!((_ == null)))) && ((!((_.rewrite_clj$node$protocols$NodeCoerceable$coerce$arity$1 == null)))))){
return _.rewrite_clj$node$protocols$NodeCoerceable$coerce$arity$1(_);
} else {
return rewrite_clj$node$protocols$NodeCoerceable$coerce$dyn_56521(_);
}
});

rewrite_clj.node.protocols.assert_sexpr_count = (function rewrite_clj$node$protocols$assert_sexpr_count(nodes,c){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.count(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(rewrite_clj.node.protocols.printable_only_QMARK_,nodes)),c)){
return null;
} else {
throw (new Error(["Assert failed: ",["can only contain",cljs.core.str.cljs$core$IFn$_invoke$arity$1(c)," non-whitespace form(s)."].join(''),"\n","(= (count (remove printable-only? nodes)) c)"].join('')));
}
});
rewrite_clj.node.protocols.assert_single_sexpr = (function rewrite_clj$node$protocols$assert_single_sexpr(nodes){
return rewrite_clj.node.protocols.assert_sexpr_count(nodes,(1));
});

//# sourceMappingURL=rewrite_clj.node.protocols.js.map
