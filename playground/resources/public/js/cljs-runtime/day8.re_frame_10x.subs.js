goog.provide('day8.re_frame_10x.subs');
goog.require('cljs.core');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core');
goog.require('day8.re_frame_10x.metamorphic');
goog.require('day8.re_frame_10x.utils.utils');
goog.require('clojure.string');
goog.require('cljs.spec.alpha');
goog.require('zprint.core');
var G__60131_60715 = new cljs.core.Keyword("settings","root","settings/root",-1329120290);
var G__60132_60716 = (function (db,_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.Keyword(null,"settings","settings",1556144875));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60131_60715,G__60132_60716) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60131_60715,G__60132_60716));
var G__60133_60717 = new cljs.core.Keyword("settings","panel-width%","settings/panel-width%",-1897072808);
var G__60134_60718 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60135_60719 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60136_60720 = (function (settings,_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(settings,new cljs.core.Keyword(null,"panel-width%","panel-width%",-110515341));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60133_60717,G__60134_60718,G__60135_60719,G__60136_60720) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60133_60717,G__60134_60718,G__60135_60719,G__60136_60720));
var G__60137_60721 = new cljs.core.Keyword("settings","panel-width%-rounded","settings/panel-width%-rounded",1475049191);
var G__60138_60722 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60139_60723 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","panel-width%","settings/panel-width%",-1897072808)], null);
var G__60140_60724 = (function (panel_width_PERCENT_,p__60141){
var vec__60142 = p__60141;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60142,(0),null);
var n = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60142,(1),null);
return (((function (){var G__60145 = ((panel_width_PERCENT_ * (100)) / n);
return Math.ceil(G__60145);
})() * n) / 100.0);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60137_60721,G__60138_60722,G__60139_60723,G__60140_60724) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60137_60721,G__60138_60722,G__60139_60723,G__60140_60724));
var G__60146_60727 = new cljs.core.Keyword("settings","window-width","settings/window-width",640332180);
var G__60147_60728 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60148_60729 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60149_60730 = (function (settings,_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(settings,new cljs.core.Keyword(null,"window-width","window-width",2057825599));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60146_60727,G__60147_60728,G__60148_60729,G__60149_60730) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60146_60727,G__60147_60728,G__60148_60729,G__60149_60730));
var G__60150_60731 = new cljs.core.Keyword("settings","window-width-rounded","settings/window-width-rounded",-924202629);
var G__60151_60732 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60152_60733 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","window-width","settings/window-width",640332180)], null);
var G__60153_60734 = (function (width,p__60154){
var vec__60155 = p__60154;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60155,(0),null);
var n = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60155,(1),null);
return ((function (){var G__60158 = (width / n);
return Math.ceil(G__60158);
})() * n);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60150_60731,G__60151_60732,G__60152_60733,G__60153_60734) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60150_60731,G__60151_60732,G__60152_60733,G__60153_60734));
var G__60159_60740 = new cljs.core.Keyword("settings","show-panel?","settings/show-panel?",-194286945);
var G__60160_60741 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60161_60742 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60162_60743 = (function (settings,_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(settings,new cljs.core.Keyword(null,"show-panel?","show-panel?",1475128892));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60159_60740,G__60160_60741,G__60161_60742,G__60162_60743) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60159_60740,G__60160_60741,G__60161_60742,G__60162_60743));
var G__60164_60744 = new cljs.core.Keyword("settings","selected-tab","settings/selected-tab",-124010089);
var G__60165_60745 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60166_60746 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60167_60747 = (function (settings,_){
if(cljs.core.truth_(new cljs.core.Keyword(null,"showing-settings?","showing-settings?",-140540878).cljs$core$IFn$_invoke$arity$1(settings))){
return new cljs.core.Keyword(null,"settings","settings",1556144875);
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(settings,new cljs.core.Keyword(null,"selected-tab","selected-tab",-1558510156));
}
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60164_60744,G__60165_60745,G__60166_60746,G__60167_60747) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60164_60744,G__60165_60745,G__60166_60746,G__60167_60747));
var G__60168_60748 = new cljs.core.Keyword("settings","number-of-retained-epochs","settings/number-of-retained-epochs",347300150);
var G__60169_60749 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60170_60750 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60171_60751 = (function (settings){
return new cljs.core.Keyword(null,"number-of-epochs","number-of-epochs",57769252).cljs$core$IFn$_invoke$arity$1(settings);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60168_60748,G__60169_60749,G__60170_60750,G__60171_60751) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60168_60748,G__60169_60749,G__60170_60750,G__60171_60751));
var G__60172_60752 = new cljs.core.Keyword("settings","ignored-events","settings/ignored-events",1377799632);
var G__60173_60753 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60174_60754 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60175_60755 = (function (settings){
return cljs.core.sort_by.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"sort","sort",953465918),cljs.core.vals(new cljs.core.Keyword(null,"ignored-events","ignored-events",1738756589).cljs$core$IFn$_invoke$arity$1(settings)));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60172_60752,G__60173_60753,G__60174_60754,G__60175_60755) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60172_60752,G__60173_60753,G__60174_60754,G__60175_60755));
var G__60176_60756 = new cljs.core.Keyword("settings","filtered-view-trace","settings/filtered-view-trace",818098350);
var G__60177_60757 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60178_60758 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60179_60759 = (function (settings){
return cljs.core.sort_by.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"sort","sort",953465918),cljs.core.vals(new cljs.core.Keyword(null,"filtered-view-trace","filtered-view-trace",-901876599).cljs$core$IFn$_invoke$arity$1(settings)));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60176_60756,G__60177_60757,G__60178_60758,G__60179_60759) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60176_60756,G__60177_60757,G__60178_60758,G__60179_60759));
var G__60180_60760 = new cljs.core.Keyword("settings","low-level-trace","settings/low-level-trace",191054289);
var G__60181_60761 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60182_60762 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60183_60763 = (function (settings){
return new cljs.core.Keyword(null,"low-level-trace","low-level-trace",638447092).cljs$core$IFn$_invoke$arity$1(settings);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60180_60760,G__60181_60761,G__60182_60762,G__60183_60763) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60180_60760,G__60181_60761,G__60182_60762,G__60183_60763));
var G__60184_60764 = new cljs.core.Keyword("settings","debug?","settings/debug?",-128490920);
var G__60185_60765 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60186_60766 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60187_60767 = (function (settings){
return new cljs.core.Keyword(null,"debug?","debug?",-1831756173).cljs$core$IFn$_invoke$arity$1(settings);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60184_60764,G__60185_60765,G__60186_60766,G__60187_60767) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60184_60764,G__60185_60765,G__60186_60766,G__60187_60767));
var G__60188_60769 = new cljs.core.Keyword("settings","app-db-follows-events?","settings/app-db-follows-events?",-115495889);
var G__60189_60770 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60190_60771 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","root","settings/root",-1329120290)], null);
var G__60191_60772 = (function (settings){
return new cljs.core.Keyword(null,"app-db-follows-events?","app-db-follows-events?",-1566738462).cljs$core$IFn$_invoke$arity$1(settings);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60188_60769,G__60189_60770,G__60190_60771,G__60191_60772) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60188_60769,G__60189_60770,G__60190_60771,G__60191_60772));
var G__60193_60773 = new cljs.core.Keyword("app-db","root","app-db/root",-1721368731);
var G__60194_60774 = (function (db,_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.Keyword(null,"app-db","app-db",865606302));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60193_60773,G__60194_60774) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60193_60773,G__60194_60774));
var G__60196_60775 = new cljs.core.Keyword("app-db","current-epoch-app-db-after","app-db/current-epoch-app-db-after",-1412128095);
var G__60197_60776 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60198_60777 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-event-trace","epochs/current-event-trace",-1911057851)], null);
var G__60199_60778 = (function (trace,_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(trace,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tags","tags",1771418977),new cljs.core.Keyword(null,"app-db-after","app-db-after",1477492964)], null));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60196_60775,G__60197_60776,G__60198_60777,G__60199_60778) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60196_60775,G__60197_60776,G__60198_60777,G__60199_60778));
var G__60201_60779 = new cljs.core.Keyword("app-db","current-epoch-app-db-before","app-db/current-epoch-app-db-before",-615465288);
var G__60202_60780 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60203_60781 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-event-trace","epochs/current-event-trace",-1911057851)], null);
var G__60204_60782 = (function (trace,_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(trace,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tags","tags",1771418977),new cljs.core.Keyword(null,"app-db-before","app-db-before",-1442902645)], null));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60201_60779,G__60202_60780,G__60203_60781,G__60204_60782) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60201_60779,G__60202_60780,G__60203_60781,G__60204_60782));
var G__60208_60783 = new cljs.core.Keyword("app-db","paths","app-db/paths",-1600032730);
var G__60209_60784 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60210_60785 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("app-db","root","app-db/root",-1721368731)], null);
var G__60211_60786 = (function (app_db_settings,_){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__60206_SHARP_){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(cljs.core.val(p1__60206_SHARP_),new cljs.core.Keyword(null,"id","id",-1388402092),cljs.core.key(p1__60206_SHARP_));
}),cljs.core.get.cljs$core$IFn$_invoke$arity$2(app_db_settings,new cljs.core.Keyword(null,"paths","paths",-1807389588)));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60208_60783,G__60209_60784,G__60210_60785,G__60211_60786) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60208_60783,G__60209_60784,G__60210_60785,G__60211_60786));
var G__60212_60787 = new cljs.core.Keyword("app-db","search-string","app-db/search-string",939397656);
var G__60213_60788 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60214_60789 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("app-db","root","app-db/root",-1721368731)], null);
var G__60215_60790 = (function (app_db_settings,_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(app_db_settings,new cljs.core.Keyword(null,"search-string","search-string",68818394));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60212_60787,G__60213_60788,G__60214_60789,G__60215_60790) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60212_60787,G__60213_60788,G__60214_60789,G__60215_60790));
var G__60216_60795 = new cljs.core.Keyword("app-db","expansions","app-db/expansions",-1814314845);
var G__60217_60796 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60218_60797 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("app-db","root","app-db/root",-1721368731)], null);
var G__60219_60798 = (function (app_db_settings,_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(app_db_settings,new cljs.core.Keyword(null,"json-ml-expansions","json-ml-expansions",1112306261));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60216_60795,G__60217_60796,G__60218_60797,G__60219_60798) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60216_60795,G__60217_60796,G__60218_60797,G__60219_60798));
var G__60221_60799 = new cljs.core.Keyword("app-db","node-expanded?","app-db/node-expanded?",-1032853540);
var G__60222_60800 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60223_60801 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("app-db","expansions","app-db/expansions",-1814314845)], null);
var G__60224_60802 = (function (expansions,p__60225){
var vec__60226 = p__60225;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60226,(0),null);
var path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60226,(1),null);
return cljs.core.contains_QMARK_(expansions,path);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60221_60799,G__60222_60800,G__60223_60801,G__60224_60802) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60221_60799,G__60222_60800,G__60223_60801,G__60224_60802));
var G__60231_60803 = new cljs.core.Keyword("app-db","reagent-id","app-db/reagent-id",916858371);
var G__60232_60804 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60233_60805 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("app-db","root","app-db/root",-1721368731)], null);
var G__60234_60806 = (function (root,_){
return new cljs.core.Keyword(null,"reagent-id","reagent-id",-766893415).cljs$core$IFn$_invoke$arity$1(root);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60231_60803,G__60232_60804,G__60233_60805,G__60234_60806) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60231_60803,G__60232_60804,G__60233_60805,G__60234_60806));
var G__60237_60807 = new cljs.core.Keyword("traces","trace-root","traces/trace-root",-1794329946);
var G__60238_60808 = (function (db,_){
return new cljs.core.Keyword(null,"traces","traces",-1301138004).cljs$core$IFn$_invoke$arity$1(db);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60237_60807,G__60238_60808) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60237_60807,G__60238_60808));
var G__60239_60809 = new cljs.core.Keyword("trace-panel","root","trace-panel/root",974242647);
var G__60240_60810 = (function (db,_){
return new cljs.core.Keyword(null,"trace-panel","trace-panel",-645338665).cljs$core$IFn$_invoke$arity$1(db);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60239_60809,G__60240_60810) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60239_60809,G__60240_60810));
var G__60241_60811 = new cljs.core.Keyword("traces","filter-items","traces/filter-items",977390347);
var G__60242_60812 = (function (db,_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"traces","traces",-1301138004),new cljs.core.Keyword(null,"filter-items","filter-items",232493909)], null));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60241_60811,G__60242_60812) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60241_60811,G__60242_60812));
var G__60245_60815 = new cljs.core.Keyword("traces","expansions","traces/expansions",1935277191);
var G__60246_60816 = (function (db,_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"traces","traces",-1301138004),new cljs.core.Keyword(null,"expansions","expansions",533713877)], null));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60245_60815,G__60246_60816) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60245_60815,G__60246_60816));
var G__60248_60817 = new cljs.core.Keyword("traces","categories","traces/categories",-821318016);
var G__60249_60818 = (function (db,_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"traces","traces",-1301138004),new cljs.core.Keyword(null,"categories","categories",178386610)], null));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60248_60817,G__60249_60818) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60248_60817,G__60249_60818));
var G__60250_60820 = new cljs.core.Keyword("traces","all-traces","traces/all-traces",-755374523);
var G__60251_60821 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60252_60822 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","trace-root","traces/trace-root",-1794329946)], null);
var G__60253_60823 = (function (traces,_){
return new cljs.core.Keyword(null,"all-traces","all-traces",-1494241641).cljs$core$IFn$_invoke$arity$1(traces);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60250_60820,G__60251_60821,G__60252_60822,G__60253_60823) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60250_60820,G__60251_60821,G__60252_60822,G__60253_60823));
var G__60254_60827 = new cljs.core.Keyword("traces","number-of-traces","traces/number-of-traces",-1195045241);
var G__60255_60828 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60256_60829 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","all-traces","traces/all-traces",-755374523)], null);
var G__60257_60830 = (function (traces,_){
return cljs.core.count(traces);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60254_60827,G__60255_60828,G__60256_60829,G__60257_60830) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60254_60827,G__60255_60828,G__60256_60829,G__60257_60830));
var G__60258_60831 = new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769);
var G__60259_60832 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60260_60833 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","all-traces","traces/all-traces",-755374523)], null);
var G__60261_60834 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60262_60835 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","beginning-trace-id","epochs/beginning-trace-id",-1340593887)], null);
var G__60263_60836 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60264_60837 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","ending-trace-id","epochs/ending-trace-id",-1920901579)], null);
var G__60265_60838 = (function (p__60266,_){
var vec__60267 = p__60266;
var traces = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60267,(0),null);
var beginning = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60267,(1),null);
var ending = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60267,(2),null);
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,day8.re_frame_10x.utils.utils.id_between_xf(beginning,ending),traces);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$8 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$8(G__60258_60831,G__60259_60832,G__60260_60833,G__60261_60834,G__60262_60835,G__60263_60836,G__60264_60837,G__60265_60838) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60258_60831,G__60259_60832,G__60260_60833,G__60261_60834,G__60262_60835,G__60263_60836,G__60264_60837,G__60265_60838));
day8.re_frame_10x.subs.filter_ignored_views = (function day8$re_frame_10x$subs$filter_ignored_views(p__60272,_){
var vec__60274 = p__60272;
var traces = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60274,(0),null);
var filtered_views = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60274,(1),null);
var munged_ns = cljs.core.set(cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.comp.cljs$core$IFn$_invoke$arity$2(cljs.core.munge,new cljs.core.Keyword(null,"ns-str","ns-str",-2062616499)),filtered_views));
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$1((function (trace){
return ((day8.re_frame_10x.metamorphic.render_QMARK_(trace)) && (cljs.core.contains_QMARK_(munged_ns,cljs.core.subs.cljs$core$IFn$_invoke$arity$3(new cljs.core.Keyword(null,"operation","operation",-1267664310).cljs$core$IFn$_invoke$arity$1(trace),(0),clojure.string.last_index_of.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"operation","operation",-1267664310).cljs$core$IFn$_invoke$arity$1(trace),".")))));
})),traces);
});
var G__60277_60848 = new cljs.core.Keyword("traces","current-event-visible-traces","traces/current-event-visible-traces",-133224585);
var G__60278_60849 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60279_60850 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769)], null);
var G__60280_60851 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60281_60852 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","filtered-view-trace","settings/filtered-view-trace",818098350)], null);
var G__60282_60853 = day8.re_frame_10x.subs.filter_ignored_views;
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60277_60848,G__60278_60849,G__60279_60850,G__60280_60851,G__60281_60852,G__60282_60853) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60277_60848,G__60278_60849,G__60279_60850,G__60280_60851,G__60281_60852,G__60282_60853));
var G__60286_60858 = new cljs.core.Keyword("traces","all-visible-traces","traces/all-visible-traces",-1694272071);
var G__60287_60859 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60288_60860 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","all-traces","traces/all-traces",-755374523)], null);
var G__60289_60861 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60290_60862 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","filtered-view-trace","settings/filtered-view-trace",818098350)], null);
var G__60291_60863 = day8.re_frame_10x.subs.filter_ignored_views;
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60286_60858,G__60287_60859,G__60288_60860,G__60289_60861,G__60290_60862,G__60291_60863) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60286_60858,G__60287_60859,G__60288_60860,G__60289_60861,G__60290_60862,G__60291_60863));
var G__60292_60864 = new cljs.core.Keyword("trace-panel","show-epoch-traces?","trace-panel/show-epoch-traces?",-826345951);
var G__60293_60865 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60294_60866 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("trace-panel","root","trace-panel/root",974242647)], null);
var G__60295_60867 = (function (trace_root){
return new cljs.core.Keyword(null,"show-epoch-traces?","show-epoch-traces?",-2096255323).cljs$core$IFn$_invoke$arity$1(trace_root);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60292_60864,G__60293_60865,G__60294_60866,G__60295_60867) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60292_60864,G__60293_60865,G__60294_60866,G__60295_60867));
var G__60296_60872 = new cljs.core.Keyword("global","unloading?","global/unloading?",-351325001);
var G__60297_60873 = (function (db,_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"global","global",93595047),new cljs.core.Keyword(null,"unloading?","unloading?",621163286)], null));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60296_60872,G__60297_60873) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60296_60872,G__60297_60873));
var G__60300_60874 = new cljs.core.Keyword("snapshot","snapshot-root","snapshot/snapshot-root",466086517);
var G__60301_60875 = (function (db,_){
return new cljs.core.Keyword(null,"snapshot","snapshot",-1274785710).cljs$core$IFn$_invoke$arity$1(db);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60300_60874,G__60301_60875) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60300_60874,G__60301_60875));
var G__60303_60876 = new cljs.core.Keyword("snapshot","snapshot-ready?","snapshot/snapshot-ready?",-1152726072);
var G__60304_60877 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60305_60878 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("snapshot","snapshot-root","snapshot/snapshot-root",466086517)], null);
var G__60306_60879 = (function (snapshot,_){
return cljs.core.contains_QMARK_(snapshot,new cljs.core.Keyword(null,"current-snapshot","current-snapshot",1368356222));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60303_60876,G__60304_60877,G__60305_60878,G__60306_60879) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60303_60876,G__60304_60877,G__60305_60878,G__60306_60879));
var G__60310_60880 = new cljs.core.Keyword("epochs","epoch-root","epochs/epoch-root",-2097634597);
var G__60311_60881 = (function (db,_){
return new cljs.core.Keyword(null,"epochs","epochs",1796936425).cljs$core$IFn$_invoke$arity$1(db);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60310_60880,G__60311_60881) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60310_60880,G__60311_60881));
var G__60312_60882 = new cljs.core.Keyword("epochs","all-events-by-id","epochs/all-events-by-id",-1225664812);
var G__60313_60883 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60314_60884 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","epoch-root","epochs/epoch-root",-2097634597)], null);
var G__60315_60885 = (function (epochs,_){
return cljs.core.sort_by.cljs$core$IFn$_invoke$arity$3(cljs.core.first,cljs.core._GT_,cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.juxt.cljs$core$IFn$_invoke$arity$2(cljs.core.key,cljs.core.comp.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"event","event",301435442),new cljs.core.Keyword(null,"tags","tags",1771418977),day8.re_frame_10x.metamorphic.matched_event,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"match-info","match-info",666319879),cljs.core.val], 0))),new cljs.core.Keyword(null,"matches-by-id","matches-by-id",1749529562).cljs$core$IFn$_invoke$arity$1(epochs)));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60312_60882,G__60313_60883,G__60314_60884,G__60315_60885) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60312_60882,G__60313_60883,G__60314_60884,G__60315_60885));
var G__60321_60890 = new cljs.core.Keyword("epochs","current-match-state","epochs/current-match-state",-1577612281);
var G__60322_60891 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60323_60892 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","epoch-root","epochs/epoch-root",-2097634597)], null);
var G__60324_60893 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60325_60894 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","match-ids","epochs/match-ids",-669448057)], null);
var G__60326_60895 = (function (p__60333,_){
var vec__60334 = p__60333;
var epochs = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60334,(0),null);
var match_ids = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60334,(1),null);
var current_id = new cljs.core.Keyword(null,"current-epoch-id","current-epoch-id",-1357591568).cljs$core$IFn$_invoke$arity$1(epochs);
var match = (((current_id == null))?cljs.core.last(new cljs.core.Keyword(null,"matches","matches",635497998).cljs$core$IFn$_invoke$arity$1(epochs)):(((current_id < cljs.core.first(match_ids)))?cljs.core.first(new cljs.core.Keyword(null,"matches","matches",635497998).cljs$core$IFn$_invoke$arity$1(epochs)):(((current_id > cljs.core.last(match_ids)))?cljs.core.last(new cljs.core.Keyword(null,"matches","matches",635497998).cljs$core$IFn$_invoke$arity$1(epochs)):cljs.core.get.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"matches-by-id","matches-by-id",1749529562).cljs$core$IFn$_invoke$arity$1(epochs),current_id)
)));
return match;
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60321_60890,G__60322_60891,G__60323_60892,G__60324_60893,G__60325_60894,G__60326_60895) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60321_60890,G__60322_60891,G__60323_60892,G__60324_60893,G__60325_60894,G__60326_60895));
var G__60338_60902 = new cljs.core.Keyword("epochs","current-match","epochs/current-match",-1687592087);
var G__60339_60903 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60340_60904 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-match-state","epochs/current-match-state",-1577612281)], null);
var G__60341_60905 = (function (match_state,_){
return new cljs.core.Keyword(null,"match-info","match-info",666319879).cljs$core$IFn$_invoke$arity$1(match_state);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60338_60902,G__60339_60903,G__60340_60904,G__60341_60905) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60338_60902,G__60339_60903,G__60340_60904,G__60341_60905));
var G__60343_60910 = new cljs.core.Keyword("epochs","current-event-trace","epochs/current-event-trace",-1911057851);
var G__60344_60911 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60345_60912 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-match","epochs/current-match",-1687592087)], null);
var G__60346_60913 = (function (match,_){
return day8.re_frame_10x.metamorphic.matched_event(match);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60343_60910,G__60344_60911,G__60345_60912,G__60346_60913) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60343_60910,G__60344_60911,G__60345_60912,G__60346_60913));
var G__60347_60917 = new cljs.core.Keyword("epochs","current-event","epochs/current-event",10990104);
var G__60348_60918 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60349_60919 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-event-trace","epochs/current-event-trace",-1911057851)], null);
var G__60350_60920 = (function (trace,_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(trace,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tags","tags",1771418977),new cljs.core.Keyword(null,"event","event",301435442)], null));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60347_60917,G__60348_60918,G__60349_60919,G__60350_60920) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60347_60917,G__60348_60918,G__60349_60919,G__60350_60920));
var G__60351_60927 = new cljs.core.Keyword("epochs","number-of-matches","epochs/number-of-matches",1018247531);
var G__60352_60928 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60353_60929 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","epoch-root","epochs/epoch-root",-2097634597)], null);
var G__60354_60930 = (function (epochs,_){
return cljs.core.count(cljs.core.get.cljs$core$IFn$_invoke$arity$2(epochs,new cljs.core.Keyword(null,"matches","matches",635497998)));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60351_60927,G__60352_60928,G__60353_60929,G__60354_60930) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60351_60927,G__60352_60928,G__60353_60929,G__60354_60930));
var G__60355_60931 = new cljs.core.Keyword("epochs","current-event-index","epochs/current-event-index",-2026503803);
var G__60356_60932 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60357_60933 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","epoch-root","epochs/epoch-root",-2097634597)], null);
var G__60358_60934 = (function (epochs,_){
return new cljs.core.Keyword(null,"current-epoch-index","current-epoch-index",-903378376).cljs$core$IFn$_invoke$arity$1(epochs);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60355_60931,G__60356_60932,G__60357_60933,G__60358_60934) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60355_60931,G__60356_60932,G__60357_60933,G__60358_60934));
var G__60359_60939 = new cljs.core.Keyword("epochs","current-epoch-id","epochs/current-epoch-id",-482598682);
var G__60360_60940 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60361_60941 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-match","epochs/current-match",-1687592087)], null);
var G__60362_60942 = (function (epochs,_){
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(cljs.core.first(epochs));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60359_60939,G__60360_60940,G__60361_60941,G__60362_60942) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60359_60939,G__60360_60940,G__60361_60941,G__60362_60942));
var G__60363_60944 = new cljs.core.Keyword("epochs","match-ids","epochs/match-ids",-669448057);
var G__60364_60945 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60365_60946 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","epoch-root","epochs/epoch-root",-2097634597)], null);
var G__60366_60947 = (function (epochs){
return new cljs.core.Keyword(null,"match-ids","match-ids",752973161).cljs$core$IFn$_invoke$arity$1(epochs);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60363_60944,G__60364_60945,G__60365_60946,G__60366_60947) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60363_60944,G__60364_60945,G__60365_60946,G__60366_60947));
var G__60367_60951 = new cljs.core.Keyword("epochs","beginning-trace-id","epochs/beginning-trace-id",-1340593887);
var G__60368_60952 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60369_60953 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-match","epochs/current-match",-1687592087)], null);
var G__60370_60954 = (function (match){
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(cljs.core.first(match));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60367_60951,G__60368_60952,G__60369_60953,G__60370_60954) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60367_60951,G__60368_60952,G__60369_60953,G__60370_60954));
var G__60375_60959 = new cljs.core.Keyword("epochs","ending-trace-id","epochs/ending-trace-id",-1920901579);
var G__60376_60960 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60377_60961 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-match","epochs/current-match",-1687592087)], null);
var G__60378_60962 = (function (match){
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(cljs.core.last(match));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60375_60959,G__60376_60960,G__60377_60961,G__60378_60962) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60375_60959,G__60376_60960,G__60377_60961,G__60378_60962));
var G__60381_60965 = new cljs.core.Keyword("epochs","older-epochs-available?","epochs/older-epochs-available?",366051335);
var G__60382_60966 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60383_60967 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-epoch-id","epochs/current-epoch-id",-482598682)], null);
var G__60384_60968 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60385_60969 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","match-ids","epochs/match-ids",-669448057)], null);
var G__60386_60970 = (function (p__60388){
var vec__60389 = p__60388;
var current = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60389,(0),null);
var ids = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60389,(1),null);
return ((((1) < cljs.core.count(ids))) && ((((current == null)) || ((current > cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ids,(0)))))));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60381_60965,G__60382_60966,G__60383_60967,G__60384_60968,G__60385_60969,G__60386_60970) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60381_60965,G__60382_60966,G__60383_60967,G__60384_60968,G__60385_60969,G__60386_60970));
var G__60392_60979 = new cljs.core.Keyword("epochs","newer-epochs-available?","epochs/newer-epochs-available?",-1411103953);
var G__60393_60980 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60394_60981 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-epoch-id","epochs/current-epoch-id",-482598682)], null);
var G__60395_60982 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60396_60983 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","match-ids","epochs/match-ids",-669448057)], null);
var G__60397_60984 = (function (p__60399){
var vec__60401 = p__60399;
var current = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60401,(0),null);
var ids = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60401,(1),null);
return ((((1) < cljs.core.count(ids))) && ((!((current == null)))) && ((current < day8.re_frame_10x.utils.utils.last_in_vec(ids))));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60392_60979,G__60393_60980,G__60394_60981,G__60395_60982,G__60396_60983,G__60397_60984) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60392_60979,G__60393_60980,G__60394_60981,G__60395_60982,G__60396_60983,G__60397_60984));
var G__60404_60991 = new cljs.core.Keyword("timing","total-epoch-time","timing/total-epoch-time",-912032018);
var G__60405_60992 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60406_60993 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769)], null);
var G__60407_60994 = (function (traces){
var start_of_epoch = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(traces,(0));
var end_of_epoch = day8.re_frame_10x.utils.utils.last_in_vec(traces);
return day8.re_frame_10x.metamorphic.elapsed_time(start_of_epoch,end_of_epoch);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60404_60991,G__60405_60992,G__60406_60993,G__60407_60994) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60404_60991,G__60405_60992,G__60406_60993,G__60407_60994));
var G__60412_60999 = new cljs.core.Keyword("timing","animation-frame-traces","timing/animation-frame-traces",394987473);
var G__60413_61000 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60414_61001 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769)], null);
var G__60415_61002 = (function (traces){
return cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (p1__60410_SHARP_){
return ((day8.re_frame_10x.metamorphic.request_animation_frame_QMARK_(p1__60410_SHARP_)) || (day8.re_frame_10x.metamorphic.request_animation_frame_end_QMARK_(p1__60410_SHARP_)));
}),traces);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60412_60999,G__60413_61000,G__60414_61001,G__60415_61002) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60412_60999,G__60413_61000,G__60414_61001,G__60415_61002));
var G__60417_61003 = new cljs.core.Keyword("timing","animation-frame-count","timing/animation-frame-count",-281075152);
var G__60418_61004 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60419_61005 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("timing","animation-frame-traces","timing/animation-frame-traces",394987473)], null);
var G__60420_61006 = (function (frame_traces){
return cljs.core.count(cljs.core.filter.cljs$core$IFn$_invoke$arity$2(day8.re_frame_10x.metamorphic.request_animation_frame_QMARK_,frame_traces));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60417_61003,G__60418_61004,G__60419_61005,G__60420_61006) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60417_61003,G__60418_61004,G__60419_61005,G__60420_61006));
/**
 * Returns the sum of nums. (+) returns nil (not 0 like in cljs.core).
 */
day8.re_frame_10x.subs._PLUS_nil = (function day8$re_frame_10x$subs$_PLUS_nil(var_args){
var G__60429 = arguments.length;
switch (G__60429) {
case 0:
return day8.re_frame_10x.subs._PLUS_nil.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return day8.re_frame_10x.subs._PLUS_nil.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return day8.re_frame_10x.subs._PLUS_nil.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
var args_arr__4757__auto__ = [];
var len__4736__auto___61010 = arguments.length;
var i__4737__auto___61011 = (0);
while(true){
if((i__4737__auto___61011 < len__4736__auto___61010)){
args_arr__4757__auto__.push((arguments[i__4737__auto___61011]));

var G__61012 = (i__4737__auto___61011 + (1));
i__4737__auto___61011 = G__61012;
continue;
} else {
}
break;
}

var argseq__4758__auto__ = (new cljs.core.IndexedSeq(args_arr__4757__auto__.slice((2)),(0),null));
return day8.re_frame_10x.subs._PLUS_nil.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4758__auto__);

}
});

(day8.re_frame_10x.subs._PLUS_nil.cljs$core$IFn$_invoke$arity$0 = (function (){
return null;
}));

(day8.re_frame_10x.subs._PLUS_nil.cljs$core$IFn$_invoke$arity$1 = (function (x){
return x;
}));

(day8.re_frame_10x.subs._PLUS_nil.cljs$core$IFn$_invoke$arity$2 = (function (x,y){
return (x + y);
}));

(day8.re_frame_10x.subs._PLUS_nil.cljs$core$IFn$_invoke$arity$variadic = (function (x,y,more){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._PLUS_,(x + y),more);
}));

/** @this {Function} */
(day8.re_frame_10x.subs._PLUS_nil.cljs$lang$applyTo = (function (seq60426){
var G__60427 = cljs.core.first(seq60426);
var seq60426__$1 = cljs.core.next(seq60426);
var G__60428 = cljs.core.first(seq60426__$1);
var seq60426__$2 = cljs.core.next(seq60426__$1);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__60427,G__60428,seq60426__$2);
}));

(day8.re_frame_10x.subs._PLUS_nil.cljs$lang$maxFixedArity = (2));

var G__60440_61019 = new cljs.core.Keyword("timing","animation-frame-time","timing/animation-frame-time",1905393593);
var G__60441_61020 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60442_61021 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("timing","animation-frame-traces","timing/animation-frame-traces",394987473)], null);
var G__60443_61022 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60444_61023 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769)], null);
var G__60445_61024 = (function (p__60446,p__60447){
var vec__60448 = p__60446;
var af_start_end = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60448,(0),null);
var epoch_traces = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60448,(1),null);
var vec__60451 = p__60447;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60451,(0),null);
var frame_number = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60451,(1),null);
var frame_pairs = cljs.core.partition.cljs$core$IFn$_invoke$arity$2((2),af_start_end);
var vec__60455 = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(frame_pairs,(frame_number - (1)));
var start = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60455,(0),null);
var end = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60455,(1),null);
var af_traces = cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,day8.re_frame_10x.metamorphic.id_between_xf(new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(start),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(end)),epoch_traces);
var total_time = day8.re_frame_10x.metamorphic.elapsed_time(start,end);
var subs_time = cljs.core.transduce.cljs$core$IFn$_invoke$arity$3(cljs.core.comp.cljs$core$IFn$_invoke$arity$2(cljs.core.filter.cljs$core$IFn$_invoke$arity$1(day8.re_frame_10x.metamorphic.subscription_QMARK_),cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"duration","duration",1444101068))),day8.re_frame_10x.subs._PLUS_nil,af_traces);
var render_time = cljs.core.transduce.cljs$core$IFn$_invoke$arity$3(cljs.core.comp.cljs$core$IFn$_invoke$arity$2(cljs.core.filter.cljs$core$IFn$_invoke$arity$1(day8.re_frame_10x.metamorphic.render_QMARK_),cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"duration","duration",1444101068))),day8.re_frame_10x.subs._PLUS_nil,af_traces);
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword("timing","animation-frame-total","timing/animation-frame-total",-1923627693),total_time,new cljs.core.Keyword("timing","animation-frame-subs","timing/animation-frame-subs",128098226),subs_time,new cljs.core.Keyword("timing","animation-frame-render","timing/animation-frame-render",1587090151),render_time,new cljs.core.Keyword("timing","animation-frame-misc","timing/animation-frame-misc",1250948573),((total_time - subs_time) - render_time)], null);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60440_61019,G__60441_61020,G__60442_61021,G__60443_61022,G__60444_61023,G__60445_61024) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60440_61019,G__60441_61020,G__60442_61021,G__60443_61022,G__60444_61023,G__60445_61024));
var G__60460_61037 = new cljs.core.Keyword("timing","event-processing-time","timing/event-processing-time",-178806297);
var G__60461_61038 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60462_61039 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-match-state","epochs/current-match-state",-1577612281)], null);
var G__60463_61040 = (function (match){
var map__60464 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(match,new cljs.core.Keyword(null,"timing","timing",-1849225195));
var map__60464__$1 = (((((!((map__60464 == null))))?(((((map__60464.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__60464.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__60464):map__60464);
var event_time = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__60464__$1,new cljs.core.Keyword("re-frame","event-time","re-frame/event-time",-1349372188));
var event_handler_time = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__60464__$1,new cljs.core.Keyword("re-frame","event-handler-time","re-frame/event-handler-time",1278050644));
var event_dofx_time = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__60464__$1,new cljs.core.Keyword("re-frame","event-dofx-time","re-frame/event-dofx-time",650880716));
var event_run_time = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__60464__$1,new cljs.core.Keyword("re-frame","event-run-time","re-frame/event-run-time",1941554897));
var remaining_interceptors = ((event_time - event_handler_time) - event_dofx_time);
return new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword("timing","event-total","timing/event-total",127009054),event_run_time,new cljs.core.Keyword("timing","event-handler","timing/event-handler",383979787),event_handler_time,new cljs.core.Keyword("timing","event-effects","timing/event-effects",-1312815404),event_dofx_time,new cljs.core.Keyword("timing","event-interceptors","timing/event-interceptors",-113832767),remaining_interceptors,new cljs.core.Keyword("timing","event-misc","timing/event-misc",1534165210),((event_run_time - event_handler_time) - event_dofx_time)], null);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60460_61037,G__60461_61038,G__60462_61039,G__60463_61040) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60460_61037,G__60461_61038,G__60462_61039,G__60463_61040));
var G__60469_61046 = new cljs.core.Keyword("timing","render-time","timing/render-time",-2042272059);
var G__60470_61047 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60471_61048 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769)], null);
var G__60472_61049 = (function (traces){
var start_of_render = cljs.core.first(cljs.core.filter.cljs$core$IFn$_invoke$arity$2(day8.re_frame_10x.metamorphic.request_animation_frame_QMARK_,traces));
var end_of_epoch = day8.re_frame_10x.utils.utils.last_in_vec(traces);
return day8.re_frame_10x.metamorphic.elapsed_time(start_of_render,end_of_epoch);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60469_61046,G__60470_61047,G__60471_61048,G__60472_61049) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60469_61046,G__60470_61047,G__60471_61048,G__60472_61049));
var G__60473_61057 = new cljs.core.Keyword("timing","data-available?","timing/data-available?",544526662);
var G__60474_61058 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60475_61059 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769)], null);
var G__60476_61060 = (function (traces){
return (!(cljs.core.empty_QMARK_(traces)));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60473_61057,G__60474_61058,G__60475_61059,G__60476_61060) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60473_61057,G__60474_61058,G__60475_61059,G__60476_61060));
var G__60478_61066 = new cljs.core.Keyword("subs","root","subs/root",-432796018);
var G__60479_61067 = (function (db,_){
return new cljs.core.Keyword(null,"subs","subs",-186681991).cljs$core$IFn$_invoke$arity$1(db);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60478_61066,G__60479_61067) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60478_61066,G__60479_61067));
var G__60480_61071 = new cljs.core.Keyword("subs","all-sub-traces","subs/all-sub-traces",1860573308);
var G__60481_61072 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60482_61073 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769)], null);
var G__60483_61074 = (function (traces){
return cljs.core.filter.cljs$core$IFn$_invoke$arity$2(day8.re_frame_10x.metamorphic.subscription_QMARK_,traces);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60480_61071,G__60481_61072,G__60482_61073,G__60483_61074) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60480_61071,G__60481_61072,G__60482_61073,G__60483_61074));
var G__60485_61078 = new cljs.core.Keyword("subs","subscription-info","subs/subscription-info",-1799592687);
var G__60486_61079 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60487_61080 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","epoch-root","epochs/epoch-root",-2097634597)], null);
var G__60488_61081 = (function (epoch){
return new cljs.core.Keyword(null,"subscription-info","subscription-info",-1785424092).cljs$core$IFn$_invoke$arity$1(epoch);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60485_61078,G__60486_61079,G__60487_61080,G__60488_61081) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60485_61078,G__60486_61079,G__60487_61080,G__60488_61081));
var G__60489_61087 = new cljs.core.Keyword("subs","sub-state","subs/sub-state",-2134091240);
var G__60490_61088 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60491_61089 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","epoch-root","epochs/epoch-root",-2097634597)], null);
var G__60492_61090 = (function (epochs){
return new cljs.core.Keyword(null,"sub-state","sub-state",-2129237981).cljs$core$IFn$_invoke$arity$1(epochs);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60489_61087,G__60490_61088,G__60491_61089,G__60492_61090) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60489_61087,G__60490_61088,G__60491_61089,G__60492_61090));
var G__60493_61093 = new cljs.core.Keyword("subs","current-epoch-sub-state","subs/current-epoch-sub-state",-1337816884);
var G__60494_61094 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60495_61095 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("epochs","current-match-state","epochs/current-match-state",-1577612281)], null);
var G__60496_61096 = (function (match_state){
return new cljs.core.Keyword(null,"sub-state","sub-state",-2129237981).cljs$core$IFn$_invoke$arity$1(match_state);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60493_61093,G__60494_61094,G__60495_61095,G__60496_61096) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60493_61093,G__60494_61094,G__60495_61095,G__60496_61096));
day8.re_frame_10x.subs.string_BANG_ = cljs.spec.alpha.and_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","not","cljs.core/not",100665144,null),cljs.core.list(new cljs.core.Symbol("cljs.core","empty?","cljs.core/empty?",1866613644,null),new cljs.core.Symbol(null,"%","%",-950237169,null))))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.string_QMARK_,(function (p1__60497_SHARP_){
return (!(cljs.core.empty_QMARK_(p1__60497_SHARP_)));
})], null),null);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","id","sub/id",-1388418028),new cljs.core.Symbol("day8.re-frame-10x.subs","string!","day8.re-frame-10x.subs/string!",1827585601,null),day8.re_frame_10x.subs.string_BANG_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","reagent-id","sub/reagent-id",-767008039),new cljs.core.Symbol("day8.re-frame-10x.subs","string!","day8.re-frame-10x.subs/string!",1827585601,null),day8.re_frame_10x.subs.string_BANG_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","run-types","sub/run-types",-1599499164),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword("sub","run","sub/run",-1821315581),"null",new cljs.core.Keyword("sub","not-run","sub/not-run",-848603451),"null",new cljs.core.Keyword("sub","create","sub/create",-1301317560),"null",new cljs.core.Keyword("sub","dispose","sub/dispose",365440536),"null"], null), null),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword("sub","run","sub/run",-1821315581),null,new cljs.core.Keyword("sub","not-run","sub/not-run",-848603451),null,new cljs.core.Keyword("sub","create","sub/create",-1301317560),null,new cljs.core.Keyword("sub","dispose","sub/dispose",365440536),null], null), null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","order","sub/order",-1254825160),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","nilable","cljs.spec.alpha/nilable",1628308748,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("sub","run-types","sub/run-types",-1599499164))),cljs.spec.alpha.nilable_impl(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("sub","run-types","sub/run-types",-1599499164)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("sub","run-types","sub/run-types",-1599499164),new cljs.core.Keyword("sub","run-types","sub/run-types",-1599499164),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),null,new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__60502){
return cljs.core.coll_QMARK_(G__60502);
}),new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("sub","run-types","sub/run-types",-1599499164))], null),null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","layer","sub/layer",-1601935853),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","nilable","cljs.spec.alpha/nilable",1628308748,null),new cljs.core.Symbol("cljs.core","pos-int?","cljs.core/pos-int?",-2115888030,null)),cljs.spec.alpha.nilable_impl(new cljs.core.Symbol("cljs.core","pos-int?","cljs.core/pos-int?",-2115888030,null),cljs.core.pos_int_QMARK_,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","path-data","sub/path-data",1442158815),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","path","sub/path",-188044288),new cljs.core.Symbol("day8.re-frame-10x.subs","string!","day8.re-frame-10x.subs/string!",1827585601,null),day8.re_frame_10x.subs.string_BANG_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","value","sub/value",307444137),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("sub","previous-value","sub/previous-value",-1638946429),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("subs","view-panel-sub","subs/view-panel-sub",-1872730057),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sub","id","sub/id",-1388418028),new cljs.core.Keyword("sub","reagent-id","sub/reagent-id",-767008039),new cljs.core.Keyword("sub","order","sub/order",-1254825160),new cljs.core.Keyword("sub","layer","sub/layer",-1601935853),new cljs.core.Keyword("sub","path-data","sub/path-data",1442158815),new cljs.core.Keyword("sub","path","sub/path",-188044288)], null),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sub","value","sub/value",307444137),new cljs.core.Keyword("sub","previous-value","sub/previous-value",-1638946429)], null)),cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sub","id","sub/id",-1388418028),new cljs.core.Keyword("sub","reagent-id","sub/reagent-id",-767008039),new cljs.core.Keyword("sub","order","sub/order",-1254825160),new cljs.core.Keyword("sub","layer","sub/layer",-1601935853),new cljs.core.Keyword("sub","path-data","sub/path-data",1442158815),new cljs.core.Keyword("sub","path","sub/path",-188044288)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sub","value","sub/value",307444137),new cljs.core.Keyword("sub","previous-value","sub/previous-value",-1638946429)], null),null,new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__60505){
return cljs.core.map_QMARK_(G__60505);
}),(function (G__60505){
return cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"id","id",-1388402092));
}),(function (G__60505){
return cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"reagent-id","reagent-id",-766893415));
}),(function (G__60505){
return cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"order","order",-1254677256));
}),(function (G__60505){
return cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"layer","layer",-1601820589));
}),(function (G__60505){
return cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"path-data","path-data",1441254047));
}),(function (G__60505){
return cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"path","path",-188191168));
})], null),(function (G__60505){
return ((cljs.core.map_QMARK_(G__60505)) && (cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"id","id",-1388402092))) && (cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"reagent-id","reagent-id",-766893415))) && (cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"order","order",-1254677256))) && (cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"layer","layer",-1601820589))) && (cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"path-data","path-data",1441254047))) && (cljs.core.contains_QMARK_(G__60505,new cljs.core.Keyword(null,"path","path",-188191168))));
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Keyword(null,"previous-value","previous-value",-1638799677)], null),new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sub","id","sub/id",-1388418028),new cljs.core.Keyword("sub","reagent-id","sub/reagent-id",-767008039),new cljs.core.Keyword("sub","order","sub/order",-1254825160),new cljs.core.Keyword("sub","layer","sub/layer",-1601935853),new cljs.core.Keyword("sub","path-data","sub/path-data",1442158815),new cljs.core.Keyword("sub","path","sub/path",-188044288)], null),null,new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"reagent-id","reagent-id",-766893415),new cljs.core.Keyword(null,"order","order",-1254677256),new cljs.core.Keyword(null,"layer","layer",-1601820589),new cljs.core.Keyword(null,"path-data","path-data",1441254047),new cljs.core.Keyword(null,"path","path",-188191168)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sub","value","sub/value",307444137),new cljs.core.Keyword("sub","previous-value","sub/previous-value",-1638946429)], null),new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword(null,"id","id",-1388402092))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword(null,"reagent-id","reagent-id",-766893415))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword(null,"order","order",-1254677256))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword(null,"layer","layer",-1601820589))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword(null,"path-data","path-data",1441254047))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword(null,"path","path",-188191168)))], null),null])));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("subs","view-subs","subs/view-subs",253084832),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("subs","view-panel-sub","subs/view-panel-sub",-1872730057)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("subs","view-panel-sub","subs/view-panel-sub",-1872730057),new cljs.core.Keyword("subs","view-panel-sub","subs/view-panel-sub",-1872730057),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),null,new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__60524){
return cljs.core.coll_QMARK_(G__60524);
}),new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("subs","view-panel-sub","subs/view-panel-sub",-1872730057))], null),null));
day8.re_frame_10x.subs.sub_type_value = (function day8$re_frame_10x$subs$sub_type_value(sub_type){
var G__60527 = sub_type;
var G__60527__$1 = (((G__60527 instanceof cljs.core.Keyword))?G__60527.fqn:null);
switch (G__60527__$1) {
case "sub/create":
return (5);

break;
case "sub/run":
return (4);

break;
case "sub/dispose":
return (3);

break;
case "sub/not-run":
return (2);

break;
default:
return (1);

}
});
/**
 * Calculate a sorting value for a series of subscription trace types.
 */
day8.re_frame_10x.subs.accumulate_sub_value = (function day8$re_frame_10x$subs$accumulate_sub_value(order){
var exp = (3);
var total = (0);
var order__$1 = order;
while(true){
var temp__5733__auto__ = cljs.core.first(order__$1);
if(cljs.core.truth_(temp__5733__auto__)){
var sub_type = temp__5733__auto__;
var G__61138 = (exp - (1));
var G__61139 = (total + (day8.re_frame_10x.subs.sub_type_value(sub_type) * Math.pow((10),exp)));
var G__61140 = cljs.core.rest(order__$1);
exp = G__61138;
total = G__61139;
order__$1 = G__61140;
continue;
} else {
return total;
}
break;
}
});
day8.re_frame_10x.subs.accumulate_sub_value_memoized = cljs.core.memoize(day8.re_frame_10x.subs.accumulate_sub_value);
day8.re_frame_10x.subs.sub_sort_val = (function day8$re_frame_10x$subs$sub_sort_val(order_x,order_y){
return cljs.core.compare((day8.re_frame_10x.subs.accumulate_sub_value_memoized.cljs$core$IFn$_invoke$arity$1 ? day8.re_frame_10x.subs.accumulate_sub_value_memoized.cljs$core$IFn$_invoke$arity$1(order_y) : day8.re_frame_10x.subs.accumulate_sub_value_memoized.call(null,order_y)),(day8.re_frame_10x.subs.accumulate_sub_value_memoized.cljs$core$IFn$_invoke$arity$1 ? day8.re_frame_10x.subs.accumulate_sub_value_memoized.cljs$core$IFn$_invoke$arity$1(order_x) : day8.re_frame_10x.subs.accumulate_sub_value_memoized.call(null,order_x)));
});
day8.re_frame_10x.subs.sub_op_type__GT_type = (function day8$re_frame_10x$subs$sub_op_type__GT_type(t){
var G__60534 = new cljs.core.Keyword(null,"op-type","op-type",-1636141668).cljs$core$IFn$_invoke$arity$1(t);
var G__60534__$1 = (((G__60534 instanceof cljs.core.Keyword))?G__60534.fqn:null);
switch (G__60534__$1) {
case "sub/create":
return new cljs.core.Keyword(null,"created","created",-704993748);

break;
case "sub/run":
return new cljs.core.Keyword(null,"re-run","re-run",-1300247905);

break;
case "sub/dispose":
return new cljs.core.Keyword(null,"destroyed","destroyed",-427566535);

break;
default:
return new cljs.core.Keyword(null,"not-run","not-run",-848069371);

}
});
/**
 * Returns sub info prepared for rendering in pods
 */
day8.re_frame_10x.subs.prepare_pod_info = (function day8$re_frame_10x$subs$prepare_pod_info(p__60535,p__60536){
var vec__60537 = p__60535;
var sub_info = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60537,(0),null);
var sub_state = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60537,(1),null);
var vec__60540 = p__60536;
var subscription = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60540,(0),null);
var remove_fn = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(subscription,new cljs.core.Keyword("subs","intra-epoch-subs","subs/intra-epoch-subs",-1298574944)))?(function (me){
return (new cljs.core.Keyword(null,"order","order",-1254677256).cljs$core$IFn$_invoke$arity$1(cljs.core.val(me)) == null);
}):cljs.core.constantly(false));
var subx = cljs.core.sort_by.cljs$core$IFn$_invoke$arity$3(new cljs.core.Keyword(null,"order","order",-1254677256),day8.re_frame_10x.subs.sub_sort_val,cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (me){
var state = cljs.core.val(me);
var subscription__$1 = new cljs.core.Keyword(null,"subscription","subscription",1949009182).cljs$core$IFn$_invoke$arity$1(state);
var sub = new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"id","id",-1388402092),cljs.core.key(me),new cljs.core.Keyword(null,"reagent-id","reagent-id",-766893415),cljs.core.key(me),new cljs.core.Keyword(null,"layer","layer",-1601820589),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(sub_info,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.first(subscription__$1),new cljs.core.Keyword(null,"layer","layer",-1601820589)], null)),new cljs.core.Keyword(null,"path-data","path-data",1441254047),subscription__$1,new cljs.core.Keyword(null,"path","path",-188191168),cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([subscription__$1], 0)),new cljs.core.Keyword(null,"order","order",-1254677256),(function (){var or__4126__auto__ = new cljs.core.Keyword(null,"order","order",-1254677256).cljs$core$IFn$_invoke$arity$1(state);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("sub","not-run","sub/not-run",-848603451)], null);
}
})(),new cljs.core.Keyword("sub","traits","sub/traits",1778340671),new cljs.core.Keyword("sub","traits","sub/traits",1778340671).cljs$core$IFn$_invoke$arity$1(state)], null);
var sub__$1 = ((cljs.core.contains_QMARK_(state,new cljs.core.Keyword(null,"value","value",305978217)))?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(sub,new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Keyword(null,"value","value",305978217).cljs$core$IFn$_invoke$arity$1(state)):sub);
var sub__$2 = ((cljs.core.contains_QMARK_(state,new cljs.core.Keyword(null,"previous-value","previous-value",-1638799677)))?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(sub__$1,new cljs.core.Keyword(null,"previous-value","previous-value",-1638799677),new cljs.core.Keyword(null,"previous-value","previous-value",-1638799677).cljs$core$IFn$_invoke$arity$1(state)):sub__$1);
return sub__$2;
}),cljs.core.remove.cljs$core$IFn$_invoke$arity$2(remove_fn,sub_state)));
return subx;
});
var G__60547_61168 = new cljs.core.Keyword("subs","pre-epoch-state","subs/pre-epoch-state",836844033);
var G__60548_61169 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60549_61170 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","current-epoch-sub-state","subs/current-epoch-sub-state",-1337816884)], null);
var G__60550_61171 = (function (sub_state){
return new cljs.core.Keyword(null,"pre-epoch-state","pre-epoch-state",834094164).cljs$core$IFn$_invoke$arity$1(sub_state);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60547_61168,G__60548_61169,G__60549_61170,G__60550_61171) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60547_61168,G__60548_61169,G__60549_61170,G__60550_61171));
var G__60552_61174 = new cljs.core.Keyword("subs","reaction-state","subs/reaction-state",955533938);
var G__60553_61175 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60554_61176 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","current-epoch-sub-state","subs/current-epoch-sub-state",-1337816884)], null);
var G__60555_61177 = (function (sub_state){
return new cljs.core.Keyword(null,"reaction-state","reaction-state",958292039).cljs$core$IFn$_invoke$arity$1(sub_state);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60552_61174,G__60553_61175,G__60554_61176,G__60555_61177) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60552_61174,G__60553_61175,G__60554_61176,G__60555_61177));
var G__60558_61179 = new cljs.core.Keyword("subs","intra-epoch-subs","subs/intra-epoch-subs",-1298574944);
var G__60559_61180 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60560_61181 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","subscription-info","subs/subscription-info",-1799592687)], null);
var G__60561_61182 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60562_61183 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","pre-epoch-state","subs/pre-epoch-state",836844033)], null);
var G__60563_61184 = day8.re_frame_10x.subs.prepare_pod_info;
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60558_61179,G__60559_61180,G__60560_61181,G__60561_61182,G__60562_61183,G__60563_61184) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60558_61179,G__60559_61180,G__60560_61181,G__60561_61182,G__60562_61183,G__60563_61184));
var G__60564_61188 = new cljs.core.Keyword("subs","all-subs","subs/all-subs",1599770325);
var G__60565_61189 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60566_61190 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","subscription-info","subs/subscription-info",-1799592687)], null);
var G__60567_61191 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60568_61192 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","reaction-state","subs/reaction-state",955533938)], null);
var G__60569_61193 = day8.re_frame_10x.subs.prepare_pod_info;
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60564_61188,G__60565_61189,G__60566_61190,G__60567_61191,G__60568_61192,G__60569_61193) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60564_61188,G__60565_61189,G__60566_61190,G__60567_61191,G__60568_61192,G__60569_61193));
var G__60570_61198 = new cljs.core.Keyword("subs","filter-str","subs/filter-str",1975403754);
var G__60571_61199 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60572_61200 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","root","subs/root",-432796018)], null);
var G__60573_61201 = (function (root,_){
return new cljs.core.Keyword(null,"filter-str","filter-str",1974484789).cljs$core$IFn$_invoke$arity$1(root);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60570_61198,G__60571_61199,G__60572_61200,G__60573_61201) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60570_61198,G__60571_61199,G__60572_61200,G__60573_61201));
var G__60576_61203 = new cljs.core.Keyword("subs","visible-subs","subs/visible-subs",-1536647862);
var G__60577_61204 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60578_61205 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","all-subs","subs/all-subs",1599770325)], null);
var G__60579_61206 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60580_61207 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","ignore-unchanged-l2-subs?","subs/ignore-unchanged-l2-subs?",-1438622704)], null);
var G__60581_61208 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60582_61209 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","filter-str","subs/filter-str",1975403754)], null);
var G__60583_61210 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60584_61211 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","sub-pins","subs/sub-pins",1713411647)], null);
var G__60585_61212 = (function (p__60586){
var vec__60587 = p__60586;
var all_subs = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60587,(0),null);
var ignore_unchanged_l2_QMARK_ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60587,(1),null);
var filter_str = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60587,(2),null);
var pins = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60587,(3),null);
var compare_fn = (function (s1,s2){
var p1 = cljs.core.boolean$(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(pins,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(s1),new cljs.core.Keyword(null,"pin?","pin?",-1347894609)], null)));
var p2 = cljs.core.boolean$(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(pins,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(s2),new cljs.core.Keyword(null,"pin?","pin?",-1347894609)], null)));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(p1,p2)){
return cljs.core.compare(new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(s1),new cljs.core.Keyword(null,"path","path",-188191168).cljs$core$IFn$_invoke$arity$1(s2));
} else {
return p1;
}
});
var G__60592 = cljs.core.sort.cljs$core$IFn$_invoke$arity$2(compare_fn,all_subs);
var G__60592__$1 = (cljs.core.truth_(ignore_unchanged_l2_QMARK_)?cljs.core.remove.cljs$core$IFn$_invoke$arity$2(day8.re_frame_10x.metamorphic.unchanged_l2_subscription_QMARK_,G__60592):G__60592);
if(cljs.core.truth_(cljs.core.not_empty(filter_str))){
return cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (p__60594){
var map__60595 = p__60594;
var map__60595__$1 = (((((!((map__60595 == null))))?(((((map__60595.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__60595.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__60595):map__60595);
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__60595__$1,new cljs.core.Keyword(null,"path","path",-188191168));
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__60595__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var or__4126__auto__ = clojure.string.includes_QMARK_(path,filter_str);
if(or__4126__auto__){
return or__4126__auto__;
} else {
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(pins,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [id,new cljs.core.Keyword(null,"pin?","pin?",-1347894609)], null));
}
}),G__60592__$1);
} else {
return G__60592__$1;
}
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$10 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$10(G__60576_61203,G__60577_61204,G__60578_61205,G__60579_61206,G__60580_61207,G__60581_61208,G__60582_61209,G__60583_61210,G__60584_61211,G__60585_61212) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60576_61203,G__60577_61204,G__60578_61205,G__60579_61206,G__60580_61207,G__60581_61208,G__60582_61209,G__60583_61210,G__60584_61211,G__60585_61212));
var G__60601_61236 = new cljs.core.Keyword("subs","sub-counts","subs/sub-counts",289359284);
var G__60602_61237 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60603_61238 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","visible-subs","subs/visible-subs",-1536647862)], null);
var G__60604_61239 = (function (subs,_){
return cljs.core.frequencies(cljs.core.mapcat.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"order","order",-1254677256),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([subs], 0)));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60601_61236,G__60602_61237,G__60603_61238,G__60604_61239) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60601_61236,G__60602_61237,G__60603_61238,G__60604_61239));
var G__60605_61241 = new cljs.core.Keyword("subs","created-count","subs/created-count",-1738545579);
var G__60606_61242 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60607_61243 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","sub-counts","subs/sub-counts",289359284)], null);
var G__60608_61244 = (function (counts){
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(counts,new cljs.core.Keyword("sub","create","sub/create",-1301317560),(0));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60605_61241,G__60606_61242,G__60607_61243,G__60608_61244) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60605_61241,G__60606_61242,G__60607_61243,G__60608_61244));
var G__60614_61249 = new cljs.core.Keyword("subs","re-run-count","subs/re-run-count",603750522);
var G__60615_61250 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60616_61251 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","sub-counts","subs/sub-counts",289359284)], null);
var G__60617_61252 = (function (counts){
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(counts,new cljs.core.Keyword("sub","run","sub/run",-1821315581),(0));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60614_61249,G__60615_61250,G__60616_61251,G__60617_61252) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60614_61249,G__60615_61250,G__60616_61251,G__60617_61252));
var G__60618_61253 = new cljs.core.Keyword("subs","destroyed-count","subs/destroyed-count",-218890338);
var G__60619_61254 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60620_61255 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","sub-counts","subs/sub-counts",289359284)], null);
var G__60621_61256 = (function (counts){
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(counts,new cljs.core.Keyword("sub","dispose","sub/dispose",365440536),(0));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60618_61253,G__60619_61254,G__60620_61255,G__60621_61256) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60618_61253,G__60619_61254,G__60620_61255,G__60621_61256));
var G__60623_61257 = new cljs.core.Keyword("subs","not-run-count","subs/not-run-count",-133338676);
var G__60625_61258 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60626_61259 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","sub-counts","subs/sub-counts",289359284)], null);
var G__60627_61260 = (function (counts){
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(counts,new cljs.core.Keyword("sub","not-run","sub/not-run",-848603451),(0));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60623_61257,G__60625_61258,G__60626_61259,G__60627_61260) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60623_61257,G__60625_61258,G__60626_61259,G__60627_61260));
var G__60628_61262 = new cljs.core.Keyword("subs","unchanged-l2-subs-count","subs/unchanged-l2-subs-count",-1997261328);
var G__60629_61263 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60630_61264 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","all-subs","subs/all-subs",1599770325)], null);
var G__60631_61265 = (function (subs){
return cljs.core.count(cljs.core.filter.cljs$core$IFn$_invoke$arity$2(day8.re_frame_10x.metamorphic.unchanged_l2_subscription_QMARK_,subs));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60628_61262,G__60629_61263,G__60630_61264,G__60631_61265) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60628_61262,G__60629_61263,G__60630_61264,G__60631_61265));
var G__60632_61266 = new cljs.core.Keyword("subs","ignore-unchanged-l2-subs?","subs/ignore-unchanged-l2-subs?",-1438622704);
var G__60633_61267 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60634_61268 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","root","subs/root",-432796018)], null);
var G__60635_61269 = (function (subs,_){
return new cljs.core.Keyword(null,"ignore-unchanged-subs?","ignore-unchanged-subs?",125806160).cljs$core$IFn$_invoke$arity$2(subs,true);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60632_61266,G__60633_61267,G__60634_61268,G__60635_61269) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60632_61266,G__60633_61267,G__60634_61268,G__60635_61269));
var G__60636_61271 = new cljs.core.Keyword("subs","sub-expansions","subs/sub-expansions",-547974030);
var G__60637_61272 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60638_61273 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","root","subs/root",-432796018)], null);
var G__60639_61274 = (function (subs,_){
return new cljs.core.Keyword(null,"expansions","expansions",533713877).cljs$core$IFn$_invoke$arity$1(subs);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60636_61271,G__60637_61272,G__60638_61273,G__60639_61274) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60636_61271,G__60637_61272,G__60638_61273,G__60639_61274));
var G__60640_61282 = new cljs.core.Keyword("subs","sub-pins","subs/sub-pins",1713411647);
var G__60641_61283 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60642_61284 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("subs","root","subs/root",-432796018)], null);
var G__60643_61285 = (function (subs,_){
return new cljs.core.Keyword(null,"pinned","pinned",-1216085339).cljs$core$IFn$_invoke$arity$1(subs);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60640_61282,G__60641_61283,G__60642_61284,G__60643_61285) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60640_61282,G__60641_61283,G__60642_61284,G__60643_61285));
var G__60644_61286 = new cljs.core.Keyword("code","root","code/root",-432838104);
var G__60645_61287 = (function (db,_){
return new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(db);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60644_61286,G__60645_61287) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60644_61286,G__60645_61287));
var G__60650_61289 = new cljs.core.Keyword("code","current-code","code/current-code",17389180);
var G__60651_61290 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60652_61291 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("traces","current-event-traces","traces/current-event-traces",1386605769)], null);
var G__60653_61292 = (function (traces,_){
return cljs.core.keep_indexed.cljs$core$IFn$_invoke$arity$2((function (i,trace){
var temp__5739__auto__ = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(trace,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tags","tags",1771418977),new cljs.core.Keyword(null,"code","code",1586293142)], null));
if((temp__5739__auto__ == null)){
return null;
} else {
var code = temp__5739__auto__;
return new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"id","id",-1388402092),i,new cljs.core.Keyword(null,"trace-id","trace-id",681947249),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(trace),new cljs.core.Keyword(null,"title","title",636505583),cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"op-type","op-type",-1636141668).cljs$core$IFn$_invoke$arity$1(trace)], 0)),new cljs.core.Keyword(null,"code","code",1586293142),cljs.core.vec(cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2((function (i__$1,code__$1){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(code__$1,new cljs.core.Keyword(null,"id","id",-1388402092),i__$1);
}),code)),new cljs.core.Keyword(null,"form","form",-1624062471),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(trace,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tags","tags",1771418977),new cljs.core.Keyword(null,"form","form",-1624062471)], null))], null);
}
}),traces);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60650_61289,G__60651_61290,G__60652_61291,G__60653_61292) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60650_61289,G__60651_61290,G__60652_61291,G__60653_61292));
var G__60654_61294 = new cljs.core.Keyword("code","current-form","code/current-form",1888662531);
var G__60655_61295 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60656_61296 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("code","current-code","code/current-code",17389180)], null);
var G__60657_61297 = (function (code,_){
return new cljs.core.Keyword(null,"form","form",-1624062471).cljs$core$IFn$_invoke$arity$1(cljs.core.first(code));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60654_61294,G__60655_61295,G__60656_61296,G__60657_61297) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60654_61294,G__60655_61295,G__60656_61296,G__60657_61297));
var G__60658_61298 = new cljs.core.Keyword("code","current-zprint-form","code/current-zprint-form",-1297458353);
var G__60659_61299 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60660_61300 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("code","current-form","code/current-form",1888662531)], null);
var G__60661_61301 = (function (form,_){
return zprint.core.zprint_str(form);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60658_61298,G__60659_61299,G__60660_61300,G__60661_61301) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60658_61298,G__60659_61299,G__60660_61300,G__60661_61301));
var G__60662_61302 = new cljs.core.Keyword("code","execution-order?","code/execution-order?",-1342853507);
var G__60663_61303 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60664_61304 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("code","root","code/root",-432838104)], null);
var G__60665_61305 = (function (code,_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(code,new cljs.core.Keyword(null,"execution-order?","execution-order?",-1342177142),true);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60662_61302,G__60663_61303,G__60664_61304,G__60665_61305) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60662_61302,G__60663_61303,G__60664_61304,G__60665_61305));
var G__60667_61308 = new cljs.core.Keyword("code","code-open?","code/code-open?",1212488779);
var G__60668_61309 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60669_61310 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("code","root","code/root",-432838104)], null);
var G__60670_61311 = (function (code,_){
return new cljs.core.Keyword(null,"code-open?","code-open?",1228336744).cljs$core$IFn$_invoke$arity$1(code);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60667_61308,G__60668_61309,G__60669_61310,G__60670_61311) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60667_61308,G__60668_61309,G__60669_61310,G__60670_61311));
var G__60672_61312 = new cljs.core.Keyword("code","highlighted-form","code/highlighted-form",-1258376614);
var G__60673_61313 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60674_61314 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("code","root","code/root",-432838104)], null);
var G__60675_61315 = (function (code,_){
return new cljs.core.Keyword(null,"highlighted-form","highlighted-form",-1255288753).cljs$core$IFn$_invoke$arity$1(code);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60672_61312,G__60673_61313,G__60674_61314,G__60675_61315) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60672_61312,G__60673_61313,G__60674_61314,G__60675_61315));
var G__60677_61316 = new cljs.core.Keyword("code","show-all-code?","code/show-all-code?",162236201);
var G__60678_61317 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60679_61318 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("code","root","code/root",-432838104)], null);
var G__60680_61319 = (function (code,_){
return new cljs.core.Keyword(null,"show-all-code?","show-all-code?",159571286).cljs$core$IFn$_invoke$arity$1(code);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60677_61316,G__60678_61317,G__60679_61318,G__60680_61319) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60677_61316,G__60678_61317,G__60679_61318,G__60680_61319));
var G__60682_61321 = new cljs.core.Keyword("code","repl-msg-state","code/repl-msg-state",-519328495);
var G__60683_61322 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60684_61323 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("code","root","code/root",-432838104)], null);
var G__60685_61324 = (function (code,_){
return new cljs.core.Keyword(null,"repl-msg-state","repl-msg-state",-522274040).cljs$core$IFn$_invoke$arity$1(code);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60682_61321,G__60683_61322,G__60684_61323,G__60685_61324) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60682_61321,G__60683_61322,G__60684_61323,G__60685_61324));
day8.re_frame_10x.subs.canvas = document.createElement("canvas");
var G__60687_61328 = new cljs.core.Keyword("code","single-character-width","code/single-character-width",-207718647);
var G__60689_61329 = (function (_,___$1){
var context = day8.re_frame_10x.subs.canvas.getContext("2d");
(context.font = "monospace 1em");

return context.measureText("T").width;
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60687_61328,G__60689_61329) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60687_61328,G__60689_61329));
var G__60690_61330 = new cljs.core.Keyword("code","max-column-width","code/max-column-width",1552872919);
var G__60691_61331 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60692_61332 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("settings","window-width-rounded","settings/window-width-rounded",-924202629),(100)], null);
var G__60693_61333 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60694_61334 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("code","single-character-width","code/single-character-width",-207718647)], null);
var G__60695_61335 = (function (p__60696,_){
var vec__60697 = p__60696;
var window_width = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60697,(0),null);
var char_width = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__60697,(1),null);
var G__60700 = (window_width / char_width);
return Math.ceil(G__60700);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$6(G__60690_61330,G__60691_61331,G__60692_61332,G__60693_61333,G__60694_61334,G__60695_61335) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60690_61330,G__60691_61331,G__60692_61332,G__60693_61333,G__60694_61334,G__60695_61335));
var G__60701_61339 = new cljs.core.Keyword("component","root","component/root",-1714299688);
var G__60702_61340 = (function (db,_){
return new cljs.core.Keyword(null,"component","component",1555936782).cljs$core$IFn$_invoke$arity$1(db);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60701_61339,G__60702_61340) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60701_61339,G__60702_61340));
var G__60703_61342 = new cljs.core.Keyword("component","direction","component/direction",785553634);
var G__60704_61343 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60705_61344 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("component","root","component/root",-1714299688)], null);
var G__60706_61345 = (function (component,_){
return new cljs.core.Keyword(null,"direction","direction",-633359395).cljs$core$IFn$_invoke$arity$1(component);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60703_61342,G__60704_61343,G__60705_61344,G__60706_61345) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60703_61342,G__60704_61343,G__60705_61344,G__60706_61345));
var G__60707_61346 = new cljs.core.Keyword("errors","root","errors/root",-1902924890);
var G__60708_61347 = (function (db,_){
return new cljs.core.Keyword(null,"errors","errors",-908790718).cljs$core$IFn$_invoke$arity$1(db);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60707_61346,G__60708_61347) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60707_61346,G__60708_61347));
var G__60709_61348 = new cljs.core.Keyword("errors","popup-failed?","errors/popup-failed?",-600155433);
var G__60710_61349 = new cljs.core.Keyword(null,"<-","<-",760412998);
var G__60711_61350 = new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("errors","root","errors/root",-1902924890)], null);
var G__60712_61351 = (function (errors,_){
return new cljs.core.Keyword(null,"popup-failed?","popup-failed?",-345183682).cljs$core$IFn$_invoke$arity$1(errors);
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$4(G__60709_61348,G__60710_61349,G__60711_61350,G__60712_61351) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60709_61348,G__60710_61349,G__60711_61350,G__60712_61351));
var G__60713_61356 = new cljs.core.Keyword("history","showing-history?","history/showing-history?",896856376);
var G__60714_61357 = (function (db,_){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(db,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"history","history",-247395220),new cljs.core.Keyword(null,"showing-history?","showing-history?",2100610492)], null));
});
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.cljs$core$IFn$_invoke$arity$2(G__60713_61356,G__60714_61357) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.core.reg_sub.call(null,G__60713_61356,G__60714_61357));

//# sourceMappingURL=day8.re_frame_10x.subs.js.map
