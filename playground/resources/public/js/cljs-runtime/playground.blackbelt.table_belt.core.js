goog.provide('playground.blackbelt.table_belt.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('reagent.dom');
var module$node_modules$faker$index=shadow.js.require("module$node_modules$faker$index", {});
goog.require('clojure.string');
goog.require('goog.string');
goog.require('goog.string.format');
playground.blackbelt.table_belt.core.style_table = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"font-family","font-family",-667419874),"monospace",new cljs.core.Keyword(null,"vertical-align","vertical-align",651007333),"top"], null);
playground.blackbelt.table_belt.core.table_belt = (function playground$blackbelt$table_belt$core$table_belt(){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.table-belt-main","div.table-belt-main",22282613),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"display","display",242065432),"flex",new cljs.core.Keyword(null,"justify-content","justify-content",-1990475787),"space-around",new cljs.core.Keyword(null,"padding-top","padding-top",1929675955),"2em"], null)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"table","table",-564943036),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),playground.blackbelt.table_belt.core.style_table], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"ID"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Task"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Metric (Ideal)"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Successes/Attempts (best)"], null)], null),(function (){var iter__4529__auto__ = (function playground$blackbelt$table_belt$core$table_belt_$_iter__55464(s__55465){
return (new cljs.core.LazySeq(null,(function (){
var s__55465__$1 = s__55465;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55465__$1);
if(temp__5735__auto__){
var s__55465__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55465__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55465__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55467 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55466 = (0);
while(true){
if((i__55466 < size__4528__auto__)){
var i = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__55466);
cljs.core.chunk_append(b__55467,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),(function (){var G__55469 = cljs.core.first((function (){var G__55471 = module$node_modules$faker$index.random.uuid();
var fexpr__55470 = ((function (i__55466,G__55471,i,c__4527__auto__,size__4528__auto__,b__55467,s__55465__$2,temp__5735__auto__){
return (function (p1__55462_SHARP_){
return clojure.string.split.cljs$core$IFn$_invoke$arity$2(p1__55462_SHARP_,"-");
});})(i__55466,G__55471,i,c__4527__auto__,size__4528__auto__,b__55467,s__55465__$2,temp__5735__auto__))
;
return fexpr__55470(G__55471);
})());
var fexpr__55468 = ((function (i__55466,G__55469,i,c__4527__auto__,size__4528__auto__,b__55467,s__55465__$2,temp__5735__auto__){
return (function (p1__55463_SHARP_){
return cljs.core.subs.cljs$core$IFn$_invoke$arity$3(p1__55463_SHARP_,(0),(8));
});})(i__55466,G__55469,i,c__4527__auto__,size__4528__auto__,b__55467,s__55465__$2,temp__5735__auto__))
;
return fexpr__55468(G__55469);
})()], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),[clojure.string.capitalize(module$node_modules$faker$index.hacker.verb())," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.noun())," using ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.adjective())," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.noun())].join('')], null),(function (){var ideal_value = cljs.core.rand.cljs$core$IFn$_invoke$arity$1((100));
var ideal_unit = module$node_modules$faker$index.hacker.noun();
var ideal = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.string.format("%.2d",ideal_value))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ideal_unit)].join('');
var best_attempt_value = (ideal_value - cljs.core.rand.cljs$core$IFn$_invoke$arity$1(ideal_value));
var attempts = cljs.core.rand_int((50));
return (new cljs.core.List(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(ideal_unit)," (",ideal,"s)"].join('')], null),(new cljs.core.List(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),[cljs.core.str.cljs$core$IFn$_invoke$arity$1((attempts - cljs.core.rand_int(attempts))),"/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(attempts)," (",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.string.format("%.2d",best_attempt_value))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ideal_unit),"s)"].join('')], null),null,(1),null)),(2),null));
})()], null));

var G__55476 = (i__55466 + (1));
i__55466 = G__55476;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55467),playground$blackbelt$table_belt$core$table_belt_$_iter__55464(cljs.core.chunk_rest(s__55465__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55467),null);
}
} else {
var i = cljs.core.first(s__55465__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),(function (){var G__55473 = cljs.core.first((function (){var G__55475 = module$node_modules$faker$index.random.uuid();
var fexpr__55474 = ((function (G__55475,i,s__55465__$2,temp__5735__auto__){
return (function (p1__55462_SHARP_){
return clojure.string.split.cljs$core$IFn$_invoke$arity$2(p1__55462_SHARP_,"-");
});})(G__55475,i,s__55465__$2,temp__5735__auto__))
;
return fexpr__55474(G__55475);
})());
var fexpr__55472 = ((function (G__55473,i,s__55465__$2,temp__5735__auto__){
return (function (p1__55463_SHARP_){
return cljs.core.subs.cljs$core$IFn$_invoke$arity$3(p1__55463_SHARP_,(0),(8));
});})(G__55473,i,s__55465__$2,temp__5735__auto__))
;
return fexpr__55472(G__55473);
})()], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),[clojure.string.capitalize(module$node_modules$faker$index.hacker.verb())," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.noun())," using ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.adjective())," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.noun())].join('')], null),(function (){var ideal_value = cljs.core.rand.cljs$core$IFn$_invoke$arity$1((100));
var ideal_unit = module$node_modules$faker$index.hacker.noun();
var ideal = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.string.format("%.2d",ideal_value))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ideal_unit)].join('');
var best_attempt_value = (ideal_value - cljs.core.rand.cljs$core$IFn$_invoke$arity$1(ideal_value));
var attempts = cljs.core.rand_int((50));
return (new cljs.core.List(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(ideal_unit)," (",ideal,"s)"].join('')], null),(new cljs.core.List(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),[cljs.core.str.cljs$core$IFn$_invoke$arity$1((attempts - cljs.core.rand_int(attempts))),"/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(attempts)," (",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.string.format("%.2d",best_attempt_value))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ideal_unit),"s)"].join('')], null),null,(1),null)),(2),null));
})()], null),playground$blackbelt$table_belt$core$table_belt_$_iter__55464(cljs.core.rest(s__55465__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1((3)));
})()], null)], null);
});
});

//# sourceMappingURL=playground.blackbelt.table_belt.core.js.map
