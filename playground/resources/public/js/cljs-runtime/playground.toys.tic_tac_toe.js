goog.provide('playground.toys.tic_tac_toe');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('reagent.dom');
playground.toys.tic_tac_toe.style = new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"display","display",242065432),"grid",new cljs.core.Keyword(null,"grid-template-columns","grid-template-columns",-594112133),"1fr 1fr 1fr",new cljs.core.Keyword(null,"grid-template-rows","grid-template-rows",-372292629),"1fr 1fr 1fr",new cljs.core.Keyword(null,"border","border",1444987323),"2px solid black"], null);
playground.toys.tic_tac_toe.style_grid_items = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"border","border",1444987323),"1px solid black"], null);
playground.toys.tic_tac_toe.style_button_items = new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"background-color","background-color",570434026),"none",new cljs.core.Keyword(null,"border","border",1444987323),"none",new cljs.core.Keyword(null,"width","width",-384071477),"100%",new cljs.core.Keyword(null,"height","height",1025178622),"2em",new cljs.core.Keyword(null,"color","color",1011675173),"black",new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"4em",new cljs.core.Keyword(null,"font-family","font-family",-667419874),"Roboto"], null);
playground.toys.tic_tac_toe.style_button_reset = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"background-color","background-color",570434026),"none",new cljs.core.Keyword(null,"border","border",1444987323),"none",new cljs.core.Keyword(null,"width","width",-384071477),"20vw",new cljs.core.Keyword(null,"font-size","font-size",-1847940346),"2em",new cljs.core.Keyword(null,"font-family","font-family",-667419874),"Roboto"], null);
playground.toys.tic_tac_toe.board_state = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.sorted_map(),cljs.core.map.cljs$core$IFn$_invoke$arity$3(cljs.core.vector,new cljs.core.PersistentVector(null, 9, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(0)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(1),(0)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(2),(0)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(1)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(1),(1)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(2),(1)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(2)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(1),(2)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(2),(2)], null)], null),cljs.core.repeat.cljs$core$IFn$_invoke$arity$1(""))));
playground.toys.tic_tac_toe.turn_state = reagent.core.atom.cljs$core$IFn$_invoke$arity$1((0));
playground.toys.tic_tac_toe.win_state = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(false);
playground.toys.tic_tac_toe.draw_state = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(false);
playground.toys.tic_tac_toe.translate = (function playground$toys$tic_tac_toe$translate(coords,delta_x,delta_y){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(delta_x + cljs.core.first(coords)),(delta_y + cljs.core.second(coords))], null);
});
playground.toys.tic_tac_toe.check_board_state = (function playground$toys$tic_tac_toe$check_board_state(coords,board_state){
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(board_state),coords);
var f = (function (p1__55491_SHARP_,p2__55492_SHARP_){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(board_state),playground.toys.tic_tac_toe.translate(coords,p1__55491_SHARP_,p2__55492_SHARP_));
});
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(source,f((1),(0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([f((2),(0))], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(source,f((1),(1)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([f((2),(2))], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(source,f((0),(-1)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([f((0),(-2))], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(source,f((-1),(-1)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([f((-2),(-2))], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(source,f((-1),(0)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([f((-2),(0))], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(source,f((-1),(1)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([f((-2),(2))], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(source,f((0),(1)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([f((0),(2))], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(source,f((1),(-1)),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([f((2),(-2))], 0))));
});
playground.toys.tic_tac_toe.player_win = (function playground$toys$tic_tac_toe$player_win(turn_state,win_state){
if(cljs.core.truth_((function (){var and__4115__auto__ = cljs.core.deref(win_state);
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((0),cljs.core.mod((cljs.core.deref(turn_state) | (0)),(2)));
} else {
return and__4115__auto__;
}
})())){
return "X";
} else {
return "O";
}
});
playground.toys.tic_tac_toe.player_draw = (function playground$toys$tic_tac_toe$player_draw(turn_state){
return (cljs.core.deref(turn_state) >= (9));
});
playground.toys.tic_tac_toe.handle_turn_BANG_ = (function playground$toys$tic_tac_toe$handle_turn_BANG_(coords,board_state,turn_state){
if(cljs.core.empty_QMARK_(cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(board_state),coords))){
var G__55496 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((0),cljs.core.mod((cljs.core.deref(turn_state) | (0)),(2))))?cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(board_state,(function (p1__55493_SHARP_){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__55493_SHARP_,coords,"O");
})):cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(board_state,(function (p1__55494_SHARP_){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__55494_SHARP_,coords,"X");
})));
var G__55497 = cljs.core.reset_BANG_(playground.toys.tic_tac_toe.win_state,playground.toys.tic_tac_toe.check_board_state(coords,board_state));
var fexpr__55495 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(turn_state,cljs.core.inc);
return (fexpr__55495.cljs$core$IFn$_invoke$arity$2 ? fexpr__55495.cljs$core$IFn$_invoke$arity$2(G__55496,G__55497) : fexpr__55495.call(null,G__55496,G__55497));
} else {
return null;
}
});
playground.toys.tic_tac_toe.handle_reset_BANG_ = (function playground$toys$tic_tac_toe$handle_reset_BANG_(board_state,turn_state,win_state,draw_state){
cljs.core.doall.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function playground$toys$tic_tac_toe$handle_reset_BANG__$_iter__55499(s__55500){
return (new cljs.core.LazySeq(null,(function (){
var s__55500__$1 = s__55500;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55500__$1);
if(temp__5735__auto__){
var s__55500__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55500__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55500__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55502 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55501 = (0);
while(true){
if((i__55501 < size__4528__auto__)){
var vec__55503 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__55501);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55503,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55503,(1),null);
cljs.core.chunk_append(b__55502,cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(board_state,((function (i__55501,vec__55503,k,v,c__4527__auto__,size__4528__auto__,b__55502,s__55500__$2,temp__5735__auto__){
return (function (p1__55498_SHARP_){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__55498_SHARP_,k,"");
});})(i__55501,vec__55503,k,v,c__4527__auto__,size__4528__auto__,b__55502,s__55500__$2,temp__5735__auto__))
));

var G__55525 = (i__55501 + (1));
i__55501 = G__55525;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55502),playground$toys$tic_tac_toe$handle_reset_BANG__$_iter__55499(cljs.core.chunk_rest(s__55500__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55502),null);
}
} else {
var vec__55506 = cljs.core.first(s__55500__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55506,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55506,(1),null);
return cljs.core.cons(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(board_state,((function (vec__55506,k,v,s__55500__$2,temp__5735__auto__){
return (function (p1__55498_SHARP_){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__55498_SHARP_,k,"");
});})(vec__55506,k,v,s__55500__$2,temp__5735__auto__))
),playground$toys$tic_tac_toe$handle_reset_BANG__$_iter__55499(cljs.core.rest(s__55500__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(cljs.core.deref(board_state));
})());

cljs.core.reset_BANG_(turn_state,(0));

cljs.core.reset_BANG_(win_state,false);

return cljs.core.reset_BANG_(draw_state,false);
});
playground.toys.tic_tac_toe.tic_tac_toe = (function playground$toys$tic_tac_toe$tic_tac_toe(){
if(cljs.core.not(cljs.core.deref(playground.toys.tic_tac_toe.win_state))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.tic-tac-toe","div.tic-tac-toe",-911736489),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),playground.toys.tic_tac_toe.style], null),cljs.core.doall.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function playground$toys$tic_tac_toe$tic_tac_toe_$_iter__55509(s__55510){
return (new cljs.core.LazySeq(null,(function (){
var s__55510__$1 = s__55510;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55510__$1);
if(temp__5735__auto__){
var s__55510__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55510__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55510__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55512 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55511 = (0);
while(true){
if((i__55511 < size__4528__auto__)){
var vec__55513 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__55511);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55513,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55513,(1),null);
cljs.core.chunk_append(b__55512,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.grid-item","div.grid-item",-1160188191),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"id","id",-1388402092),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.first(k)),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.second(k))].join(''),new cljs.core.Keyword(null,"style","style",-496642736),playground.toys.tic_tac_toe.style_grid_items], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"style","style",-496642736),playground.toys.tic_tac_toe.style_button_items,new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (i__55511,vec__55513,k,v,c__4527__auto__,size__4528__auto__,b__55512,s__55510__$2,temp__5735__auto__){
return (function (){
return playground.toys.tic_tac_toe.handle_turn_BANG_(k,playground.toys.tic_tac_toe.board_state,playground.toys.tic_tac_toe.turn_state);
});})(i__55511,vec__55513,k,v,c__4527__auto__,size__4528__auto__,b__55512,s__55510__$2,temp__5735__auto__))
], null),v], null)], null));

var G__55527 = (i__55511 + (1));
i__55511 = G__55527;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55512),playground$toys$tic_tac_toe$tic_tac_toe_$_iter__55509(cljs.core.chunk_rest(s__55510__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55512),null);
}
} else {
var vec__55516 = cljs.core.first(s__55510__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55516,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__55516,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.grid-item","div.grid-item",-1160188191),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"id","id",-1388402092),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.first(k)),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.second(k))].join(''),new cljs.core.Keyword(null,"style","style",-496642736),playground.toys.tic_tac_toe.style_grid_items], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"style","style",-496642736),playground.toys.tic_tac_toe.style_button_items,new cljs.core.Keyword(null,"on-click","on-click",1632826543),((function (vec__55516,k,v,s__55510__$2,temp__5735__auto__){
return (function (){
return playground.toys.tic_tac_toe.handle_turn_BANG_(k,playground.toys.tic_tac_toe.board_state,playground.toys.tic_tac_toe.turn_state);
});})(vec__55516,k,v,s__55510__$2,temp__5735__auto__))
], null),v], null)], null),playground$toys$tic_tac_toe$tic_tac_toe_$_iter__55509(cljs.core.rest(s__55510__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(cljs.core.deref(playground.toys.tic_tac_toe.board_state));
})())], null);
} else {
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"display","display",242065432),"flex",new cljs.core.Keyword(null,"flex-direction","flex-direction",364609438),"column",new cljs.core.Keyword(null,"place-items","place-items",-1530443900),"center"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"src","src",-1651076051),"https://media1.giphy.com/media/5kq0GCjHA8Rwc/giphy.gif",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"height","height",1025178622),"auto"], null)], null)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),"Player ",playground.toys.tic_tac_toe.player_win(playground.toys.tic_tac_toe.turn_state,playground.toys.tic_tac_toe.win_state)," wins!"], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button","button",1456579943),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"style","style",-496642736),playground.toys.tic_tac_toe.style_button_reset,new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return playground.toys.tic_tac_toe.handle_reset_BANG_(playground.toys.tic_tac_toe.board_state,playground.toys.tic_tac_toe.turn_state,playground.toys.tic_tac_toe.win_state,playground.toys.tic_tac_toe.draw_state);
})], null),"Play again?"], null)], null);
}
});
goog.exportSymbol('playground.toys.tic_tac_toe.tic_tac_toe', playground.toys.tic_tac_toe.tic_tac_toe);

//# sourceMappingURL=playground.toys.tic_tac_toe.js.map
