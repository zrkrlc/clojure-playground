goog.provide('reagent.debug');
goog.require('cljs.core');
reagent.debug.has_console = (typeof console !== 'undefined');
reagent.debug.tracking = false;
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.warnings !== 'undefined')){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.track_console !== 'undefined')){
} else {
reagent.debug.track_console = (function (){var o = ({});
(o.warn = (function() { 
var G__62239__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__62239 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__62240__i = 0, G__62240__a = new Array(arguments.length -  0);
while (G__62240__i < G__62240__a.length) {G__62240__a[G__62240__i] = arguments[G__62240__i + 0]; ++G__62240__i;}
  args = new cljs.core.IndexedSeq(G__62240__a,0,null);
} 
return G__62239__delegate.call(this,args);};
G__62239.cljs$lang$maxFixedArity = 0;
G__62239.cljs$lang$applyTo = (function (arglist__62241){
var args = cljs.core.seq(arglist__62241);
return G__62239__delegate(args);
});
G__62239.cljs$core$IFn$_invoke$arity$variadic = G__62239__delegate;
return G__62239;
})()
);

(o.error = (function() { 
var G__62242__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__62242 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__62243__i = 0, G__62243__a = new Array(arguments.length -  0);
while (G__62243__i < G__62243__a.length) {G__62243__a[G__62243__i] = arguments[G__62243__i + 0]; ++G__62243__i;}
  args = new cljs.core.IndexedSeq(G__62243__a,0,null);
} 
return G__62242__delegate.call(this,args);};
G__62242.cljs$lang$maxFixedArity = 0;
G__62242.cljs$lang$applyTo = (function (arglist__62244){
var args = cljs.core.seq(arglist__62244);
return G__62242__delegate(args);
});
G__62242.cljs$core$IFn$_invoke$arity$variadic = G__62242__delegate;
return G__62242;
})()
);

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
(reagent.debug.tracking = true);

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

(reagent.debug.tracking = false);

return warns;
});

//# sourceMappingURL=reagent.debug.js.map
