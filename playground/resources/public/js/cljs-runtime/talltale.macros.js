goog.provide('talltale.macros');
goog.require('cljs.core');
goog.require('cljs.spec.gen.alpha');
goog.require('talltale.samples');
/**
 * returns the raw data for the generator, locale is keyword (eg. :en or :fr) and ks is a vector of keys like with get-in
 */
talltale.macros.raw = (function talltale$macros$raw(locale,ks){
return cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(talltale.samples.data,cljs.core.cons(locale,ks));
});
talltale.macros.rand_data = (function talltale$macros$rand_data(locale,ks){
var data = talltale.macros.raw(locale,ks);
if(cljs.core.coll_QMARK_(data)){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(data,cljs.core.rand_int(cljs.core.count(data)));
} else {
return data;
}
});
talltale.macros.rand_excluding = (function talltale$macros$rand_excluding(n,excluding){
var r = cljs.core.rand_int(n);
while(true){
if(cljs.core.truth_((excluding.cljs$core$IFn$_invoke$arity$1 ? excluding.cljs$core$IFn$_invoke$arity$1(r) : excluding.call(null,r)))){
var G__53377 = cljs.core.rand_int(n);
r = G__53377;
continue;
} else {
return r;
}
break;
}
});

//# sourceMappingURL=talltale.macros.js.map
