goog.provide('playground.toys.nameless_company');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('reagent.dom');
goog.require('playground.toys.nameless_company_state');
var module$node_modules$faker$index=shadow.js.require("module$node_modules$faker$index", {});
playground.toys.nameless_company.nameless_company = (function playground$toys$nameless_company$nameless_company(){
var users = cljs.core.deref(playground.toys.nameless_company_state.users);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.main","div.main",-117780813),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"table","table",-564943036),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"border","border",1444987323),"1px solid red",new cljs.core.Keyword(null,"table-layout","table-layout",-1260034150),"fixed",new cljs.core.Keyword(null,"width","width",-384071477),"100%"], null)], null),new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"word-wrap","word-wrap",-1700975926),"break-word"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),""], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Name"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Nickname"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"E-mail"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"th","th",-545608566),"Date modified"], null)], null),(function (){var iter__4529__auto__ = (function playground$toys$nameless_company$nameless_company_$_iter__53473(s__53474){
return (new cljs.core.LazySeq(null,(function (){
var s__53474__$1 = s__53474;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__53474__$1);
if(temp__5735__auto__){
var s__53474__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__53474__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__53474__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__53476 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__53475 = (0);
while(true){
if((i__53475 < size__4528__auto__)){
var vec__53477 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__53475);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__53477,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__53477,(1),null);
cljs.core.chunk_append(b__53476,new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"src","src",-1651076051),new cljs.core.Keyword(null,"image-url","image-url",-1064784064).cljs$core$IFn$_invoke$arity$1(v),new cljs.core.Keyword(null,"width","width",-384071477),"80px"], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),new cljs.core.Keyword(null,"firstname","firstname",1659984849).cljs$core$IFn$_invoke$arity$1(v)," ",new cljs.core.Keyword(null,"lastname","lastname",-265181465).cljs$core$IFn$_invoke$arity$1(v)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),clojure.string.capitalize(module$node_modules$faker$index.hacker.adjective())," ",new cljs.core.Keyword(null,"firstname","firstname",1659984849).cljs$core$IFn$_invoke$arity$1(v)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),module$node_modules$faker$index.internet.userName(),"\n","@",module$node_modules$faker$index.internet.domainName()], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.date.recent())], null)], null)], null));

var G__53483 = (i__53475 + (1));
i__53475 = G__53483;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__53476),playground$toys$nameless_company$nameless_company_$_iter__53473(cljs.core.chunk_rest(s__53474__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__53476),null);
}
} else {
var vec__53480 = cljs.core.first(s__53474__$2);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__53480,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__53480,(1),null);
return cljs.core.cons(new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tr","tr",-1424774646),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"src","src",-1651076051),new cljs.core.Keyword(null,"image-url","image-url",-1064784064).cljs$core$IFn$_invoke$arity$1(v),new cljs.core.Keyword(null,"width","width",-384071477),"80px"], null)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),new cljs.core.Keyword(null,"firstname","firstname",1659984849).cljs$core$IFn$_invoke$arity$1(v)," ",new cljs.core.Keyword(null,"lastname","lastname",-265181465).cljs$core$IFn$_invoke$arity$1(v)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),clojure.string.capitalize(module$node_modules$faker$index.hacker.adjective())," ",new cljs.core.Keyword(null,"firstname","firstname",1659984849).cljs$core$IFn$_invoke$arity$1(v)], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),module$node_modules$faker$index.internet.userName(),"\n","@",module$node_modules$faker$index.internet.domainName()], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"td","td",1479933353),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p","p",151049309),cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.date.recent())], null)], null)], null),playground$toys$nameless_company$nameless_company_$_iter__53473(cljs.core.rest(s__53474__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(users);
})()], null)], null);
});

//# sourceMappingURL=playground.toys.nameless_company.js.map
