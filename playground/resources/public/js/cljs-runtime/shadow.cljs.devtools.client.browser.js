goog.provide('shadow.cljs.devtools.client.browser');
goog.require('cljs.core');
goog.require('cljs.reader');
goog.require('clojure.string');
goog.require('goog.dom');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('goog.net.XhrIo');
goog.require('shadow.cljs.devtools.client.env');
goog.require('shadow.cljs.devtools.client.console');
goog.require('shadow.cljs.devtools.client.hud');
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.active_modules_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.active_modules_ref = cljs.core.volatile_BANG_(cljs.core.PersistentHashSet.EMPTY);
}
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.repl_ns_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.repl_ns_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
shadow.cljs.devtools.client.browser.module_loaded = (function shadow$cljs$devtools$client$browser$module_loaded(name){
return shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IVolatile$_vreset_BANG_$arity$2(null,cljs.core.conj.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.active_modules_ref.cljs$core$IDeref$_deref$arity$1(null),cljs.core.keyword.cljs$core$IFn$_invoke$arity$1(name)));
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.socket_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.socket_ref = cljs.core.volatile_BANG_(null);
}
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___67663 = arguments.length;
var i__4737__auto___67664 = (0);
while(true){
if((i__4737__auto___67664 < len__4736__auto___67663)){
args__4742__auto__.push((arguments[i__4737__auto___67664]));

var G__67665 = (i__4737__auto___67664 + (1));
i__4737__auto___67664 = G__67665;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
return console.log.apply(null,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),"color: blue;"], null),args)));
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq67500){
var G__67501 = cljs.core.first(seq67500);
var seq67500__$1 = cljs.core.next(seq67500);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__67501,seq67500__$1);
}));

shadow.cljs.devtools.client.browser.ws_msg = (function shadow$cljs$devtools$client$browser$ws_msg(msg){
var temp__5733__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5733__auto__)){
var s = temp__5733__auto__;
return s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
} else {
return console.warn("WEBSOCKET NOT CONNECTED",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0)));
}
});
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.scripts_to_load !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.scripts_to_load = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
}
shadow.cljs.devtools.client.browser.loaded_QMARK_ = goog.isProvided_;
shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$goog_is_loaded_QMARK_(name){
return $CLJS.SHADOW_ENV.isLoaded(name);
});
shadow.cljs.devtools.client.browser.goog_base_rc = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("shadow.build.classpath","resource","shadow.build.classpath/resource",-879517823),"goog/base.js"], null);
shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_ = (function shadow$cljs$devtools$client$browser$src_is_loaded_QMARK_(p__67512){
var map__67513 = p__67512;
var map__67513__$1 = (((((!((map__67513 == null))))?(((((map__67513.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67513.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67513):map__67513);
var src = map__67513__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67513__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67513__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var or__4126__auto__ = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.goog_base_rc,resource_id);
if(or__4126__auto__){
return or__4126__auto__;
} else {
return shadow.cljs.devtools.client.browser.goog_is_loaded_QMARK_(output_name);
}
});
shadow.cljs.devtools.client.browser.module_is_active_QMARK_ = (function shadow$cljs$devtools$client$browser$module_is_active_QMARK_(module){
return cljs.core.contains_QMARK_(cljs.core.deref(shadow.cljs.devtools.client.browser.active_modules_ref),module);
});
shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__67517 = cljs.core.seq(sources);
var chunk__67518 = null;
var count__67519 = (0);
var i__67520 = (0);
while(true){
if((i__67520 < count__67519)){
var map__67526 = chunk__67518.cljs$core$IIndexed$_nth$arity$2(null,i__67520);
var map__67526__$1 = (((((!((map__67526 == null))))?(((((map__67526.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67526.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67526):map__67526);
var src = map__67526__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67526__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67526__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67526__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67526__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));


var G__67666 = seq__67517;
var G__67667 = chunk__67518;
var G__67668 = count__67519;
var G__67669 = (i__67520 + (1));
seq__67517 = G__67666;
chunk__67518 = G__67667;
count__67519 = G__67668;
i__67520 = G__67669;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__67517);
if(temp__5735__auto__){
var seq__67517__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__67517__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__67517__$1);
var G__67670 = cljs.core.chunk_rest(seq__67517__$1);
var G__67671 = c__4556__auto__;
var G__67672 = cljs.core.count(c__4556__auto__);
var G__67673 = (0);
seq__67517 = G__67670;
chunk__67518 = G__67671;
count__67519 = G__67672;
i__67520 = G__67673;
continue;
} else {
var map__67528 = cljs.core.first(seq__67517__$1);
var map__67528__$1 = (((((!((map__67528 == null))))?(((((map__67528.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67528.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67528):map__67528);
var src = map__67528__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67528__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67528__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67528__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67528__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''));


var G__67674 = cljs.core.next(seq__67517__$1);
var G__67675 = null;
var G__67676 = (0);
var G__67677 = (0);
seq__67517 = G__67674;
chunk__67518 = G__67675;
count__67519 = G__67676;
i__67520 = G__67677;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["can't find fn ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__67532 = cljs.core.seq(js_requires);
var chunk__67533 = null;
var count__67534 = (0);
var i__67535 = (0);
while(true){
if((i__67535 < count__67534)){
var js_ns = chunk__67533.cljs$core$IIndexed$_nth$arity$2(null,i__67535);
var require_str_67678 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_67678);


var G__67679 = seq__67532;
var G__67680 = chunk__67533;
var G__67681 = count__67534;
var G__67682 = (i__67535 + (1));
seq__67532 = G__67679;
chunk__67533 = G__67680;
count__67534 = G__67681;
i__67535 = G__67682;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__67532);
if(temp__5735__auto__){
var seq__67532__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__67532__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__67532__$1);
var G__67683 = cljs.core.chunk_rest(seq__67532__$1);
var G__67684 = c__4556__auto__;
var G__67685 = cljs.core.count(c__4556__auto__);
var G__67686 = (0);
seq__67532 = G__67683;
chunk__67533 = G__67684;
count__67534 = G__67685;
i__67535 = G__67686;
continue;
} else {
var js_ns = cljs.core.first(seq__67532__$1);
var require_str_67687 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_67687);


var G__67688 = cljs.core.next(seq__67532__$1);
var G__67689 = null;
var G__67690 = (0);
var G__67691 = (0);
seq__67532 = G__67688;
chunk__67533 = G__67689;
count__67534 = G__67690;
i__67535 = G__67691;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.load_sources = (function shadow$cljs$devtools$client$browser$load_sources(sources,callback){
if(cljs.core.empty_QMARK_(sources)){
var G__67536 = cljs.core.PersistentVector.EMPTY;
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(G__67536) : callback.call(null,G__67536));
} else {
var G__67537 = shadow.cljs.devtools.client.env.files_url();
var G__67538 = (function (res){
var req = this;
var content = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(content) : callback.call(null,content));
});
var G__67539 = "POST";
var G__67540 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"client","client",-1323448117),new cljs.core.Keyword(null,"browser","browser",828191719),new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources)], null)], 0));
var G__67541 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__67537,G__67538,G__67539,G__67540,G__67541);
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(p__67544){
var map__67546 = p__67544;
var map__67546__$1 = (((((!((map__67546 == null))))?(((((map__67546.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67546.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67546):map__67546);
var msg = map__67546__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67546__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67546__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var map__67548 = info;
var map__67548__$1 = (((((!((map__67548 == null))))?(((((map__67548.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67548.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67548):map__67548);
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67548__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var compiled = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67548__$1,new cljs.core.Keyword(null,"compiled","compiled",850043082));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__67550(s__67551){
return (new cljs.core.LazySeq(null,(function (){
var s__67551__$1 = s__67551;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__67551__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var map__67556 = cljs.core.first(xs__6292__auto__);
var map__67556__$1 = (((((!((map__67556 == null))))?(((((map__67556.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67556.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67556):map__67556);
var src = map__67556__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67556__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67556__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4525__auto__ = ((function (s__67551__$1,map__67556,map__67556__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__67548,map__67548__$1,sources,compiled,map__67546,map__67546__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__67550_$_iter__67552(s__67553){
return (new cljs.core.LazySeq(null,((function (s__67551__$1,map__67556,map__67556__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__67548,map__67548__$1,sources,compiled,map__67546,map__67546__$1,msg,info,reload_info){
return (function (){
var s__67553__$1 = s__67553;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__67553__$1);
if(temp__5735__auto____$1){
var s__67553__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__67553__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__67553__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__67555 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__67554 = (0);
while(true){
if((i__67554 < size__4528__auto__)){
var warning = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__67554);
cljs.core.chunk_append(b__67555,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__67692 = (i__67554 + (1));
i__67554 = G__67692;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__67555),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__67550_$_iter__67552(cljs.core.chunk_rest(s__67553__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__67555),null);
}
} else {
var warning = cljs.core.first(s__67553__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__67550_$_iter__67552(cljs.core.rest(s__67553__$2)));
}
} else {
return null;
}
break;
}
});})(s__67551__$1,map__67556,map__67556__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__67548,map__67548__$1,sources,compiled,map__67546,map__67546__$1,msg,info,reload_info))
,null,null));
});})(s__67551__$1,map__67556,map__67556__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__67548,map__67548__$1,sources,compiled,map__67546,map__67546__$1,msg,info,reload_info))
;
var fs__4526__auto__ = cljs.core.seq(iterys__4525__auto__(warnings));
if(fs__4526__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4526__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__67550(cljs.core.rest(s__67551__$1)));
} else {
var G__67693 = cljs.core.rest(s__67551__$1);
s__67551__$1 = G__67693;
continue;
}
} else {
var G__67694 = cljs.core.rest(s__67551__$1);
s__67551__$1 = G__67694;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(sources);
})()));
var seq__67558_67695 = cljs.core.seq(warnings);
var chunk__67559_67696 = null;
var count__67560_67697 = (0);
var i__67561_67698 = (0);
while(true){
if((i__67561_67698 < count__67560_67697)){
var map__67566_67699 = chunk__67559_67696.cljs$core$IIndexed$_nth$arity$2(null,i__67561_67698);
var map__67566_67700__$1 = (((((!((map__67566_67699 == null))))?(((((map__67566_67699.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67566_67699.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67566_67699):map__67566_67699);
var w_67701 = map__67566_67700__$1;
var msg_67702__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67566_67700__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_67703 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67566_67700__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_67704 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67566_67700__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_67705 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67566_67700__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_67705)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_67703),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_67704),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_67702__$1)].join(''));


var G__67706 = seq__67558_67695;
var G__67707 = chunk__67559_67696;
var G__67708 = count__67560_67697;
var G__67709 = (i__67561_67698 + (1));
seq__67558_67695 = G__67706;
chunk__67559_67696 = G__67707;
count__67560_67697 = G__67708;
i__67561_67698 = G__67709;
continue;
} else {
var temp__5735__auto___67710 = cljs.core.seq(seq__67558_67695);
if(temp__5735__auto___67710){
var seq__67558_67711__$1 = temp__5735__auto___67710;
if(cljs.core.chunked_seq_QMARK_(seq__67558_67711__$1)){
var c__4556__auto___67712 = cljs.core.chunk_first(seq__67558_67711__$1);
var G__67713 = cljs.core.chunk_rest(seq__67558_67711__$1);
var G__67714 = c__4556__auto___67712;
var G__67715 = cljs.core.count(c__4556__auto___67712);
var G__67716 = (0);
seq__67558_67695 = G__67713;
chunk__67559_67696 = G__67714;
count__67560_67697 = G__67715;
i__67561_67698 = G__67716;
continue;
} else {
var map__67568_67717 = cljs.core.first(seq__67558_67711__$1);
var map__67568_67718__$1 = (((((!((map__67568_67717 == null))))?(((((map__67568_67717.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67568_67717.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67568_67717):map__67568_67717);
var w_67719 = map__67568_67718__$1;
var msg_67720__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67568_67718__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_67721 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67568_67718__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_67722 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67568_67718__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_67723 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67568_67718__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_67723)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_67721),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_67722),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_67720__$1)].join(''));


var G__67724 = cljs.core.next(seq__67558_67711__$1);
var G__67725 = null;
var G__67726 = (0);
var G__67727 = (0);
seq__67558_67695 = G__67724;
chunk__67559_67696 = G__67725;
count__67560_67697 = G__67726;
i__67561_67698 = G__67727;
continue;
}
} else {
}
}
break;
}

if(cljs.core.not(shadow.cljs.devtools.client.env.autoload)){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.truth_((function (){var or__4126__auto__ = cljs.core.empty_QMARK_(warnings);
if(or__4126__auto__){
return or__4126__auto__;
} else {
return shadow.cljs.devtools.client.env.ignore_warnings;
}
})())){
var sources_to_get = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (p__67570){
var map__67571 = p__67570;
var map__67571__$1 = (((((!((map__67571 == null))))?(((((map__67571.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67571.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67571):map__67571);
var src = map__67571__$1;
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67571__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67571__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
return ((cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"always-load","always-load",66405637).cljs$core$IFn$_invoke$arity$1(reload_info),ns)) || (cljs.core.not(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src))) || (((cljs.core.contains_QMARK_(compiled,resource_id)) && (cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))))));
}),cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__67573){
var map__67574 = p__67573;
var map__67574__$1 = (((((!((map__67574 == null))))?(((((map__67574.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67574.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67574):map__67574);
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67574__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"never-load","never-load",1300896819).cljs$core$IFn$_invoke$arity$1(reload_info),ns);
}),cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (p__67576){
var map__67577 = p__67576;
var map__67577__$1 = (((((!((map__67577 == null))))?(((((map__67577.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67577.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67577):map__67577);
var rc = map__67577__$1;
var module = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67577__$1,new cljs.core.Keyword(null,"module","module",1424618191));
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("js",shadow.cljs.devtools.client.env.module_format)) || (shadow.cljs.devtools.client.browser.module_is_active_QMARK_(module)));
}),sources))));
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.browser.load_sources(sources_to_get,(function (p1__67543_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__67543_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$)){
return new$;
} else {
return false;
}
} else {
return false;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_watch = (function shadow$cljs$devtools$client$browser$handle_asset_watch(p__67581){
var map__67582 = p__67581;
var map__67582__$1 = (((((!((map__67582 == null))))?(((((map__67582.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67582.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67582):map__67582);
var msg = map__67582__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67582__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__67584 = cljs.core.seq(updates);
var chunk__67586 = null;
var count__67587 = (0);
var i__67588 = (0);
while(true){
if((i__67588 < count__67587)){
var path = chunk__67586.cljs$core$IIndexed$_nth$arity$2(null,i__67588);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__67614_67728 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__67617_67729 = null;
var count__67618_67730 = (0);
var i__67619_67731 = (0);
while(true){
if((i__67619_67731 < count__67618_67730)){
var node_67732 = chunk__67617_67729.cljs$core$IIndexed$_nth$arity$2(null,i__67619_67731);
var path_match_67733 = shadow.cljs.devtools.client.browser.match_paths(node_67732.getAttribute("href"),path);
if(cljs.core.truth_(path_match_67733)){
var new_link_67734 = (function (){var G__67624 = node_67732.cloneNode(true);
G__67624.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_67733),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__67624;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_67733], 0));

goog.dom.insertSiblingAfter(new_link_67734,node_67732);

goog.dom.removeNode(node_67732);


var G__67735 = seq__67614_67728;
var G__67736 = chunk__67617_67729;
var G__67737 = count__67618_67730;
var G__67738 = (i__67619_67731 + (1));
seq__67614_67728 = G__67735;
chunk__67617_67729 = G__67736;
count__67618_67730 = G__67737;
i__67619_67731 = G__67738;
continue;
} else {
var G__67739 = seq__67614_67728;
var G__67740 = chunk__67617_67729;
var G__67741 = count__67618_67730;
var G__67742 = (i__67619_67731 + (1));
seq__67614_67728 = G__67739;
chunk__67617_67729 = G__67740;
count__67618_67730 = G__67741;
i__67619_67731 = G__67742;
continue;
}
} else {
var temp__5735__auto___67743 = cljs.core.seq(seq__67614_67728);
if(temp__5735__auto___67743){
var seq__67614_67744__$1 = temp__5735__auto___67743;
if(cljs.core.chunked_seq_QMARK_(seq__67614_67744__$1)){
var c__4556__auto___67745 = cljs.core.chunk_first(seq__67614_67744__$1);
var G__67746 = cljs.core.chunk_rest(seq__67614_67744__$1);
var G__67747 = c__4556__auto___67745;
var G__67748 = cljs.core.count(c__4556__auto___67745);
var G__67749 = (0);
seq__67614_67728 = G__67746;
chunk__67617_67729 = G__67747;
count__67618_67730 = G__67748;
i__67619_67731 = G__67749;
continue;
} else {
var node_67750 = cljs.core.first(seq__67614_67744__$1);
var path_match_67751 = shadow.cljs.devtools.client.browser.match_paths(node_67750.getAttribute("href"),path);
if(cljs.core.truth_(path_match_67751)){
var new_link_67752 = (function (){var G__67625 = node_67750.cloneNode(true);
G__67625.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_67751),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__67625;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_67751], 0));

goog.dom.insertSiblingAfter(new_link_67752,node_67750);

goog.dom.removeNode(node_67750);


var G__67753 = cljs.core.next(seq__67614_67744__$1);
var G__67754 = null;
var G__67755 = (0);
var G__67756 = (0);
seq__67614_67728 = G__67753;
chunk__67617_67729 = G__67754;
count__67618_67730 = G__67755;
i__67619_67731 = G__67756;
continue;
} else {
var G__67757 = cljs.core.next(seq__67614_67744__$1);
var G__67758 = null;
var G__67759 = (0);
var G__67760 = (0);
seq__67614_67728 = G__67757;
chunk__67617_67729 = G__67758;
count__67618_67730 = G__67759;
i__67619_67731 = G__67760;
continue;
}
}
} else {
}
}
break;
}


var G__67761 = seq__67584;
var G__67762 = chunk__67586;
var G__67763 = count__67587;
var G__67764 = (i__67588 + (1));
seq__67584 = G__67761;
chunk__67586 = G__67762;
count__67587 = G__67763;
i__67588 = G__67764;
continue;
} else {
var G__67765 = seq__67584;
var G__67766 = chunk__67586;
var G__67767 = count__67587;
var G__67768 = (i__67588 + (1));
seq__67584 = G__67765;
chunk__67586 = G__67766;
count__67587 = G__67767;
i__67588 = G__67768;
continue;
}
} else {
var temp__5735__auto__ = cljs.core.seq(seq__67584);
if(temp__5735__auto__){
var seq__67584__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__67584__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__67584__$1);
var G__67769 = cljs.core.chunk_rest(seq__67584__$1);
var G__67770 = c__4556__auto__;
var G__67771 = cljs.core.count(c__4556__auto__);
var G__67772 = (0);
seq__67584 = G__67769;
chunk__67586 = G__67770;
count__67587 = G__67771;
i__67588 = G__67772;
continue;
} else {
var path = cljs.core.first(seq__67584__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__67626_67773 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__67629_67774 = null;
var count__67630_67775 = (0);
var i__67631_67776 = (0);
while(true){
if((i__67631_67776 < count__67630_67775)){
var node_67777 = chunk__67629_67774.cljs$core$IIndexed$_nth$arity$2(null,i__67631_67776);
var path_match_67778 = shadow.cljs.devtools.client.browser.match_paths(node_67777.getAttribute("href"),path);
if(cljs.core.truth_(path_match_67778)){
var new_link_67779 = (function (){var G__67636 = node_67777.cloneNode(true);
G__67636.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_67778),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__67636;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_67778], 0));

goog.dom.insertSiblingAfter(new_link_67779,node_67777);

goog.dom.removeNode(node_67777);


var G__67780 = seq__67626_67773;
var G__67781 = chunk__67629_67774;
var G__67782 = count__67630_67775;
var G__67783 = (i__67631_67776 + (1));
seq__67626_67773 = G__67780;
chunk__67629_67774 = G__67781;
count__67630_67775 = G__67782;
i__67631_67776 = G__67783;
continue;
} else {
var G__67784 = seq__67626_67773;
var G__67785 = chunk__67629_67774;
var G__67786 = count__67630_67775;
var G__67787 = (i__67631_67776 + (1));
seq__67626_67773 = G__67784;
chunk__67629_67774 = G__67785;
count__67630_67775 = G__67786;
i__67631_67776 = G__67787;
continue;
}
} else {
var temp__5735__auto___67788__$1 = cljs.core.seq(seq__67626_67773);
if(temp__5735__auto___67788__$1){
var seq__67626_67789__$1 = temp__5735__auto___67788__$1;
if(cljs.core.chunked_seq_QMARK_(seq__67626_67789__$1)){
var c__4556__auto___67790 = cljs.core.chunk_first(seq__67626_67789__$1);
var G__67791 = cljs.core.chunk_rest(seq__67626_67789__$1);
var G__67792 = c__4556__auto___67790;
var G__67793 = cljs.core.count(c__4556__auto___67790);
var G__67794 = (0);
seq__67626_67773 = G__67791;
chunk__67629_67774 = G__67792;
count__67630_67775 = G__67793;
i__67631_67776 = G__67794;
continue;
} else {
var node_67795 = cljs.core.first(seq__67626_67789__$1);
var path_match_67796 = shadow.cljs.devtools.client.browser.match_paths(node_67795.getAttribute("href"),path);
if(cljs.core.truth_(path_match_67796)){
var new_link_67797 = (function (){var G__67637 = node_67795.cloneNode(true);
G__67637.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_67796),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__67637;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_67796], 0));

goog.dom.insertSiblingAfter(new_link_67797,node_67795);

goog.dom.removeNode(node_67795);


var G__67798 = cljs.core.next(seq__67626_67789__$1);
var G__67799 = null;
var G__67800 = (0);
var G__67801 = (0);
seq__67626_67773 = G__67798;
chunk__67629_67774 = G__67799;
count__67630_67775 = G__67800;
i__67631_67776 = G__67801;
continue;
} else {
var G__67802 = cljs.core.next(seq__67626_67789__$1);
var G__67803 = null;
var G__67804 = (0);
var G__67805 = (0);
seq__67626_67773 = G__67802;
chunk__67629_67774 = G__67803;
count__67630_67775 = G__67804;
i__67631_67776 = G__67805;
continue;
}
}
} else {
}
}
break;
}


var G__67806 = cljs.core.next(seq__67584__$1);
var G__67807 = null;
var G__67808 = (0);
var G__67809 = (0);
seq__67584 = G__67806;
chunk__67586 = G__67807;
count__67587 = G__67808;
i__67588 = G__67809;
continue;
} else {
var G__67810 = cljs.core.next(seq__67584__$1);
var G__67811 = null;
var G__67812 = (0);
var G__67813 = (0);
seq__67584 = G__67810;
chunk__67586 = G__67811;
count__67587 = G__67812;
i__67588 = G__67813;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.get_ua_product = (function shadow$cljs$devtools$client$browser$get_ua_product(){
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
});
shadow.cljs.devtools.client.browser.get_asset_root = (function shadow$cljs$devtools$client$browser$get_asset_root(){
var loc = (new goog.Uri(document.location.href));
var cbp = (new goog.Uri(CLOSURE_BASE_PATH));
var s = loc.resolve(cbp).toString();
return clojure.string.replace(s,/^file:\//,"file:///");
});
shadow.cljs.devtools.client.browser.repl_error = (function shadow$cljs$devtools$client$browser$repl_error(e){
console.error("repl/invoke error",e);

return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(shadow.cljs.devtools.client.env.repl_error(e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),shadow.cljs.devtools.client.browser.get_ua_product(),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"asset-root","asset-root",1771735072),shadow.cljs.devtools.client.browser.get_asset_root()], 0));
});
shadow.cljs.devtools.client.browser.repl_invoke = (function shadow$cljs$devtools$client$browser$repl_invoke(p__67638){
var map__67639 = p__67638;
var map__67639__$1 = (((((!((map__67639 == null))))?(((((map__67639.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67639.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67639):map__67639);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67639__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67639__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var result = shadow.cljs.devtools.client.env.repl_call((function (){
return eval(js);
}),shadow.cljs.devtools.client.browser.repl_error);
return shadow.cljs.devtools.client.browser.ws_msg(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(result,new cljs.core.Keyword(null,"id","id",-1388402092),id));
});
shadow.cljs.devtools.client.browser.repl_require = (function shadow$cljs$devtools$client$browser$repl_require(p__67641){
var map__67642 = p__67641;
var map__67642__$1 = (((((!((map__67642 == null))))?(((((map__67642.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67642.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67642):map__67642);
var msg = map__67642__$1;
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67642__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67642__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67642__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67642__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__67644){
var map__67645 = p__67644;
var map__67645__$1 = (((((!((map__67645 == null))))?(((((map__67645.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67645.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67645):map__67645);
var src = map__67645__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67645__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4115__auto__ = shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4115__auto__;
}
}),sources));
return shadow.cljs.devtools.client.browser.load_sources(sources_to_load,(function (sources__$1){
shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","require-complete","repl/require-complete",-2140254719),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));
}));
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(p__67647){
var map__67648 = p__67647;
var map__67648__$1 = (((((!((map__67648 == null))))?(((((map__67648.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67648.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67648):map__67648);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67648__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67648__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
return shadow.cljs.devtools.client.browser.load_sources(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.browser.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","init-complete","repl/init-complete",-162252879),new cljs.core.Keyword(null,"id","id",-1388402092),id], null));

return shadow.cljs.devtools.client.browser.devtools_msg("REPL session start successful");
}));
});
shadow.cljs.devtools.client.browser.repl_set_ns = (function shadow$cljs$devtools$client$browser$repl_set_ns(p__67650){
var map__67651 = p__67650;
var map__67651__$1 = (((((!((map__67651 == null))))?(((((map__67651.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67651.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67651):map__67651);
var id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67651__$1,new cljs.core.Keyword(null,"id","id",-1388402092));
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67651__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return shadow.cljs.devtools.client.browser.ws_msg(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword("repl","set-ns-complete","repl/set-ns-complete",680944662),new cljs.core.Keyword(null,"id","id",-1388402092),id,new cljs.core.Keyword(null,"ns","ns",441598760),ns], null));
});
shadow.cljs.devtools.client.browser.close_reason_ref = cljs.core.volatile_BANG_(null);
shadow.cljs.devtools.client.browser.handle_message = (function shadow$cljs$devtools$client$browser$handle_message(p__67653){
var map__67654 = p__67653;
var map__67654__$1 = (((((!((map__67654 == null))))?(((((map__67654.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__67654.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__67654):map__67654);
var msg = map__67654__$1;
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__67654__$1,new cljs.core.Keyword(null,"type","type",1174270348));
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

var G__67656 = type;
var G__67656__$1 = (((G__67656 instanceof cljs.core.Keyword))?G__67656.fqn:null);
switch (G__67656__$1) {
case "asset-watch":
return shadow.cljs.devtools.client.browser.handle_asset_watch(msg);

break;
case "repl/invoke":
return shadow.cljs.devtools.client.browser.repl_invoke(msg);

break;
case "repl/require":
return shadow.cljs.devtools.client.browser.repl_require(msg);

break;
case "repl/set-ns":
return shadow.cljs.devtools.client.browser.repl_set_ns(msg);

break;
case "repl/init":
return shadow.cljs.devtools.client.browser.repl_init(msg);

break;
case "repl/session-start":
return shadow.cljs.devtools.client.browser.repl_init(msg);

break;
case "build-complete":
shadow.cljs.devtools.client.hud.hud_warnings(msg);

return shadow.cljs.devtools.client.browser.handle_build_complete(msg);

break;
case "build-failure":
shadow.cljs.devtools.client.hud.load_end();

return shadow.cljs.devtools.client.hud.hud_error(msg);

break;
case "build-init":
return shadow.cljs.devtools.client.hud.hud_warnings(msg);

break;
case "build-start":
shadow.cljs.devtools.client.hud.hud_hide();

return shadow.cljs.devtools.client.hud.load_start();

break;
case "pong":
return null;

break;
case "client/stale":
return cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,"Stale Client! You are not using the latest compilation output!");

break;
case "client/no-worker":
return cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,["watch for build \"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.build_id),"\" not running"].join(''));

break;
case "custom-msg":
return shadow.cljs.devtools.client.env.publish_BANG_(new cljs.core.Keyword(null,"payload","payload",-383036092).cljs$core$IFn$_invoke$arity$1(msg));

break;
default:
return new cljs.core.Keyword(null,"ignored","ignored",1227374526);

}
});
shadow.cljs.devtools.client.browser.compile = (function shadow$cljs$devtools$client$browser$compile(text,callback){
var G__67657 = ["http",(cljs.core.truth_(shadow.cljs.devtools.client.env.ssl)?"s":null),"://",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.server_host),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.server_port),"/worker/compile/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.build_id),"/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.env.proc_id),"/browser"].join('');
var G__67658 = (function (res){
var req = this;
var actions = cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(req.getResponseText());
if(cljs.core.truth_(callback)){
return (callback.cljs$core$IFn$_invoke$arity$1 ? callback.cljs$core$IFn$_invoke$arity$1(actions) : callback.call(null,actions));
} else {
return null;
}
});
var G__67659 = "POST";
var G__67660 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"input","input",556931961),text], null)], 0));
var G__67661 = ({"content-type": "application/edn; charset=utf-8"});
return goog.net.XhrIo.send(G__67657,G__67658,G__67659,G__67660,G__67661);
});
shadow.cljs.devtools.client.browser.heartbeat_BANG_ = (function shadow$cljs$devtools$client$browser$heartbeat_BANG_(){
var temp__5735__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto__)){
var s = temp__5735__auto__;
s.send(cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"ping","ping",-1670114784),new cljs.core.Keyword(null,"v","v",21465059),Date.now()], null)], 0)));

return setTimeout(shadow.cljs.devtools.client.browser.heartbeat_BANG_,(30000));
} else {
return null;
}
});
shadow.cljs.devtools.client.browser.ws_connect = (function shadow$cljs$devtools$client$browser$ws_connect(){
try{var print_fn = cljs.core._STAR_print_fn_STAR_;
var ws_url = shadow.cljs.devtools.client.env.ws_url(new cljs.core.Keyword(null,"browser","browser",828191719));
var socket = (new WebSocket(ws_url));
cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,socket);

(socket.onmessage = (function (e){
return shadow.cljs.devtools.client.env.process_ws_msg(e.data,shadow.cljs.devtools.client.browser.handle_message);
}));

(socket.onopen = (function (e){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.close_reason_ref,null);

if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("goog",shadow.cljs.devtools.client.env.module_format)){
(goog.provide = goog.constructNamespace_);
} else {
}

shadow.cljs.devtools.client.env.set_print_fns_BANG_(shadow.cljs.devtools.client.browser.ws_msg);

return shadow.cljs.devtools.client.browser.devtools_msg("WebSocket connected!");
}));

(socket.onclose = (function (e){
shadow.cljs.devtools.client.browser.devtools_msg("WebSocket disconnected!");

shadow.cljs.devtools.client.hud.connection_error((function (){var or__4126__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.close_reason_ref);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "Connection closed!";
}
})());

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);

return shadow.cljs.devtools.client.env.reset_print_fns_BANG_();
}));

(socket.onerror = (function (e){
shadow.cljs.devtools.client.hud.connection_error("Connection failed!");

return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("websocket error",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
}));

return setTimeout(shadow.cljs.devtools.client.browser.heartbeat_BANG_,(30000));
}catch (e67662){var e = e67662;
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("WebSocket setup failed",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e], 0));
}});
if(shadow.cljs.devtools.client.env.enabled){
var temp__5735__auto___67815 = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto___67815)){
var s_67816 = temp__5735__auto___67815;
shadow.cljs.devtools.client.browser.devtools_msg("connection reset!");

(s_67816.onclose = (function (e){
return null;
}));

s_67816.close();

cljs.core.vreset_BANG_(shadow.cljs.devtools.client.browser.socket_ref,null);
} else {
}

window.addEventListener("beforeunload",(function (){
var temp__5735__auto__ = cljs.core.deref(shadow.cljs.devtools.client.browser.socket_ref);
if(cljs.core.truth_(temp__5735__auto__)){
var s = temp__5735__auto__;
return s.close();
} else {
return null;
}
}));

if(cljs.core.truth_((function (){var and__4115__auto__ = document;
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("loading",document.readyState);
} else {
return and__4115__auto__;
}
})())){
window.addEventListener("DOMContentLoaded",shadow.cljs.devtools.client.browser.ws_connect);
} else {
setTimeout(shadow.cljs.devtools.client.browser.ws_connect,(10));
}
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
