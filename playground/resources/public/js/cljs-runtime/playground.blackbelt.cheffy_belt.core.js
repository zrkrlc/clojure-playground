goog.provide('playground.blackbelt.cheffy_belt.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('re_frame.core');
goog.require('playground.blackbelt.state');
goog.require('playground.blackbelt.cheffy_belt.subs');
goog.require('playground.blackbelt.cheffy_belt.events');
goog.require('playground.blackbelt.cheffy_belt.views');
playground.blackbelt.cheffy_belt.core.pages = (function playground$blackbelt$cheffy_belt$core$pages(page_name){
var G__55113 = page_name;
var G__55113__$1 = (((G__55113 instanceof cljs.core.Keyword))?G__55113.fqn:null);
switch (G__55113__$1) {
case "home":
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.cheffy_belt.views.home], null);

break;
default:
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.cheffy_belt.views.home], null);

}
});
playground.blackbelt.cheffy_belt.core.cheffy_belt = (function playground$blackbelt$cheffy_belt$core$cheffy_belt(){
var active_nav = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"active-nav","active-nav",-275650057)], null)));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.cheffy-belt","div.cheffy-belt",-1620648487),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.cheffy_belt.views.nav], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.cheffy_belt.core.pages,active_nav], null)], null);
});

//# sourceMappingURL=playground.blackbelt.cheffy_belt.core.js.map
