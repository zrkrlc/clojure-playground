goog.provide('day8.re_frame_10x.utils.localstorage');
goog.require('cljs.core');
goog.require('goog.storage.Storage');
goog.require('goog.storage.mechanism.HTML5LocalStorage');
goog.require('cljs.reader');
goog.require('clojure.string');
day8.re_frame_10x.utils.localstorage.storage = (new goog.storage.Storage((new goog.storage.mechanism.HTML5LocalStorage())));
day8.re_frame_10x.utils.localstorage.safe_prefix = "day8.re-frame-10x.";
day8.re_frame_10x.utils.localstorage.safe_key = (function day8$re_frame_10x$utils$localstorage$safe_key(key){

return [day8.re_frame_10x.utils.localstorage.safe_prefix,cljs.core.str.cljs$core$IFn$_invoke$arity$1(key)].join('');
});
/**
 * Gets a re-frame-10x value from local storage.
 */
day8.re_frame_10x.utils.localstorage.get = (function day8$re_frame_10x$utils$localstorage$get(var_args){
var G__58742 = arguments.length;
switch (G__58742) {
case 1:
return day8.re_frame_10x.utils.localstorage.get.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return day8.re_frame_10x.utils.localstorage.get.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(day8.re_frame_10x.utils.localstorage.get.cljs$core$IFn$_invoke$arity$1 = (function (key){
return day8.re_frame_10x.utils.localstorage.get.cljs$core$IFn$_invoke$arity$2(key,null);
}));

(day8.re_frame_10x.utils.localstorage.get.cljs$core$IFn$_invoke$arity$2 = (function (key,not_found){
var value = day8.re_frame_10x.utils.localstorage.storage.get(day8.re_frame_10x.utils.localstorage.safe_key(key));
if((void 0 === value)){
return not_found;
} else {
return cljs.reader.read_string.cljs$core$IFn$_invoke$arity$1(value);
}
}));

(day8.re_frame_10x.utils.localstorage.get.cljs$lang$maxFixedArity = 2);

/**
 * Saves a re-frame-10x value to local storage.
 */
day8.re_frame_10x.utils.localstorage.save_BANG_ = (function day8$re_frame_10x$utils$localstorage$save_BANG_(key,value){
return day8.re_frame_10x.utils.localstorage.storage.set(day8.re_frame_10x.utils.localstorage.safe_key(key),cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([value], 0)));
});
/**
 * Deletes all re-frame-10x config keys
 */
day8.re_frame_10x.utils.localstorage.delete_all_keys_BANG_ = (function day8$re_frame_10x$utils$localstorage$delete_all_keys_BANG_(){
var seq__58752 = cljs.core.seq(Object.keys(localStorage));
var chunk__58753 = null;
var count__58754 = (0);
var i__58755 = (0);
while(true){
if((i__58755 < count__58754)){
var k = chunk__58753.cljs$core$IIndexed$_nth$arity$2(null,i__58755);
if(clojure.string.starts_with_QMARK_(k,day8.re_frame_10x.utils.localstorage.safe_prefix)){
day8.re_frame_10x.utils.localstorage.storage.remove(k);
} else {
}


var G__58768 = seq__58752;
var G__58769 = chunk__58753;
var G__58770 = count__58754;
var G__58771 = (i__58755 + (1));
seq__58752 = G__58768;
chunk__58753 = G__58769;
count__58754 = G__58770;
i__58755 = G__58771;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__58752);
if(temp__5735__auto__){
var seq__58752__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__58752__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__58752__$1);
var G__58775 = cljs.core.chunk_rest(seq__58752__$1);
var G__58776 = c__4556__auto__;
var G__58777 = cljs.core.count(c__4556__auto__);
var G__58778 = (0);
seq__58752 = G__58775;
chunk__58753 = G__58776;
count__58754 = G__58777;
i__58755 = G__58778;
continue;
} else {
var k = cljs.core.first(seq__58752__$1);
if(clojure.string.starts_with_QMARK_(k,day8.re_frame_10x.utils.localstorage.safe_prefix)){
day8.re_frame_10x.utils.localstorage.storage.remove(k);
} else {
}


var G__58785 = cljs.core.next(seq__58752__$1);
var G__58786 = null;
var G__58787 = (0);
var G__58788 = (0);
seq__58752 = G__58785;
chunk__58753 = G__58786;
count__58754 = G__58787;
i__58755 = G__58788;
continue;
}
} else {
return null;
}
}
break;
}
});

//# sourceMappingURL=day8.re_frame_10x.utils.localstorage.js.map
