goog.provide('playground.blackbelt.state');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('cljs.pprint');
goog.require('reagent.core');
goog.require('re_frame.core');
var module$node_modules$faker$index=shadow.js.require("module$node_modules$faker$index", {});
playground.blackbelt.state.get_id = (function playground$blackbelt$state$get_id(n){
var alphabet = cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.char$,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.range.cljs$core$IFn$_invoke$arity$2((48),(58)),cljs.core.range.cljs$core$IFn$_invoke$arity$2((97),(123))));
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$2(n,(function (){
return cljs.core.rand_nth(cljs.core.map.cljs$core$IFn$_invoke$arity$2(cljs.core.char$,alphabet));
})));
});
playground.blackbelt.state.get_user = (function playground$blackbelt$state$get_user(){
var user_id = playground.blackbelt.state.get_id((8));
var first_name = module$node_modules$faker$index.name.firstName();
var last_name = module$node_modules$faker$index.name.lastName();
return cljs.core.PersistentArrayMap.createAsIfByAssoc([user_id,cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"image-url","image-url",-1064784064),new cljs.core.Keyword(null,"email","email",1415816706),new cljs.core.Keyword(null,"last-name","last-name",-1695738974),new cljs.core.Keyword(null,"password","password",417022471),new cljs.core.Keyword(null,"authored-tasks","authored-tasks",731637256),new cljs.core.Keyword(null,"user-id","user-id",-206822291),new cljs.core.Keyword(null,"first-name","first-name",-1559982131),new cljs.core.Keyword(null,"authored-task-trees","authored-task-trees",1111439032),new cljs.core.Keyword(null,"date-joined","date-joined",-206927907)],[module$node_modules$faker$index.image.avatar(),clojure.string.lower_case([cljs.core.str.cljs$core$IFn$_invoke$arity$1(first_name),".",cljs.core.str.cljs$core$IFn$_invoke$arity$1(last_name),"@gmail.com"].join('')),last_name,"password",cljs.core.PersistentHashSet.EMPTY,user_id,first_name,cljs.core.PersistentHashSet.EMPTY,(Date.now() - cljs.core.rand_int(Math.pow((2),(34))))])]);
});
playground.blackbelt.state._STAR_users = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.merge,cljs.core.take.cljs$core$IFn$_invoke$arity$2((10),cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$1(new cljs.core.Var(function(){return playground.blackbelt.state.get_user;},new cljs.core.Symbol("playground.blackbelt.state","get-user","playground.blackbelt.state/get-user",1402270739,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"playground.blackbelt.state","playground.blackbelt.state",-929047165,null),new cljs.core.Symbol(null,"get-user","get-user",650221929,null),"playground/blackbelt/state.cljs",15,1,15,15,cljs.core.list(cljs.core.PersistentVector.EMPTY),null,(cljs.core.truth_(playground.blackbelt.state.get_user)?playground.blackbelt.state.get_user.cljs$lang$test:null)]))))));
/**
 * When a user opens a task, the task components will look for data here.
 *   <user>.<tasks>.<task-01> keys should mirror <task-01>.<components>;
 *   <user>.<tasks>.<task-01> should give USER data, not task data.
 */
playground.blackbelt.state._STAR_user_data = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"user-01","user-01",1852854892),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"user-id","user-id",-206822291),new cljs.core.Keyword(null,"user-01","user-01",1852854892),new cljs.core.Keyword(null,"task-trees","task-trees",1431516324),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"task-tree-01","task-tree-01",-2112892967),cljs.core.PersistentArrayMap.EMPTY], null),new cljs.core.Keyword(null,"tasks","tasks",-1754368880),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"task-01","task-01",-1454906931),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"repetition","repetition",1938392115),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"bounty","bounty",-1268700828),cljs.core.PersistentArrayMap.EMPTY], null),new cljs.core.Keyword(null,"task-02","task-02",-2006197240),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"repetition","repetition",1938392115),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"bounty","bounty",-1268700828),cljs.core.PersistentArrayMap.EMPTY], null)], null)], null)], null));
playground.blackbelt.state.get_task = (function playground$blackbelt$state$get_task(){
var task_id = playground.blackbelt.state.get_id((16));
return cljs.core.PersistentArrayMap.createAsIfByAssoc([task_id,cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"description","description",-1428560544),new cljs.core.Keyword(null,"image-url","image-url",-1064784064),new cljs.core.Keyword(null,"task-id","task-id",-1045480126),new cljs.core.Keyword(null,"date-created","date-created",603296579),new cljs.core.Keyword(null,"date-modified","date-modified",-976204023),new cljs.core.Keyword(null,"title","title",636505583),new cljs.core.Keyword(null,"author","author",2111686192),new cljs.core.Keyword(null,"components","components",-1073188942),new cljs.core.Keyword(null,"subtitle","subtitle",-1614524363),new cljs.core.Keyword(null,"materials","materials",2036902582),new cljs.core.Keyword(null,"version","version",425292698)],[module$node_modules$faker$index.lorem.paragraph(),module$node_modules$faker$index.image.technics(),task_id,(Date.now() - cljs.core.rand_int(Math.pow((2),(32)))),(Date.now() - cljs.core.rand_int(Math.pow((2),(30)))),[clojure.string.capitalize(module$node_modules$faker$index.hacker.verb())," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.noun()),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand_nth(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [" with "," using "," via "], null))),cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.adjective())," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.noun())].join(''),playground.blackbelt.state.get_id((8)),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"repetition","repetition",1938392115),new cljs.core.Keyword(null,"bounty","bounty",-1268700828)], null),clojure.string.capitalize(module$node_modules$faker$index.hacker.phrase()),cljs.core.vec((function (){var iter__4529__auto__ = (function playground$blackbelt$state$get_task_$_iter__55108(s__55109){
return (new cljs.core.LazySeq(null,(function (){
var s__55109__$1 = s__55109;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55109__$1);
if(temp__5735__auto__){
var s__55109__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55109__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55109__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55111 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55110 = (0);
while(true){
if((i__55110 < size__4528__auto__)){
var x = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__55110);
cljs.core.chunk_append(b__55111,(new cljs.core.List(null,module$node_modules$faker$index.commerce.product(),(new cljs.core.List(null,cljs.core.rand_int((10)),null,(1),null)),(2),null)));

var G__55115 = (i__55110 + (1));
i__55110 = G__55115;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55111),playground$blackbelt$state$get_task_$_iter__55108(cljs.core.chunk_rest(s__55109__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55111),null);
}
} else {
var x = cljs.core.first(s__55109__$2);
return cljs.core.cons((new cljs.core.List(null,module$node_modules$faker$index.commerce.product(),(new cljs.core.List(null,cljs.core.rand_int((10)),null,(1),null)),(2),null)),playground$blackbelt$state$get_task_$_iter__55108(cljs.core.rest(s__55109__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(cljs.core.range.cljs$core$IFn$_invoke$arity$1(cljs.core.rand_int((5))));
})()),cljs.core.rand_int((5))])]);
});
playground.blackbelt.state._STAR_tasks = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.merge,cljs.core.take.cljs$core$IFn$_invoke$arity$2((20),cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$1(playground.blackbelt.state.get_task))));
playground.blackbelt.state.get_task_tree = (function playground$blackbelt$state$get_task_tree(_STAR_tasks){
var tasks = cljs.core.deref(_STAR_tasks);
var task_tree_id = playground.blackbelt.state.get_id((12));
var task_keys = cljs.core.keys(tasks);
var root = cljs.core.rand_nth(cljs.core.keys(tasks));
return cljs.core.PersistentArrayMap.createAsIfByAssoc([task_tree_id,cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"description","description",-1428560544),new cljs.core.Keyword(null,"image-url","image-url",-1064784064),new cljs.core.Keyword(null,"children","children",-940561982),new cljs.core.Keyword(null,"date-created","date-created",603296579),new cljs.core.Keyword(null,"date-modified","date-modified",-976204023),new cljs.core.Keyword(null,"title","title",636505583),new cljs.core.Keyword(null,"author","author",2111686192),new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"subtitle","subtitle",-1614524363),new cljs.core.Keyword(null,"version","version",425292698),new cljs.core.Keyword(null,"task-tree-id","task-tree-id",-888073541)],[clojure.string.capitalize(module$node_modules$faker$index.lorem.paragraph()),module$node_modules$faker$index.image.technics(),cljs.core.set(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__55112_SHARP_){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(p1__55112_SHARP_,root);
}),cljs.core.take.cljs$core$IFn$_invoke$arity$2((5),task_keys))),(Date.now() - cljs.core.rand_int(Math.pow((2),(32)))),(Date.now() - cljs.core.rand_int(Math.pow((2),(30)))),[clojure.string.capitalize(module$node_modules$faker$index.hacker.ingverb())," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(module$node_modules$faker$index.hacker.noun())].join(''),playground.blackbelt.state.get_id((8)),root,clojure.string.capitalize(module$node_modules$faker$index.hacker.phrase()),cljs.core.rand_int((5)),task_tree_id])]);
});
playground.blackbelt.state._STAR_task_trees = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.merge,cljs.core.take.cljs$core$IFn$_invoke$arity$2((4),cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$1((function (){
return playground.blackbelt.state.get_task_tree(playground.blackbelt.state._STAR_tasks);
})))));
playground.blackbelt.state.app_db = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"authenticated","authenticated",1282954211),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"user-id","user-id",-206822291),null], null),new cljs.core.Keyword(null,"errors","errors",-908790718),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"nav","nav",719540477),new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"active-page","active-page",370357330),new cljs.core.Keyword(null,"home","home",-74557309),new cljs.core.Keyword(null,"active-nav","active-nav",-275650057),new cljs.core.Keyword(null,"home","home",-74557309),new cljs.core.Keyword(null,"active-modal","active-modal",1608517958),null,new cljs.core.Keyword(null,"active-profile","active-profile",1308207560),null,new cljs.core.Keyword(null,"active-task","active-task",-1554185386),null,new cljs.core.Keyword(null,"active-task-tree","active-task-tree",-1024825055),null], null),new cljs.core.Keyword(null,"users","users",-713552705),cljs.core.deref(playground.blackbelt.state._STAR_users),new cljs.core.Keyword(null,"tasks","tasks",-1754368880),cljs.core.deref(playground.blackbelt.state._STAR_tasks),new cljs.core.Keyword(null,"task-trees","task-trees",1431516324),cljs.core.deref(playground.blackbelt.state._STAR_task_trees)], null);
re_frame.core.reg_event_db.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"initialize-blackbelt-db","initialize-blackbelt-db",1389266178),(function (_,___$1){
return playground.blackbelt.state.app_db;
}));

//# sourceMappingURL=playground.blackbelt.state.js.map
