goog.provide('re_frame.fx');
goog.require('cljs.core');
goog.require('re_frame.router');
goog.require('re_frame.db');
goog.require('re_frame.interceptor');
goog.require('re_frame.interop');
goog.require('re_frame.events');
goog.require('re_frame.registrar');
goog.require('re_frame.loggers');
goog.require('re_frame.trace');
re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_((re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1 ? re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1(re_frame.fx.kind) : re_frame.registrar.kinds.call(null,re_frame.fx.kind)))){
} else {
throw (new Error("Assert failed: (re-frame.registrar/kinds kind)"));
}
re_frame.fx.reg_fx = (function re_frame$fx$reg_fx(id,handler){
return re_frame.registrar.register_handler(re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed, other than that
 *   `:db` is guaranteed to be executed first.
 */
re_frame.fx.do_fx = re_frame.interceptor.__GT_interceptor.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function re_frame$fx$do_fx_after(context){
if(re_frame.trace.is_trace_enabled_QMARK_()){
var _STAR_current_trace_STAR__orig_val__58765 = re_frame.trace._STAR_current_trace_STAR_;
var _STAR_current_trace_STAR__temp_val__58766 = re_frame.trace.start_trace(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));
(re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__temp_val__58766);

try{try{var effects = new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context);
var effects_without_db = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(effects,new cljs.core.Keyword(null,"db","db",993250759));
var temp__5735__auto___59022 = new cljs.core.Keyword(null,"db","db",993250759).cljs$core$IFn$_invoke$arity$1(effects);
if(cljs.core.truth_(temp__5735__auto___59022)){
var new_db_59023 = temp__5735__auto___59022;
var fexpr__58779_59024 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,new cljs.core.Keyword(null,"db","db",993250759),false);
(fexpr__58779_59024.cljs$core$IFn$_invoke$arity$1 ? fexpr__58779_59024.cljs$core$IFn$_invoke$arity$1(new_db_59023) : fexpr__58779_59024.call(null,new_db_59023));
} else {
}

var seq__58780 = cljs.core.seq(effects_without_db);
var chunk__58781 = null;
var count__58782 = (0);
var i__58783 = (0);
while(true){
if((i__58783 < count__58782)){
var vec__58806 = chunk__58781.cljs$core$IIndexed$_nth$arity$2(null,i__58783);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58806,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58806,(1),null);
var temp__5733__auto___59027 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___59027)){
var effect_fn_59028 = temp__5733__auto___59027;
(effect_fn_59028.cljs$core$IFn$_invoke$arity$1 ? effect_fn_59028.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_59028.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__59029 = seq__58780;
var G__59030 = chunk__58781;
var G__59031 = count__58782;
var G__59032 = (i__58783 + (1));
seq__58780 = G__59029;
chunk__58781 = G__59030;
count__58782 = G__59031;
i__58783 = G__59032;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__58780);
if(temp__5735__auto__){
var seq__58780__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__58780__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__58780__$1);
var G__59037 = cljs.core.chunk_rest(seq__58780__$1);
var G__59038 = c__4556__auto__;
var G__59039 = cljs.core.count(c__4556__auto__);
var G__59040 = (0);
seq__58780 = G__59037;
chunk__58781 = G__59038;
count__58782 = G__59039;
i__58783 = G__59040;
continue;
} else {
var vec__58816 = cljs.core.first(seq__58780__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58816,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58816,(1),null);
var temp__5733__auto___59042 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___59042)){
var effect_fn_59043 = temp__5733__auto___59042;
(effect_fn_59043.cljs$core$IFn$_invoke$arity$1 ? effect_fn_59043.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_59043.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__59044 = cljs.core.next(seq__58780__$1);
var G__59045 = null;
var G__59046 = (0);
var G__59047 = (0);
seq__58780 = G__59044;
chunk__58781 = G__59045;
count__58782 = G__59046;
i__58783 = G__59047;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(re_frame.trace.is_trace_enabled_QMARK_()){
var end__58157__auto___59051 = re_frame.interop.now();
var duration__58158__auto___59052 = (end__58157__auto___59051 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__58158__auto___59052,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),re_frame.interop.now()], 0)));

re_frame.trace.run_tracing_callbacks_BANG_(end__58157__auto___59051);
} else {
}
}}finally {(re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__orig_val__58765);
}} else {
var effects = new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context);
var effects_without_db = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(effects,new cljs.core.Keyword(null,"db","db",993250759));
var temp__5735__auto___59058 = new cljs.core.Keyword(null,"db","db",993250759).cljs$core$IFn$_invoke$arity$1(effects);
if(cljs.core.truth_(temp__5735__auto___59058)){
var new_db_59059 = temp__5735__auto___59058;
var fexpr__58823_59060 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,new cljs.core.Keyword(null,"db","db",993250759),false);
(fexpr__58823_59060.cljs$core$IFn$_invoke$arity$1 ? fexpr__58823_59060.cljs$core$IFn$_invoke$arity$1(new_db_59059) : fexpr__58823_59060.call(null,new_db_59059));
} else {
}

var seq__58824 = cljs.core.seq(effects_without_db);
var chunk__58825 = null;
var count__58826 = (0);
var i__58827 = (0);
while(true){
if((i__58827 < count__58826)){
var vec__58841 = chunk__58825.cljs$core$IIndexed$_nth$arity$2(null,i__58827);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58841,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58841,(1),null);
var temp__5733__auto___59061 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___59061)){
var effect_fn_59066 = temp__5733__auto___59061;
(effect_fn_59066.cljs$core$IFn$_invoke$arity$1 ? effect_fn_59066.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_59066.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__59071 = seq__58824;
var G__59072 = chunk__58825;
var G__59073 = count__58826;
var G__59074 = (i__58827 + (1));
seq__58824 = G__59071;
chunk__58825 = G__59072;
count__58826 = G__59073;
i__58827 = G__59074;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__58824);
if(temp__5735__auto__){
var seq__58824__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__58824__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__58824__$1);
var G__59079 = cljs.core.chunk_rest(seq__58824__$1);
var G__59080 = c__4556__auto__;
var G__59081 = cljs.core.count(c__4556__auto__);
var G__59082 = (0);
seq__58824 = G__59079;
chunk__58825 = G__59080;
count__58826 = G__59081;
i__58827 = G__59082;
continue;
} else {
var vec__58851 = cljs.core.first(seq__58824__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58851,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58851,(1),null);
var temp__5733__auto___59090 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___59090)){
var effect_fn_59091 = temp__5733__auto___59090;
(effect_fn_59091.cljs$core$IFn$_invoke$arity$1 ? effect_fn_59091.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_59091.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__59096 = cljs.core.next(seq__58824__$1);
var G__59097 = null;
var G__59098 = (0);
var G__59099 = (0);
seq__58824 = G__59096;
chunk__58825 = G__59097;
count__58826 = G__59098;
i__58827 = G__59099;
continue;
}
} else {
return null;
}
}
break;
}
}
})], 0));
re_frame.fx.dispatch_later = (function re_frame$fx$dispatch_later(p__58859){
var map__58860 = p__58859;
var map__58860__$1 = (((((!((map__58860 == null))))?(((((map__58860.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__58860.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__58860):map__58860);
var effect = map__58860__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__58860__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__58860__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
return re_frame.interop.set_timeout_BANG_((function (){
return re_frame.router.dispatch(dispatch);
}),ms);
}
});
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
if(cljs.core.map_QMARK_(value)){
return re_frame.fx.dispatch_later(value);
} else {
var seq__58866 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__58867 = null;
var count__58868 = (0);
var i__58869 = (0);
while(true){
if((i__58869 < count__58868)){
var effect = chunk__58867.cljs$core$IIndexed$_nth$arity$2(null,i__58869);
re_frame.fx.dispatch_later(effect);


var G__59111 = seq__58866;
var G__59112 = chunk__58867;
var G__59113 = count__58868;
var G__59114 = (i__58869 + (1));
seq__58866 = G__59111;
chunk__58867 = G__59112;
count__58868 = G__59113;
i__58869 = G__59114;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__58866);
if(temp__5735__auto__){
var seq__58866__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__58866__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__58866__$1);
var G__59120 = cljs.core.chunk_rest(seq__58866__$1);
var G__59121 = c__4556__auto__;
var G__59122 = cljs.core.count(c__4556__auto__);
var G__59123 = (0);
seq__58866 = G__59120;
chunk__58867 = G__59121;
count__58868 = G__59122;
i__58869 = G__59123;
continue;
} else {
var effect = cljs.core.first(seq__58866__$1);
re_frame.fx.dispatch_later(effect);


var G__59127 = cljs.core.next(seq__58866__$1);
var G__59128 = null;
var G__59129 = (0);
var G__59130 = (0);
seq__58866 = G__59127;
chunk__58867 = G__59128;
count__58868 = G__59129;
i__58869 = G__59130;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"fx","fx",-1237829572),(function (seq_of_effects){
if((!(cljs.core.sequential_QMARK_(seq_of_effects)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect expects a seq, but was given ",cljs.core.type(seq_of_effects)], 0));
} else {
var seq__58881 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,seq_of_effects));
var chunk__58882 = null;
var count__58883 = (0);
var i__58884 = (0);
while(true){
if((i__58884 < count__58883)){
var vec__58914 = chunk__58882.cljs$core$IIndexed$_nth$arity$2(null,i__58884);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58914,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58914,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"db","db",993250759),effect_key)){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect should not contain a :db effect"], 0));
} else {
}

var temp__5733__auto___59142 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___59142)){
var effect_fn_59146 = temp__5733__auto___59142;
(effect_fn_59146.cljs$core$IFn$_invoke$arity$1 ? effect_fn_59146.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_59146.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: in \":fx\" effect found ",effect_key," which has no associated handler. Ignoring."], 0));
}


var G__59152 = seq__58881;
var G__59153 = chunk__58882;
var G__59154 = count__58883;
var G__59155 = (i__58884 + (1));
seq__58881 = G__59152;
chunk__58882 = G__59153;
count__58883 = G__59154;
i__58884 = G__59155;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__58881);
if(temp__5735__auto__){
var seq__58881__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__58881__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__58881__$1);
var G__59158 = cljs.core.chunk_rest(seq__58881__$1);
var G__59159 = c__4556__auto__;
var G__59160 = cljs.core.count(c__4556__auto__);
var G__59161 = (0);
seq__58881 = G__59158;
chunk__58882 = G__59159;
count__58883 = G__59160;
i__58884 = G__59161;
continue;
} else {
var vec__58933 = cljs.core.first(seq__58881__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58933,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58933,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"db","db",993250759),effect_key)){
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: \":fx\" effect should not contain a :db effect"], 0));
} else {
}

var temp__5733__auto___59171 = re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___59171)){
var effect_fn_59173 = temp__5733__auto___59171;
(effect_fn_59173.cljs$core$IFn$_invoke$arity$1 ? effect_fn_59173.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_59173.call(null,effect_value));
} else {
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: in \":fx\" effect found ",effect_key," which has no associated handler. Ignoring."], 0));
}


var G__59177 = cljs.core.next(seq__58881__$1);
var G__59178 = null;
var G__59179 = (0);
var G__59180 = (0);
seq__58881 = G__59177;
chunk__58882 = G__59178;
count__58883 = G__59179;
i__58884 = G__59180;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if((!(cljs.core.vector_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value], 0));
} else {
return re_frame.router.dispatch(value);
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if((!(cljs.core.sequential_QMARK_(value)))){
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-n value. Expected a collection, but got:",value], 0));
} else {
var seq__58950 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__58951 = null;
var count__58952 = (0);
var i__58953 = (0);
while(true){
if((i__58953 < count__58952)){
var event = chunk__58951.cljs$core$IIndexed$_nth$arity$2(null,i__58953);
re_frame.router.dispatch(event);


var G__59186 = seq__58950;
var G__59187 = chunk__58951;
var G__59188 = count__58952;
var G__59189 = (i__58953 + (1));
seq__58950 = G__59186;
chunk__58951 = G__59187;
count__58952 = G__59188;
i__58953 = G__59189;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__58950);
if(temp__5735__auto__){
var seq__58950__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__58950__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__58950__$1);
var G__59191 = cljs.core.chunk_rest(seq__58950__$1);
var G__59192 = c__4556__auto__;
var G__59193 = cljs.core.count(c__4556__auto__);
var G__59194 = (0);
seq__58950 = G__59191;
chunk__58951 = G__59192;
count__58952 = G__59193;
i__58953 = G__59194;
continue;
} else {
var event = cljs.core.first(seq__58950__$1);
re_frame.router.dispatch(event);


var G__59196 = cljs.core.next(seq__58950__$1);
var G__59197 = null;
var G__59198 = (0);
var G__59199 = (0);
seq__58950 = G__59196;
chunk__58951 = G__59197;
count__58952 = G__59198;
i__58953 = G__59199;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.cljs$core$IFn$_invoke$arity$2(re_frame.registrar.clear_handlers,re_frame.events.kind);
if(cljs.core.sequential_QMARK_(value)){
var seq__58972 = cljs.core.seq(value);
var chunk__58973 = null;
var count__58974 = (0);
var i__58975 = (0);
while(true){
if((i__58975 < count__58974)){
var event = chunk__58973.cljs$core$IIndexed$_nth$arity$2(null,i__58975);
(clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(event) : clear_event.call(null,event));


var G__59204 = seq__58972;
var G__59205 = chunk__58973;
var G__59206 = count__58974;
var G__59207 = (i__58975 + (1));
seq__58972 = G__59204;
chunk__58973 = G__59205;
count__58974 = G__59206;
i__58975 = G__59207;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__58972);
if(temp__5735__auto__){
var seq__58972__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__58972__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__58972__$1);
var G__59210 = cljs.core.chunk_rest(seq__58972__$1);
var G__59211 = c__4556__auto__;
var G__59212 = cljs.core.count(c__4556__auto__);
var G__59213 = (0);
seq__58972 = G__59210;
chunk__58973 = G__59211;
count__58974 = G__59212;
i__58975 = G__59213;
continue;
} else {
var event = cljs.core.first(seq__58972__$1);
(clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(event) : clear_event.call(null,event));


var G__59214 = cljs.core.next(seq__58972__$1);
var G__59215 = null;
var G__59216 = (0);
var G__59217 = (0);
seq__58972 = G__59214;
chunk__58973 = G__59215;
count__58974 = G__59216;
i__58975 = G__59217;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return (clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(value) : clear_event.call(null,value));
}
}));
re_frame.fx.reg_fx(new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if((!((cljs.core.deref(re_frame.db.app_db) === value)))){
return cljs.core.reset_BANG_(re_frame.db.app_db,value);
} else {
return null;
}
}));

//# sourceMappingURL=re_frame.fx.js.map
