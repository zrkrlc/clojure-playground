goog.provide('shadow.dom');
goog.require('cljs.core');
goog.require('goog.dom');
goog.require('goog.dom.forms');
goog.require('goog.dom.classlist');
goog.require('goog.style');
goog.require('goog.style.transition');
goog.require('goog.string');
goog.require('clojure.string');
goog.require('cljs.core.async');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_66122 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_66122(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_66125 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_66125(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__65206 = coll;
var G__65207 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__65206,G__65207) : shadow.dom.lazy_native_coll_seq.call(null,G__65206,G__65207));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4126__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__65276 = arguments.length;
switch (G__65276) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__65288 = arguments.length;
switch (G__65288) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__65311 = arguments.length;
switch (G__65311) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__65333 = arguments.length;
switch (G__65333) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__65343 = arguments.length;
switch (G__65343) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
var G__65344 = document;
var G__65345 = shadow.dom.dom_node(el);
return goog.dom.contains(G__65344,G__65345);
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
var G__65346 = shadow.dom.dom_node(parent);
var G__65347 = shadow.dom.dom_node(el);
return goog.dom.contains(G__65346,G__65347);
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
var G__65349 = shadow.dom.dom_node(el);
var G__65350 = cls;
return goog.dom.classlist.add(G__65349,G__65350);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
var G__65352 = shadow.dom.dom_node(el);
var G__65353 = cls;
return goog.dom.classlist.remove(G__65352,G__65353);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__65357 = arguments.length;
switch (G__65357) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
var G__65362 = shadow.dom.dom_node(el);
var G__65363 = cls;
return goog.dom.classlist.toggle(G__65362,G__65363);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e65379){if((e65379 instanceof Object)){
var e = e65379;
return console.log("didnt support attachEvent",el,e);
} else {
throw e65379;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__65396 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__65397 = null;
var count__65398 = (0);
var i__65399 = (0);
while(true){
if((i__65399 < count__65398)){
var el = chunk__65397.cljs$core$IIndexed$_nth$arity$2(null,i__65399);
var handler_66152__$1 = ((function (seq__65396,chunk__65397,count__65398,i__65399,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__65396,chunk__65397,count__65398,i__65399,el))
;
var G__65414_66153 = el;
var G__65415_66154 = cljs.core.name(ev);
var G__65416_66155 = handler_66152__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__65414_66153,G__65415_66154,G__65416_66155) : shadow.dom.dom_listen.call(null,G__65414_66153,G__65415_66154,G__65416_66155));


var G__66156 = seq__65396;
var G__66157 = chunk__65397;
var G__66158 = count__65398;
var G__66159 = (i__65399 + (1));
seq__65396 = G__66156;
chunk__65397 = G__66157;
count__65398 = G__66158;
i__65399 = G__66159;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__65396);
if(temp__5735__auto__){
var seq__65396__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__65396__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__65396__$1);
var G__66161 = cljs.core.chunk_rest(seq__65396__$1);
var G__66162 = c__4556__auto__;
var G__66163 = cljs.core.count(c__4556__auto__);
var G__66164 = (0);
seq__65396 = G__66161;
chunk__65397 = G__66162;
count__65398 = G__66163;
i__65399 = G__66164;
continue;
} else {
var el = cljs.core.first(seq__65396__$1);
var handler_66165__$1 = ((function (seq__65396,chunk__65397,count__65398,i__65399,el,seq__65396__$1,temp__5735__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__65396,chunk__65397,count__65398,i__65399,el,seq__65396__$1,temp__5735__auto__))
;
var G__65417_66166 = el;
var G__65418_66167 = cljs.core.name(ev);
var G__65419_66168 = handler_66165__$1;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__65417_66166,G__65418_66167,G__65419_66168) : shadow.dom.dom_listen.call(null,G__65417_66166,G__65418_66167,G__65419_66168));


var G__66170 = cljs.core.next(seq__65396__$1);
var G__66171 = null;
var G__66172 = (0);
var G__66173 = (0);
seq__65396 = G__66170;
chunk__65397 = G__66171;
count__65398 = G__66172;
i__65399 = G__66173;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__65424 = arguments.length;
switch (G__65424) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
var G__65429 = shadow.dom.dom_node(el);
var G__65430 = cljs.core.name(ev);
var G__65431 = handler__$1;
return (shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__65429,G__65430,G__65431) : shadow.dom.dom_listen.call(null,G__65429,G__65430,G__65431));
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
var G__65434 = shadow.dom.dom_node(el);
var G__65435 = cljs.core.name(ev);
var G__65436 = handler;
return (shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen_remove.cljs$core$IFn$_invoke$arity$3(G__65434,G__65435,G__65436) : shadow.dom.dom_listen_remove.call(null,G__65434,G__65435,G__65436));
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__65439 = cljs.core.seq(events);
var chunk__65440 = null;
var count__65441 = (0);
var i__65442 = (0);
while(true){
if((i__65442 < count__65441)){
var vec__65458 = chunk__65440.cljs$core$IIndexed$_nth$arity$2(null,i__65442);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65458,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65458,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__66180 = seq__65439;
var G__66181 = chunk__65440;
var G__66182 = count__65441;
var G__66183 = (i__65442 + (1));
seq__65439 = G__66180;
chunk__65440 = G__66181;
count__65441 = G__66182;
i__65442 = G__66183;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__65439);
if(temp__5735__auto__){
var seq__65439__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__65439__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__65439__$1);
var G__66185 = cljs.core.chunk_rest(seq__65439__$1);
var G__66186 = c__4556__auto__;
var G__66187 = cljs.core.count(c__4556__auto__);
var G__66188 = (0);
seq__65439 = G__66185;
chunk__65440 = G__66186;
count__65441 = G__66187;
i__65442 = G__66188;
continue;
} else {
var vec__65465 = cljs.core.first(seq__65439__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65465,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65465,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__66190 = cljs.core.next(seq__65439__$1);
var G__66191 = null;
var G__66192 = (0);
var G__66193 = (0);
seq__65439 = G__66190;
chunk__65440 = G__66191;
count__65441 = G__66192;
i__65442 = G__66193;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__65477 = cljs.core.seq(styles);
var chunk__65478 = null;
var count__65479 = (0);
var i__65480 = (0);
while(true){
if((i__65480 < count__65479)){
var vec__65505 = chunk__65478.cljs$core$IIndexed$_nth$arity$2(null,i__65480);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65505,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65505,(1),null);
var G__65508_66195 = dom;
var G__65509_66196 = cljs.core.name(k);
var G__65510_66197 = (((v == null))?"":v);
goog.style.setStyle(G__65508_66195,G__65509_66196,G__65510_66197);


var G__66198 = seq__65477;
var G__66199 = chunk__65478;
var G__66200 = count__65479;
var G__66201 = (i__65480 + (1));
seq__65477 = G__66198;
chunk__65478 = G__66199;
count__65479 = G__66200;
i__65480 = G__66201;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__65477);
if(temp__5735__auto__){
var seq__65477__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__65477__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__65477__$1);
var G__66203 = cljs.core.chunk_rest(seq__65477__$1);
var G__66204 = c__4556__auto__;
var G__66205 = cljs.core.count(c__4556__auto__);
var G__66206 = (0);
seq__65477 = G__66203;
chunk__65478 = G__66204;
count__65479 = G__66205;
i__65480 = G__66206;
continue;
} else {
var vec__65517 = cljs.core.first(seq__65477__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65517,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65517,(1),null);
var G__65521_66207 = dom;
var G__65523_66208 = cljs.core.name(k);
var G__65524_66209 = (((v == null))?"":v);
goog.style.setStyle(G__65521_66207,G__65523_66208,G__65524_66209);


var G__66211 = cljs.core.next(seq__65477__$1);
var G__66212 = null;
var G__66213 = (0);
var G__66214 = (0);
seq__65477 = G__66211;
chunk__65478 = G__66212;
count__65479 = G__66213;
i__65480 = G__66214;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__65529_66216 = key;
var G__65529_66217__$1 = (((G__65529_66216 instanceof cljs.core.Keyword))?G__65529_66216.fqn:null);
switch (G__65529_66217__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_66225 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4126__auto__ = goog.string.startsWith(ks_66225,"data-");
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return goog.string.startsWith(ks_66225,"aria-");
}
})())){
el.setAttribute(ks_66225,value);
} else {
(el[ks_66225] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
var G__65541 = shadow.dom.dom_node(el);
var G__65542 = cls;
return goog.dom.classlist.contains(G__65541,G__65542);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__65553){
var map__65554 = p__65553;
var map__65554__$1 = (((((!((map__65554 == null))))?(((((map__65554.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__65554.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__65554):map__65554);
var props = map__65554__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__65554__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__65556 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65556,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65556,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65556,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__65559 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__65559,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__65559;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__65575 = arguments.length;
switch (G__65575) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__65579){
var vec__65581 = p__65579;
var seq__65582 = cljs.core.seq(vec__65581);
var first__65583 = cljs.core.first(seq__65582);
var seq__65582__$1 = cljs.core.next(seq__65582);
var nn = first__65583;
var first__65583__$1 = cljs.core.first(seq__65582__$1);
var seq__65582__$2 = cljs.core.next(seq__65582__$1);
var np = first__65583__$1;
var nc = seq__65582__$2;
var node = vec__65581;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__65585 = nn;
var G__65586 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__65585,G__65586) : create_fn.call(null,G__65585,G__65586));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__65589 = nn;
var G__65590 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__65589,G__65590) : create_fn.call(null,G__65589,G__65590));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__65592 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65592,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65592,(1),null);
var seq__65596_66247 = cljs.core.seq(node_children);
var chunk__65597_66248 = null;
var count__65598_66249 = (0);
var i__65599_66250 = (0);
while(true){
if((i__65599_66250 < count__65598_66249)){
var child_struct_66253 = chunk__65597_66248.cljs$core$IIndexed$_nth$arity$2(null,i__65599_66250);
var children_66254 = shadow.dom.dom_node(child_struct_66253);
if(cljs.core.seq_QMARK_(children_66254)){
var seq__65634_66255 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_66254));
var chunk__65636_66256 = null;
var count__65637_66257 = (0);
var i__65638_66258 = (0);
while(true){
if((i__65638_66258 < count__65637_66257)){
var child_66260 = chunk__65636_66256.cljs$core$IIndexed$_nth$arity$2(null,i__65638_66258);
if(cljs.core.truth_(child_66260)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_66260);


var G__66261 = seq__65634_66255;
var G__66262 = chunk__65636_66256;
var G__66263 = count__65637_66257;
var G__66264 = (i__65638_66258 + (1));
seq__65634_66255 = G__66261;
chunk__65636_66256 = G__66262;
count__65637_66257 = G__66263;
i__65638_66258 = G__66264;
continue;
} else {
var G__66265 = seq__65634_66255;
var G__66266 = chunk__65636_66256;
var G__66267 = count__65637_66257;
var G__66268 = (i__65638_66258 + (1));
seq__65634_66255 = G__66265;
chunk__65636_66256 = G__66266;
count__65637_66257 = G__66267;
i__65638_66258 = G__66268;
continue;
}
} else {
var temp__5735__auto___66270 = cljs.core.seq(seq__65634_66255);
if(temp__5735__auto___66270){
var seq__65634_66271__$1 = temp__5735__auto___66270;
if(cljs.core.chunked_seq_QMARK_(seq__65634_66271__$1)){
var c__4556__auto___66272 = cljs.core.chunk_first(seq__65634_66271__$1);
var G__66273 = cljs.core.chunk_rest(seq__65634_66271__$1);
var G__66274 = c__4556__auto___66272;
var G__66275 = cljs.core.count(c__4556__auto___66272);
var G__66276 = (0);
seq__65634_66255 = G__66273;
chunk__65636_66256 = G__66274;
count__65637_66257 = G__66275;
i__65638_66258 = G__66276;
continue;
} else {
var child_66280 = cljs.core.first(seq__65634_66271__$1);
if(cljs.core.truth_(child_66280)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_66280);


var G__66281 = cljs.core.next(seq__65634_66271__$1);
var G__66282 = null;
var G__66283 = (0);
var G__66284 = (0);
seq__65634_66255 = G__66281;
chunk__65636_66256 = G__66282;
count__65637_66257 = G__66283;
i__65638_66258 = G__66284;
continue;
} else {
var G__66285 = cljs.core.next(seq__65634_66271__$1);
var G__66286 = null;
var G__66287 = (0);
var G__66288 = (0);
seq__65634_66255 = G__66285;
chunk__65636_66256 = G__66286;
count__65637_66257 = G__66287;
i__65638_66258 = G__66288;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_66254);
}


var G__66290 = seq__65596_66247;
var G__66291 = chunk__65597_66248;
var G__66292 = count__65598_66249;
var G__66293 = (i__65599_66250 + (1));
seq__65596_66247 = G__66290;
chunk__65597_66248 = G__66291;
count__65598_66249 = G__66292;
i__65599_66250 = G__66293;
continue;
} else {
var temp__5735__auto___66295 = cljs.core.seq(seq__65596_66247);
if(temp__5735__auto___66295){
var seq__65596_66296__$1 = temp__5735__auto___66295;
if(cljs.core.chunked_seq_QMARK_(seq__65596_66296__$1)){
var c__4556__auto___66297 = cljs.core.chunk_first(seq__65596_66296__$1);
var G__66298 = cljs.core.chunk_rest(seq__65596_66296__$1);
var G__66299 = c__4556__auto___66297;
var G__66300 = cljs.core.count(c__4556__auto___66297);
var G__66301 = (0);
seq__65596_66247 = G__66298;
chunk__65597_66248 = G__66299;
count__65598_66249 = G__66300;
i__65599_66250 = G__66301;
continue;
} else {
var child_struct_66306 = cljs.core.first(seq__65596_66296__$1);
var children_66308 = shadow.dom.dom_node(child_struct_66306);
if(cljs.core.seq_QMARK_(children_66308)){
var seq__65642_66309 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_66308));
var chunk__65644_66310 = null;
var count__65645_66311 = (0);
var i__65646_66312 = (0);
while(true){
if((i__65646_66312 < count__65645_66311)){
var child_66314 = chunk__65644_66310.cljs$core$IIndexed$_nth$arity$2(null,i__65646_66312);
if(cljs.core.truth_(child_66314)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_66314);


var G__66315 = seq__65642_66309;
var G__66316 = chunk__65644_66310;
var G__66317 = count__65645_66311;
var G__66318 = (i__65646_66312 + (1));
seq__65642_66309 = G__66315;
chunk__65644_66310 = G__66316;
count__65645_66311 = G__66317;
i__65646_66312 = G__66318;
continue;
} else {
var G__66320 = seq__65642_66309;
var G__66321 = chunk__65644_66310;
var G__66322 = count__65645_66311;
var G__66323 = (i__65646_66312 + (1));
seq__65642_66309 = G__66320;
chunk__65644_66310 = G__66321;
count__65645_66311 = G__66322;
i__65646_66312 = G__66323;
continue;
}
} else {
var temp__5735__auto___66324__$1 = cljs.core.seq(seq__65642_66309);
if(temp__5735__auto___66324__$1){
var seq__65642_66325__$1 = temp__5735__auto___66324__$1;
if(cljs.core.chunked_seq_QMARK_(seq__65642_66325__$1)){
var c__4556__auto___66326 = cljs.core.chunk_first(seq__65642_66325__$1);
var G__66328 = cljs.core.chunk_rest(seq__65642_66325__$1);
var G__66329 = c__4556__auto___66326;
var G__66330 = cljs.core.count(c__4556__auto___66326);
var G__66331 = (0);
seq__65642_66309 = G__66328;
chunk__65644_66310 = G__66329;
count__65645_66311 = G__66330;
i__65646_66312 = G__66331;
continue;
} else {
var child_66332 = cljs.core.first(seq__65642_66325__$1);
if(cljs.core.truth_(child_66332)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_66332);


var G__66333 = cljs.core.next(seq__65642_66325__$1);
var G__66334 = null;
var G__66335 = (0);
var G__66336 = (0);
seq__65642_66309 = G__66333;
chunk__65644_66310 = G__66334;
count__65645_66311 = G__66335;
i__65646_66312 = G__66336;
continue;
} else {
var G__66337 = cljs.core.next(seq__65642_66325__$1);
var G__66338 = null;
var G__66339 = (0);
var G__66340 = (0);
seq__65642_66309 = G__66337;
chunk__65644_66310 = G__66338;
count__65645_66311 = G__66339;
i__65646_66312 = G__66340;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_66308);
}


var G__66341 = cljs.core.next(seq__65596_66296__$1);
var G__66342 = null;
var G__66343 = (0);
var G__66344 = (0);
seq__65596_66247 = G__66341;
chunk__65597_66248 = G__66342;
count__65598_66249 = G__66343;
i__65599_66250 = G__66344;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
var G__65672 = shadow.dom.dom_node(node);
return goog.dom.removeChildren(G__65672);
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__65676 = cljs.core.seq(node);
var chunk__65677 = null;
var count__65678 = (0);
var i__65679 = (0);
while(true){
if((i__65679 < count__65678)){
var n = chunk__65677.cljs$core$IIndexed$_nth$arity$2(null,i__65679);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__66351 = seq__65676;
var G__66352 = chunk__65677;
var G__66353 = count__65678;
var G__66354 = (i__65679 + (1));
seq__65676 = G__66351;
chunk__65677 = G__66352;
count__65678 = G__66353;
i__65679 = G__66354;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__65676);
if(temp__5735__auto__){
var seq__65676__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__65676__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__65676__$1);
var G__66355 = cljs.core.chunk_rest(seq__65676__$1);
var G__66356 = c__4556__auto__;
var G__66357 = cljs.core.count(c__4556__auto__);
var G__66358 = (0);
seq__65676 = G__66355;
chunk__65677 = G__66356;
count__65678 = G__66357;
i__65679 = G__66358;
continue;
} else {
var n = cljs.core.first(seq__65676__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__66362 = cljs.core.next(seq__65676__$1);
var G__66363 = null;
var G__66364 = (0);
var G__66365 = (0);
seq__65676 = G__66362;
chunk__65677 = G__66363;
count__65678 = G__66364;
i__65679 = G__66365;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
var G__65692 = shadow.dom.dom_node(new$);
var G__65693 = shadow.dom.dom_node(old);
return goog.dom.replaceNode(G__65692,G__65693);
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__65704 = arguments.length;
switch (G__65704) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__65711 = arguments.length;
switch (G__65711) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__65724 = arguments.length;
switch (G__65724) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4126__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4742__auto__ = [];
var len__4736__auto___66375 = arguments.length;
var i__4737__auto___66376 = (0);
while(true){
if((i__4737__auto___66376 < len__4736__auto___66375)){
args__4742__auto__.push((arguments[i__4737__auto___66376]));

var G__66377 = (i__4737__auto___66376 + (1));
i__4737__auto___66376 = G__66377;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__65752_66378 = cljs.core.seq(nodes);
var chunk__65753_66379 = null;
var count__65754_66380 = (0);
var i__65755_66381 = (0);
while(true){
if((i__65755_66381 < count__65754_66380)){
var node_66382 = chunk__65753_66379.cljs$core$IIndexed$_nth$arity$2(null,i__65755_66381);
fragment.appendChild(shadow.dom._to_dom(node_66382));


var G__66383 = seq__65752_66378;
var G__66384 = chunk__65753_66379;
var G__66385 = count__65754_66380;
var G__66386 = (i__65755_66381 + (1));
seq__65752_66378 = G__66383;
chunk__65753_66379 = G__66384;
count__65754_66380 = G__66385;
i__65755_66381 = G__66386;
continue;
} else {
var temp__5735__auto___66387 = cljs.core.seq(seq__65752_66378);
if(temp__5735__auto___66387){
var seq__65752_66388__$1 = temp__5735__auto___66387;
if(cljs.core.chunked_seq_QMARK_(seq__65752_66388__$1)){
var c__4556__auto___66389 = cljs.core.chunk_first(seq__65752_66388__$1);
var G__66390 = cljs.core.chunk_rest(seq__65752_66388__$1);
var G__66391 = c__4556__auto___66389;
var G__66392 = cljs.core.count(c__4556__auto___66389);
var G__66393 = (0);
seq__65752_66378 = G__66390;
chunk__65753_66379 = G__66391;
count__65754_66380 = G__66392;
i__65755_66381 = G__66393;
continue;
} else {
var node_66394 = cljs.core.first(seq__65752_66388__$1);
fragment.appendChild(shadow.dom._to_dom(node_66394));


var G__66395 = cljs.core.next(seq__65752_66388__$1);
var G__66396 = null;
var G__66397 = (0);
var G__66398 = (0);
seq__65752_66378 = G__66395;
chunk__65753_66379 = G__66396;
count__65754_66380 = G__66397;
i__65755_66381 = G__66398;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq65748){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq65748));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__65767_66399 = cljs.core.seq(scripts);
var chunk__65768_66400 = null;
var count__65769_66401 = (0);
var i__65770_66402 = (0);
while(true){
if((i__65770_66402 < count__65769_66401)){
var vec__65780_66403 = chunk__65768_66400.cljs$core$IIndexed$_nth$arity$2(null,i__65770_66402);
var script_tag_66404 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65780_66403,(0),null);
var script_body_66405 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65780_66403,(1),null);
eval(script_body_66405);


var G__66406 = seq__65767_66399;
var G__66407 = chunk__65768_66400;
var G__66408 = count__65769_66401;
var G__66409 = (i__65770_66402 + (1));
seq__65767_66399 = G__66406;
chunk__65768_66400 = G__66407;
count__65769_66401 = G__66408;
i__65770_66402 = G__66409;
continue;
} else {
var temp__5735__auto___66410 = cljs.core.seq(seq__65767_66399);
if(temp__5735__auto___66410){
var seq__65767_66411__$1 = temp__5735__auto___66410;
if(cljs.core.chunked_seq_QMARK_(seq__65767_66411__$1)){
var c__4556__auto___66412 = cljs.core.chunk_first(seq__65767_66411__$1);
var G__66415 = cljs.core.chunk_rest(seq__65767_66411__$1);
var G__66416 = c__4556__auto___66412;
var G__66417 = cljs.core.count(c__4556__auto___66412);
var G__66418 = (0);
seq__65767_66399 = G__66415;
chunk__65768_66400 = G__66416;
count__65769_66401 = G__66417;
i__65770_66402 = G__66418;
continue;
} else {
var vec__65785_66419 = cljs.core.first(seq__65767_66411__$1);
var script_tag_66420 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65785_66419,(0),null);
var script_body_66421 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65785_66419,(1),null);
eval(script_body_66421);


var G__66423 = cljs.core.next(seq__65767_66411__$1);
var G__66424 = null;
var G__66425 = (0);
var G__66426 = (0);
seq__65767_66399 = G__66423;
chunk__65768_66400 = G__66424;
count__65769_66401 = G__66425;
i__65770_66402 = G__66426;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__65788){
var vec__65789 = p__65788;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65789,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65789,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
var G__65797 = shadow.dom.dom_node(el);
var G__65798 = cls;
return goog.dom.getAncestorByClass(G__65797,G__65798);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__65803 = arguments.length;
switch (G__65803) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
var G__65809 = shadow.dom.dom_node(el);
var G__65810 = cljs.core.name(tag);
return goog.dom.getAncestorByTagNameAndClass(G__65809,G__65810);
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
var G__65815 = shadow.dom.dom_node(el);
var G__65816 = cljs.core.name(tag);
var G__65817 = cljs.core.name(cls);
return goog.dom.getAncestorByTagNameAndClass(G__65815,G__65816,G__65817);
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
var G__65821 = shadow.dom.dom_node(dom);
return goog.dom.forms.getValue(G__65821);
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
var G__65822 = shadow.dom.dom_node(dom);
var G__65823 = value;
return goog.dom.forms.setValue(G__65822,G__65823);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__65830 = cljs.core.seq(style_keys);
var chunk__65831 = null;
var count__65832 = (0);
var i__65833 = (0);
while(true){
if((i__65833 < count__65832)){
var it = chunk__65831.cljs$core$IIndexed$_nth$arity$2(null,i__65833);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__66445 = seq__65830;
var G__66446 = chunk__65831;
var G__66447 = count__65832;
var G__66448 = (i__65833 + (1));
seq__65830 = G__66445;
chunk__65831 = G__66446;
count__65832 = G__66447;
i__65833 = G__66448;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__65830);
if(temp__5735__auto__){
var seq__65830__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__65830__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__65830__$1);
var G__66451 = cljs.core.chunk_rest(seq__65830__$1);
var G__66452 = c__4556__auto__;
var G__66453 = cljs.core.count(c__4556__auto__);
var G__66454 = (0);
seq__65830 = G__66451;
chunk__65831 = G__66452;
count__65832 = G__66453;
i__65833 = G__66454;
continue;
} else {
var it = cljs.core.first(seq__65830__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__66458 = cljs.core.next(seq__65830__$1);
var G__66459 = null;
var G__66460 = (0);
var G__66461 = (0);
seq__65830 = G__66458;
chunk__65831 = G__66459;
count__65832 = G__66460;
i__65833 = G__66461;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k65841,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__65850 = k65841;
var G__65850__$1 = (((G__65850 instanceof cljs.core.Keyword))?G__65850.fqn:null);
switch (G__65850__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k65841,else__4383__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__65855){
var vec__65856 = p__65855;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65856,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65856,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__65840){
var self__ = this;
var G__65840__$1 = this;
return (new cljs.core.RecordIter((0),G__65840__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (){var fexpr__65868 = (function (coll__4377__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
});
return fexpr__65868(this__4376__auto____$1);
})();
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this65842,other65843){
var self__ = this;
var this65842__$1 = this;
return (((!((other65843 == null)))) && ((this65842__$1.constructor === other65843.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this65842__$1.x,other65843.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this65842__$1.y,other65843.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this65842__$1.__extmap,other65843.__extmap)));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__65840){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__65874 = cljs.core.keyword_identical_QMARK_;
var expr__65875 = k__4388__auto__;
if(cljs.core.truth_((function (){var G__65877 = new cljs.core.Keyword(null,"x","x",2099068185);
var G__65878 = expr__65875;
return (pred__65874.cljs$core$IFn$_invoke$arity$2 ? pred__65874.cljs$core$IFn$_invoke$arity$2(G__65877,G__65878) : pred__65874.call(null,G__65877,G__65878));
})())){
return (new shadow.dom.Coordinate(G__65840,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__65879 = new cljs.core.Keyword(null,"y","y",-1757859776);
var G__65880 = expr__65875;
return (pred__65874.cljs$core$IFn$_invoke$arity$2 ? pred__65874.cljs$core$IFn$_invoke$arity$2(G__65879,G__65880) : pred__65874.call(null,G__65879,G__65880));
})())){
return (new shadow.dom.Coordinate(self__.x,G__65840,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__65840),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__65840){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__65840,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4386__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__65845){
var extmap__4419__auto__ = (function (){var G__65891 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__65845,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__65845)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__65891);
} else {
return G__65891;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__65845),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__65845),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = (function (){var G__65893 = shadow.dom.dom_node(el);
return goog.style.getPosition(G__65893);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = (function (){var G__65897 = shadow.dom.dom_node(el);
return goog.style.getClientPosition(G__65897);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = (function (){var G__65899 = shadow.dom.dom_node(el);
return goog.style.getPageOffset(G__65899);
})();
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k65903,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__65910 = k65903;
var G__65910__$1 = (((G__65910 instanceof cljs.core.Keyword))?G__65910.fqn:null);
switch (G__65910__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k65903,else__4383__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__65912){
var vec__65914 = p__65912;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65914,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65914,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Size{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__65902){
var self__ = this;
var G__65902__$1 = this;
return (new cljs.core.RecordIter((0),G__65902__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (){var fexpr__65919 = (function (coll__4377__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
});
return fexpr__65919(this__4376__auto____$1);
})();
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this65904,other65905){
var self__ = this;
var this65904__$1 = this;
return (((!((other65905 == null)))) && ((this65904__$1.constructor === other65905.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this65904__$1.w,other65905.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this65904__$1.h,other65905.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this65904__$1.__extmap,other65905.__extmap)));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__65902){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__65922 = cljs.core.keyword_identical_QMARK_;
var expr__65923 = k__4388__auto__;
if(cljs.core.truth_((function (){var G__65926 = new cljs.core.Keyword(null,"w","w",354169001);
var G__65927 = expr__65923;
return (pred__65922.cljs$core$IFn$_invoke$arity$2 ? pred__65922.cljs$core$IFn$_invoke$arity$2(G__65926,G__65927) : pred__65922.call(null,G__65926,G__65927));
})())){
return (new shadow.dom.Size(G__65902,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((function (){var G__65928 = new cljs.core.Keyword(null,"h","h",1109658740);
var G__65929 = expr__65923;
return (pred__65922.cljs$core$IFn$_invoke$arity$2 ? pred__65922.cljs$core$IFn$_invoke$arity$2(G__65928,G__65929) : pred__65922.call(null,G__65928,G__65929));
})())){
return (new shadow.dom.Size(self__.w,G__65902,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__65902),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__65902){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__65902,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4386__auto__,(0)),cljs.core._nth.cljs$core$IFn$_invoke$arity$2(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__65908){
var extmap__4419__auto__ = (function (){var G__65939 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__65908,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__65908)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__65939);
} else {
return G__65939;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__65908),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__65908),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj((function (){var G__65940 = shadow.dom.dom_node(el);
return goog.style.getSize(G__65940);
})());
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4610__auto__ = opts;
var l__4611__auto__ = a__4610__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4611__auto__)){
var G__66520 = (i + (1));
var G__66521 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__66520;
ret = G__66521;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__65946){
var vec__65947 = p__65946;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65947,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65947,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__65952 = arguments.length;
switch (G__65952) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__65962_66532 = new_node;
var G__65963_66533 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingAfter(G__65962_66532,G__65963_66533);

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
var G__65966_66535 = new_node;
var G__65967_66536 = shadow.dom.dom_node(ref);
goog.dom.insertSiblingBefore(G__65966_66535,G__65967_66536);

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5733__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5733__auto__)){
var child = temp__5733__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__66538 = ps;
var G__66539 = (i + (1));
el__$1 = G__66538;
i = G__66539;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
var G__65974 = shadow.dom.dom_node(el);
return goog.dom.getParentElement(G__65974);
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
var G__65980 = shadow.dom.dom_node(el);
return goog.dom.getNextElementSibling(G__65980);
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
var G__65982 = shadow.dom.dom_node(el);
return goog.dom.getPreviousElementSibling(G__65982);
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__65986 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65986,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65986,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__65986,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__65990_66546 = cljs.core.seq(props);
var chunk__65991_66547 = null;
var count__65992_66548 = (0);
var i__65993_66549 = (0);
while(true){
if((i__65993_66549 < count__65992_66548)){
var vec__66007_66551 = chunk__65991_66547.cljs$core$IIndexed$_nth$arity$2(null,i__65993_66549);
var k_66552 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__66007_66551,(0),null);
var v_66553 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__66007_66551,(1),null);
el.setAttributeNS((function (){var temp__5735__auto__ = cljs.core.namespace(k_66552);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_66552),v_66553);


var G__66555 = seq__65990_66546;
var G__66556 = chunk__65991_66547;
var G__66557 = count__65992_66548;
var G__66558 = (i__65993_66549 + (1));
seq__65990_66546 = G__66555;
chunk__65991_66547 = G__66556;
count__65992_66548 = G__66557;
i__65993_66549 = G__66558;
continue;
} else {
var temp__5735__auto___66559 = cljs.core.seq(seq__65990_66546);
if(temp__5735__auto___66559){
var seq__65990_66560__$1 = temp__5735__auto___66559;
if(cljs.core.chunked_seq_QMARK_(seq__65990_66560__$1)){
var c__4556__auto___66561 = cljs.core.chunk_first(seq__65990_66560__$1);
var G__66562 = cljs.core.chunk_rest(seq__65990_66560__$1);
var G__66563 = c__4556__auto___66561;
var G__66564 = cljs.core.count(c__4556__auto___66561);
var G__66565 = (0);
seq__65990_66546 = G__66562;
chunk__65991_66547 = G__66563;
count__65992_66548 = G__66564;
i__65993_66549 = G__66565;
continue;
} else {
var vec__66015_66566 = cljs.core.first(seq__65990_66560__$1);
var k_66567 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__66015_66566,(0),null);
var v_66568 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__66015_66566,(1),null);
el.setAttributeNS((function (){var temp__5735__auto____$1 = cljs.core.namespace(k_66567);
if(cljs.core.truth_(temp__5735__auto____$1)){
var ns = temp__5735__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_66567),v_66568);


var G__66570 = cljs.core.next(seq__65990_66560__$1);
var G__66571 = null;
var G__66572 = (0);
var G__66573 = (0);
seq__65990_66546 = G__66570;
chunk__65991_66547 = G__66571;
count__65992_66548 = G__66572;
i__65993_66549 = G__66573;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__66024 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__66024,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__66024,(1),null);
var seq__66027_66577 = cljs.core.seq(node_children);
var chunk__66029_66578 = null;
var count__66030_66579 = (0);
var i__66031_66580 = (0);
while(true){
if((i__66031_66580 < count__66030_66579)){
var child_struct_66581 = chunk__66029_66578.cljs$core$IIndexed$_nth$arity$2(null,i__66031_66580);
if((!((child_struct_66581 == null)))){
if(typeof child_struct_66581 === 'string'){
var text_66583 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_66583),child_struct_66581].join(''));
} else {
var children_66584 = shadow.dom.svg_node(child_struct_66581);
if(cljs.core.seq_QMARK_(children_66584)){
var seq__66054_66585 = cljs.core.seq(children_66584);
var chunk__66056_66586 = null;
var count__66057_66587 = (0);
var i__66058_66588 = (0);
while(true){
if((i__66058_66588 < count__66057_66587)){
var child_66589 = chunk__66056_66586.cljs$core$IIndexed$_nth$arity$2(null,i__66058_66588);
if(cljs.core.truth_(child_66589)){
node.appendChild(child_66589);


var G__66594 = seq__66054_66585;
var G__66595 = chunk__66056_66586;
var G__66596 = count__66057_66587;
var G__66597 = (i__66058_66588 + (1));
seq__66054_66585 = G__66594;
chunk__66056_66586 = G__66595;
count__66057_66587 = G__66596;
i__66058_66588 = G__66597;
continue;
} else {
var G__66599 = seq__66054_66585;
var G__66600 = chunk__66056_66586;
var G__66601 = count__66057_66587;
var G__66602 = (i__66058_66588 + (1));
seq__66054_66585 = G__66599;
chunk__66056_66586 = G__66600;
count__66057_66587 = G__66601;
i__66058_66588 = G__66602;
continue;
}
} else {
var temp__5735__auto___66604 = cljs.core.seq(seq__66054_66585);
if(temp__5735__auto___66604){
var seq__66054_66605__$1 = temp__5735__auto___66604;
if(cljs.core.chunked_seq_QMARK_(seq__66054_66605__$1)){
var c__4556__auto___66606 = cljs.core.chunk_first(seq__66054_66605__$1);
var G__66608 = cljs.core.chunk_rest(seq__66054_66605__$1);
var G__66609 = c__4556__auto___66606;
var G__66610 = cljs.core.count(c__4556__auto___66606);
var G__66611 = (0);
seq__66054_66585 = G__66608;
chunk__66056_66586 = G__66609;
count__66057_66587 = G__66610;
i__66058_66588 = G__66611;
continue;
} else {
var child_66612 = cljs.core.first(seq__66054_66605__$1);
if(cljs.core.truth_(child_66612)){
node.appendChild(child_66612);


var G__66614 = cljs.core.next(seq__66054_66605__$1);
var G__66615 = null;
var G__66616 = (0);
var G__66617 = (0);
seq__66054_66585 = G__66614;
chunk__66056_66586 = G__66615;
count__66057_66587 = G__66616;
i__66058_66588 = G__66617;
continue;
} else {
var G__66618 = cljs.core.next(seq__66054_66605__$1);
var G__66619 = null;
var G__66620 = (0);
var G__66621 = (0);
seq__66054_66585 = G__66618;
chunk__66056_66586 = G__66619;
count__66057_66587 = G__66620;
i__66058_66588 = G__66621;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_66584);
}
}


var G__66624 = seq__66027_66577;
var G__66625 = chunk__66029_66578;
var G__66626 = count__66030_66579;
var G__66627 = (i__66031_66580 + (1));
seq__66027_66577 = G__66624;
chunk__66029_66578 = G__66625;
count__66030_66579 = G__66626;
i__66031_66580 = G__66627;
continue;
} else {
var G__66628 = seq__66027_66577;
var G__66629 = chunk__66029_66578;
var G__66630 = count__66030_66579;
var G__66631 = (i__66031_66580 + (1));
seq__66027_66577 = G__66628;
chunk__66029_66578 = G__66629;
count__66030_66579 = G__66630;
i__66031_66580 = G__66631;
continue;
}
} else {
var temp__5735__auto___66632 = cljs.core.seq(seq__66027_66577);
if(temp__5735__auto___66632){
var seq__66027_66633__$1 = temp__5735__auto___66632;
if(cljs.core.chunked_seq_QMARK_(seq__66027_66633__$1)){
var c__4556__auto___66635 = cljs.core.chunk_first(seq__66027_66633__$1);
var G__66636 = cljs.core.chunk_rest(seq__66027_66633__$1);
var G__66637 = c__4556__auto___66635;
var G__66638 = cljs.core.count(c__4556__auto___66635);
var G__66639 = (0);
seq__66027_66577 = G__66636;
chunk__66029_66578 = G__66637;
count__66030_66579 = G__66638;
i__66031_66580 = G__66639;
continue;
} else {
var child_struct_66642 = cljs.core.first(seq__66027_66633__$1);
if((!((child_struct_66642 == null)))){
if(typeof child_struct_66642 === 'string'){
var text_66643 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_66643),child_struct_66642].join(''));
} else {
var children_66644 = shadow.dom.svg_node(child_struct_66642);
if(cljs.core.seq_QMARK_(children_66644)){
var seq__66063_66645 = cljs.core.seq(children_66644);
var chunk__66065_66646 = null;
var count__66066_66647 = (0);
var i__66067_66648 = (0);
while(true){
if((i__66067_66648 < count__66066_66647)){
var child_66649 = chunk__66065_66646.cljs$core$IIndexed$_nth$arity$2(null,i__66067_66648);
if(cljs.core.truth_(child_66649)){
node.appendChild(child_66649);


var G__66651 = seq__66063_66645;
var G__66652 = chunk__66065_66646;
var G__66653 = count__66066_66647;
var G__66654 = (i__66067_66648 + (1));
seq__66063_66645 = G__66651;
chunk__66065_66646 = G__66652;
count__66066_66647 = G__66653;
i__66067_66648 = G__66654;
continue;
} else {
var G__66657 = seq__66063_66645;
var G__66658 = chunk__66065_66646;
var G__66659 = count__66066_66647;
var G__66660 = (i__66067_66648 + (1));
seq__66063_66645 = G__66657;
chunk__66065_66646 = G__66658;
count__66066_66647 = G__66659;
i__66067_66648 = G__66660;
continue;
}
} else {
var temp__5735__auto___66661__$1 = cljs.core.seq(seq__66063_66645);
if(temp__5735__auto___66661__$1){
var seq__66063_66662__$1 = temp__5735__auto___66661__$1;
if(cljs.core.chunked_seq_QMARK_(seq__66063_66662__$1)){
var c__4556__auto___66663 = cljs.core.chunk_first(seq__66063_66662__$1);
var G__66665 = cljs.core.chunk_rest(seq__66063_66662__$1);
var G__66666 = c__4556__auto___66663;
var G__66667 = cljs.core.count(c__4556__auto___66663);
var G__66668 = (0);
seq__66063_66645 = G__66665;
chunk__66065_66646 = G__66666;
count__66066_66647 = G__66667;
i__66067_66648 = G__66668;
continue;
} else {
var child_66669 = cljs.core.first(seq__66063_66662__$1);
if(cljs.core.truth_(child_66669)){
node.appendChild(child_66669);


var G__66670 = cljs.core.next(seq__66063_66662__$1);
var G__66671 = null;
var G__66672 = (0);
var G__66673 = (0);
seq__66063_66645 = G__66670;
chunk__66065_66646 = G__66671;
count__66066_66647 = G__66672;
i__66067_66648 = G__66673;
continue;
} else {
var G__66674 = cljs.core.next(seq__66063_66662__$1);
var G__66675 = null;
var G__66677 = (0);
var G__66678 = (0);
seq__66063_66645 = G__66674;
chunk__66065_66646 = G__66675;
count__66066_66647 = G__66677;
i__66067_66648 = G__66678;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_66644);
}
}


var G__66683 = cljs.core.next(seq__66027_66633__$1);
var G__66684 = null;
var G__66685 = (0);
var G__66686 = (0);
seq__66027_66577 = G__66683;
chunk__66029_66578 = G__66684;
count__66030_66579 = G__66685;
i__66031_66580 = G__66686;
continue;
} else {
var G__66687 = cljs.core.next(seq__66027_66633__$1);
var G__66688 = null;
var G__66689 = (0);
var G__66690 = (0);
seq__66027_66577 = G__66687;
chunk__66029_66578 = G__66688;
count__66030_66579 = G__66689;
i__66031_66580 = G__66690;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

var G__66071_66691 = shadow.dom._to_svg;
var G__66072_66692 = "string";
var G__66073_66693 = (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
});
goog.object.set(G__66071_66691,G__66072_66692,G__66073_66693);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

goog.object.set(shadow.dom.SVGElement,"null",true);

var G__66081_66698 = shadow.dom._to_svg;
var G__66082_66699 = "null";
var G__66083_66700 = (function (_){
return null;
});
goog.object.set(G__66081_66698,G__66082_66699,G__66083_66700);
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___66702 = arguments.length;
var i__4737__auto___66703 = (0);
while(true){
if((i__4737__auto___66703 < len__4736__auto___66702)){
args__4742__auto__.push((arguments[i__4737__auto___66703]));

var G__66705 = (i__4737__auto___66703 + (1));
i__4737__auto___66703 = G__66705;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq66085){
var G__66086 = cljs.core.first(seq66085);
var seq66085__$1 = cljs.core.next(seq66085);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__66086,seq66085__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__66096 = arguments.length;
switch (G__66096) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
var G__66101_66712 = shadow.dom.dom_node(el);
var G__66102_66713 = cljs.core.name(event);
var G__66103_66714 = event_fn;
(shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3 ? shadow.dom.dom_listen.cljs$core$IFn$_invoke$arity$3(G__66101_66712,G__66102_66713,G__66103_66714) : shadow.dom.dom_listen.call(null,G__66101_66712,G__66102_66713,G__66103_66714));

if(cljs.core.truth_((function (){var and__4115__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4115__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4115__auto__;
}
})())){
var c__61983__auto___66716 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_66109){
var state_val_66110 = (state_66109[(1)]);
if((state_val_66110 === (1))){
var state_66109__$1 = state_66109;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_66109__$1,(2),once_or_cleanup);
} else {
if((state_val_66110 === (2))){
var inst_66106 = (state_66109[(2)]);
var inst_66107 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_66109__$1 = (function (){var statearr_66113 = state_66109;
(statearr_66113[(7)] = inst_66106);

return statearr_66113;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_66109__$1,inst_66107);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__61706__auto__ = null;
var shadow$dom$state_machine__61706__auto____0 = (function (){
var statearr_66114 = [null,null,null,null,null,null,null,null];
(statearr_66114[(0)] = shadow$dom$state_machine__61706__auto__);

(statearr_66114[(1)] = (1));

return statearr_66114;
});
var shadow$dom$state_machine__61706__auto____1 = (function (state_66109){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_66109);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e66115){if((e66115 instanceof Object)){
var ex__61709__auto__ = e66115;
var statearr_66117_66722 = state_66109;
(statearr_66117_66722[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_66109);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e66115;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66723 = state_66109;
state_66109 = G__66723;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
shadow$dom$state_machine__61706__auto__ = function(state_66109){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__61706__auto____0.call(this);
case 1:
return shadow$dom$state_machine__61706__auto____1.call(this,state_66109);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__61706__auto____0;
shadow$dom$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__61706__auto____1;
return shadow$dom$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_66120 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_66120[(6)] = c__61983__auto___66716);

return statearr_66120;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
