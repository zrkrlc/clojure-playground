goog.provide('playground.views');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('reagent.dom');
playground.views.header = (function playground$views$header(){
return (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h1","h1",-1896887462),"Why hello there"], null);
});
});

//# sourceMappingURL=playground.views.js.map
