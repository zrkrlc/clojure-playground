goog.provide('talltale.core');
goog.require('cljs.core');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('cljs.pprint');
goog.require('clojure.test.check.generators');
goog.require('cljs.spec.alpha');
goog.require('cljs.spec.gen.alpha');
goog.require('cljs_time.core');
goog.require('cljs_time.coerce');
goog.require('talltale.macros');
talltale.core.lorem_ipsum = (function talltale$core$lorem_ipsum(){
return talltale.macros.rand_data(new cljs.core.Keyword(null,"en","en",88457073),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"lorem-ipsum","lorem-ipsum",231925911)], null));
});
talltale.core.lorem_ipsum_gen = (function talltale$core$lorem_ipsum_gen(var_args){
var G__53308 = arguments.length;
switch (G__53308) {
case 0:
return talltale.core.lorem_ipsum_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.lorem_ipsum_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.lorem_ipsum_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.lorem_ipsum_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.lorem_ipsum_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return clojure.test.check.generators.return$(talltale.macros.raw(locale,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"lorem-ipsum","lorem-ipsum",231925911)], null)));
}));

(talltale.core.lorem_ipsum_gen.cljs$lang$maxFixedArity = 1);

talltale.core.text = (function talltale$core$text(var_args){
var G__53310 = arguments.length;
switch (G__53310) {
case 0:
return talltale.core.text.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.text.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.text.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.text.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return talltale.macros.rand_data(locale,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"text","text",-1790561697)], null));
}));

(talltale.core.text.cljs$lang$maxFixedArity = 1);

talltale.core.text_gen = (function talltale$core$text_gen(var_args){
var G__53312 = arguments.length;
switch (G__53312) {
case 0:
return talltale.core.text_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.text_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.text_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.text_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.text_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return clojure.test.check.generators.return$(talltale.macros.rand_data(locale,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"text","text",-1790561697)], null)));
}));

(talltale.core.text_gen.cljs$lang$maxFixedArity = 1);

talltale.core.street_number = (function talltale$core$street_number(){
return cljs.core.rand_int((1000));
});
talltale.core.street_number_gen = (function talltale$core$street_number_gen(){
return clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(1),new cljs.core.Keyword(null,"max","max",61366548),(1000)], null));
});
talltale.core.street = (function talltale$core$street(var_args){
var G__53314 = arguments.length;
switch (G__53314) {
case 0:
return talltale.core.street.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.street.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.street.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.street.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.street.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"street","street",1870012303)], null));
}));

(talltale.core.street.cljs$lang$maxFixedArity = 1);


talltale.core.street_gen = (function talltale$core$street_gen(var_args){
var G__53316 = arguments.length;
switch (G__53316) {
case 0:
return talltale.core.street_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.street_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.street_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.street_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.street_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"street","street",1870012303)], null))], 0));
}));

(talltale.core.street_gen.cljs$lang$maxFixedArity = 1);

talltale.core.postal_code = (function talltale$core$postal_code(var_args){
var G__53318 = arguments.length;
switch (G__53318) {
case 0:
return talltale.core.postal_code.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.postal_code.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return talltale.core.postal_code.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.postal_code.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.postal_code.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.postal_code.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return talltale.core.postal_code.cljs$core$IFn$_invoke$arity$2(locale,cljs.core.rand_int((99999)));
}));

(talltale.core.postal_code.cljs$core$IFn$_invoke$arity$2 = (function (locale,rand){
var control_string = talltale.macros.rand_data(locale,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"postal-code","postal-code",368585871)], null));
return cljs.pprint.cl_format.cljs$core$IFn$_invoke$arity$variadic(null,control_string,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([rand], 0));
}));

(talltale.core.postal_code.cljs$lang$maxFixedArity = 2);

talltale.core.postal_code_gen = (function talltale$core$postal_code_gen(var_args){
var G__53320 = arguments.length;
switch (G__53320) {
case 0:
return talltale.core.postal_code_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.postal_code_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.postal_code_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.postal_code_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.postal_code_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return cljs.spec.gen.alpha.fmap.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.partial.cljs$core$IFn$_invoke$arity$2(talltale.core.postal_code,locale),clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(10000),new cljs.core.Keyword(null,"max","max",61366548),(99999)], null))], 0));
}));

(talltale.core.postal_code_gen.cljs$lang$maxFixedArity = 1);

talltale.core.city = (function talltale$core$city(var_args){
var G__53322 = arguments.length;
switch (G__53322) {
case 0:
return talltale.core.city.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.city.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.city.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.city.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.city.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"city","city",-393302614)], null));
}));

(talltale.core.city.cljs$lang$maxFixedArity = 1);


talltale.core.city_gen = (function talltale$core$city_gen(var_args){
var G__53324 = arguments.length;
switch (G__53324) {
case 0:
return talltale.core.city_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.city_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.city_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.city_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.city_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"city","city",-393302614)], null))], 0));
}));

(talltale.core.city_gen.cljs$lang$maxFixedArity = 1);

talltale.core.phone_number = (function talltale$core$phone_number(var_args){
var G__53328 = arguments.length;
switch (G__53328) {
case 0:
return talltale.core.phone_number.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.phone_number.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
var args_arr__4757__auto__ = [];
var len__4736__auto___53468 = arguments.length;
var i__4737__auto___53469 = (0);
while(true){
if((i__4737__auto___53469 < len__4736__auto___53468)){
args_arr__4757__auto__.push((arguments[i__4737__auto___53469]));

var G__53470 = (i__4737__auto___53469 + (1));
i__4737__auto___53469 = G__53470;
continue;
} else {
}
break;
}

var argseq__4758__auto__ = (new cljs.core.IndexedSeq(args_arr__4757__auto__.slice((1)),(0),null));
return talltale.core.phone_number.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4758__auto__);

}
});

(talltale.core.phone_number.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.phone_number.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.phone_number.cljs$core$IFn$_invoke$arity$1 = (function (locale){
var G__53329 = locale;
var G__53329__$1 = (((G__53329 instanceof cljs.core.Keyword))?G__53329.fqn:null);
switch (G__53329__$1) {
case "en":
return talltale.core.phone_number.cljs$core$IFn$_invoke$arity$variadic(locale,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.rand_int((999)),cljs.core.rand_int((999)),cljs.core.rand_int((999))], 0));

break;
case "fr":
return talltale.core.phone_number.cljs$core$IFn$_invoke$arity$variadic(locale,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.rand_int((999999999)),null,null], 0));

break;
default:
return talltale.core.phone_number.cljs$core$IFn$_invoke$arity$variadic(locale,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.rand_int((999999999)),null,null], 0));

}
}));

(talltale.core.phone_number.cljs$core$IFn$_invoke$arity$variadic = (function (locale,p__53330){
var vec__53331 = p__53330;
var r1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__53331,(0),null);
var r2 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__53331,(1),null);
var r3 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__53331,(2),null);
var control_string = talltale.macros.rand_data(locale,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"phone-number-format","phone-number-format",1355807521)], null));
var G__53334 = locale;
var G__53334__$1 = (((G__53334 instanceof cljs.core.Keyword))?G__53334.fqn:null);
switch (G__53334__$1) {
case "en":
return cljs.pprint.cl_format.cljs$core$IFn$_invoke$arity$variadic(null,control_string,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([r1,r2,r3], 0));

break;
case "fr":
return cljs.pprint.cl_format.cljs$core$IFn$_invoke$arity$variadic(null,control_string,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([r1], 0));

break;
default:
return cljs.pprint.cl_format.cljs$core$IFn$_invoke$arity$variadic(null,"~10,'0d",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([r1], 0));

}
}));

/** @this {Function} */
(talltale.core.phone_number.cljs$lang$applyTo = (function (seq53326){
var G__53327 = cljs.core.first(seq53326);
var seq53326__$1 = cljs.core.next(seq53326);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__53327,seq53326__$1);
}));

(talltale.core.phone_number.cljs$lang$maxFixedArity = (1));

talltale.core.phone_number_gen = (function talltale$core$phone_number_gen(var_args){
var G__53336 = arguments.length;
switch (G__53336) {
case 0:
return talltale.core.phone_number_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.phone_number_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.phone_number_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.phone_number_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.phone_number_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
var G__53337 = locale;
var G__53337__$1 = (((G__53337 instanceof cljs.core.Keyword))?G__53337.fqn:null);
switch (G__53337__$1) {
case "en":
return clojure.test.check.generators.bind(clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(100),new cljs.core.Keyword(null,"max","max",61366548),(999)], null)),(function (r1){
return clojure.test.check.generators.bind(clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(100),new cljs.core.Keyword(null,"max","max",61366548),(999)], null)),(function (r2){
return clojure.test.check.generators.bind(clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(100),new cljs.core.Keyword(null,"max","max",61366548),(999)], null)),(function (r3){
var val__38745__auto__ = talltale.core.phone_number.cljs$core$IFn$_invoke$arity$variadic(locale,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([r1,r2,r3], 0));
if(clojure.test.check.generators.generator_QMARK_(val__38745__auto__)){
return val__38745__auto__;
} else {
return clojure.test.check.generators.return$(val__38745__auto__);
}
}));
}));
}));

break;
case "fr":
return clojure.test.check.generators.fmap(cljs.core.partial.cljs$core$IFn$_invoke$arity$2(talltale.core.phone_number,locale),clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(100000000),new cljs.core.Keyword(null,"max","max",61366548),(999999999)], null)));

break;
default:
return clojure.test.check.generators.fmap(cljs.core.partial.cljs$core$IFn$_invoke$arity$2(talltale.core.phone_number,locale),clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(100000000),new cljs.core.Keyword(null,"max","max",61366548),(999999999)], null)));

}
}));

(talltale.core.phone_number_gen.cljs$lang$maxFixedArity = 1);

talltale.core.address = (function talltale$core$address(var_args){
var G__53339 = arguments.length;
switch (G__53339) {
case 0:
return talltale.core.address.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.address.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.address.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.address.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.address.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"street","street",1870012303),talltale.core.street.cljs$core$IFn$_invoke$arity$1(locale),new cljs.core.Keyword(null,"street-number","street-number",-1946770407),talltale.core.street_number(),new cljs.core.Keyword(null,"postal-code","postal-code",368585871),talltale.core.postal_code.cljs$core$IFn$_invoke$arity$1(locale),new cljs.core.Keyword(null,"city","city",-393302614),talltale.core.city.cljs$core$IFn$_invoke$arity$1(locale)], null);
}));

(talltale.core.address.cljs$lang$maxFixedArity = 1);

talltale.core.address_gen = (function talltale$core$address_gen(var_args){
var G__53341 = arguments.length;
switch (G__53341) {
case 0:
return talltale.core.address_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.address_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.address_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.address_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.address_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return clojure.test.check.generators.bind(talltale.core.street_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (street){
return clojure.test.check.generators.bind(talltale.core.postal_code_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (postal_code){
return clojure.test.check.generators.bind(talltale.core.city_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (city){
return clojure.test.check.generators.bind(talltale.core.street_number_gen(),(function (street_number){
var val__38745__auto__ = new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"street","street",1870012303),street,new cljs.core.Keyword(null,"street-number","street-number",-1946770407),street_number,new cljs.core.Keyword(null,"postal-code","postal-code",368585871),postal_code,new cljs.core.Keyword(null,"city","city",-393302614),city], null);
if(clojure.test.check.generators.generator_QMARK_(val__38745__auto__)){
return val__38745__auto__;
} else {
return clojure.test.check.generators.return$(val__38745__auto__);
}
}));
}));
}));
}));
}));

(talltale.core.address_gen.cljs$lang$maxFixedArity = 1);

talltale.core.first_name_male = (function talltale$core$first_name_male(var_args){
var G__53343 = arguments.length;
switch (G__53343) {
case 0:
return talltale.core.first_name_male.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.first_name_male.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.first_name_male.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.first_name_male.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.first_name_male.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"first-name-male","first-name-male",517180691)], null));
}));

(talltale.core.first_name_male.cljs$lang$maxFixedArity = 1);


talltale.core.first_name_male_gen = (function talltale$core$first_name_male_gen(var_args){
var G__53345 = arguments.length;
switch (G__53345) {
case 0:
return talltale.core.first_name_male_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.first_name_male_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.first_name_male_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.first_name_male_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.first_name_male_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"first-name-male","first-name-male",517180691)], null))], 0));
}));

(talltale.core.first_name_male_gen.cljs$lang$maxFixedArity = 1);

talltale.core.first_name_female = (function talltale$core$first_name_female(var_args){
var G__53347 = arguments.length;
switch (G__53347) {
case 0:
return talltale.core.first_name_female.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.first_name_female.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.first_name_female.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.first_name_female.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.first_name_female.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"first-name-female","first-name-female",1879822571)], null));
}));

(talltale.core.first_name_female.cljs$lang$maxFixedArity = 1);


talltale.core.first_name_female_gen = (function talltale$core$first_name_female_gen(var_args){
var G__53349 = arguments.length;
switch (G__53349) {
case 0:
return talltale.core.first_name_female_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.first_name_female_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.first_name_female_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.first_name_female_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.first_name_female_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"first-name-female","first-name-female",1879822571)], null))], 0));
}));

(talltale.core.first_name_female_gen.cljs$lang$maxFixedArity = 1);

talltale.core.last_name_male = (function talltale$core$last_name_male(var_args){
var G__53351 = arguments.length;
switch (G__53351) {
case 0:
return talltale.core.last_name_male.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.last_name_male.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.last_name_male.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.last_name_male.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.last_name_male.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"last-name-male","last-name-male",502905982)], null));
}));

(talltale.core.last_name_male.cljs$lang$maxFixedArity = 1);


talltale.core.last_name_male_gen = (function talltale$core$last_name_male_gen(var_args){
var G__53353 = arguments.length;
switch (G__53353) {
case 0:
return talltale.core.last_name_male_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.last_name_male_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.last_name_male_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.last_name_male_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.last_name_male_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"last-name-male","last-name-male",502905982)], null))], 0));
}));

(talltale.core.last_name_male_gen.cljs$lang$maxFixedArity = 1);

talltale.core.last_name_female = (function talltale$core$last_name_female(var_args){
var G__53355 = arguments.length;
switch (G__53355) {
case 0:
return talltale.core.last_name_female.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.last_name_female.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.last_name_female.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.last_name_female.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.last_name_female.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"last-name-female","last-name-female",194104534)], null));
}));

(talltale.core.last_name_female.cljs$lang$maxFixedArity = 1);


talltale.core.last_name_female_gen = (function talltale$core$last_name_female_gen(var_args){
var G__53357 = arguments.length;
switch (G__53357) {
case 0:
return talltale.core.last_name_female_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.last_name_female_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.last_name_female_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.last_name_female_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.last_name_female_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"last-name-female","last-name-female",194104534)], null))], 0));
}));

(talltale.core.last_name_female_gen.cljs$lang$maxFixedArity = 1);

talltale.core.position = (function talltale$core$position(var_args){
var G__53359 = arguments.length;
switch (G__53359) {
case 0:
return talltale.core.position.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.position.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.position.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.position.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.position.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"position","position",-2011731912)], null));
}));

(talltale.core.position.cljs$lang$maxFixedArity = 1);


talltale.core.position_gen = (function talltale$core$position_gen(var_args){
var G__53361 = arguments.length;
switch (G__53361) {
case 0:
return talltale.core.position_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.position_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.position_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.position_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.position_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"position","position",-2011731912)], null))], 0));
}));

(talltale.core.position_gen.cljs$lang$maxFixedArity = 1);

talltale.core.first_name = (function talltale$core$first_name(var_args){
var G__53363 = arguments.length;
switch (G__53363) {
case 0:
return talltale.core.first_name.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.first_name.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.first_name.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.first_name.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.first_name.cljs$core$IFn$_invoke$arity$1 = (function (locale){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.rand_int((2)),(0))){
return talltale.core.first_name_male.cljs$core$IFn$_invoke$arity$1(locale);
} else {
return talltale.core.first_name_female.cljs$core$IFn$_invoke$arity$1(locale);
}
}));

(talltale.core.first_name.cljs$lang$maxFixedArity = 1);

talltale.core.first_name_gen = (function talltale$core$first_name_gen(var_args){
var G__53365 = arguments.length;
switch (G__53365) {
case 0:
return talltale.core.first_name_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.first_name_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.first_name_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.first_name_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.first_name_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.rand_int((2)),(0))){
return talltale.core.first_name_male_gen.cljs$core$IFn$_invoke$arity$1(locale);
} else {
return talltale.core.first_name_female_gen.cljs$core$IFn$_invoke$arity$1(locale);
}
}));

(talltale.core.first_name_gen.cljs$lang$maxFixedArity = 1);

talltale.core.last_name = (function talltale$core$last_name(var_args){
var G__53367 = arguments.length;
switch (G__53367) {
case 0:
return talltale.core.last_name.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.last_name.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.last_name.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.last_name.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.last_name.cljs$core$IFn$_invoke$arity$1 = (function (locale){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.rand_int((2)),(0))){
return talltale.core.last_name_male.cljs$core$IFn$_invoke$arity$1(locale);
} else {
return talltale.core.last_name_female.cljs$core$IFn$_invoke$arity$1(locale);
}
}));

(talltale.core.last_name.cljs$lang$maxFixedArity = 1);

talltale.core.last_name_gen = (function talltale$core$last_name_gen(var_args){
var G__53369 = arguments.length;
switch (G__53369) {
case 0:
return talltale.core.last_name_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.last_name_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.last_name_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.last_name_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.last_name_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.rand_int((2)),(0))){
return talltale.core.last_name_male_gen.cljs$core$IFn$_invoke$arity$1(locale);
} else {
return talltale.core.last_name_female_gen.cljs$core$IFn$_invoke$arity$1(locale);
}
}));

(talltale.core.last_name_gen.cljs$lang$maxFixedArity = 1);

talltale.core.age = (function talltale$core$age(){
return cljs.core.rand_int((110));
});
talltale.core.age_gen = (function talltale$core$age_gen(){
return clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(18),new cljs.core.Keyword(null,"max","max",61366548),(110)], null));
});
talltale.core.date_of_birth = (function talltale$core$date_of_birth(var_args){
var G__53371 = arguments.length;
switch (G__53371) {
case 0:
return talltale.core.date_of_birth.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.date_of_birth.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.date_of_birth.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.date_of_birth.cljs$core$IFn$_invoke$arity$1(talltale.core.age());
}));

(talltale.core.date_of_birth.cljs$core$IFn$_invoke$arity$1 = (function (age){
return cljs_time.coerce.to_date(cljs_time.core.minus.cljs$core$IFn$_invoke$arity$2(cljs_time.core.today(),cljs_time.core.years.cljs$core$IFn$_invoke$arity$1(age)));
}));

(talltale.core.date_of_birth.cljs$lang$maxFixedArity = 1);

talltale.core.date_of_birth_gen = (function talltale$core$date_of_birth_gen(age){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.date_of_birth.cljs$core$IFn$_invoke$arity$1(age)], 0));
});
talltale.core.identifier = (function talltale$core$identifier(first_name,last_name){
var lower_fn = clojure.string.lower_case(first_name);
var lower_ln = clojure.string.lower_case(last_name);
var generators = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"initial-last-name","initial-last-name",1997093484),(function (){
return [cljs.core.subs.cljs$core$IFn$_invoke$arity$3(lower_fn,(0),(1)),lower_ln].join('');
}),new cljs.core.Keyword(null,"first-dot-last","first-dot-last",2030255870),(function (){
return [lower_fn,".",lower_ln].join('');
}),new cljs.core.Keyword(null,"first-number","first-number",112096594),(function (){
return [lower_fn,cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand_int((999)))].join('');
}),new cljs.core.Keyword(null,"last","last",1105735132),(function (){
return lower_ln;
}),new cljs.core.Keyword(null,"first","first",-644103046),(function (){
return lower_fn;
})], null);
return cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var fexpr__53373 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.vec(cljs.core.vals(generators)),cljs.core.rand_int(cljs.core.count(generators)));
return (fexpr__53373.cljs$core$IFn$_invoke$arity$0 ? fexpr__53373.cljs$core$IFn$_invoke$arity$0() : fexpr__53373.call(null));
})());
});
talltale.core.username = (function talltale$core$username(var_args){
var G__53375 = arguments.length;
switch (G__53375) {
case 0:
return talltale.core.username.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.username.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return talltale.core.username.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.username.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.username.cljs$core$IFn$_invoke$arity$2(talltale.core.first_name.cljs$core$IFn$_invoke$arity$0(),talltale.core.last_name.cljs$core$IFn$_invoke$arity$0());
}));

(talltale.core.username.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return talltale.core.username.cljs$core$IFn$_invoke$arity$2(talltale.core.first_name.cljs$core$IFn$_invoke$arity$1(locale),talltale.core.last_name.cljs$core$IFn$_invoke$arity$1(locale));
}));

(talltale.core.username.cljs$core$IFn$_invoke$arity$2 = (function (first_name,last_name){
return talltale.core.identifier(first_name,last_name);
}));

(talltale.core.username.cljs$lang$maxFixedArity = 2);

talltale.core.username_gen = (function talltale$core$username_gen(var_args){
var G__53377 = arguments.length;
switch (G__53377) {
case 0:
return talltale.core.username_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.username_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return talltale.core.username_gen.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.username_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.username_gen.cljs$core$IFn$_invoke$arity$2(talltale.core.first_name.cljs$core$IFn$_invoke$arity$0(),talltale.core.last_name.cljs$core$IFn$_invoke$arity$0());
}));

(talltale.core.username_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return talltale.core.username_gen.cljs$core$IFn$_invoke$arity$2(talltale.core.first_name.cljs$core$IFn$_invoke$arity$1(locale),talltale.core.last_name.cljs$core$IFn$_invoke$arity$1(locale));
}));

(talltale.core.username_gen.cljs$core$IFn$_invoke$arity$2 = (function (first_name,last_name){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.username.cljs$core$IFn$_invoke$arity$2(first_name,last_name)], 0));
}));

(talltale.core.username_gen.cljs$lang$maxFixedArity = 2);

talltale.core.email = (function talltale$core$email(var_args){
var G__53379 = arguments.length;
switch (G__53379) {
case 0:
return talltale.core.email.cljs$core$IFn$_invoke$arity$0();

break;
case 2:
return talltale.core.email.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return talltale.core.email.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.email.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.email.cljs$core$IFn$_invoke$arity$2(talltale.core.first_name.cljs$core$IFn$_invoke$arity$0(),talltale.core.last_name.cljs$core$IFn$_invoke$arity$0());
}));

(talltale.core.email.cljs$core$IFn$_invoke$arity$2 = (function (first_name,last_name){
return talltale.core.email.cljs$core$IFn$_invoke$arity$3(new cljs.core.Keyword(null,"en","en",88457073),first_name,last_name);
}));

(talltale.core.email.cljs$core$IFn$_invoke$arity$3 = (function (locale,first_name,last_name){
return [talltale.core.identifier(first_name,last_name),"@",cljs.core.str.cljs$core$IFn$_invoke$arity$1(talltale.macros.rand_data(locale,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"person","person",-1059806875),new cljs.core.Keyword(null,"personal-email","personal-email",1990693853)], null)))].join('');
}));

(talltale.core.email.cljs$lang$maxFixedArity = 3);

talltale.core.email_gen = (function talltale$core$email_gen(locale,first_name,last_name){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.email.cljs$core$IFn$_invoke$arity$3(locale,first_name,last_name)], 0));
});
talltale.core.sex = (function talltale$core$sex(){
var G__53380 = cljs.core.rand_int((2));
switch (G__53380) {
case (0):
return new cljs.core.Keyword(null,"male","male",-560253338);

break;
case (1):
return new cljs.core.Keyword(null,"female","female",1810186049);

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__53380)].join('')));

}
});
talltale.core.picture_url = (function talltale$core$picture_url(var_args){
var G__53382 = arguments.length;
switch (G__53382) {
case 0:
return talltale.core.picture_url.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.picture_url.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return talltale.core.picture_url.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.picture_url.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.picture_url.cljs$core$IFn$_invoke$arity$1(talltale.core.sex());
}));

(talltale.core.picture_url.cljs$core$IFn$_invoke$arity$1 = (function (sex){
return talltale.core.picture_url.cljs$core$IFn$_invoke$arity$2(sex,cljs.core.rand_int((100)));
}));

(talltale.core.picture_url.cljs$core$IFn$_invoke$arity$2 = (function (sex,r){
var s = (function (){var G__53383 = sex;
var G__53383__$1 = (((G__53383 instanceof cljs.core.Keyword))?G__53383.fqn:null);
switch (G__53383__$1) {
case "male":
return "men";

break;
case "female":
return "women";

break;
default:
return "men";

}
})();
return ["https://randomuser.me/api/portraits/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(s),"/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(r),".jpg"].join('');
}));

(talltale.core.picture_url.cljs$lang$maxFixedArity = 2);

talltale.core.picture_url_gen = (function talltale$core$picture_url_gen(sex){
return cljs.spec.gen.alpha.fmap.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.partial.cljs$core$IFn$_invoke$arity$2(talltale.core.picture_url,sex),clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(0),new cljs.core.Keyword(null,"max","max",61366548),(99)], null))], 0));
});
talltale.core.person_all = (function talltale$core$person_all(locale,p__53384){
var map__53385 = p__53384;
var map__53385__$1 = (((((!((map__53385 == null))))?(((((map__53385.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53385.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53385):map__53385);
var specific = map__53385__$1;
var first_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53385__$1,new cljs.core.Keyword(null,"first-name","first-name",-1559982131));
var last_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53385__$1,new cljs.core.Keyword(null,"last-name","last-name",-1695738974));
var sex = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53385__$1,new cljs.core.Keyword(null,"sex","sex",-2056008571));
var age = talltale.core.age();
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([specific,new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"username","username",1605666410),talltale.core.username.cljs$core$IFn$_invoke$arity$2(first_name,last_name),new cljs.core.Keyword(null,"email","email",1415816706),talltale.core.email.cljs$core$IFn$_invoke$arity$3(locale,first_name,last_name),new cljs.core.Keyword(null,"position","position",-2011731912),talltale.core.position.cljs$core$IFn$_invoke$arity$1(locale),new cljs.core.Keyword(null,"phone-number","phone-number",453925028),talltale.core.phone_number.cljs$core$IFn$_invoke$arity$1(locale),new cljs.core.Keyword(null,"age","age",-604307804),age,new cljs.core.Keyword(null,"date-of-birth","date-of-birth",-848850674),talltale.core.date_of_birth.cljs$core$IFn$_invoke$arity$1(age),new cljs.core.Keyword(null,"picture-url","picture-url",1282469169),talltale.core.picture_url.cljs$core$IFn$_invoke$arity$1(sex),new cljs.core.Keyword(null,"address","address",559499426),talltale.core.address.cljs$core$IFn$_invoke$arity$1(locale)], null)], 0));
});
talltale.core.person_male = (function talltale$core$person_male(var_args){
var G__53388 = arguments.length;
switch (G__53388) {
case 0:
return talltale.core.person_male.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.person_male.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.person_male.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.person_male.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.person_male.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return talltale.core.person_all(locale,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"first-name","first-name",-1559982131),talltale.core.first_name_male.cljs$core$IFn$_invoke$arity$1(locale),new cljs.core.Keyword(null,"last-name","last-name",-1695738974),talltale.core.last_name_male.cljs$core$IFn$_invoke$arity$1(locale),new cljs.core.Keyword(null,"sex","sex",-2056008571),new cljs.core.Keyword(null,"male","male",-560253338)], null));
}));

(talltale.core.person_male.cljs$lang$maxFixedArity = 1);

talltale.core.person_male_gen = (function talltale$core$person_male_gen(var_args){
var G__53390 = arguments.length;
switch (G__53390) {
case 0:
return talltale.core.person_male_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.person_male_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.person_male_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.person_male_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.person_male_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return clojure.test.check.generators.bind(talltale.core.first_name_male_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (first_name){
return clojure.test.check.generators.bind(talltale.core.last_name_male_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (last_name){
return clojure.test.check.generators.bind(cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"male","male",-560253338)], 0)),(function (sex){
return clojure.test.check.generators.bind(talltale.core.username_gen.cljs$core$IFn$_invoke$arity$2(first_name,last_name),(function (username){
return clojure.test.check.generators.bind(talltale.core.email_gen(locale,first_name,last_name),(function (email){
return clojure.test.check.generators.bind(talltale.core.phone_number_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (phone_number){
return clojure.test.check.generators.bind(talltale.core.age_gen(),(function (age){
return clojure.test.check.generators.bind(talltale.core.date_of_birth_gen(age),(function (date_of_birth){
return clojure.test.check.generators.bind(talltale.core.position_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (position){
return clojure.test.check.generators.bind(talltale.core.picture_url_gen(sex),(function (picture_url){
return clojure.test.check.generators.bind(talltale.core.address_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (address){
var val__38745__auto__ = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"email","email",1415816706),new cljs.core.Keyword(null,"last-name","last-name",-1695738974),new cljs.core.Keyword(null,"phone-number","phone-number",453925028),new cljs.core.Keyword(null,"age","age",-604307804),new cljs.core.Keyword(null,"sex","sex",-2056008571),new cljs.core.Keyword(null,"username","username",1605666410),new cljs.core.Keyword(null,"first-name","first-name",-1559982131),new cljs.core.Keyword(null,"date-of-birth","date-of-birth",-848850674),new cljs.core.Keyword(null,"picture-url","picture-url",1282469169)],[address,email,last_name,phone_number,age,sex,username,first_name,date_of_birth,picture_url]);
if(clojure.test.check.generators.generator_QMARK_(val__38745__auto__)){
return val__38745__auto__;
} else {
return clojure.test.check.generators.return$(val__38745__auto__);
}
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));

(talltale.core.person_male_gen.cljs$lang$maxFixedArity = 1);

talltale.core.person_female = (function talltale$core$person_female(var_args){
var G__53392 = arguments.length;
switch (G__53392) {
case 0:
return talltale.core.person_female.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.person_female.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.person_female.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.person_female.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.person_female.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return talltale.core.person_all(locale,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"first-name","first-name",-1559982131),talltale.core.first_name_female.cljs$core$IFn$_invoke$arity$1(locale),new cljs.core.Keyword(null,"last-name","last-name",-1695738974),talltale.core.last_name_female.cljs$core$IFn$_invoke$arity$1(locale),new cljs.core.Keyword(null,"sex","sex",-2056008571),new cljs.core.Keyword(null,"female","female",1810186049)], null));
}));

(talltale.core.person_female.cljs$lang$maxFixedArity = 1);

talltale.core.person_female_gen = (function talltale$core$person_female_gen(var_args){
var G__53394 = arguments.length;
switch (G__53394) {
case 0:
return talltale.core.person_female_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.person_female_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.person_female_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.person_female_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.person_female_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return clojure.test.check.generators.bind(talltale.core.first_name_female_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (first_name){
return clojure.test.check.generators.bind(talltale.core.last_name_female_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (last_name){
return clojure.test.check.generators.bind(cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"female","female",1810186049)], 0)),(function (sex){
return clojure.test.check.generators.bind(talltale.core.username_gen.cljs$core$IFn$_invoke$arity$2(first_name,last_name),(function (username){
return clojure.test.check.generators.bind(talltale.core.email_gen(locale,first_name,last_name),(function (email){
return clojure.test.check.generators.bind(talltale.core.phone_number_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (phone_number){
return clojure.test.check.generators.bind(talltale.core.age_gen(),(function (age){
return clojure.test.check.generators.bind(talltale.core.date_of_birth_gen(age),(function (date_of_birth){
return clojure.test.check.generators.bind(talltale.core.position_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (position){
return clojure.test.check.generators.bind(talltale.core.picture_url_gen(sex),(function (picture_url){
return clojure.test.check.generators.bind(talltale.core.address_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (address){
var val__38745__auto__ = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"email","email",1415816706),new cljs.core.Keyword(null,"last-name","last-name",-1695738974),new cljs.core.Keyword(null,"phone-number","phone-number",453925028),new cljs.core.Keyword(null,"age","age",-604307804),new cljs.core.Keyword(null,"sex","sex",-2056008571),new cljs.core.Keyword(null,"username","username",1605666410),new cljs.core.Keyword(null,"first-name","first-name",-1559982131),new cljs.core.Keyword(null,"date-of-birth","date-of-birth",-848850674),new cljs.core.Keyword(null,"picture-url","picture-url",1282469169)],[address,email,last_name,phone_number,age,sex,username,first_name,date_of_birth,picture_url]);
if(clojure.test.check.generators.generator_QMARK_(val__38745__auto__)){
return val__38745__auto__;
} else {
return clojure.test.check.generators.return$(val__38745__auto__);
}
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));

(talltale.core.person_female_gen.cljs$lang$maxFixedArity = 1);

talltale.core.person = (function talltale$core$person(var_args){
var G__53396 = arguments.length;
switch (G__53396) {
case 0:
return talltale.core.person.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.person.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.person.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.person.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.person.cljs$core$IFn$_invoke$arity$1 = (function (locale){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.rand_int((2)),(0))){
return talltale.core.person_male.cljs$core$IFn$_invoke$arity$1(locale);
} else {
return talltale.core.person_female.cljs$core$IFn$_invoke$arity$1(locale);
}
}));

(talltale.core.person.cljs$lang$maxFixedArity = 1);

talltale.core.person_gen = (function talltale$core$person_gen(var_args){
var G__53398 = arguments.length;
switch (G__53398) {
case 0:
return talltale.core.person_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.person_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.person_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.person_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.person_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.rand_int((2)),(0))){
return talltale.core.person_male_gen.cljs$core$IFn$_invoke$arity$1(locale);
} else {
return talltale.core.person_female_gen.cljs$core$IFn$_invoke$arity$1(locale);
}
}));

(talltale.core.person_gen.cljs$lang$maxFixedArity = 1);

talltale.core.company_name = (function talltale$core$company_name(var_args){
var G__53400 = arguments.length;
switch (G__53400) {
case 0:
return talltale.core.company_name.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.company_name.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.company_name.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.company_name.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.company_name.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"company-name","company-name",1213910953)], null));
}));

(talltale.core.company_name.cljs$lang$maxFixedArity = 1);


talltale.core.company_name_gen = (function talltale$core$company_name_gen(var_args){
var G__53402 = arguments.length;
switch (G__53402) {
case 0:
return talltale.core.company_name_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.company_name_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.company_name_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.company_name_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.company_name_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"company-name","company-name",1213910953)], null))], 0));
}));

(talltale.core.company_name_gen.cljs$lang$maxFixedArity = 1);

talltale.core.company_type = (function talltale$core$company_type(var_args){
var G__53404 = arguments.length;
switch (G__53404) {
case 0:
return talltale.core.company_type.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.company_type.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.company_type.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.company_type.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.company_type.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"company-type","company-type",-981329286)], null));
}));

(talltale.core.company_type.cljs$lang$maxFixedArity = 1);


talltale.core.company_type_gen = (function talltale$core$company_type_gen(var_args){
var G__53406 = arguments.length;
switch (G__53406) {
case 0:
return talltale.core.company_type_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.company_type_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.company_type_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.company_type_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.company_type_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"company-type","company-type",-981329286)], null))], 0));
}));

(talltale.core.company_type_gen.cljs$lang$maxFixedArity = 1);

talltale.core.tld = (function talltale$core$tld(var_args){
var G__53408 = arguments.length;
switch (G__53408) {
case 0:
return talltale.core.tld.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.tld.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.tld.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.tld.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.tld.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"tld","tld",-2134284264)], null));
}));

(talltale.core.tld.cljs$lang$maxFixedArity = 1);


talltale.core.tld_gen = (function talltale$core$tld_gen(var_args){
var G__53410 = arguments.length;
switch (G__53410) {
case 0:
return talltale.core.tld_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.tld_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.tld_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.tld_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.tld_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"tld","tld",-2134284264)], null))], 0));
}));

(talltale.core.tld_gen.cljs$lang$maxFixedArity = 1);

talltale.core.function$ = (function talltale$core$function(var_args){
var G__53412 = arguments.length;
switch (G__53412) {
case 0:
return talltale.core.function$.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.function$.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.function$.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.function$.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.function$.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"function","function",-2127255473)], null));
}));

(talltale.core.function$.cljs$lang$maxFixedArity = 1);


talltale.core.function_gen = (function talltale$core$function_gen(var_args){
var G__53414 = arguments.length;
switch (G__53414) {
case 0:
return talltale.core.function_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.function_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.function_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.function_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.function_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"function","function",-2127255473)], null))], 0));
}));

(talltale.core.function_gen.cljs$lang$maxFixedArity = 1);

talltale.core.full_name = (function talltale$core$full_name(name,type){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(name)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(type)].join('');
});
talltale.core.full_name_gen = (function talltale$core$full_name_gen(name,type){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.full_name(name,type)], 0));
});
talltale.core.domain = (function talltale$core$domain(name,tld){
return [clojure.string.lower_case(name),".",cljs.core.str.cljs$core$IFn$_invoke$arity$1(tld)].join('');
});
talltale.core.domain_gen = (function talltale$core$domain_gen(name,tld){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.domain(name,tld)], 0));
});
talltale.core.url = (function talltale$core$url(domain){
return ["https://www.",cljs.core.str.cljs$core$IFn$_invoke$arity$1(domain)].join('');
});
talltale.core.url_gen = (function talltale$core$url_gen(domain){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.url(domain)], 0));
});
talltale.core.logo_url = (function talltale$core$logo_url(name){
return ["http://via.placeholder.com/350x150?text=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(name)].join('');
});
talltale.core.logo_url_gen = (function talltale$core$logo_url_gen(name){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.logo_url(name)], 0));
});
talltale.core.company_email = (function talltale$core$company_email(var_args){
var G__53416 = arguments.length;
switch (G__53416) {
case 1:
return talltale.core.company_email.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return talltale.core.company_email.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.company_email.cljs$core$IFn$_invoke$arity$1 = (function (domain){
return talltale.core.company_email.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"en","en",88457073),domain);
}));

(talltale.core.company_email.cljs$core$IFn$_invoke$arity$2 = (function (locale,domain){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(talltale.macros.rand_data(locale,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"email","email",1415816706)], null))),"@",cljs.core.str.cljs$core$IFn$_invoke$arity$1(domain)].join('');
}));

(talltale.core.company_email.cljs$lang$maxFixedArity = 2);

talltale.core.company_email_gen = (function talltale$core$company_email_gen(var_args){
var G__53418 = arguments.length;
switch (G__53418) {
case 1:
return talltale.core.company_email_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return talltale.core.company_email_gen.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.company_email_gen.cljs$core$IFn$_invoke$arity$1 = (function (domain){
return talltale.core.company_email_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.company_email_gen.cljs$core$IFn$_invoke$arity$2 = (function (locale,domain){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.company_email.cljs$core$IFn$_invoke$arity$2(locale,domain)], 0));
}));

(talltale.core.company_email_gen.cljs$lang$maxFixedArity = 2);

talltale.core.in_en = (function talltale$core$in_en(var_args){
var G__53420 = arguments.length;
switch (G__53420) {
case 0:
return talltale.core.in_en.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.in_en.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.in_en.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.in_en.cljs$core$IFn$_invoke$arity$1(cljs.core.rand_int((9999)));
}));

(talltale.core.in_en.cljs$core$IFn$_invoke$arity$1 = (function (rand){
var serial = cljs.pprint.cl_format.cljs$core$IFn$_invoke$arity$variadic(null,"~7,'0d",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([rand], 0));
var area_excluding_numbers = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 17, [(70),null,(7),null,(69),null,(41),null,(89),null,(29),null,(28),null,(17),null,(47),null,(97),null,(19),null,(9),null,(79),null,(96),null,(18),null,(8),null,(49),null], null), null);
var area = cljs.pprint.cl_format.cljs$core$IFn$_invoke$arity$variadic(null,"~2,'0d",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.rand_excluding((99),area_excluding_numbers)], 0));
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(area),"-",cljs.core.str.cljs$core$IFn$_invoke$arity$1(serial)].join('');
}));

(talltale.core.in_en.cljs$lang$maxFixedArity = 1);

talltale.core.in_fr = (function talltale$core$in_fr(var_args){
var G__53422 = arguments.length;
switch (G__53422) {
case 0:
return talltale.core.in_fr.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.in_fr.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.in_fr.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.in_fr.cljs$core$IFn$_invoke$arity$1(cljs.core.rand_int((99999999)));
}));

(talltale.core.in_fr.cljs$core$IFn$_invoke$arity$1 = (function (rand){
return cljs.pprint.cl_format.cljs$core$IFn$_invoke$arity$variadic(null,"~8,'0d",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([rand], 0));
}));

(talltale.core.in_fr.cljs$lang$maxFixedArity = 1);

talltale.core.identification_number = (function talltale$core$identification_number(var_args){
var G__53424 = arguments.length;
switch (G__53424) {
case 0:
return talltale.core.identification_number.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.identification_number.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return talltale.core.identification_number.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.identification_number.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.in_en.cljs$core$IFn$_invoke$arity$0();
}));

(talltale.core.identification_number.cljs$core$IFn$_invoke$arity$1 = (function (locale){
var G__53425 = locale;
var G__53425__$1 = (((G__53425 instanceof cljs.core.Keyword))?G__53425.fqn:null);
switch (G__53425__$1) {
case "en":
return talltale.core.in_en.cljs$core$IFn$_invoke$arity$0();

break;
case "fr":
return talltale.core.in_fr.cljs$core$IFn$_invoke$arity$0();

break;
default:
return talltale.core.in_en.cljs$core$IFn$_invoke$arity$0();

}
}));

(talltale.core.identification_number.cljs$core$IFn$_invoke$arity$2 = (function (locale,rand){
var G__53426 = locale;
var G__53426__$1 = (((G__53426 instanceof cljs.core.Keyword))?G__53426.fqn:null);
switch (G__53426__$1) {
case "en":
return talltale.core.in_en.cljs$core$IFn$_invoke$arity$1(rand);

break;
case "fr":
return talltale.core.in_fr.cljs$core$IFn$_invoke$arity$1(rand);

break;
default:
return talltale.core.in_en.cljs$core$IFn$_invoke$arity$1(rand);

}
}));

(talltale.core.identification_number.cljs$lang$maxFixedArity = 2);

talltale.core.identification_number_gen = (function talltale$core$identification_number_gen(var_args){
var G__53428 = arguments.length;
switch (G__53428) {
case 0:
return talltale.core.identification_number_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.identification_number_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.identification_number_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.identification_number_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.identification_number_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
var G__53429 = locale;
var G__53429__$1 = (((G__53429 instanceof cljs.core.Keyword))?G__53429.fqn:null);
switch (G__53429__$1) {
case "en":
return cljs.spec.gen.alpha.fmap.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.in_en,clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(1),new cljs.core.Keyword(null,"max","max",61366548),(9999)], null))], 0));

break;
case "fr":
return cljs.spec.gen.alpha.fmap.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.in_fr,clojure.test.check.generators.large_integer_STAR_(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"min","min",444991522),(1),new cljs.core.Keyword(null,"max","max",61366548),(99999999)], null))], 0));

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__53429__$1)].join('')));

}
}));

(talltale.core.identification_number_gen.cljs$lang$maxFixedArity = 1);

talltale.core.company_id = (function talltale$core$company_id(name){
return clojure.string.replace(name,/ /,"");
});
talltale.core.company_id_gen = (function talltale$core$company_id_gen(name){
return cljs.spec.gen.alpha.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.core.company_id(name)], 0));
});
talltale.core.department = (function talltale$core$department(var_args){
var G__53431 = arguments.length;
switch (G__53431) {
case 0:
return talltale.core.department.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.department.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.department.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.department.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.department.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"department","department",-359157087)], null));
}));

(talltale.core.department.cljs$lang$maxFixedArity = 1);


talltale.core.department_gen = (function talltale$core$department_gen(var_args){
var G__53433 = arguments.length;
switch (G__53433) {
case 0:
return talltale.core.department_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.department_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.department_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.department_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.department_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"company","company",-340475075),new cljs.core.Keyword(null,"department","department",-359157087)], null))], 0));
}));

(talltale.core.department_gen.cljs$lang$maxFixedArity = 1);

talltale.core.company = (function talltale$core$company(var_args){
var G__53435 = arguments.length;
switch (G__53435) {
case 0:
return talltale.core.company.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.company.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return talltale.core.company.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.company.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.company.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.company.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return talltale.core.company.cljs$core$IFn$_invoke$arity$2(locale,talltale.core.company_name.cljs$core$IFn$_invoke$arity$1(locale));
}));

(talltale.core.company.cljs$core$IFn$_invoke$arity$2 = (function (locale,name){
var type = talltale.core.company_type.cljs$core$IFn$_invoke$arity$1(locale);
var full_name = talltale.core.full_name(name,type);
var domain = talltale.core.domain(name,talltale.core.tld.cljs$core$IFn$_invoke$arity$1(locale));
var url = talltale.core.url(domain);
var email = talltale.core.company_email.cljs$core$IFn$_invoke$arity$2(locale,domain);
return cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"updated-at","updated-at",-1592622336),new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"email","email",1415816706),new cljs.core.Keyword(null,"phone-number","phone-number",453925028),new cljs.core.Keyword(null,"company-name","company-name",1213910953),new cljs.core.Keyword(null,"company-id","company-id",-947141651),new cljs.core.Keyword(null,"updated-by","updated-by",-1596579598),new cljs.core.Keyword(null,"url","url",276297046),new cljs.core.Keyword(null,"full-name","full-name",408178550),new cljs.core.Keyword(null,"logo-url","logo-url",-1629105032),new cljs.core.Keyword(null,"identification-number","identification-number",-750071400),new cljs.core.Keyword(null,"domain","domain",1847214937),new cljs.core.Keyword(null,"company-type","company-type",-981329286)],[(talltale.core.instant.cljs$core$IFn$_invoke$arity$0 ? talltale.core.instant.cljs$core$IFn$_invoke$arity$0() : talltale.core.instant.call(null)),talltale.core.address.cljs$core$IFn$_invoke$arity$1(locale),email,talltale.core.phone_number.cljs$core$IFn$_invoke$arity$1(locale),name,talltale.core.company_id(name),talltale.core.username.cljs$core$IFn$_invoke$arity$1(locale),url,full_name,talltale.core.logo_url(name),talltale.core.identification_number.cljs$core$IFn$_invoke$arity$1(locale),domain,type]);
}));

(talltale.core.company.cljs$lang$maxFixedArity = 2);

talltale.core.company_with_name = (function talltale$core$company_with_name(name){
return talltale.core.company.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"en","en",88457073),name);
});
talltale.core.company_gen = (function talltale$core$company_gen(var_args){
var G__53437 = arguments.length;
switch (G__53437) {
case 0:
return talltale.core.company_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.company_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.company_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.company_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.company_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale){
return clojure.test.check.generators.bind(talltale.core.company_name_gen.cljs$core$IFn$_invoke$arity$0(),(function (company_name){
return clojure.test.check.generators.bind(talltale.core.company_id_gen(cljs.core.name),(function (company_id){
return clojure.test.check.generators.bind(talltale.core.company_type_gen.cljs$core$IFn$_invoke$arity$0(),(function (company_type){
return clojure.test.check.generators.bind(talltale.core.identification_number_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (identification_number){
return clojure.test.check.generators.bind(talltale.core.full_name_gen(cljs.core.name,cljs.core.type),(function (full_name){
return clojure.test.check.generators.bind(talltale.core.tld_gen.cljs$core$IFn$_invoke$arity$0(),(function (tld){
return clojure.test.check.generators.bind(talltale.core.domain_gen(cljs.core.name,tld),(function (domain){
return clojure.test.check.generators.bind(talltale.core.url_gen(domain),(function (url){
return clojure.test.check.generators.bind(talltale.core.logo_url_gen(cljs.core.name),(function (logo_url){
return clojure.test.check.generators.bind(talltale.core.company_email_gen.cljs$core$IFn$_invoke$arity$2(locale,domain),(function (email){
return clojure.test.check.generators.bind(talltale.core.phone_number_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (phone_number){
return clojure.test.check.generators.bind(talltale.core.address_gen.cljs$core$IFn$_invoke$arity$1(locale),(function (address){
return clojure.test.check.generators.bind(talltale.core.username_gen.cljs$core$IFn$_invoke$arity$0(),(function (updated_by){
var val__38745__auto__ = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"address","address",559499426),new cljs.core.Keyword(null,"email","email",1415816706),new cljs.core.Keyword(null,"phone-number","phone-number",453925028),new cljs.core.Keyword(null,"company-name","company-name",1213910953),new cljs.core.Keyword(null,"company-id","company-id",-947141651),new cljs.core.Keyword(null,"url","url",276297046),new cljs.core.Keyword(null,"full-name","full-name",408178550),new cljs.core.Keyword(null,"tld","tld",-2134284264),new cljs.core.Keyword(null,"logo-url","logo-url",-1629105032),new cljs.core.Keyword(null,"identification-number","identification-number",-750071400),new cljs.core.Keyword(null,"domain","domain",1847214937),new cljs.core.Keyword(null,"company-type","company-type",-981329286)],[address,email,phone_number,company_name,company_id,url,full_name,tld,logo_url,identification_number,domain,company_type]);
if(clojure.test.check.generators.generator_QMARK_(val__38745__auto__)){
return val__38745__auto__;
} else {
return clojure.test.check.generators.return$(val__38745__auto__);
}
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));
}));

(talltale.core.company_gen.cljs$lang$maxFixedArity = 1);

talltale.core.quality = (function talltale$core$quality(var_args){
var G__53439 = arguments.length;
switch (G__53439) {
case 0:
return talltale.core.quality.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.quality.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.quality.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.quality.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.quality.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"quality","quality",147850199)], null));
}));

(talltale.core.quality.cljs$lang$maxFixedArity = 1);


talltale.core.quality_gen = (function talltale$core$quality_gen(var_args){
var G__53441 = arguments.length;
switch (G__53441) {
case 0:
return talltale.core.quality_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.quality_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.quality_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.quality_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.quality_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"quality","quality",147850199)], null))], 0));
}));

(talltale.core.quality_gen.cljs$lang$maxFixedArity = 1);

talltale.core.shape = (function talltale$core$shape(var_args){
var G__53443 = arguments.length;
switch (G__53443) {
case 0:
return talltale.core.shape.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.shape.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.shape.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.shape.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.shape.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"shape","shape",1190694006)], null));
}));

(talltale.core.shape.cljs$lang$maxFixedArity = 1);


talltale.core.shape_gen = (function talltale$core$shape_gen(var_args){
var G__53445 = arguments.length;
switch (G__53445) {
case 0:
return talltale.core.shape_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.shape_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.shape_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.shape_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.shape_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"shape","shape",1190694006)], null))], 0));
}));

(talltale.core.shape_gen.cljs$lang$maxFixedArity = 1);

talltale.core.color = (function talltale$core$color(var_args){
var G__53447 = arguments.length;
switch (G__53447) {
case 0:
return talltale.core.color.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.color.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.color.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.color.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.color.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"color","color",1011675173)], null));
}));

(talltale.core.color.cljs$lang$maxFixedArity = 1);


talltale.core.color_gen = (function talltale$core$color_gen(var_args){
var G__53449 = arguments.length;
switch (G__53449) {
case 0:
return talltale.core.color_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.color_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.color_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.color_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.color_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"color","color",1011675173)], null))], 0));
}));

(talltale.core.color_gen.cljs$lang$maxFixedArity = 1);

talltale.core.animal = (function talltale$core$animal(var_args){
var G__53451 = arguments.length;
switch (G__53451) {
case 0:
return talltale.core.animal.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.animal.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.animal.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.animal.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.animal.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"animal","animal",-1445940363)], null));
}));

(talltale.core.animal.cljs$lang$maxFixedArity = 1);


talltale.core.animal_gen = (function talltale$core$animal_gen(var_args){
var G__53453 = arguments.length;
switch (G__53453) {
case 0:
return talltale.core.animal_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.animal_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.animal_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.animal_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.animal_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"animal","animal",-1445940363)], null))], 0));
}));

(talltale.core.animal_gen.cljs$lang$maxFixedArity = 1);

talltale.core.landform = (function talltale$core$landform(var_args){
var G__53455 = arguments.length;
switch (G__53455) {
case 0:
return talltale.core.landform.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.landform.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.landform.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.landform.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.landform.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return talltale.macros.rand_data(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"landform","landform",1045440285)], null));
}));

(talltale.core.landform.cljs$lang$maxFixedArity = 1);


talltale.core.landform_gen = (function talltale$core$landform_gen(var_args){
var G__53457 = arguments.length;
switch (G__53457) {
case 0:
return talltale.core.landform_gen.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return talltale.core.landform_gen.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(talltale.core.landform_gen.cljs$core$IFn$_invoke$arity$0 = (function (){
return talltale.core.landform_gen.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"en","en",88457073));
}));

(talltale.core.landform_gen.cljs$core$IFn$_invoke$arity$1 = (function (locale__53301__auto__){
return cljs.spec.gen.alpha.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([talltale.macros.raw(locale__53301__auto__,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"landform","landform",1045440285)], null))], 0));
}));

(talltale.core.landform_gen.cljs$lang$maxFixedArity = 1);

talltale.core.quality_color_animal = (function talltale$core$quality_color_animal(){
return clojure.string.join.cljs$core$IFn$_invoke$arity$2(" ",new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [talltale.core.quality.cljs$core$IFn$_invoke$arity$0(),talltale.core.color.cljs$core$IFn$_invoke$arity$0(),talltale.core.animal.cljs$core$IFn$_invoke$arity$0()], null));
});

//# sourceMappingURL=talltale.core.js.map
