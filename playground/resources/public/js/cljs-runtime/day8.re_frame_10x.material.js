goog.provide('day8.re_frame_10x.material');
goog.require('cljs.core');
day8.re_frame_10x.material.add = (function day8$re_frame_10x$material$add(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53906 = arguments.length;
var i__4737__auto___53907 = (0);
while(true){
if((i__4737__auto___53907 < len__4736__auto___53906)){
args__4742__auto__.push((arguments[i__4737__auto___53907]));

var G__53908 = (i__4737__auto___53907 + (1));
i__4737__auto___53907 = G__53908;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.add.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.add.cljs$core$IFn$_invoke$arity$variadic = (function (p__53733){
var map__53734 = p__53733;
var map__53734__$1 = (((((!((map__53734 == null))))?(((((map__53734.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53734.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53734):map__53734);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53734__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M19 13h-6v6h-2v-6H5v-2h6V5h2v6h6v2z"], null)], null)], null);
}));

(day8.re_frame_10x.material.add.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.add.cljs$lang$applyTo = (function (seq53731){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53731));
}));

day8.re_frame_10x.material.arrow_drop_down = (function day8$re_frame_10x$material$arrow_drop_down(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53912 = arguments.length;
var i__4737__auto___53913 = (0);
while(true){
if((i__4737__auto___53913 < len__4736__auto___53912)){
args__4742__auto__.push((arguments[i__4737__auto___53913]));

var G__53914 = (i__4737__auto___53913 + (1));
i__4737__auto___53913 = G__53914;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.arrow_drop_down.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.arrow_drop_down.cljs$core$IFn$_invoke$arity$variadic = (function (p__53737){
var map__53738 = p__53737;
var map__53738__$1 = (((((!((map__53738 == null))))?(((((map__53738.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53738.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53738):map__53738);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53738__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M7 10l5 5 5-5H7z"], null)], null)], null);
}));

(day8.re_frame_10x.material.arrow_drop_down.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.arrow_drop_down.cljs$lang$applyTo = (function (seq53736){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53736));
}));

day8.re_frame_10x.material.arrow_drop_up = (function day8$re_frame_10x$material$arrow_drop_up(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53918 = arguments.length;
var i__4737__auto___53919 = (0);
while(true){
if((i__4737__auto___53919 < len__4736__auto___53918)){
args__4742__auto__.push((arguments[i__4737__auto___53919]));

var G__53920 = (i__4737__auto___53919 + (1));
i__4737__auto___53919 = G__53920;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.arrow_drop_up.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.arrow_drop_up.cljs$core$IFn$_invoke$arity$variadic = (function (p__53742){
var map__53744 = p__53742;
var map__53744__$1 = (((((!((map__53744 == null))))?(((((map__53744.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53744.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53744):map__53744);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53744__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M7 14l5-5 5 5H7z"], null)], null)], null);
}));

(day8.re_frame_10x.material.arrow_drop_up.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.arrow_drop_up.cljs$lang$applyTo = (function (seq53740){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53740));
}));

day8.re_frame_10x.material.arrow_left = (function day8$re_frame_10x$material$arrow_left(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53937 = arguments.length;
var i__4737__auto___53939 = (0);
while(true){
if((i__4737__auto___53939 < len__4736__auto___53937)){
args__4742__auto__.push((arguments[i__4737__auto___53939]));

var G__53940 = (i__4737__auto___53939 + (1));
i__4737__auto___53939 = G__53940;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.arrow_left.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.arrow_left.cljs$core$IFn$_invoke$arity$variadic = (function (p__53757){
var map__53758 = p__53757;
var map__53758__$1 = (((((!((map__53758 == null))))?(((((map__53758.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53758.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53758):map__53758);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53758__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M14 7l-5 5 5 5V7z"], null)], null)], null);
}));

(day8.re_frame_10x.material.arrow_left.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.arrow_left.cljs$lang$applyTo = (function (seq53756){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53756));
}));

day8.re_frame_10x.material.arrow_right = (function day8$re_frame_10x$material$arrow_right(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53944 = arguments.length;
var i__4737__auto___53945 = (0);
while(true){
if((i__4737__auto___53945 < len__4736__auto___53944)){
args__4742__auto__.push((arguments[i__4737__auto___53945]));

var G__53946 = (i__4737__auto___53945 + (1));
i__4737__auto___53945 = G__53946;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.arrow_right.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.arrow_right.cljs$core$IFn$_invoke$arity$variadic = (function (p__53774){
var map__53775 = p__53774;
var map__53775__$1 = (((((!((map__53775 == null))))?(((((map__53775.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53775.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53775):map__53775);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53775__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M10 17l5-5-5-5v10z"], null)], null)], null);
}));

(day8.re_frame_10x.material.arrow_right.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.arrow_right.cljs$lang$applyTo = (function (seq53765){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53765));
}));

day8.re_frame_10x.material.help = (function day8$re_frame_10x$material$help(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53961 = arguments.length;
var i__4737__auto___53962 = (0);
while(true){
if((i__4737__auto___53962 < len__4736__auto___53961)){
args__4742__auto__.push((arguments[i__4737__auto___53962]));

var G__53963 = (i__4737__auto___53962 + (1));
i__4737__auto___53962 = G__53963;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.help.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.help.cljs$core$IFn$_invoke$arity$variadic = (function (p__53790){
var map__53791 = p__53790;
var map__53791__$1 = (((((!((map__53791 == null))))?(((((map__53791.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53791.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53791):map__53791);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53791__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 17h-2v-2h2v2zm2.07-7.75l-.9.92C13.45 12.9 13 13.5 13 15h-2v-.5c0-1.1.45-2.1 1.17-2.83l1.24-1.26c.37-.36.59-.86.59-1.41 0-1.1-.9-2-2-2s-2 .9-2 2H8c0-2.21 1.79-4 4-4s4 1.79 4 4c0 .88-.36 1.68-.93 2.25z"], null)], null)], null);
}));

(day8.re_frame_10x.material.help.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.help.cljs$lang$applyTo = (function (seq53786){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53786));
}));

day8.re_frame_10x.material.open_in_new = (function day8$re_frame_10x$material$open_in_new(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53971 = arguments.length;
var i__4737__auto___53973 = (0);
while(true){
if((i__4737__auto___53973 < len__4736__auto___53971)){
args__4742__auto__.push((arguments[i__4737__auto___53973]));

var G__53975 = (i__4737__auto___53973 + (1));
i__4737__auto___53973 = G__53975;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.open_in_new.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.open_in_new.cljs$core$IFn$_invoke$arity$variadic = (function (p__53815){
var map__53816 = p__53815;
var map__53816__$1 = (((((!((map__53816 == null))))?(((((map__53816.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53816.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53816):map__53816);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53816__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M19 19H5V5h7V3H3v18h18v-9h-2v7zM14 3v2h3.59l-9.83 9.83 1.41 1.41L19 6.41V10h2V3h-7z"], null)], null)], null);
}));

(day8.re_frame_10x.material.open_in_new.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.open_in_new.cljs$lang$applyTo = (function (seq53809){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53809));
}));

day8.re_frame_10x.material.refresh = (function day8$re_frame_10x$material$refresh(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53983 = arguments.length;
var i__4737__auto___53984 = (0);
while(true){
if((i__4737__auto___53984 < len__4736__auto___53983)){
args__4742__auto__.push((arguments[i__4737__auto___53984]));

var G__53985 = (i__4737__auto___53984 + (1));
i__4737__auto___53984 = G__53985;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.refresh.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.refresh.cljs$core$IFn$_invoke$arity$variadic = (function (p__53831){
var map__53832 = p__53831;
var map__53832__$1 = (((((!((map__53832 == null))))?(((((map__53832.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53832.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53832):map__53832);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53832__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M17.65 6.35C16.2 4.9 14.21 4 12 4c-4.42 0-7.99 3.58-7.99 8s3.57 8 7.99 8c3.73 0 6.84-2.55 7.73-6h-2.08c-.82 2.33-3.04 4-5.65 4-3.31 0-6-2.69-6-6s2.69-6 6-6c1.66 0 3.14.69 4.22 1.78L13 11h7V4l-2.35 2.35z"], null)], null)], null);
}));

(day8.re_frame_10x.material.refresh.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.refresh.cljs$lang$applyTo = (function (seq53823){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53823));
}));

day8.re_frame_10x.material.settings = (function day8$re_frame_10x$material$settings(var_args){
var args__4742__auto__ = [];
var len__4736__auto___53988 = arguments.length;
var i__4737__auto___53989 = (0);
while(true){
if((i__4737__auto___53989 < len__4736__auto___53988)){
args__4742__auto__.push((arguments[i__4737__auto___53989]));

var G__53990 = (i__4737__auto___53989 + (1));
i__4737__auto___53989 = G__53990;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.settings.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.settings.cljs$core$IFn$_invoke$arity$variadic = (function (p__53848){
var map__53849 = p__53848;
var map__53849__$1 = (((((!((map__53849 == null))))?(((((map__53849.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53849.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53849):map__53849);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53849__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M19.44 12.99l-.01.02c.04-.33.08-.67.08-1.01 0-.34-.03-.66-.07-.99l.01.02 2.44-1.92-2.43-4.22-2.87 1.16.01.01c-.52-.4-1.09-.74-1.71-1h.01L14.44 2H9.57l-.44 3.07h.01c-.62.26-1.19.6-1.71 1l.01-.01-2.88-1.17-2.44 4.22 2.44 1.92.01-.02c-.04.33-.07.65-.07.99 0 .34.03.68.08 1.01l-.01-.02-2.1 1.65-.33.26 2.43 4.2 2.88-1.15-.02-.04c.53.41 1.1.75 1.73 1.01h-.03L9.58 22h4.85s.03-.18.06-.42l.38-2.65h-.01c.62-.26 1.2-.6 1.73-1.01l-.02.04 2.88 1.15 2.43-4.2s-.14-.12-.33-.26l-2.11-1.66zM12 15.5c-1.93 0-3.5-1.57-3.5-3.5s1.57-3.5 3.5-3.5 3.5 1.57 3.5 3.5-1.57 3.5-3.5 3.5z"], null)], null)], null);
}));

(day8.re_frame_10x.material.settings.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.settings.cljs$lang$applyTo = (function (seq53841){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53841));
}));

day8.re_frame_10x.material.skip_next = (function day8$re_frame_10x$material$skip_next(var_args){
var args__4742__auto__ = [];
var len__4736__auto___54002 = arguments.length;
var i__4737__auto___54003 = (0);
while(true){
if((i__4737__auto___54003 < len__4736__auto___54002)){
args__4742__auto__.push((arguments[i__4737__auto___54003]));

var G__54006 = (i__4737__auto___54003 + (1));
i__4737__auto___54003 = G__54006;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.skip_next.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.skip_next.cljs$core$IFn$_invoke$arity$variadic = (function (p__53870){
var map__53874 = p__53870;
var map__53874__$1 = (((((!((map__53874 == null))))?(((((map__53874.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53874.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53874):map__53874);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53874__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z"], null)], null)], null);
}));

(day8.re_frame_10x.material.skip_next.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.skip_next.cljs$lang$applyTo = (function (seq53860){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53860));
}));

day8.re_frame_10x.material.unfold_less = (function day8$re_frame_10x$material$unfold_less(var_args){
var args__4742__auto__ = [];
var len__4736__auto___54021 = arguments.length;
var i__4737__auto___54023 = (0);
while(true){
if((i__4737__auto___54023 < len__4736__auto___54021)){
args__4742__auto__.push((arguments[i__4737__auto___54023]));

var G__54024 = (i__4737__auto___54023 + (1));
i__4737__auto___54023 = G__54024;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.unfold_less.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.unfold_less.cljs$core$IFn$_invoke$arity$variadic = (function (p__53893){
var map__53894 = p__53893;
var map__53894__$1 = (((((!((map__53894 == null))))?(((((map__53894.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53894.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53894):map__53894);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53894__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M7.41 18.59L8.83 20 12 16.83 15.17 20l1.41-1.41L12 14l-4.59 4.59zm9.18-13.18L15.17 4 12 7.17 8.83 4 7.41 5.41 12 10l4.59-4.59z"], null)], null)], null);
}));

(day8.re_frame_10x.material.unfold_less.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.unfold_less.cljs$lang$applyTo = (function (seq53890){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53890));
}));

day8.re_frame_10x.material.unfold_more = (function day8$re_frame_10x$material$unfold_more(var_args){
var args__4742__auto__ = [];
var len__4736__auto___54029 = arguments.length;
var i__4737__auto___54030 = (0);
while(true){
if((i__4737__auto___54030 < len__4736__auto___54029)){
args__4742__auto__.push((arguments[i__4737__auto___54030]));

var G__54031 = (i__4737__auto___54030 + (1));
i__4737__auto___54030 = G__54031;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return day8.re_frame_10x.material.unfold_more.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(day8.re_frame_10x.material.unfold_more.cljs$core$IFn$_invoke$arity$variadic = (function (p__53903){
var map__53904 = p__53903;
var map__53904__$1 = (((((!((map__53904 == null))))?(((((map__53904.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__53904.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__53904):map__53904);
var fill = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__53904__$1,new cljs.core.Keyword(null,"fill","fill",883462889));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"fill","fill",883462889),fill,new cljs.core.Keyword(null,"height","height",1025178622),"24",new cljs.core.Keyword(null,"viewBox","viewBox",-469489477),"0 0 24 24",new cljs.core.Keyword(null,"width","width",-384071477),"24"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"path","path",-188191168),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"d","d",1972142424),"M12 5.83L15.17 9l1.41-1.41L12 3 7.41 7.59 8.83 9 12 5.83zm0 12.34L8.83 15l-1.41 1.41L12 21l4.59-4.59L15.17 15 12 18.17z"], null)], null)], null);
}));

(day8.re_frame_10x.material.unfold_more.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(day8.re_frame_10x.material.unfold_more.cljs$lang$applyTo = (function (seq53898){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq53898));
}));


//# sourceMappingURL=day8.re_frame_10x.material.js.map
