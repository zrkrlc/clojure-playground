goog.provide('re_frame.trace');
goog.require('cljs.core');
goog.require('re_frame.interop');
goog.require('re_frame.loggers');
goog.require('goog.functions');
re_frame.trace.id = cljs.core.atom.cljs$core$IFn$_invoke$arity$1((0));
re_frame.trace._STAR_current_trace_STAR_ = null;
re_frame.trace.reset_tracing_BANG_ = (function re_frame$trace$reset_tracing_BANG_(){
return cljs.core.reset_BANG_(re_frame.trace.id,(0));
});

/**
 * @define {boolean}
 */
re_frame.trace.trace_enabled_QMARK_ = goog.define("re_frame.trace.trace_enabled_QMARK_",false);
/**
 * See https://groups.google.com/d/msg/clojurescript/jk43kmYiMhA/IHglVr_TPdgJ for more details
 */
re_frame.trace.is_trace_enabled_QMARK_ = (function re_frame$trace$is_trace_enabled_QMARK_(){
return re_frame.trace.trace_enabled_QMARK_;
});
re_frame.trace.trace_cbs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
if((typeof re_frame !== 'undefined') && (typeof re_frame.trace !== 'undefined') && (typeof re_frame.trace.traces !== 'undefined')){
} else {
re_frame.trace.traces = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentVector.EMPTY);
}
if((typeof re_frame !== 'undefined') && (typeof re_frame.trace !== 'undefined') && (typeof re_frame.trace.next_delivery !== 'undefined')){
} else {
re_frame.trace.next_delivery = cljs.core.atom.cljs$core$IFn$_invoke$arity$1((0));
}
/**
 * Registers a tracing callback function which will receive a collection of one or more traces.
 *   Will replace an existing callback function if it shares the same key.
 */
re_frame.trace.register_trace_cb = (function re_frame$trace$register_trace_cb(key,f){
if(cljs.core.truth_(re_frame.trace.trace_enabled_QMARK_)){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(re_frame.trace.trace_cbs,cljs.core.assoc,key,f);
} else {
return re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"warn","warn",-436710552),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Tracing is not enabled. Please set {\"re_frame.trace.trace_enabled_QMARK_\" true} in :closure-defines. See: https://github.com/day8/re-frame-10x#installation."], 0));
}
});
re_frame.trace.remove_trace_cb = (function re_frame$trace$remove_trace_cb(key){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(re_frame.trace.trace_cbs,cljs.core.dissoc,key);

return null;
});
re_frame.trace.next_id = (function re_frame$trace$next_id(){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(re_frame.trace.id,cljs.core.inc);
});
re_frame.trace.start_trace = (function re_frame$trace$start_trace(p__58184){
var map__58185 = p__58184;
var map__58185__$1 = (((((!((map__58185 == null))))?(((((map__58185.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__58185.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__58185):map__58185);
var operation = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__58185__$1,new cljs.core.Keyword(null,"operation","operation",-1267664310));
var op_type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__58185__$1,new cljs.core.Keyword(null,"op-type","op-type",-1636141668));
var tags = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__58185__$1,new cljs.core.Keyword(null,"tags","tags",1771418977));
var child_of = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__58185__$1,new cljs.core.Keyword(null,"child-of","child-of",-903376662));
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"id","id",-1388402092),re_frame.trace.next_id(),new cljs.core.Keyword(null,"operation","operation",-1267664310),operation,new cljs.core.Keyword(null,"op-type","op-type",-1636141668),op_type,new cljs.core.Keyword(null,"tags","tags",1771418977),tags,new cljs.core.Keyword(null,"child-of","child-of",-903376662),(function (){var or__4126__auto__ = child_of;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(re_frame.trace._STAR_current_trace_STAR_);
}
})(),new cljs.core.Keyword(null,"start","start",-355208981),re_frame.interop.now()], null);
});
re_frame.trace.debounce_time = (50);
re_frame.trace.debounce = (function re_frame$trace$debounce(f,interval){
return goog.functions.debounce(f,interval);
});
re_frame.trace.schedule_debounce = re_frame.trace.debounce((function re_frame$trace$tracing_cb_debounced(){
var seq__58189_58225 = cljs.core.seq(cljs.core.deref(re_frame.trace.trace_cbs));
var chunk__58190_58226 = null;
var count__58191_58227 = (0);
var i__58192_58228 = (0);
while(true){
if((i__58192_58228 < count__58191_58227)){
var vec__58206_58230 = chunk__58190_58226.cljs$core$IIndexed$_nth$arity$2(null,i__58192_58228);
var k_58231 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58206_58230,(0),null);
var cb_58232 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58206_58230,(1),null);
try{var G__58210_58241 = cljs.core.deref(re_frame.trace.traces);
(cb_58232.cljs$core$IFn$_invoke$arity$1 ? cb_58232.cljs$core$IFn$_invoke$arity$1(G__58210_58241) : cb_58232.call(null,G__58210_58241));
}catch (e58209){var e_58243 = e58209;
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Error thrown from trace cb",k_58231,"while storing",cljs.core.deref(re_frame.trace.traces),e_58243], 0));
}

var G__58244 = seq__58189_58225;
var G__58245 = chunk__58190_58226;
var G__58246 = count__58191_58227;
var G__58247 = (i__58192_58228 + (1));
seq__58189_58225 = G__58244;
chunk__58190_58226 = G__58245;
count__58191_58227 = G__58246;
i__58192_58228 = G__58247;
continue;
} else {
var temp__5735__auto___58253 = cljs.core.seq(seq__58189_58225);
if(temp__5735__auto___58253){
var seq__58189_58255__$1 = temp__5735__auto___58253;
if(cljs.core.chunked_seq_QMARK_(seq__58189_58255__$1)){
var c__4556__auto___58259 = cljs.core.chunk_first(seq__58189_58255__$1);
var G__58261 = cljs.core.chunk_rest(seq__58189_58255__$1);
var G__58262 = c__4556__auto___58259;
var G__58263 = cljs.core.count(c__4556__auto___58259);
var G__58264 = (0);
seq__58189_58225 = G__58261;
chunk__58190_58226 = G__58262;
count__58191_58227 = G__58263;
i__58192_58228 = G__58264;
continue;
} else {
var vec__58211_58268 = cljs.core.first(seq__58189_58255__$1);
var k_58269 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58211_58268,(0),null);
var cb_58270 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__58211_58268,(1),null);
try{var G__58216_58271 = cljs.core.deref(re_frame.trace.traces);
(cb_58270.cljs$core$IFn$_invoke$arity$1 ? cb_58270.cljs$core$IFn$_invoke$arity$1(G__58216_58271) : cb_58270.call(null,G__58216_58271));
}catch (e58215){var e_58272 = e58215;
re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Error thrown from trace cb",k_58269,"while storing",cljs.core.deref(re_frame.trace.traces),e_58272], 0));
}

var G__58279 = cljs.core.next(seq__58189_58255__$1);
var G__58280 = null;
var G__58281 = (0);
var G__58282 = (0);
seq__58189_58225 = G__58279;
chunk__58190_58226 = G__58280;
count__58191_58227 = G__58281;
i__58192_58228 = G__58282;
continue;
}
} else {
}
}
break;
}

return cljs.core.reset_BANG_(re_frame.trace.traces,cljs.core.PersistentVector.EMPTY);
}),re_frame.trace.debounce_time);
re_frame.trace.run_tracing_callbacks_BANG_ = (function re_frame$trace$run_tracing_callbacks_BANG_(now){
if(((cljs.core.deref(re_frame.trace.next_delivery) - (25)) < now)){
(re_frame.trace.schedule_debounce.cljs$core$IFn$_invoke$arity$0 ? re_frame.trace.schedule_debounce.cljs$core$IFn$_invoke$arity$0() : re_frame.trace.schedule_debounce.call(null));

return cljs.core.reset_BANG_(re_frame.trace.next_delivery,(now + re_frame.trace.debounce_time));
} else {
return null;
}
});

//# sourceMappingURL=re_frame.trace.js.map
