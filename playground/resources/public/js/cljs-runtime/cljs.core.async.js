goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__62066 = arguments.length;
switch (G__62066) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async62069 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async62069 = (function (f,blockable,meta62070){
this.f = f;
this.blockable = blockable;
this.meta62070 = meta62070;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async62069.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_62071,meta62070__$1){
var self__ = this;
var _62071__$1 = this;
return (new cljs.core.async.t_cljs$core$async62069(self__.f,self__.blockable,meta62070__$1));
}));

(cljs.core.async.t_cljs$core$async62069.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_62071){
var self__ = this;
var _62071__$1 = this;
return self__.meta62070;
}));

(cljs.core.async.t_cljs$core$async62069.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async62069.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async62069.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async62069.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async62069.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta62070","meta62070",1956863135,null)], null);
}));

(cljs.core.async.t_cljs$core$async62069.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async62069.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async62069");

(cljs.core.async.t_cljs$core$async62069.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async62069");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async62069.
 */
cljs.core.async.__GT_t_cljs$core$async62069 = (function cljs$core$async$__GT_t_cljs$core$async62069(f__$1,blockable__$1,meta62070){
return (new cljs.core.async.t_cljs$core$async62069(f__$1,blockable__$1,meta62070));
});

}

return (new cljs.core.async.t_cljs$core$async62069(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__62163 = arguments.length;
switch (G__62163) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__62186 = arguments.length;
switch (G__62186) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__62204 = arguments.length;
switch (G__62204) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_65178 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_65178) : fn1.call(null,val_65178));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_65178) : fn1.call(null,val_65178));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__62230 = arguments.length;
switch (G__62230) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5733__auto__)){
var ret = temp__5733__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5733__auto__)){
var retb = temp__5733__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4613__auto___65224 = n;
var x_65226 = (0);
while(true){
if((x_65226 < n__4613__auto___65224)){
(a[x_65226] = (0));

var G__65227 = (x_65226 + (1));
x_65226 = G__65227;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(i,n)){
return a;
} else {
var j = cljs.core.rand_int(i);
(a[i] = (a[j]));

(a[j] = i);

var G__65236 = (i + (1));
i = G__65236;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async62248 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async62248 = (function (flag,meta62249){
this.flag = flag;
this.meta62249 = meta62249;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async62248.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_62250,meta62249__$1){
var self__ = this;
var _62250__$1 = this;
return (new cljs.core.async.t_cljs$core$async62248(self__.flag,meta62249__$1));
}));

(cljs.core.async.t_cljs$core$async62248.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_62250){
var self__ = this;
var _62250__$1 = this;
return self__.meta62249;
}));

(cljs.core.async.t_cljs$core$async62248.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async62248.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async62248.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async62248.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async62248.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta62249","meta62249",1005433890,null)], null);
}));

(cljs.core.async.t_cljs$core$async62248.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async62248.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async62248");

(cljs.core.async.t_cljs$core$async62248.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async62248");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async62248.
 */
cljs.core.async.__GT_t_cljs$core$async62248 = (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async62248(flag__$1,meta62249){
return (new cljs.core.async.t_cljs$core$async62248(flag__$1,meta62249));
});

}

return (new cljs.core.async.t_cljs$core$async62248(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async62270 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async62270 = (function (flag,cb,meta62271){
this.flag = flag;
this.cb = cb;
this.meta62271 = meta62271;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async62270.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_62272,meta62271__$1){
var self__ = this;
var _62272__$1 = this;
return (new cljs.core.async.t_cljs$core$async62270(self__.flag,self__.cb,meta62271__$1));
}));

(cljs.core.async.t_cljs$core$async62270.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_62272){
var self__ = this;
var _62272__$1 = this;
return self__.meta62271;
}));

(cljs.core.async.t_cljs$core$async62270.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async62270.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async62270.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async62270.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async62270.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta62271","meta62271",-622146818,null)], null);
}));

(cljs.core.async.t_cljs$core$async62270.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async62270.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async62270");

(cljs.core.async.t_cljs$core$async62270.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async62270");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async62270.
 */
cljs.core.async.__GT_t_cljs$core$async62270 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async62270(flag__$1,cb__$1,meta62271){
return (new cljs.core.async.t_cljs$core$async62270(flag__$1,cb__$1,meta62271));
});

}

return (new cljs.core.async.t_cljs$core$async62270(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__62275_SHARP_){
var G__62277 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__62275_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__62277) : fret.call(null,G__62277));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__62276_SHARP_){
var G__62278 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__62276_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__62278) : fret.call(null,G__62278));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4126__auto__ = wport;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return port;
}
})()], null));
} else {
var G__65334 = (i + (1));
i = G__65334;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4126__auto__ = ret;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5735__auto__ = (function (){var and__4115__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4115__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4115__auto__;
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var got = temp__5735__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___65337 = arguments.length;
var i__4737__auto___65338 = (0);
while(true){
if((i__4737__auto___65338 < len__4736__auto___65337)){
args__4742__auto__.push((arguments[i__4737__auto___65338]));

var G__65339 = (i__4737__auto___65338 + (1));
i__4737__auto___65338 = G__65339;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__62293){
var map__62294 = p__62293;
var map__62294__$1 = (((((!((map__62294 == null))))?(((((map__62294.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__62294.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__62294):map__62294);
var opts = map__62294__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq62287){
var G__62288 = cljs.core.first(seq62287);
var seq62287__$1 = cljs.core.next(seq62287);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__62288,seq62287__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__62297 = arguments.length;
switch (G__62297) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__61983__auto___65354 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_62339){
var state_val_62340 = (state_62339[(1)]);
if((state_val_62340 === (7))){
var inst_62332 = (state_62339[(2)]);
var state_62339__$1 = state_62339;
var statearr_62342_65358 = state_62339__$1;
(statearr_62342_65358[(2)] = inst_62332);

(statearr_62342_65358[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (1))){
var state_62339__$1 = state_62339;
var statearr_62343_65360 = state_62339__$1;
(statearr_62343_65360[(2)] = null);

(statearr_62343_65360[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (4))){
var inst_62312 = (state_62339[(7)]);
var inst_62312__$1 = (state_62339[(2)]);
var inst_62313 = (inst_62312__$1 == null);
var state_62339__$1 = (function (){var statearr_62344 = state_62339;
(statearr_62344[(7)] = inst_62312__$1);

return statearr_62344;
})();
if(cljs.core.truth_(inst_62313)){
var statearr_62345_65372 = state_62339__$1;
(statearr_62345_65372[(1)] = (5));

} else {
var statearr_62346_65373 = state_62339__$1;
(statearr_62346_65373[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (13))){
var state_62339__$1 = state_62339;
var statearr_62347_65375 = state_62339__$1;
(statearr_62347_65375[(2)] = null);

(statearr_62347_65375[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (6))){
var inst_62312 = (state_62339[(7)]);
var state_62339__$1 = state_62339;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_62339__$1,(11),to,inst_62312);
} else {
if((state_val_62340 === (3))){
var inst_62334 = (state_62339[(2)]);
var state_62339__$1 = state_62339;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62339__$1,inst_62334);
} else {
if((state_val_62340 === (12))){
var state_62339__$1 = state_62339;
var statearr_62349_65382 = state_62339__$1;
(statearr_62349_65382[(2)] = null);

(statearr_62349_65382[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (2))){
var state_62339__$1 = state_62339;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62339__$1,(4),from);
} else {
if((state_val_62340 === (11))){
var inst_62324 = (state_62339[(2)]);
var state_62339__$1 = state_62339;
if(cljs.core.truth_(inst_62324)){
var statearr_62351_65391 = state_62339__$1;
(statearr_62351_65391[(1)] = (12));

} else {
var statearr_62352_65392 = state_62339__$1;
(statearr_62352_65392[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (9))){
var state_62339__$1 = state_62339;
var statearr_62353_65393 = state_62339__$1;
(statearr_62353_65393[(2)] = null);

(statearr_62353_65393[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (5))){
var state_62339__$1 = state_62339;
if(cljs.core.truth_(close_QMARK_)){
var statearr_62354_65394 = state_62339__$1;
(statearr_62354_65394[(1)] = (8));

} else {
var statearr_62355_65395 = state_62339__$1;
(statearr_62355_65395[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (14))){
var inst_62330 = (state_62339[(2)]);
var state_62339__$1 = state_62339;
var statearr_62356_65403 = state_62339__$1;
(statearr_62356_65403[(2)] = inst_62330);

(statearr_62356_65403[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (10))){
var inst_62321 = (state_62339[(2)]);
var state_62339__$1 = state_62339;
var statearr_62361_65407 = state_62339__$1;
(statearr_62361_65407[(2)] = inst_62321);

(statearr_62361_65407[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62340 === (8))){
var inst_62316 = cljs.core.async.close_BANG_(to);
var state_62339__$1 = state_62339;
var statearr_62365_65408 = state_62339__$1;
(statearr_62365_65408[(2)] = inst_62316);

(statearr_62365_65408[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_62366 = [null,null,null,null,null,null,null,null];
(statearr_62366[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_62366[(1)] = (1));

return statearr_62366;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_62339){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62339);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62369){if((e62369 instanceof Object)){
var ex__61709__auto__ = e62369;
var statearr_62370_65412 = state_62339;
(statearr_62370_65412[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62339);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62369;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65413 = state_62339;
state_62339 = G__65413;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_62339){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_62339);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_62371 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62371[(6)] = c__61983__auto___65354);

return statearr_62371;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = (function (p__62379){
var vec__62380 = p__62379;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62380,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62380,(1),null);
var job = vec__62380;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__61983__auto___65420 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_62387){
var state_val_62388 = (state_62387[(1)]);
if((state_val_62388 === (1))){
var state_62387__$1 = state_62387;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_62387__$1,(2),res,v);
} else {
if((state_val_62388 === (2))){
var inst_62384 = (state_62387[(2)]);
var inst_62385 = cljs.core.async.close_BANG_(res);
var state_62387__$1 = (function (){var statearr_62398 = state_62387;
(statearr_62398[(7)] = inst_62384);

return statearr_62398;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_62387__$1,inst_62385);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0 = (function (){
var statearr_62399 = [null,null,null,null,null,null,null,null];
(statearr_62399[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__);

(statearr_62399[(1)] = (1));

return statearr_62399;
});
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1 = (function (state_62387){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62387);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62400){if((e62400 instanceof Object)){
var ex__61709__auto__ = e62400;
var statearr_62401_65428 = state_62387;
(statearr_62401_65428[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62387);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62400;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65433 = state_62387;
state_62387 = G__65433;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = function(state_62387){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1.call(this,state_62387);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_62402 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62402[(6)] = c__61983__auto___65420);

return statearr_62402;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__62403){
var vec__62404 = p__62403;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62404,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__62404,(1),null);
var job = vec__62404;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__4613__auto___65449 = n;
var __65450 = (0);
while(true){
if((__65450 < n__4613__auto___65449)){
var G__62407_65451 = type;
var G__62407_65452__$1 = (((G__62407_65451 instanceof cljs.core.Keyword))?G__62407_65451.fqn:null);
switch (G__62407_65452__$1) {
case "compute":
var c__61983__auto___65457 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__65450,c__61983__auto___65457,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async){
return (function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = ((function (__65450,c__61983__auto___65457,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async){
return (function (state_62421){
var state_val_62422 = (state_62421[(1)]);
if((state_val_62422 === (1))){
var state_62421__$1 = state_62421;
var statearr_62426_65464 = state_62421__$1;
(statearr_62426_65464[(2)] = null);

(statearr_62426_65464[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62422 === (2))){
var state_62421__$1 = state_62421;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62421__$1,(4),jobs);
} else {
if((state_val_62422 === (3))){
var inst_62419 = (state_62421[(2)]);
var state_62421__$1 = state_62421;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62421__$1,inst_62419);
} else {
if((state_val_62422 === (4))){
var inst_62411 = (state_62421[(2)]);
var inst_62412 = process(inst_62411);
var state_62421__$1 = state_62421;
if(cljs.core.truth_(inst_62412)){
var statearr_62427_65483 = state_62421__$1;
(statearr_62427_65483[(1)] = (5));

} else {
var statearr_62428_65491 = state_62421__$1;
(statearr_62428_65491[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62422 === (5))){
var state_62421__$1 = state_62421;
var statearr_62429_65493 = state_62421__$1;
(statearr_62429_65493[(2)] = null);

(statearr_62429_65493[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62422 === (6))){
var state_62421__$1 = state_62421;
var statearr_62430_65496 = state_62421__$1;
(statearr_62430_65496[(2)] = null);

(statearr_62430_65496[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62422 === (7))){
var inst_62417 = (state_62421[(2)]);
var state_62421__$1 = state_62421;
var statearr_62431_65503 = state_62421__$1;
(statearr_62431_65503[(2)] = inst_62417);

(statearr_62431_65503[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__65450,c__61983__auto___65457,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async))
;
return ((function (__65450,switch__61705__auto__,c__61983__auto___65457,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0 = (function (){
var statearr_62433 = [null,null,null,null,null,null,null];
(statearr_62433[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__);

(statearr_62433[(1)] = (1));

return statearr_62433;
});
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1 = (function (state_62421){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62421);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62435){if((e62435 instanceof Object)){
var ex__61709__auto__ = e62435;
var statearr_62436_65516 = state_62421;
(statearr_62436_65516[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62421);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62435;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65526 = state_62421;
state_62421 = G__65526;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = function(state_62421){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1.call(this,state_62421);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__;
})()
;})(__65450,switch__61705__auto__,c__61983__auto___65457,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async))
})();
var state__61985__auto__ = (function (){var statearr_62437 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62437[(6)] = c__61983__auto___65457);

return statearr_62437;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
});})(__65450,c__61983__auto___65457,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async))
);


break;
case "async":
var c__61983__auto___65528 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__65450,c__61983__auto___65528,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async){
return (function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = ((function (__65450,c__61983__auto___65528,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async){
return (function (state_62451){
var state_val_62452 = (state_62451[(1)]);
if((state_val_62452 === (1))){
var state_62451__$1 = state_62451;
var statearr_62453_65530 = state_62451__$1;
(statearr_62453_65530[(2)] = null);

(statearr_62453_65530[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62452 === (2))){
var state_62451__$1 = state_62451;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62451__$1,(4),jobs);
} else {
if((state_val_62452 === (3))){
var inst_62449 = (state_62451[(2)]);
var state_62451__$1 = state_62451;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62451__$1,inst_62449);
} else {
if((state_val_62452 === (4))){
var inst_62440 = (state_62451[(2)]);
var inst_62441 = async(inst_62440);
var state_62451__$1 = state_62451;
if(cljs.core.truth_(inst_62441)){
var statearr_62455_65533 = state_62451__$1;
(statearr_62455_65533[(1)] = (5));

} else {
var statearr_62456_65534 = state_62451__$1;
(statearr_62456_65534[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62452 === (5))){
var state_62451__$1 = state_62451;
var statearr_62457_65535 = state_62451__$1;
(statearr_62457_65535[(2)] = null);

(statearr_62457_65535[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62452 === (6))){
var state_62451__$1 = state_62451;
var statearr_62458_65537 = state_62451__$1;
(statearr_62458_65537[(2)] = null);

(statearr_62458_65537[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62452 === (7))){
var inst_62446 = (state_62451[(2)]);
var state_62451__$1 = state_62451;
var statearr_62459_65538 = state_62451__$1;
(statearr_62459_65538[(2)] = inst_62446);

(statearr_62459_65538[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__65450,c__61983__auto___65528,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async))
;
return ((function (__65450,switch__61705__auto__,c__61983__auto___65528,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0 = (function (){
var statearr_62461 = [null,null,null,null,null,null,null];
(statearr_62461[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__);

(statearr_62461[(1)] = (1));

return statearr_62461;
});
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1 = (function (state_62451){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62451);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62462){if((e62462 instanceof Object)){
var ex__61709__auto__ = e62462;
var statearr_62463_65543 = state_62451;
(statearr_62463_65543[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62451);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62462;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65544 = state_62451;
state_62451 = G__65544;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = function(state_62451){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1.call(this,state_62451);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__;
})()
;})(__65450,switch__61705__auto__,c__61983__auto___65528,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async))
})();
var state__61985__auto__ = (function (){var statearr_62464 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62464[(6)] = c__61983__auto___65528);

return statearr_62464;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
});})(__65450,c__61983__auto___65528,G__62407_65451,G__62407_65452__$1,n__4613__auto___65449,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__62407_65452__$1)].join('')));

}

var G__65545 = (__65450 + (1));
__65450 = G__65545;
continue;
} else {
}
break;
}

var c__61983__auto___65546 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_62489){
var state_val_62491 = (state_62489[(1)]);
if((state_val_62491 === (7))){
var inst_62485 = (state_62489[(2)]);
var state_62489__$1 = state_62489;
var statearr_62492_65547 = state_62489__$1;
(statearr_62492_65547[(2)] = inst_62485);

(statearr_62492_65547[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62491 === (1))){
var state_62489__$1 = state_62489;
var statearr_62493_65548 = state_62489__$1;
(statearr_62493_65548[(2)] = null);

(statearr_62493_65548[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62491 === (4))){
var inst_62468 = (state_62489[(7)]);
var inst_62468__$1 = (state_62489[(2)]);
var inst_62469 = (inst_62468__$1 == null);
var state_62489__$1 = (function (){var statearr_62494 = state_62489;
(statearr_62494[(7)] = inst_62468__$1);

return statearr_62494;
})();
if(cljs.core.truth_(inst_62469)){
var statearr_62497_65550 = state_62489__$1;
(statearr_62497_65550[(1)] = (5));

} else {
var statearr_62498_65552 = state_62489__$1;
(statearr_62498_65552[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62491 === (6))){
var inst_62468 = (state_62489[(7)]);
var inst_62473 = (state_62489[(8)]);
var inst_62473__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_62475 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_62477 = [inst_62468,inst_62473__$1];
var inst_62478 = (new cljs.core.PersistentVector(null,2,(5),inst_62475,inst_62477,null));
var state_62489__$1 = (function (){var statearr_62499 = state_62489;
(statearr_62499[(8)] = inst_62473__$1);

return statearr_62499;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_62489__$1,(8),jobs,inst_62478);
} else {
if((state_val_62491 === (3))){
var inst_62487 = (state_62489[(2)]);
var state_62489__$1 = state_62489;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62489__$1,inst_62487);
} else {
if((state_val_62491 === (2))){
var state_62489__$1 = state_62489;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62489__$1,(4),from);
} else {
if((state_val_62491 === (9))){
var inst_62482 = (state_62489[(2)]);
var state_62489__$1 = (function (){var statearr_62500 = state_62489;
(statearr_62500[(9)] = inst_62482);

return statearr_62500;
})();
var statearr_62501_65560 = state_62489__$1;
(statearr_62501_65560[(2)] = null);

(statearr_62501_65560[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62491 === (5))){
var inst_62471 = cljs.core.async.close_BANG_(jobs);
var state_62489__$1 = state_62489;
var statearr_62505_65570 = state_62489__$1;
(statearr_62505_65570[(2)] = inst_62471);

(statearr_62505_65570[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62491 === (8))){
var inst_62473 = (state_62489[(8)]);
var inst_62480 = (state_62489[(2)]);
var state_62489__$1 = (function (){var statearr_62506 = state_62489;
(statearr_62506[(10)] = inst_62480);

return statearr_62506;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_62489__$1,(9),results,inst_62473);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0 = (function (){
var statearr_62508 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_62508[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__);

(statearr_62508[(1)] = (1));

return statearr_62508;
});
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1 = (function (state_62489){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62489);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62509){if((e62509 instanceof Object)){
var ex__61709__auto__ = e62509;
var statearr_62510_65576 = state_62489;
(statearr_62510_65576[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62489);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62509;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65577 = state_62489;
state_62489 = G__65577;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = function(state_62489){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1.call(this,state_62489);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_62511 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62511[(6)] = c__61983__auto___65546);

return statearr_62511;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


var c__61983__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_62555){
var state_val_62556 = (state_62555[(1)]);
if((state_val_62556 === (7))){
var inst_62551 = (state_62555[(2)]);
var state_62555__$1 = state_62555;
var statearr_62562_65578 = state_62555__$1;
(statearr_62562_65578[(2)] = inst_62551);

(statearr_62562_65578[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (20))){
var state_62555__$1 = state_62555;
var statearr_62565_65580 = state_62555__$1;
(statearr_62565_65580[(2)] = null);

(statearr_62565_65580[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (1))){
var state_62555__$1 = state_62555;
var statearr_62567_65584 = state_62555__$1;
(statearr_62567_65584[(2)] = null);

(statearr_62567_65584[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (4))){
var inst_62515 = (state_62555[(7)]);
var inst_62515__$1 = (state_62555[(2)]);
var inst_62516 = (inst_62515__$1 == null);
var state_62555__$1 = (function (){var statearr_62568 = state_62555;
(statearr_62568[(7)] = inst_62515__$1);

return statearr_62568;
})();
if(cljs.core.truth_(inst_62516)){
var statearr_62569_65587 = state_62555__$1;
(statearr_62569_65587[(1)] = (5));

} else {
var statearr_62570_65588 = state_62555__$1;
(statearr_62570_65588[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (15))){
var inst_62531 = (state_62555[(8)]);
var state_62555__$1 = state_62555;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_62555__$1,(18),to,inst_62531);
} else {
if((state_val_62556 === (21))){
var inst_62546 = (state_62555[(2)]);
var state_62555__$1 = state_62555;
var statearr_62571_65591 = state_62555__$1;
(statearr_62571_65591[(2)] = inst_62546);

(statearr_62571_65591[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (13))){
var inst_62548 = (state_62555[(2)]);
var state_62555__$1 = (function (){var statearr_62572 = state_62555;
(statearr_62572[(9)] = inst_62548);

return statearr_62572;
})();
var statearr_62574_65595 = state_62555__$1;
(statearr_62574_65595[(2)] = null);

(statearr_62574_65595[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (6))){
var inst_62515 = (state_62555[(7)]);
var state_62555__$1 = state_62555;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62555__$1,(11),inst_62515);
} else {
if((state_val_62556 === (17))){
var inst_62540 = (state_62555[(2)]);
var state_62555__$1 = state_62555;
if(cljs.core.truth_(inst_62540)){
var statearr_62580_65606 = state_62555__$1;
(statearr_62580_65606[(1)] = (19));

} else {
var statearr_62581_65607 = state_62555__$1;
(statearr_62581_65607[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (3))){
var inst_62553 = (state_62555[(2)]);
var state_62555__$1 = state_62555;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62555__$1,inst_62553);
} else {
if((state_val_62556 === (12))){
var inst_62526 = (state_62555[(10)]);
var state_62555__$1 = state_62555;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62555__$1,(14),inst_62526);
} else {
if((state_val_62556 === (2))){
var state_62555__$1 = state_62555;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62555__$1,(4),results);
} else {
if((state_val_62556 === (19))){
var state_62555__$1 = state_62555;
var statearr_62583_65608 = state_62555__$1;
(statearr_62583_65608[(2)] = null);

(statearr_62583_65608[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (11))){
var inst_62526 = (state_62555[(2)]);
var state_62555__$1 = (function (){var statearr_62588 = state_62555;
(statearr_62588[(10)] = inst_62526);

return statearr_62588;
})();
var statearr_62589_65611 = state_62555__$1;
(statearr_62589_65611[(2)] = null);

(statearr_62589_65611[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (9))){
var state_62555__$1 = state_62555;
var statearr_62590_65612 = state_62555__$1;
(statearr_62590_65612[(2)] = null);

(statearr_62590_65612[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (5))){
var state_62555__$1 = state_62555;
if(cljs.core.truth_(close_QMARK_)){
var statearr_62591_65613 = state_62555__$1;
(statearr_62591_65613[(1)] = (8));

} else {
var statearr_62592_65614 = state_62555__$1;
(statearr_62592_65614[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (14))){
var inst_62531 = (state_62555[(8)]);
var inst_62531__$1 = (state_62555[(2)]);
var inst_62532 = (inst_62531__$1 == null);
var inst_62533 = cljs.core.not(inst_62532);
var state_62555__$1 = (function (){var statearr_62599 = state_62555;
(statearr_62599[(8)] = inst_62531__$1);

return statearr_62599;
})();
if(inst_62533){
var statearr_62603_65617 = state_62555__$1;
(statearr_62603_65617[(1)] = (15));

} else {
var statearr_62604_65618 = state_62555__$1;
(statearr_62604_65618[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (16))){
var state_62555__$1 = state_62555;
var statearr_62606_65619 = state_62555__$1;
(statearr_62606_65619[(2)] = false);

(statearr_62606_65619[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (10))){
var inst_62523 = (state_62555[(2)]);
var state_62555__$1 = state_62555;
var statearr_62607_65629 = state_62555__$1;
(statearr_62607_65629[(2)] = inst_62523);

(statearr_62607_65629[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (18))){
var inst_62537 = (state_62555[(2)]);
var state_62555__$1 = state_62555;
var statearr_62610_65630 = state_62555__$1;
(statearr_62610_65630[(2)] = inst_62537);

(statearr_62610_65630[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62556 === (8))){
var inst_62519 = cljs.core.async.close_BANG_(to);
var state_62555__$1 = state_62555;
var statearr_62617_65631 = state_62555__$1;
(statearr_62617_65631[(2)] = inst_62519);

(statearr_62617_65631[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0 = (function (){
var statearr_62625 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_62625[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__);

(statearr_62625[(1)] = (1));

return statearr_62625;
});
var cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1 = (function (state_62555){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62555);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62626){if((e62626 instanceof Object)){
var ex__61709__auto__ = e62626;
var statearr_62628_65632 = state_62555;
(statearr_62628_65632[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62555);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62626;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65633 = state_62555;
state_62555 = G__65633;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__ = function(state_62555){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1.call(this,state_62555);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__61706__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_62633 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62633[(6)] = c__61983__auto__);

return statearr_62633;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));

return c__61983__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__62640 = arguments.length;
switch (G__62640) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__62657 = arguments.length;
switch (G__62657) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__62678 = arguments.length;
switch (G__62678) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__61983__auto___65652 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_62715){
var state_val_62716 = (state_62715[(1)]);
if((state_val_62716 === (7))){
var inst_62711 = (state_62715[(2)]);
var state_62715__$1 = state_62715;
var statearr_62719_65653 = state_62715__$1;
(statearr_62719_65653[(2)] = inst_62711);

(statearr_62719_65653[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (1))){
var state_62715__$1 = state_62715;
var statearr_62720_65654 = state_62715__$1;
(statearr_62720_65654[(2)] = null);

(statearr_62720_65654[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (4))){
var inst_62692 = (state_62715[(7)]);
var inst_62692__$1 = (state_62715[(2)]);
var inst_62693 = (inst_62692__$1 == null);
var state_62715__$1 = (function (){var statearr_62721 = state_62715;
(statearr_62721[(7)] = inst_62692__$1);

return statearr_62721;
})();
if(cljs.core.truth_(inst_62693)){
var statearr_62722_65656 = state_62715__$1;
(statearr_62722_65656[(1)] = (5));

} else {
var statearr_62723_65658 = state_62715__$1;
(statearr_62723_65658[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (13))){
var state_62715__$1 = state_62715;
var statearr_62724_65659 = state_62715__$1;
(statearr_62724_65659[(2)] = null);

(statearr_62724_65659[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (6))){
var inst_62692 = (state_62715[(7)]);
var inst_62698 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_62692) : p.call(null,inst_62692));
var state_62715__$1 = state_62715;
if(cljs.core.truth_(inst_62698)){
var statearr_62728_65663 = state_62715__$1;
(statearr_62728_65663[(1)] = (9));

} else {
var statearr_62731_65664 = state_62715__$1;
(statearr_62731_65664[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (3))){
var inst_62713 = (state_62715[(2)]);
var state_62715__$1 = state_62715;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62715__$1,inst_62713);
} else {
if((state_val_62716 === (12))){
var state_62715__$1 = state_62715;
var statearr_62737_65665 = state_62715__$1;
(statearr_62737_65665[(2)] = null);

(statearr_62737_65665[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (2))){
var state_62715__$1 = state_62715;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62715__$1,(4),ch);
} else {
if((state_val_62716 === (11))){
var inst_62692 = (state_62715[(7)]);
var inst_62702 = (state_62715[(2)]);
var state_62715__$1 = state_62715;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_62715__$1,(8),inst_62702,inst_62692);
} else {
if((state_val_62716 === (9))){
var state_62715__$1 = state_62715;
var statearr_62745_65666 = state_62715__$1;
(statearr_62745_65666[(2)] = tc);

(statearr_62745_65666[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (5))){
var inst_62695 = cljs.core.async.close_BANG_(tc);
var inst_62696 = cljs.core.async.close_BANG_(fc);
var state_62715__$1 = (function (){var statearr_62750 = state_62715;
(statearr_62750[(8)] = inst_62695);

return statearr_62750;
})();
var statearr_62755_65667 = state_62715__$1;
(statearr_62755_65667[(2)] = inst_62696);

(statearr_62755_65667[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (14))){
var inst_62709 = (state_62715[(2)]);
var state_62715__$1 = state_62715;
var statearr_62760_65668 = state_62715__$1;
(statearr_62760_65668[(2)] = inst_62709);

(statearr_62760_65668[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (10))){
var state_62715__$1 = state_62715;
var statearr_62761_65669 = state_62715__$1;
(statearr_62761_65669[(2)] = fc);

(statearr_62761_65669[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62716 === (8))){
var inst_62704 = (state_62715[(2)]);
var state_62715__$1 = state_62715;
if(cljs.core.truth_(inst_62704)){
var statearr_62764_65670 = state_62715__$1;
(statearr_62764_65670[(1)] = (12));

} else {
var statearr_62765_65671 = state_62715__$1;
(statearr_62765_65671[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_62766 = [null,null,null,null,null,null,null,null,null];
(statearr_62766[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_62766[(1)] = (1));

return statearr_62766;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_62715){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62715);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62767){if((e62767 instanceof Object)){
var ex__61709__auto__ = e62767;
var statearr_62771_65673 = state_62715;
(statearr_62771_65673[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62715);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62767;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65674 = state_62715;
state_62715 = G__65674;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_62715){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_62715);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_62772 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62772[(6)] = c__61983__auto___65652);

return statearr_62772;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__61983__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_62793){
var state_val_62794 = (state_62793[(1)]);
if((state_val_62794 === (7))){
var inst_62789 = (state_62793[(2)]);
var state_62793__$1 = state_62793;
var statearr_62796_65680 = state_62793__$1;
(statearr_62796_65680[(2)] = inst_62789);

(statearr_62796_65680[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62794 === (1))){
var inst_62773 = init;
var state_62793__$1 = (function (){var statearr_62797 = state_62793;
(statearr_62797[(7)] = inst_62773);

return statearr_62797;
})();
var statearr_62798_65681 = state_62793__$1;
(statearr_62798_65681[(2)] = null);

(statearr_62798_65681[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62794 === (4))){
var inst_62776 = (state_62793[(8)]);
var inst_62776__$1 = (state_62793[(2)]);
var inst_62777 = (inst_62776__$1 == null);
var state_62793__$1 = (function (){var statearr_62799 = state_62793;
(statearr_62799[(8)] = inst_62776__$1);

return statearr_62799;
})();
if(cljs.core.truth_(inst_62777)){
var statearr_62800_65682 = state_62793__$1;
(statearr_62800_65682[(1)] = (5));

} else {
var statearr_62801_65683 = state_62793__$1;
(statearr_62801_65683[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62794 === (6))){
var inst_62773 = (state_62793[(7)]);
var inst_62776 = (state_62793[(8)]);
var inst_62780 = (state_62793[(9)]);
var inst_62780__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_62773,inst_62776) : f.call(null,inst_62773,inst_62776));
var inst_62781 = cljs.core.reduced_QMARK_(inst_62780__$1);
var state_62793__$1 = (function (){var statearr_62802 = state_62793;
(statearr_62802[(9)] = inst_62780__$1);

return statearr_62802;
})();
if(inst_62781){
var statearr_62803_65690 = state_62793__$1;
(statearr_62803_65690[(1)] = (8));

} else {
var statearr_62804_65691 = state_62793__$1;
(statearr_62804_65691[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62794 === (3))){
var inst_62791 = (state_62793[(2)]);
var state_62793__$1 = state_62793;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62793__$1,inst_62791);
} else {
if((state_val_62794 === (2))){
var state_62793__$1 = state_62793;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62793__$1,(4),ch);
} else {
if((state_val_62794 === (9))){
var inst_62780 = (state_62793[(9)]);
var inst_62773 = inst_62780;
var state_62793__$1 = (function (){var statearr_62805 = state_62793;
(statearr_62805[(7)] = inst_62773);

return statearr_62805;
})();
var statearr_62806_65694 = state_62793__$1;
(statearr_62806_65694[(2)] = null);

(statearr_62806_65694[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62794 === (5))){
var inst_62773 = (state_62793[(7)]);
var state_62793__$1 = state_62793;
var statearr_62807_65700 = state_62793__$1;
(statearr_62807_65700[(2)] = inst_62773);

(statearr_62807_65700[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62794 === (10))){
var inst_62787 = (state_62793[(2)]);
var state_62793__$1 = state_62793;
var statearr_62808_65703 = state_62793__$1;
(statearr_62808_65703[(2)] = inst_62787);

(statearr_62808_65703[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62794 === (8))){
var inst_62780 = (state_62793[(9)]);
var inst_62783 = cljs.core.deref(inst_62780);
var state_62793__$1 = state_62793;
var statearr_62813_65705 = state_62793__$1;
(statearr_62813_65705[(2)] = inst_62783);

(statearr_62813_65705[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__61706__auto__ = null;
var cljs$core$async$reduce_$_state_machine__61706__auto____0 = (function (){
var statearr_62818 = [null,null,null,null,null,null,null,null,null,null];
(statearr_62818[(0)] = cljs$core$async$reduce_$_state_machine__61706__auto__);

(statearr_62818[(1)] = (1));

return statearr_62818;
});
var cljs$core$async$reduce_$_state_machine__61706__auto____1 = (function (state_62793){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62793);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62819){if((e62819 instanceof Object)){
var ex__61709__auto__ = e62819;
var statearr_62823_65707 = state_62793;
(statearr_62823_65707[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62793);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62819;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65709 = state_62793;
state_62793 = G__65709;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__61706__auto__ = function(state_62793){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__61706__auto____1.call(this,state_62793);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__61706__auto____0;
cljs$core$async$reduce_$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__61706__auto____1;
return cljs$core$async$reduce_$_state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_62829 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62829[(6)] = c__61983__auto__);

return statearr_62829;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));

return c__61983__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__61983__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_62838){
var state_val_62839 = (state_62838[(1)]);
if((state_val_62839 === (1))){
var inst_62832 = cljs.core.async.reduce(f__$1,init,ch);
var state_62838__$1 = state_62838;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_62838__$1,(2),inst_62832);
} else {
if((state_val_62839 === (2))){
var inst_62834 = (state_62838[(2)]);
var inst_62835 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_62834) : f__$1.call(null,inst_62834));
var state_62838__$1 = state_62838;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62838__$1,inst_62835);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__61706__auto__ = null;
var cljs$core$async$transduce_$_state_machine__61706__auto____0 = (function (){
var statearr_62843 = [null,null,null,null,null,null,null];
(statearr_62843[(0)] = cljs$core$async$transduce_$_state_machine__61706__auto__);

(statearr_62843[(1)] = (1));

return statearr_62843;
});
var cljs$core$async$transduce_$_state_machine__61706__auto____1 = (function (state_62838){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62838);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62846){if((e62846 instanceof Object)){
var ex__61709__auto__ = e62846;
var statearr_62848_65716 = state_62838;
(statearr_62848_65716[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62838);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62846;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65717 = state_62838;
state_62838 = G__65717;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__61706__auto__ = function(state_62838){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__61706__auto____1.call(this,state_62838);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__61706__auto____0;
cljs$core$async$transduce_$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__61706__auto____1;
return cljs$core$async$transduce_$_state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_62849 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62849[(6)] = c__61983__auto__);

return statearr_62849;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));

return c__61983__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__62857 = arguments.length;
switch (G__62857) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__61983__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_62885){
var state_val_62886 = (state_62885[(1)]);
if((state_val_62886 === (7))){
var inst_62867 = (state_62885[(2)]);
var state_62885__$1 = state_62885;
var statearr_62889_65723 = state_62885__$1;
(statearr_62889_65723[(2)] = inst_62867);

(statearr_62889_65723[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (1))){
var inst_62859 = cljs.core.seq(coll);
var inst_62860 = inst_62859;
var state_62885__$1 = (function (){var statearr_62892 = state_62885;
(statearr_62892[(7)] = inst_62860);

return statearr_62892;
})();
var statearr_62894_65725 = state_62885__$1;
(statearr_62894_65725[(2)] = null);

(statearr_62894_65725[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (4))){
var inst_62860 = (state_62885[(7)]);
var inst_62865 = cljs.core.first(inst_62860);
var state_62885__$1 = state_62885;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_62885__$1,(7),ch,inst_62865);
} else {
if((state_val_62886 === (13))){
var inst_62879 = (state_62885[(2)]);
var state_62885__$1 = state_62885;
var statearr_62895_65726 = state_62885__$1;
(statearr_62895_65726[(2)] = inst_62879);

(statearr_62895_65726[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (6))){
var inst_62870 = (state_62885[(2)]);
var state_62885__$1 = state_62885;
if(cljs.core.truth_(inst_62870)){
var statearr_62896_65727 = state_62885__$1;
(statearr_62896_65727[(1)] = (8));

} else {
var statearr_62899_65731 = state_62885__$1;
(statearr_62899_65731[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (3))){
var inst_62883 = (state_62885[(2)]);
var state_62885__$1 = state_62885;
return cljs.core.async.impl.ioc_helpers.return_chan(state_62885__$1,inst_62883);
} else {
if((state_val_62886 === (12))){
var state_62885__$1 = state_62885;
var statearr_62902_65735 = state_62885__$1;
(statearr_62902_65735[(2)] = null);

(statearr_62902_65735[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (2))){
var inst_62860 = (state_62885[(7)]);
var state_62885__$1 = state_62885;
if(cljs.core.truth_(inst_62860)){
var statearr_62907_65737 = state_62885__$1;
(statearr_62907_65737[(1)] = (4));

} else {
var statearr_62908_65738 = state_62885__$1;
(statearr_62908_65738[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (11))){
var inst_62876 = cljs.core.async.close_BANG_(ch);
var state_62885__$1 = state_62885;
var statearr_62912_65739 = state_62885__$1;
(statearr_62912_65739[(2)] = inst_62876);

(statearr_62912_65739[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (9))){
var state_62885__$1 = state_62885;
if(cljs.core.truth_(close_QMARK_)){
var statearr_62916_65740 = state_62885__$1;
(statearr_62916_65740[(1)] = (11));

} else {
var statearr_62917_65741 = state_62885__$1;
(statearr_62917_65741[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (5))){
var inst_62860 = (state_62885[(7)]);
var state_62885__$1 = state_62885;
var statearr_62920_65743 = state_62885__$1;
(statearr_62920_65743[(2)] = inst_62860);

(statearr_62920_65743[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (10))){
var inst_62881 = (state_62885[(2)]);
var state_62885__$1 = state_62885;
var statearr_62924_65745 = state_62885__$1;
(statearr_62924_65745[(2)] = inst_62881);

(statearr_62924_65745[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_62886 === (8))){
var inst_62860 = (state_62885[(7)]);
var inst_62872 = cljs.core.next(inst_62860);
var inst_62860__$1 = inst_62872;
var state_62885__$1 = (function (){var statearr_62925 = state_62885;
(statearr_62925[(7)] = inst_62860__$1);

return statearr_62925;
})();
var statearr_62926_65746 = state_62885__$1;
(statearr_62926_65746[(2)] = null);

(statearr_62926_65746[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_62929 = [null,null,null,null,null,null,null,null];
(statearr_62929[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_62929[(1)] = (1));

return statearr_62929;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_62885){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_62885);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e62930){if((e62930 instanceof Object)){
var ex__61709__auto__ = e62930;
var statearr_62931_65749 = state_62885;
(statearr_62931_65749[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_62885);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e62930;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65751 = state_62885;
state_62885 = G__65751;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_62885){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_62885);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_62932 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_62932[(6)] = c__61983__auto__);

return statearr_62932;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));

return c__61983__auto__;
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_65756 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_65756(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_65760 = (function (m,ch,close_QMARK_){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4429__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4426__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_65760(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_65762 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_65762(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_65771 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_65771(m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async62956 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async62956 = (function (ch,cs,meta62957){
this.ch = ch;
this.cs = cs;
this.meta62957 = meta62957;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async62956.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_62958,meta62957__$1){
var self__ = this;
var _62958__$1 = this;
return (new cljs.core.async.t_cljs$core$async62956(self__.ch,self__.cs,meta62957__$1));
}));

(cljs.core.async.t_cljs$core$async62956.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_62958){
var self__ = this;
var _62958__$1 = this;
return self__.meta62957;
}));

(cljs.core.async.t_cljs$core$async62956.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async62956.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async62956.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async62956.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async62956.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async62956.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async62956.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta62957","meta62957",97247696,null)], null);
}));

(cljs.core.async.t_cljs$core$async62956.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async62956.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async62956");

(cljs.core.async.t_cljs$core$async62956.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async62956");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async62956.
 */
cljs.core.async.__GT_t_cljs$core$async62956 = (function cljs$core$async$mult_$___GT_t_cljs$core$async62956(ch__$1,cs__$1,meta62957){
return (new cljs.core.async.t_cljs$core$async62956(ch__$1,cs__$1,meta62957));
});

}

return (new cljs.core.async.t_cljs$core$async62956(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__61983__auto___65793 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_63108){
var state_val_63113 = (state_63108[(1)]);
if((state_val_63113 === (7))){
var inst_63097 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63124_65794 = state_63108__$1;
(statearr_63124_65794[(2)] = inst_63097);

(statearr_63124_65794[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (20))){
var inst_62997 = (state_63108[(7)]);
var inst_63012 = cljs.core.first(inst_62997);
var inst_63013 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_63012,(0),null);
var inst_63014 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_63012,(1),null);
var state_63108__$1 = (function (){var statearr_63125 = state_63108;
(statearr_63125[(8)] = inst_63013);

return statearr_63125;
})();
if(cljs.core.truth_(inst_63014)){
var statearr_63126_65795 = state_63108__$1;
(statearr_63126_65795[(1)] = (22));

} else {
var statearr_63131_65796 = state_63108__$1;
(statearr_63131_65796[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (27))){
var inst_63044 = (state_63108[(9)]);
var inst_63042 = (state_63108[(10)]);
var inst_63049 = (state_63108[(11)]);
var inst_62965 = (state_63108[(12)]);
var inst_63049__$1 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_63042,inst_63044);
var inst_63050 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_63049__$1,inst_62965,done);
var state_63108__$1 = (function (){var statearr_63136 = state_63108;
(statearr_63136[(11)] = inst_63049__$1);

return statearr_63136;
})();
if(cljs.core.truth_(inst_63050)){
var statearr_63137_65799 = state_63108__$1;
(statearr_63137_65799[(1)] = (30));

} else {
var statearr_63138_65800 = state_63108__$1;
(statearr_63138_65800[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (1))){
var state_63108__$1 = state_63108;
var statearr_63139_65801 = state_63108__$1;
(statearr_63139_65801[(2)] = null);

(statearr_63139_65801[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (24))){
var inst_62997 = (state_63108[(7)]);
var inst_63019 = (state_63108[(2)]);
var inst_63020 = cljs.core.next(inst_62997);
var inst_62974 = inst_63020;
var inst_62975 = null;
var inst_62976 = (0);
var inst_62977 = (0);
var state_63108__$1 = (function (){var statearr_63140 = state_63108;
(statearr_63140[(13)] = inst_62976);

(statearr_63140[(14)] = inst_62977);

(statearr_63140[(15)] = inst_62975);

(statearr_63140[(16)] = inst_62974);

(statearr_63140[(17)] = inst_63019);

return statearr_63140;
})();
var statearr_63141_65807 = state_63108__$1;
(statearr_63141_65807[(2)] = null);

(statearr_63141_65807[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (39))){
var state_63108__$1 = state_63108;
var statearr_63150_65808 = state_63108__$1;
(statearr_63150_65808[(2)] = null);

(statearr_63150_65808[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (4))){
var inst_62965 = (state_63108[(12)]);
var inst_62965__$1 = (state_63108[(2)]);
var inst_62966 = (inst_62965__$1 == null);
var state_63108__$1 = (function (){var statearr_63151 = state_63108;
(statearr_63151[(12)] = inst_62965__$1);

return statearr_63151;
})();
if(cljs.core.truth_(inst_62966)){
var statearr_63152_65818 = state_63108__$1;
(statearr_63152_65818[(1)] = (5));

} else {
var statearr_63153_65819 = state_63108__$1;
(statearr_63153_65819[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (15))){
var inst_62976 = (state_63108[(13)]);
var inst_62977 = (state_63108[(14)]);
var inst_62975 = (state_63108[(15)]);
var inst_62974 = (state_63108[(16)]);
var inst_62993 = (state_63108[(2)]);
var inst_62994 = (inst_62977 + (1));
var tmp63147 = inst_62976;
var tmp63148 = inst_62975;
var tmp63149 = inst_62974;
var inst_62974__$1 = tmp63149;
var inst_62975__$1 = tmp63148;
var inst_62976__$1 = tmp63147;
var inst_62977__$1 = inst_62994;
var state_63108__$1 = (function (){var statearr_63154 = state_63108;
(statearr_63154[(13)] = inst_62976__$1);

(statearr_63154[(14)] = inst_62977__$1);

(statearr_63154[(15)] = inst_62975__$1);

(statearr_63154[(18)] = inst_62993);

(statearr_63154[(16)] = inst_62974__$1);

return statearr_63154;
})();
var statearr_63155_65824 = state_63108__$1;
(statearr_63155_65824[(2)] = null);

(statearr_63155_65824[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (21))){
var inst_63023 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63159_65825 = state_63108__$1;
(statearr_63159_65825[(2)] = inst_63023);

(statearr_63159_65825[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (31))){
var inst_63049 = (state_63108[(11)]);
var inst_63053 = done(null);
var inst_63054 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_63049);
var state_63108__$1 = (function (){var statearr_63161 = state_63108;
(statearr_63161[(19)] = inst_63053);

return statearr_63161;
})();
var statearr_63162_65827 = state_63108__$1;
(statearr_63162_65827[(2)] = inst_63054);

(statearr_63162_65827[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (32))){
var inst_63044 = (state_63108[(9)]);
var inst_63043 = (state_63108[(20)]);
var inst_63041 = (state_63108[(21)]);
var inst_63042 = (state_63108[(10)]);
var inst_63056 = (state_63108[(2)]);
var inst_63057 = (inst_63044 + (1));
var tmp63156 = inst_63043;
var tmp63157 = inst_63041;
var tmp63158 = inst_63042;
var inst_63041__$1 = tmp63157;
var inst_63042__$1 = tmp63158;
var inst_63043__$1 = tmp63156;
var inst_63044__$1 = inst_63057;
var state_63108__$1 = (function (){var statearr_63164 = state_63108;
(statearr_63164[(9)] = inst_63044__$1);

(statearr_63164[(20)] = inst_63043__$1);

(statearr_63164[(22)] = inst_63056);

(statearr_63164[(21)] = inst_63041__$1);

(statearr_63164[(10)] = inst_63042__$1);

return statearr_63164;
})();
var statearr_63168_65828 = state_63108__$1;
(statearr_63168_65828[(2)] = null);

(statearr_63168_65828[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (40))){
var inst_63069 = (state_63108[(23)]);
var inst_63073 = done(null);
var inst_63074 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_63069);
var state_63108__$1 = (function (){var statearr_63170 = state_63108;
(statearr_63170[(24)] = inst_63073);

return statearr_63170;
})();
var statearr_63171_65834 = state_63108__$1;
(statearr_63171_65834[(2)] = inst_63074);

(statearr_63171_65834[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (33))){
var inst_63060 = (state_63108[(25)]);
var inst_63062 = cljs.core.chunked_seq_QMARK_(inst_63060);
var state_63108__$1 = state_63108;
if(inst_63062){
var statearr_63173_65835 = state_63108__$1;
(statearr_63173_65835[(1)] = (36));

} else {
var statearr_63174_65836 = state_63108__$1;
(statearr_63174_65836[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (13))){
var inst_62986 = (state_63108[(26)]);
var inst_62990 = cljs.core.async.close_BANG_(inst_62986);
var state_63108__$1 = state_63108;
var statearr_63175_65837 = state_63108__$1;
(statearr_63175_65837[(2)] = inst_62990);

(statearr_63175_65837[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (22))){
var inst_63013 = (state_63108[(8)]);
var inst_63016 = cljs.core.async.close_BANG_(inst_63013);
var state_63108__$1 = state_63108;
var statearr_63176_65838 = state_63108__$1;
(statearr_63176_65838[(2)] = inst_63016);

(statearr_63176_65838[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (36))){
var inst_63060 = (state_63108[(25)]);
var inst_63064 = cljs.core.chunk_first(inst_63060);
var inst_63065 = cljs.core.chunk_rest(inst_63060);
var inst_63066 = cljs.core.count(inst_63064);
var inst_63041 = inst_63065;
var inst_63042 = inst_63064;
var inst_63043 = inst_63066;
var inst_63044 = (0);
var state_63108__$1 = (function (){var statearr_63177 = state_63108;
(statearr_63177[(9)] = inst_63044);

(statearr_63177[(20)] = inst_63043);

(statearr_63177[(21)] = inst_63041);

(statearr_63177[(10)] = inst_63042);

return statearr_63177;
})();
var statearr_63183_65839 = state_63108__$1;
(statearr_63183_65839[(2)] = null);

(statearr_63183_65839[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (41))){
var inst_63060 = (state_63108[(25)]);
var inst_63076 = (state_63108[(2)]);
var inst_63077 = cljs.core.next(inst_63060);
var inst_63041 = inst_63077;
var inst_63042 = null;
var inst_63043 = (0);
var inst_63044 = (0);
var state_63108__$1 = (function (){var statearr_63185 = state_63108;
(statearr_63185[(9)] = inst_63044);

(statearr_63185[(20)] = inst_63043);

(statearr_63185[(27)] = inst_63076);

(statearr_63185[(21)] = inst_63041);

(statearr_63185[(10)] = inst_63042);

return statearr_63185;
})();
var statearr_63186_65846 = state_63108__$1;
(statearr_63186_65846[(2)] = null);

(statearr_63186_65846[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (43))){
var state_63108__$1 = state_63108;
var statearr_63187_65847 = state_63108__$1;
(statearr_63187_65847[(2)] = null);

(statearr_63187_65847[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (29))){
var inst_63085 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63193_65848 = state_63108__$1;
(statearr_63193_65848[(2)] = inst_63085);

(statearr_63193_65848[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (44))){
var inst_63094 = (state_63108[(2)]);
var state_63108__$1 = (function (){var statearr_63195 = state_63108;
(statearr_63195[(28)] = inst_63094);

return statearr_63195;
})();
var statearr_63196_65849 = state_63108__$1;
(statearr_63196_65849[(2)] = null);

(statearr_63196_65849[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (6))){
var inst_63033 = (state_63108[(29)]);
var inst_63032 = cljs.core.deref(cs);
var inst_63033__$1 = cljs.core.keys(inst_63032);
var inst_63034 = cljs.core.count(inst_63033__$1);
var inst_63035 = cljs.core.reset_BANG_(dctr,inst_63034);
var inst_63040 = cljs.core.seq(inst_63033__$1);
var inst_63041 = inst_63040;
var inst_63042 = null;
var inst_63043 = (0);
var inst_63044 = (0);
var state_63108__$1 = (function (){var statearr_63199 = state_63108;
(statearr_63199[(9)] = inst_63044);

(statearr_63199[(30)] = inst_63035);

(statearr_63199[(29)] = inst_63033__$1);

(statearr_63199[(20)] = inst_63043);

(statearr_63199[(21)] = inst_63041);

(statearr_63199[(10)] = inst_63042);

return statearr_63199;
})();
var statearr_63202_65854 = state_63108__$1;
(statearr_63202_65854[(2)] = null);

(statearr_63202_65854[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (28))){
var inst_63060 = (state_63108[(25)]);
var inst_63041 = (state_63108[(21)]);
var inst_63060__$1 = cljs.core.seq(inst_63041);
var state_63108__$1 = (function (){var statearr_63203 = state_63108;
(statearr_63203[(25)] = inst_63060__$1);

return statearr_63203;
})();
if(inst_63060__$1){
var statearr_63204_65859 = state_63108__$1;
(statearr_63204_65859[(1)] = (33));

} else {
var statearr_63207_65860 = state_63108__$1;
(statearr_63207_65860[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (25))){
var inst_63044 = (state_63108[(9)]);
var inst_63043 = (state_63108[(20)]);
var inst_63046 = (inst_63044 < inst_63043);
var inst_63047 = inst_63046;
var state_63108__$1 = state_63108;
if(cljs.core.truth_(inst_63047)){
var statearr_63213_65861 = state_63108__$1;
(statearr_63213_65861[(1)] = (27));

} else {
var statearr_63214_65862 = state_63108__$1;
(statearr_63214_65862[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (34))){
var state_63108__$1 = state_63108;
var statearr_63218_65863 = state_63108__$1;
(statearr_63218_65863[(2)] = null);

(statearr_63218_65863[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (17))){
var state_63108__$1 = state_63108;
var statearr_63222_65864 = state_63108__$1;
(statearr_63222_65864[(2)] = null);

(statearr_63222_65864[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (3))){
var inst_63099 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63108__$1,inst_63099);
} else {
if((state_val_63113 === (12))){
var inst_63028 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63226_65865 = state_63108__$1;
(statearr_63226_65865[(2)] = inst_63028);

(statearr_63226_65865[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (2))){
var state_63108__$1 = state_63108;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63108__$1,(4),ch);
} else {
if((state_val_63113 === (23))){
var state_63108__$1 = state_63108;
var statearr_63227_65866 = state_63108__$1;
(statearr_63227_65866[(2)] = null);

(statearr_63227_65866[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (35))){
var inst_63083 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63229_65869 = state_63108__$1;
(statearr_63229_65869[(2)] = inst_63083);

(statearr_63229_65869[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (19))){
var inst_62997 = (state_63108[(7)]);
var inst_63001 = cljs.core.chunk_first(inst_62997);
var inst_63002 = cljs.core.chunk_rest(inst_62997);
var inst_63003 = cljs.core.count(inst_63001);
var inst_62974 = inst_63002;
var inst_62975 = inst_63001;
var inst_62976 = inst_63003;
var inst_62977 = (0);
var state_63108__$1 = (function (){var statearr_63231 = state_63108;
(statearr_63231[(13)] = inst_62976);

(statearr_63231[(14)] = inst_62977);

(statearr_63231[(15)] = inst_62975);

(statearr_63231[(16)] = inst_62974);

return statearr_63231;
})();
var statearr_63232_65870 = state_63108__$1;
(statearr_63232_65870[(2)] = null);

(statearr_63232_65870[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (11))){
var inst_62997 = (state_63108[(7)]);
var inst_62974 = (state_63108[(16)]);
var inst_62997__$1 = cljs.core.seq(inst_62974);
var state_63108__$1 = (function (){var statearr_63234 = state_63108;
(statearr_63234[(7)] = inst_62997__$1);

return statearr_63234;
})();
if(inst_62997__$1){
var statearr_63235_65871 = state_63108__$1;
(statearr_63235_65871[(1)] = (16));

} else {
var statearr_63239_65872 = state_63108__$1;
(statearr_63239_65872[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (9))){
var inst_63030 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63242_65873 = state_63108__$1;
(statearr_63242_65873[(2)] = inst_63030);

(statearr_63242_65873[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (5))){
var inst_62972 = cljs.core.deref(cs);
var inst_62973 = cljs.core.seq(inst_62972);
var inst_62974 = inst_62973;
var inst_62975 = null;
var inst_62976 = (0);
var inst_62977 = (0);
var state_63108__$1 = (function (){var statearr_63244 = state_63108;
(statearr_63244[(13)] = inst_62976);

(statearr_63244[(14)] = inst_62977);

(statearr_63244[(15)] = inst_62975);

(statearr_63244[(16)] = inst_62974);

return statearr_63244;
})();
var statearr_63245_65881 = state_63108__$1;
(statearr_63245_65881[(2)] = null);

(statearr_63245_65881[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (14))){
var state_63108__$1 = state_63108;
var statearr_63248_65882 = state_63108__$1;
(statearr_63248_65882[(2)] = null);

(statearr_63248_65882[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (45))){
var inst_63091 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63249_65883 = state_63108__$1;
(statearr_63249_65883[(2)] = inst_63091);

(statearr_63249_65883[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (26))){
var inst_63033 = (state_63108[(29)]);
var inst_63087 = (state_63108[(2)]);
var inst_63088 = cljs.core.seq(inst_63033);
var state_63108__$1 = (function (){var statearr_63255 = state_63108;
(statearr_63255[(31)] = inst_63087);

return statearr_63255;
})();
if(inst_63088){
var statearr_63257_65884 = state_63108__$1;
(statearr_63257_65884[(1)] = (42));

} else {
var statearr_63258_65885 = state_63108__$1;
(statearr_63258_65885[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (16))){
var inst_62997 = (state_63108[(7)]);
var inst_62999 = cljs.core.chunked_seq_QMARK_(inst_62997);
var state_63108__$1 = state_63108;
if(inst_62999){
var statearr_63260_65886 = state_63108__$1;
(statearr_63260_65886[(1)] = (19));

} else {
var statearr_63261_65887 = state_63108__$1;
(statearr_63261_65887[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (38))){
var inst_63080 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63266_65888 = state_63108__$1;
(statearr_63266_65888[(2)] = inst_63080);

(statearr_63266_65888[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (30))){
var state_63108__$1 = state_63108;
var statearr_63268_65890 = state_63108__$1;
(statearr_63268_65890[(2)] = null);

(statearr_63268_65890[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (10))){
var inst_62977 = (state_63108[(14)]);
var inst_62975 = (state_63108[(15)]);
var inst_62985 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_62975,inst_62977);
var inst_62986 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_62985,(0),null);
var inst_62987 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_62985,(1),null);
var state_63108__$1 = (function (){var statearr_63276 = state_63108;
(statearr_63276[(26)] = inst_62986);

return statearr_63276;
})();
if(cljs.core.truth_(inst_62987)){
var statearr_63280_65892 = state_63108__$1;
(statearr_63280_65892[(1)] = (13));

} else {
var statearr_63281_65894 = state_63108__$1;
(statearr_63281_65894[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (18))){
var inst_63026 = (state_63108[(2)]);
var state_63108__$1 = state_63108;
var statearr_63291_65896 = state_63108__$1;
(statearr_63291_65896[(2)] = inst_63026);

(statearr_63291_65896[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (42))){
var state_63108__$1 = state_63108;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63108__$1,(45),dchan);
} else {
if((state_val_63113 === (37))){
var inst_63060 = (state_63108[(25)]);
var inst_62965 = (state_63108[(12)]);
var inst_63069 = (state_63108[(23)]);
var inst_63069__$1 = cljs.core.first(inst_63060);
var inst_63070 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_63069__$1,inst_62965,done);
var state_63108__$1 = (function (){var statearr_63307 = state_63108;
(statearr_63307[(23)] = inst_63069__$1);

return statearr_63307;
})();
if(cljs.core.truth_(inst_63070)){
var statearr_63312_65898 = state_63108__$1;
(statearr_63312_65898[(1)] = (39));

} else {
var statearr_63313_65901 = state_63108__$1;
(statearr_63313_65901[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63113 === (8))){
var inst_62976 = (state_63108[(13)]);
var inst_62977 = (state_63108[(14)]);
var inst_62979 = (inst_62977 < inst_62976);
var inst_62980 = inst_62979;
var state_63108__$1 = state_63108;
if(cljs.core.truth_(inst_62980)){
var statearr_63323_65906 = state_63108__$1;
(statearr_63323_65906[(1)] = (10));

} else {
var statearr_63324_65907 = state_63108__$1;
(statearr_63324_65907[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__61706__auto__ = null;
var cljs$core$async$mult_$_state_machine__61706__auto____0 = (function (){
var statearr_63337 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_63337[(0)] = cljs$core$async$mult_$_state_machine__61706__auto__);

(statearr_63337[(1)] = (1));

return statearr_63337;
});
var cljs$core$async$mult_$_state_machine__61706__auto____1 = (function (state_63108){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_63108);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e63338){if((e63338 instanceof Object)){
var ex__61709__auto__ = e63338;
var statearr_63340_65909 = state_63108;
(statearr_63340_65909[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_63108);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e63338;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__65911 = state_63108;
state_63108 = G__65911;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__61706__auto__ = function(state_63108){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__61706__auto____1.call(this,state_63108);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__61706__auto____0;
cljs$core$async$mult_$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__61706__auto____1;
return cljs$core$async$mult_$_state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_63342 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_63342[(6)] = c__61983__auto___65793);

return statearr_63342;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__63348 = arguments.length;
switch (G__63348) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_65917 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_65917(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_65918 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_65918(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_65920 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_65920(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_65921 = (function (m,state_map){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4429__auto__.call(null,m,state_map));
} else {
var m__4426__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4426__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_65921(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_65925 = (function (m,mode){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4429__auto__.call(null,m,mode));
} else {
var m__4426__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4426__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_65925(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___65932 = arguments.length;
var i__4737__auto___65933 = (0);
while(true){
if((i__4737__auto___65933 < len__4736__auto___65932)){
args__4742__auto__.push((arguments[i__4737__auto___65933]));

var G__65935 = (i__4737__auto___65933 + (1));
i__4737__auto___65933 = G__65935;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__63399){
var map__63400 = p__63399;
var map__63400__$1 = (((((!((map__63400 == null))))?(((((map__63400.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__63400.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__63400):map__63400);
var opts = map__63400__$1;
var statearr_63403_65936 = state;
(statearr_63403_65936[(1)] = cont_block);


var temp__5735__auto__ = cljs.core.async.do_alts((function (val){
var statearr_63404_65937 = state;
(statearr_63404_65937[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5735__auto__)){
var cb = temp__5735__auto__;
var statearr_63407_65938 = state;
(statearr_63407_65938[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq63386){
var G__63387 = cljs.core.first(seq63386);
var seq63386__$1 = cljs.core.next(seq63386);
var G__63388 = cljs.core.first(seq63386__$1);
var seq63386__$2 = cljs.core.next(seq63386__$1);
var G__63389 = cljs.core.first(seq63386__$2);
var seq63386__$3 = cljs.core.next(seq63386__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__63387,G__63388,G__63389,seq63386__$3);
}));

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async63427 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async63427 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta63428){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta63428 = meta63428;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_63429,meta63428__$1){
var self__ = this;
var _63429__$1 = this;
return (new cljs.core.async.t_cljs$core$async63427(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta63428__$1));
}));

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_63429){
var self__ = this;
var _63429__$1 = this;
return self__.meta63428;
}));

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async63427.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async63427.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta63428","meta63428",1259585437,null)], null);
}));

(cljs.core.async.t_cljs$core$async63427.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async63427.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async63427");

(cljs.core.async.t_cljs$core$async63427.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async63427");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async63427.
 */
cljs.core.async.__GT_t_cljs$core$async63427 = (function cljs$core$async$mix_$___GT_t_cljs$core$async63427(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta63428){
return (new cljs.core.async.t_cljs$core$async63427(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta63428));
});

}

return (new cljs.core.async.t_cljs$core$async63427(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__61983__auto___65951 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_63563){
var state_val_63564 = (state_63563[(1)]);
if((state_val_63564 === (7))){
var inst_63462 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
var statearr_63566_65953 = state_63563__$1;
(statearr_63566_65953[(2)] = inst_63462);

(statearr_63566_65953[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (20))){
var inst_63474 = (state_63563[(7)]);
var state_63563__$1 = state_63563;
var statearr_63567_65954 = state_63563__$1;
(statearr_63567_65954[(2)] = inst_63474);

(statearr_63567_65954[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (27))){
var state_63563__$1 = state_63563;
var statearr_63570_65955 = state_63563__$1;
(statearr_63570_65955[(2)] = null);

(statearr_63570_65955[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (1))){
var inst_63448 = (state_63563[(8)]);
var inst_63448__$1 = calc_state();
var inst_63450 = (inst_63448__$1 == null);
var inst_63451 = cljs.core.not(inst_63450);
var state_63563__$1 = (function (){var statearr_63572 = state_63563;
(statearr_63572[(8)] = inst_63448__$1);

return statearr_63572;
})();
if(inst_63451){
var statearr_63573_65956 = state_63563__$1;
(statearr_63573_65956[(1)] = (2));

} else {
var statearr_63574_65957 = state_63563__$1;
(statearr_63574_65957[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (24))){
var inst_63506 = (state_63563[(9)]);
var inst_63537 = (state_63563[(10)]);
var inst_63519 = (state_63563[(11)]);
var inst_63537__$1 = (inst_63506.cljs$core$IFn$_invoke$arity$1 ? inst_63506.cljs$core$IFn$_invoke$arity$1(inst_63519) : inst_63506.call(null,inst_63519));
var state_63563__$1 = (function (){var statearr_63576 = state_63563;
(statearr_63576[(10)] = inst_63537__$1);

return statearr_63576;
})();
if(cljs.core.truth_(inst_63537__$1)){
var statearr_63577_65958 = state_63563__$1;
(statearr_63577_65958[(1)] = (29));

} else {
var statearr_63578_65959 = state_63563__$1;
(statearr_63578_65959[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (4))){
var inst_63465 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
if(cljs.core.truth_(inst_63465)){
var statearr_63579_65960 = state_63563__$1;
(statearr_63579_65960[(1)] = (8));

} else {
var statearr_63581_65961 = state_63563__$1;
(statearr_63581_65961[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (15))){
var inst_63500 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
if(cljs.core.truth_(inst_63500)){
var statearr_63582_65964 = state_63563__$1;
(statearr_63582_65964[(1)] = (19));

} else {
var statearr_63583_65965 = state_63563__$1;
(statearr_63583_65965[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (21))){
var inst_63505 = (state_63563[(12)]);
var inst_63505__$1 = (state_63563[(2)]);
var inst_63506 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_63505__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_63507 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_63505__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_63510 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_63505__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_63563__$1 = (function (){var statearr_63584 = state_63563;
(statearr_63584[(9)] = inst_63506);

(statearr_63584[(13)] = inst_63507);

(statearr_63584[(12)] = inst_63505__$1);

return statearr_63584;
})();
return cljs.core.async.ioc_alts_BANG_(state_63563__$1,(22),inst_63510);
} else {
if((state_val_63564 === (31))){
var inst_63545 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
if(cljs.core.truth_(inst_63545)){
var statearr_63586_65968 = state_63563__$1;
(statearr_63586_65968[(1)] = (32));

} else {
var statearr_63588_65969 = state_63563__$1;
(statearr_63588_65969[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (32))){
var inst_63518 = (state_63563[(14)]);
var state_63563__$1 = state_63563;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_63563__$1,(35),out,inst_63518);
} else {
if((state_val_63564 === (33))){
var inst_63505 = (state_63563[(12)]);
var inst_63474 = inst_63505;
var state_63563__$1 = (function (){var statearr_63591 = state_63563;
(statearr_63591[(7)] = inst_63474);

return statearr_63591;
})();
var statearr_63594_65970 = state_63563__$1;
(statearr_63594_65970[(2)] = null);

(statearr_63594_65970[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (13))){
var inst_63474 = (state_63563[(7)]);
var inst_63487 = inst_63474.cljs$lang$protocol_mask$partition0$;
var inst_63488 = (inst_63487 & (64));
var inst_63489 = inst_63474.cljs$core$ISeq$;
var inst_63490 = (cljs.core.PROTOCOL_SENTINEL === inst_63489);
var inst_63491 = ((inst_63488) || (inst_63490));
var state_63563__$1 = state_63563;
if(cljs.core.truth_(inst_63491)){
var statearr_63596_65971 = state_63563__$1;
(statearr_63596_65971[(1)] = (16));

} else {
var statearr_63597_65972 = state_63563__$1;
(statearr_63597_65972[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (22))){
var inst_63518 = (state_63563[(14)]);
var inst_63519 = (state_63563[(11)]);
var inst_63517 = (state_63563[(2)]);
var inst_63518__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_63517,(0),null);
var inst_63519__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_63517,(1),null);
var inst_63522 = (inst_63518__$1 == null);
var inst_63523 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_63519__$1,change);
var inst_63524 = ((inst_63522) || (inst_63523));
var state_63563__$1 = (function (){var statearr_63600 = state_63563;
(statearr_63600[(14)] = inst_63518__$1);

(statearr_63600[(11)] = inst_63519__$1);

return statearr_63600;
})();
if(cljs.core.truth_(inst_63524)){
var statearr_63601_65975 = state_63563__$1;
(statearr_63601_65975[(1)] = (23));

} else {
var statearr_63602_65976 = state_63563__$1;
(statearr_63602_65976[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (36))){
var inst_63505 = (state_63563[(12)]);
var inst_63474 = inst_63505;
var state_63563__$1 = (function (){var statearr_63604 = state_63563;
(statearr_63604[(7)] = inst_63474);

return statearr_63604;
})();
var statearr_63605_65977 = state_63563__$1;
(statearr_63605_65977[(2)] = null);

(statearr_63605_65977[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (29))){
var inst_63537 = (state_63563[(10)]);
var state_63563__$1 = state_63563;
var statearr_63607_65978 = state_63563__$1;
(statearr_63607_65978[(2)] = inst_63537);

(statearr_63607_65978[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (6))){
var state_63563__$1 = state_63563;
var statearr_63608_65979 = state_63563__$1;
(statearr_63608_65979[(2)] = false);

(statearr_63608_65979[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (28))){
var inst_63533 = (state_63563[(2)]);
var inst_63534 = calc_state();
var inst_63474 = inst_63534;
var state_63563__$1 = (function (){var statearr_63615 = state_63563;
(statearr_63615[(15)] = inst_63533);

(statearr_63615[(7)] = inst_63474);

return statearr_63615;
})();
var statearr_63621_65981 = state_63563__$1;
(statearr_63621_65981[(2)] = null);

(statearr_63621_65981[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (25))){
var inst_63559 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
var statearr_63628_65983 = state_63563__$1;
(statearr_63628_65983[(2)] = inst_63559);

(statearr_63628_65983[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (34))){
var inst_63557 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
var statearr_63634_65984 = state_63563__$1;
(statearr_63634_65984[(2)] = inst_63557);

(statearr_63634_65984[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (17))){
var state_63563__$1 = state_63563;
var statearr_63635_65985 = state_63563__$1;
(statearr_63635_65985[(2)] = false);

(statearr_63635_65985[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (3))){
var state_63563__$1 = state_63563;
var statearr_63640_65989 = state_63563__$1;
(statearr_63640_65989[(2)] = false);

(statearr_63640_65989[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (12))){
var inst_63561 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63563__$1,inst_63561);
} else {
if((state_val_63564 === (2))){
var inst_63448 = (state_63563[(8)]);
var inst_63454 = inst_63448.cljs$lang$protocol_mask$partition0$;
var inst_63455 = (inst_63454 & (64));
var inst_63456 = inst_63448.cljs$core$ISeq$;
var inst_63457 = (cljs.core.PROTOCOL_SENTINEL === inst_63456);
var inst_63458 = ((inst_63455) || (inst_63457));
var state_63563__$1 = state_63563;
if(cljs.core.truth_(inst_63458)){
var statearr_63649_65997 = state_63563__$1;
(statearr_63649_65997[(1)] = (5));

} else {
var statearr_63650_65998 = state_63563__$1;
(statearr_63650_65998[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (23))){
var inst_63518 = (state_63563[(14)]);
var inst_63526 = (inst_63518 == null);
var state_63563__$1 = state_63563;
if(cljs.core.truth_(inst_63526)){
var statearr_63651_65999 = state_63563__$1;
(statearr_63651_65999[(1)] = (26));

} else {
var statearr_63652_66000 = state_63563__$1;
(statearr_63652_66000[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (35))){
var inst_63548 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
if(cljs.core.truth_(inst_63548)){
var statearr_63653_66004 = state_63563__$1;
(statearr_63653_66004[(1)] = (36));

} else {
var statearr_63654_66005 = state_63563__$1;
(statearr_63654_66005[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (19))){
var inst_63474 = (state_63563[(7)]);
var inst_63502 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_63474);
var state_63563__$1 = state_63563;
var statearr_63655_66006 = state_63563__$1;
(statearr_63655_66006[(2)] = inst_63502);

(statearr_63655_66006[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (11))){
var inst_63474 = (state_63563[(7)]);
var inst_63481 = (inst_63474 == null);
var inst_63482 = cljs.core.not(inst_63481);
var state_63563__$1 = state_63563;
if(inst_63482){
var statearr_63656_66010 = state_63563__$1;
(statearr_63656_66010[(1)] = (13));

} else {
var statearr_63657_66012 = state_63563__$1;
(statearr_63657_66012[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (9))){
var inst_63448 = (state_63563[(8)]);
var state_63563__$1 = state_63563;
var statearr_63658_66013 = state_63563__$1;
(statearr_63658_66013[(2)] = inst_63448);

(statearr_63658_66013[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (5))){
var state_63563__$1 = state_63563;
var statearr_63659_66014 = state_63563__$1;
(statearr_63659_66014[(2)] = true);

(statearr_63659_66014[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (14))){
var state_63563__$1 = state_63563;
var statearr_63660_66018 = state_63563__$1;
(statearr_63660_66018[(2)] = false);

(statearr_63660_66018[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (26))){
var inst_63519 = (state_63563[(11)]);
var inst_63530 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_63519);
var state_63563__$1 = state_63563;
var statearr_63661_66019 = state_63563__$1;
(statearr_63661_66019[(2)] = inst_63530);

(statearr_63661_66019[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (16))){
var state_63563__$1 = state_63563;
var statearr_63667_66020 = state_63563__$1;
(statearr_63667_66020[(2)] = true);

(statearr_63667_66020[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (38))){
var inst_63553 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
var statearr_63674_66022 = state_63563__$1;
(statearr_63674_66022[(2)] = inst_63553);

(statearr_63674_66022[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (30))){
var inst_63506 = (state_63563[(9)]);
var inst_63507 = (state_63563[(13)]);
var inst_63519 = (state_63563[(11)]);
var inst_63540 = cljs.core.empty_QMARK_(inst_63506);
var inst_63541 = (inst_63507.cljs$core$IFn$_invoke$arity$1 ? inst_63507.cljs$core$IFn$_invoke$arity$1(inst_63519) : inst_63507.call(null,inst_63519));
var inst_63542 = cljs.core.not(inst_63541);
var inst_63543 = ((inst_63540) && (inst_63542));
var state_63563__$1 = state_63563;
var statearr_63685_66023 = state_63563__$1;
(statearr_63685_66023[(2)] = inst_63543);

(statearr_63685_66023[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (10))){
var inst_63448 = (state_63563[(8)]);
var inst_63470 = (state_63563[(2)]);
var inst_63471 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_63470,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_63472 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_63470,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_63473 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_63470,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_63474 = inst_63448;
var state_63563__$1 = (function (){var statearr_63692 = state_63563;
(statearr_63692[(16)] = inst_63471);

(statearr_63692[(17)] = inst_63473);

(statearr_63692[(18)] = inst_63472);

(statearr_63692[(7)] = inst_63474);

return statearr_63692;
})();
var statearr_63693_66039 = state_63563__$1;
(statearr_63693_66039[(2)] = null);

(statearr_63693_66039[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (18))){
var inst_63495 = (state_63563[(2)]);
var state_63563__$1 = state_63563;
var statearr_63694_66040 = state_63563__$1;
(statearr_63694_66040[(2)] = inst_63495);

(statearr_63694_66040[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (37))){
var state_63563__$1 = state_63563;
var statearr_63695_66041 = state_63563__$1;
(statearr_63695_66041[(2)] = null);

(statearr_63695_66041[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63564 === (8))){
var inst_63448 = (state_63563[(8)]);
var inst_63467 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_63448);
var state_63563__$1 = state_63563;
var statearr_63696_66042 = state_63563__$1;
(statearr_63696_66042[(2)] = inst_63467);

(statearr_63696_66042[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__61706__auto__ = null;
var cljs$core$async$mix_$_state_machine__61706__auto____0 = (function (){
var statearr_63697 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_63697[(0)] = cljs$core$async$mix_$_state_machine__61706__auto__);

(statearr_63697[(1)] = (1));

return statearr_63697;
});
var cljs$core$async$mix_$_state_machine__61706__auto____1 = (function (state_63563){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_63563);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e63698){if((e63698 instanceof Object)){
var ex__61709__auto__ = e63698;
var statearr_63699_66043 = state_63563;
(statearr_63699_66043[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_63563);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e63698;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66044 = state_63563;
state_63563 = G__66044;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__61706__auto__ = function(state_63563){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__61706__auto____1.call(this,state_63563);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__61706__auto____0;
cljs$core$async$mix_$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__61706__auto____1;
return cljs$core$async$mix_$_state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_63703 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_63703[(6)] = c__61983__auto___65951);

return statearr_63703;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_66052 = (function (p,v,ch,close_QMARK_){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4429__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4426__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_66052(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_66053 = (function (p,v,ch){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4429__auto__.call(null,p,v,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4426__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_66053(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_66060 = (function() {
var G__66061 = null;
var G__66061__1 = (function (p){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4429__auto__.call(null,p));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4426__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__66061__2 = (function (p,v){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4429__auto__.call(null,p,v));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4426__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__66061 = function(p,v){
switch(arguments.length){
case 1:
return G__66061__1.call(this,p);
case 2:
return G__66061__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__66061.cljs$core$IFn$_invoke$arity$1 = G__66061__1;
G__66061.cljs$core$IFn$_invoke$arity$2 = G__66061__2;
return G__66061;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__63717 = arguments.length;
switch (G__63717) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_66060.cljs$core$IFn$_invoke$arity$1(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_66060.cljs$core$IFn$_invoke$arity$2(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__63720 = arguments.length;
switch (G__63720) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__4126__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__63718_SHARP_){
if(cljs.core.truth_((p1__63718_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__63718_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__63718_SHARP_.call(null,topic)))){
return p1__63718_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__63718_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async63724 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async63724 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta63725){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta63725 = meta63725;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_63726,meta63725__$1){
var self__ = this;
var _63726__$1 = this;
return (new cljs.core.async.t_cljs$core$async63724(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta63725__$1));
}));

(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_63726){
var self__ = this;
var _63726__$1 = this;
return self__.meta63725;
}));

(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async63724.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async63724.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta63725","meta63725",-2008511139,null)], null);
}));

(cljs.core.async.t_cljs$core$async63724.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async63724.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async63724");

(cljs.core.async.t_cljs$core$async63724.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async63724");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async63724.
 */
cljs.core.async.__GT_t_cljs$core$async63724 = (function cljs$core$async$__GT_t_cljs$core$async63724(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta63725){
return (new cljs.core.async.t_cljs$core$async63724(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta63725));
});

}

return (new cljs.core.async.t_cljs$core$async63724(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__61983__auto___66078 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_63832){
var state_val_63833 = (state_63832[(1)]);
if((state_val_63833 === (7))){
var inst_63828 = (state_63832[(2)]);
var state_63832__$1 = state_63832;
var statearr_63835_66079 = state_63832__$1;
(statearr_63835_66079[(2)] = inst_63828);

(statearr_63835_66079[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (20))){
var state_63832__$1 = state_63832;
var statearr_63838_66080 = state_63832__$1;
(statearr_63838_66080[(2)] = null);

(statearr_63838_66080[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (1))){
var state_63832__$1 = state_63832;
var statearr_63839_66084 = state_63832__$1;
(statearr_63839_66084[(2)] = null);

(statearr_63839_66084[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (24))){
var inst_63811 = (state_63832[(7)]);
var inst_63820 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_63811);
var state_63832__$1 = state_63832;
var statearr_63840_66087 = state_63832__$1;
(statearr_63840_66087[(2)] = inst_63820);

(statearr_63840_66087[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (4))){
var inst_63747 = (state_63832[(8)]);
var inst_63747__$1 = (state_63832[(2)]);
var inst_63748 = (inst_63747__$1 == null);
var state_63832__$1 = (function (){var statearr_63842 = state_63832;
(statearr_63842[(8)] = inst_63747__$1);

return statearr_63842;
})();
if(cljs.core.truth_(inst_63748)){
var statearr_63852_66088 = state_63832__$1;
(statearr_63852_66088[(1)] = (5));

} else {
var statearr_63853_66089 = state_63832__$1;
(statearr_63853_66089[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (15))){
var inst_63805 = (state_63832[(2)]);
var state_63832__$1 = state_63832;
var statearr_63854_66090 = state_63832__$1;
(statearr_63854_66090[(2)] = inst_63805);

(statearr_63854_66090[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (21))){
var inst_63825 = (state_63832[(2)]);
var state_63832__$1 = (function (){var statearr_63855 = state_63832;
(statearr_63855[(9)] = inst_63825);

return statearr_63855;
})();
var statearr_63856_66091 = state_63832__$1;
(statearr_63856_66091[(2)] = null);

(statearr_63856_66091[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (13))){
var inst_63782 = (state_63832[(10)]);
var inst_63784 = cljs.core.chunked_seq_QMARK_(inst_63782);
var state_63832__$1 = state_63832;
if(inst_63784){
var statearr_63860_66092 = state_63832__$1;
(statearr_63860_66092[(1)] = (16));

} else {
var statearr_63861_66093 = state_63832__$1;
(statearr_63861_66093[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (22))){
var inst_63817 = (state_63832[(2)]);
var state_63832__$1 = state_63832;
if(cljs.core.truth_(inst_63817)){
var statearr_63862_66095 = state_63832__$1;
(statearr_63862_66095[(1)] = (23));

} else {
var statearr_63863_66097 = state_63832__$1;
(statearr_63863_66097[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (6))){
var inst_63813 = (state_63832[(11)]);
var inst_63747 = (state_63832[(8)]);
var inst_63811 = (state_63832[(7)]);
var inst_63811__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_63747) : topic_fn.call(null,inst_63747));
var inst_63812 = cljs.core.deref(mults);
var inst_63813__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_63812,inst_63811__$1);
var state_63832__$1 = (function (){var statearr_63866 = state_63832;
(statearr_63866[(11)] = inst_63813__$1);

(statearr_63866[(7)] = inst_63811__$1);

return statearr_63866;
})();
if(cljs.core.truth_(inst_63813__$1)){
var statearr_63868_66098 = state_63832__$1;
(statearr_63868_66098[(1)] = (19));

} else {
var statearr_63869_66099 = state_63832__$1;
(statearr_63869_66099[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (25))){
var inst_63822 = (state_63832[(2)]);
var state_63832__$1 = state_63832;
var statearr_63870_66100 = state_63832__$1;
(statearr_63870_66100[(2)] = inst_63822);

(statearr_63870_66100[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (17))){
var inst_63782 = (state_63832[(10)]);
var inst_63796 = cljs.core.first(inst_63782);
var inst_63797 = cljs.core.async.muxch_STAR_(inst_63796);
var inst_63798 = cljs.core.async.close_BANG_(inst_63797);
var inst_63799 = cljs.core.next(inst_63782);
var inst_63757 = inst_63799;
var inst_63758 = null;
var inst_63760 = (0);
var inst_63761 = (0);
var state_63832__$1 = (function (){var statearr_63874 = state_63832;
(statearr_63874[(12)] = inst_63798);

(statearr_63874[(13)] = inst_63758);

(statearr_63874[(14)] = inst_63757);

(statearr_63874[(15)] = inst_63760);

(statearr_63874[(16)] = inst_63761);

return statearr_63874;
})();
var statearr_63875_66104 = state_63832__$1;
(statearr_63875_66104[(2)] = null);

(statearr_63875_66104[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (3))){
var inst_63830 = (state_63832[(2)]);
var state_63832__$1 = state_63832;
return cljs.core.async.impl.ioc_helpers.return_chan(state_63832__$1,inst_63830);
} else {
if((state_val_63833 === (12))){
var inst_63807 = (state_63832[(2)]);
var state_63832__$1 = state_63832;
var statearr_63880_66111 = state_63832__$1;
(statearr_63880_66111[(2)] = inst_63807);

(statearr_63880_66111[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (2))){
var state_63832__$1 = state_63832;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_63832__$1,(4),ch);
} else {
if((state_val_63833 === (23))){
var state_63832__$1 = state_63832;
var statearr_63881_66112 = state_63832__$1;
(statearr_63881_66112[(2)] = null);

(statearr_63881_66112[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (19))){
var inst_63813 = (state_63832[(11)]);
var inst_63747 = (state_63832[(8)]);
var inst_63815 = cljs.core.async.muxch_STAR_(inst_63813);
var state_63832__$1 = state_63832;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_63832__$1,(22),inst_63815,inst_63747);
} else {
if((state_val_63833 === (11))){
var inst_63782 = (state_63832[(10)]);
var inst_63757 = (state_63832[(14)]);
var inst_63782__$1 = cljs.core.seq(inst_63757);
var state_63832__$1 = (function (){var statearr_63887 = state_63832;
(statearr_63887[(10)] = inst_63782__$1);

return statearr_63887;
})();
if(inst_63782__$1){
var statearr_63889_66116 = state_63832__$1;
(statearr_63889_66116[(1)] = (13));

} else {
var statearr_63893_66118 = state_63832__$1;
(statearr_63893_66118[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (9))){
var inst_63809 = (state_63832[(2)]);
var state_63832__$1 = state_63832;
var statearr_63895_66119 = state_63832__$1;
(statearr_63895_66119[(2)] = inst_63809);

(statearr_63895_66119[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (5))){
var inst_63754 = cljs.core.deref(mults);
var inst_63755 = cljs.core.vals(inst_63754);
var inst_63756 = cljs.core.seq(inst_63755);
var inst_63757 = inst_63756;
var inst_63758 = null;
var inst_63760 = (0);
var inst_63761 = (0);
var state_63832__$1 = (function (){var statearr_63901 = state_63832;
(statearr_63901[(13)] = inst_63758);

(statearr_63901[(14)] = inst_63757);

(statearr_63901[(15)] = inst_63760);

(statearr_63901[(16)] = inst_63761);

return statearr_63901;
})();
var statearr_63907_66121 = state_63832__$1;
(statearr_63907_66121[(2)] = null);

(statearr_63907_66121[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (14))){
var state_63832__$1 = state_63832;
var statearr_63916_66123 = state_63832__$1;
(statearr_63916_66123[(2)] = null);

(statearr_63916_66123[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (16))){
var inst_63782 = (state_63832[(10)]);
var inst_63786 = cljs.core.chunk_first(inst_63782);
var inst_63788 = cljs.core.chunk_rest(inst_63782);
var inst_63789 = cljs.core.count(inst_63786);
var inst_63757 = inst_63788;
var inst_63758 = inst_63786;
var inst_63760 = inst_63789;
var inst_63761 = (0);
var state_63832__$1 = (function (){var statearr_63918 = state_63832;
(statearr_63918[(13)] = inst_63758);

(statearr_63918[(14)] = inst_63757);

(statearr_63918[(15)] = inst_63760);

(statearr_63918[(16)] = inst_63761);

return statearr_63918;
})();
var statearr_63922_66124 = state_63832__$1;
(statearr_63922_66124[(2)] = null);

(statearr_63922_66124[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (10))){
var inst_63758 = (state_63832[(13)]);
var inst_63757 = (state_63832[(14)]);
var inst_63760 = (state_63832[(15)]);
var inst_63761 = (state_63832[(16)]);
var inst_63768 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_63758,inst_63761);
var inst_63773 = cljs.core.async.muxch_STAR_(inst_63768);
var inst_63774 = cljs.core.async.close_BANG_(inst_63773);
var inst_63775 = (inst_63761 + (1));
var tmp63913 = inst_63758;
var tmp63914 = inst_63757;
var tmp63915 = inst_63760;
var inst_63757__$1 = tmp63914;
var inst_63758__$1 = tmp63913;
var inst_63760__$1 = tmp63915;
var inst_63761__$1 = inst_63775;
var state_63832__$1 = (function (){var statearr_63929 = state_63832;
(statearr_63929[(13)] = inst_63758__$1);

(statearr_63929[(14)] = inst_63757__$1);

(statearr_63929[(17)] = inst_63774);

(statearr_63929[(15)] = inst_63760__$1);

(statearr_63929[(16)] = inst_63761__$1);

return statearr_63929;
})();
var statearr_63935_66126 = state_63832__$1;
(statearr_63935_66126[(2)] = null);

(statearr_63935_66126[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (18))){
var inst_63802 = (state_63832[(2)]);
var state_63832__$1 = state_63832;
var statearr_63941_66127 = state_63832__$1;
(statearr_63941_66127[(2)] = inst_63802);

(statearr_63941_66127[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_63833 === (8))){
var inst_63760 = (state_63832[(15)]);
var inst_63761 = (state_63832[(16)]);
var inst_63765 = (inst_63761 < inst_63760);
var inst_63766 = inst_63765;
var state_63832__$1 = state_63832;
if(cljs.core.truth_(inst_63766)){
var statearr_63942_66128 = state_63832__$1;
(statearr_63942_66128[(1)] = (10));

} else {
var statearr_63948_66129 = state_63832__$1;
(statearr_63948_66129[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_63949 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_63949[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_63949[(1)] = (1));

return statearr_63949;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_63832){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_63832);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e63950){if((e63950 instanceof Object)){
var ex__61709__auto__ = e63950;
var statearr_63951_66130 = state_63832;
(statearr_63951_66130[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_63832);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e63950;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66131 = state_63832;
state_63832 = G__66131;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_63832){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_63832);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_63955 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_63955[(6)] = c__61983__auto___66078);

return statearr_63955;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__63961 = arguments.length;
switch (G__63961) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__63966 = arguments.length;
switch (G__63966) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__63968 = arguments.length;
switch (G__63968) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__61983__auto___66140 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_64018){
var state_val_64019 = (state_64018[(1)]);
if((state_val_64019 === (7))){
var state_64018__$1 = state_64018;
var statearr_64020_66141 = state_64018__$1;
(statearr_64020_66141[(2)] = null);

(statearr_64020_66141[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (1))){
var state_64018__$1 = state_64018;
var statearr_64021_66142 = state_64018__$1;
(statearr_64021_66142[(2)] = null);

(statearr_64021_66142[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (4))){
var inst_63975 = (state_64018[(7)]);
var inst_63977 = (inst_63975 < cnt);
var state_64018__$1 = state_64018;
if(cljs.core.truth_(inst_63977)){
var statearr_64025_66143 = state_64018__$1;
(statearr_64025_66143[(1)] = (6));

} else {
var statearr_64026_66145 = state_64018__$1;
(statearr_64026_66145[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (15))){
var inst_64011 = (state_64018[(2)]);
var state_64018__$1 = state_64018;
var statearr_64029_66146 = state_64018__$1;
(statearr_64029_66146[(2)] = inst_64011);

(statearr_64029_66146[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (13))){
var inst_64002 = cljs.core.async.close_BANG_(out);
var state_64018__$1 = state_64018;
var statearr_64031_66147 = state_64018__$1;
(statearr_64031_66147[(2)] = inst_64002);

(statearr_64031_66147[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (6))){
var state_64018__$1 = state_64018;
var statearr_64035_66148 = state_64018__$1;
(statearr_64035_66148[(2)] = null);

(statearr_64035_66148[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (3))){
var inst_64013 = (state_64018[(2)]);
var state_64018__$1 = state_64018;
return cljs.core.async.impl.ioc_helpers.return_chan(state_64018__$1,inst_64013);
} else {
if((state_val_64019 === (12))){
var inst_63999 = (state_64018[(8)]);
var inst_63999__$1 = (state_64018[(2)]);
var inst_64000 = cljs.core.some(cljs.core.nil_QMARK_,inst_63999__$1);
var state_64018__$1 = (function (){var statearr_64039 = state_64018;
(statearr_64039[(8)] = inst_63999__$1);

return statearr_64039;
})();
if(cljs.core.truth_(inst_64000)){
var statearr_64040_66149 = state_64018__$1;
(statearr_64040_66149[(1)] = (13));

} else {
var statearr_64041_66150 = state_64018__$1;
(statearr_64041_66150[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (2))){
var inst_63974 = cljs.core.reset_BANG_(dctr,cnt);
var inst_63975 = (0);
var state_64018__$1 = (function (){var statearr_64042 = state_64018;
(statearr_64042[(9)] = inst_63974);

(statearr_64042[(7)] = inst_63975);

return statearr_64042;
})();
var statearr_64043_66151 = state_64018__$1;
(statearr_64043_66151[(2)] = null);

(statearr_64043_66151[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (11))){
var inst_63975 = (state_64018[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame(state_64018,(10),Object,null,(9));
var inst_63986 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_63975) : chs__$1.call(null,inst_63975));
var inst_63987 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_63975) : done.call(null,inst_63975));
var inst_63988 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_63986,inst_63987);
var state_64018__$1 = state_64018;
var statearr_64047_66160 = state_64018__$1;
(statearr_64047_66160[(2)] = inst_63988);


cljs.core.async.impl.ioc_helpers.process_exception(state_64018__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (9))){
var inst_63975 = (state_64018[(7)]);
var inst_63990 = (state_64018[(2)]);
var inst_63991 = (inst_63975 + (1));
var inst_63975__$1 = inst_63991;
var state_64018__$1 = (function (){var statearr_64048 = state_64018;
(statearr_64048[(10)] = inst_63990);

(statearr_64048[(7)] = inst_63975__$1);

return statearr_64048;
})();
var statearr_64049_66169 = state_64018__$1;
(statearr_64049_66169[(2)] = null);

(statearr_64049_66169[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (5))){
var inst_63997 = (state_64018[(2)]);
var state_64018__$1 = (function (){var statearr_64053 = state_64018;
(statearr_64053[(11)] = inst_63997);

return statearr_64053;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_64018__$1,(12),dchan);
} else {
if((state_val_64019 === (14))){
var inst_63999 = (state_64018[(8)]);
var inst_64004 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_63999);
var state_64018__$1 = state_64018;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64018__$1,(16),out,inst_64004);
} else {
if((state_val_64019 === (16))){
var inst_64006 = (state_64018[(2)]);
var state_64018__$1 = (function (){var statearr_64054 = state_64018;
(statearr_64054[(12)] = inst_64006);

return statearr_64054;
})();
var statearr_64055_66175 = state_64018__$1;
(statearr_64055_66175[(2)] = null);

(statearr_64055_66175[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (10))){
var inst_63980 = (state_64018[(2)]);
var inst_63981 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_64018__$1 = (function (){var statearr_64057 = state_64018;
(statearr_64057[(13)] = inst_63980);

return statearr_64057;
})();
var statearr_64058_66176 = state_64018__$1;
(statearr_64058_66176[(2)] = inst_63981);


cljs.core.async.impl.ioc_helpers.process_exception(state_64018__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64019 === (8))){
var inst_63995 = (state_64018[(2)]);
var state_64018__$1 = state_64018;
var statearr_64062_66177 = state_64018__$1;
(statearr_64062_66177[(2)] = inst_63995);

(statearr_64062_66177[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_64063 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_64063[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_64063[(1)] = (1));

return statearr_64063;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_64018){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_64018);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e64067){if((e64067 instanceof Object)){
var ex__61709__auto__ = e64067;
var statearr_64068_66184 = state_64018;
(statearr_64068_66184[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_64018);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e64067;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66189 = state_64018;
state_64018 = G__66189;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_64018){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_64018);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_64070 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_64070[(6)] = c__61983__auto___66140);

return statearr_64070;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__64076 = arguments.length;
switch (G__64076) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__61983__auto___66202 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_64122){
var state_val_64126 = (state_64122[(1)]);
if((state_val_64126 === (7))){
var inst_64098 = (state_64122[(7)]);
var inst_64096 = (state_64122[(8)]);
var inst_64096__$1 = (state_64122[(2)]);
var inst_64098__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_64096__$1,(0),null);
var inst_64099 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_64096__$1,(1),null);
var inst_64100 = (inst_64098__$1 == null);
var state_64122__$1 = (function (){var statearr_64130 = state_64122;
(statearr_64130[(7)] = inst_64098__$1);

(statearr_64130[(9)] = inst_64099);

(statearr_64130[(8)] = inst_64096__$1);

return statearr_64130;
})();
if(cljs.core.truth_(inst_64100)){
var statearr_64131_66215 = state_64122__$1;
(statearr_64131_66215[(1)] = (8));

} else {
var statearr_64133_66219 = state_64122__$1;
(statearr_64133_66219[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64126 === (1))){
var inst_64080 = cljs.core.vec(chs);
var inst_64083 = inst_64080;
var state_64122__$1 = (function (){var statearr_64136 = state_64122;
(statearr_64136[(10)] = inst_64083);

return statearr_64136;
})();
var statearr_64138_66221 = state_64122__$1;
(statearr_64138_66221[(2)] = null);

(statearr_64138_66221[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64126 === (4))){
var inst_64083 = (state_64122[(10)]);
var state_64122__$1 = state_64122;
return cljs.core.async.ioc_alts_BANG_(state_64122__$1,(7),inst_64083);
} else {
if((state_val_64126 === (6))){
var inst_64118 = (state_64122[(2)]);
var state_64122__$1 = state_64122;
var statearr_64144_66223 = state_64122__$1;
(statearr_64144_66223[(2)] = inst_64118);

(statearr_64144_66223[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64126 === (3))){
var inst_64120 = (state_64122[(2)]);
var state_64122__$1 = state_64122;
return cljs.core.async.impl.ioc_helpers.return_chan(state_64122__$1,inst_64120);
} else {
if((state_val_64126 === (2))){
var inst_64083 = (state_64122[(10)]);
var inst_64085 = cljs.core.count(inst_64083);
var inst_64086 = (inst_64085 > (0));
var state_64122__$1 = state_64122;
if(cljs.core.truth_(inst_64086)){
var statearr_64150_66226 = state_64122__$1;
(statearr_64150_66226[(1)] = (4));

} else {
var statearr_64155_66227 = state_64122__$1;
(statearr_64155_66227[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64126 === (11))){
var inst_64083 = (state_64122[(10)]);
var inst_64111 = (state_64122[(2)]);
var tmp64145 = inst_64083;
var inst_64083__$1 = tmp64145;
var state_64122__$1 = (function (){var statearr_64156 = state_64122;
(statearr_64156[(10)] = inst_64083__$1);

(statearr_64156[(11)] = inst_64111);

return statearr_64156;
})();
var statearr_64158_66228 = state_64122__$1;
(statearr_64158_66228[(2)] = null);

(statearr_64158_66228[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64126 === (9))){
var inst_64098 = (state_64122[(7)]);
var state_64122__$1 = state_64122;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64122__$1,(11),out,inst_64098);
} else {
if((state_val_64126 === (5))){
var inst_64116 = cljs.core.async.close_BANG_(out);
var state_64122__$1 = state_64122;
var statearr_64164_66229 = state_64122__$1;
(statearr_64164_66229[(2)] = inst_64116);

(statearr_64164_66229[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64126 === (10))){
var inst_64114 = (state_64122[(2)]);
var state_64122__$1 = state_64122;
var statearr_64169_66231 = state_64122__$1;
(statearr_64169_66231[(2)] = inst_64114);

(statearr_64169_66231[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64126 === (8))){
var inst_64098 = (state_64122[(7)]);
var inst_64099 = (state_64122[(9)]);
var inst_64083 = (state_64122[(10)]);
var inst_64096 = (state_64122[(8)]);
var inst_64105 = (function (){var cs = inst_64083;
var vec__64089 = inst_64096;
var v = inst_64098;
var c = inst_64099;
return (function (p1__64071_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__64071_SHARP_);
});
})();
var inst_64107 = cljs.core.filterv(inst_64105,inst_64083);
var inst_64083__$1 = inst_64107;
var state_64122__$1 = (function (){var statearr_64171 = state_64122;
(statearr_64171[(10)] = inst_64083__$1);

return statearr_64171;
})();
var statearr_64173_66234 = state_64122__$1;
(statearr_64173_66234[(2)] = null);

(statearr_64173_66234[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_64179 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_64179[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_64179[(1)] = (1));

return statearr_64179;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_64122){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_64122);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e64184){if((e64184 instanceof Object)){
var ex__61709__auto__ = e64184;
var statearr_64187_66236 = state_64122;
(statearr_64187_66236[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_64122);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e64184;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66238 = state_64122;
state_64122 = G__66238;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_64122){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_64122);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_64194 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_64194[(6)] = c__61983__auto___66202);

return statearr_64194;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__64218 = arguments.length;
switch (G__64218) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__61983__auto___66246 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_64254){
var state_val_64255 = (state_64254[(1)]);
if((state_val_64255 === (7))){
var inst_64236 = (state_64254[(7)]);
var inst_64236__$1 = (state_64254[(2)]);
var inst_64237 = (inst_64236__$1 == null);
var inst_64238 = cljs.core.not(inst_64237);
var state_64254__$1 = (function (){var statearr_64266 = state_64254;
(statearr_64266[(7)] = inst_64236__$1);

return statearr_64266;
})();
if(inst_64238){
var statearr_64274_66251 = state_64254__$1;
(statearr_64274_66251[(1)] = (8));

} else {
var statearr_64275_66252 = state_64254__$1;
(statearr_64275_66252[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64255 === (1))){
var inst_64227 = (0);
var state_64254__$1 = (function (){var statearr_64279 = state_64254;
(statearr_64279[(8)] = inst_64227);

return statearr_64279;
})();
var statearr_64285_66259 = state_64254__$1;
(statearr_64285_66259[(2)] = null);

(statearr_64285_66259[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64255 === (4))){
var state_64254__$1 = state_64254;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_64254__$1,(7),ch);
} else {
if((state_val_64255 === (6))){
var inst_64249 = (state_64254[(2)]);
var state_64254__$1 = state_64254;
var statearr_64297_66269 = state_64254__$1;
(statearr_64297_66269[(2)] = inst_64249);

(statearr_64297_66269[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64255 === (3))){
var inst_64251 = (state_64254[(2)]);
var inst_64252 = cljs.core.async.close_BANG_(out);
var state_64254__$1 = (function (){var statearr_64301 = state_64254;
(statearr_64301[(9)] = inst_64251);

return statearr_64301;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_64254__$1,inst_64252);
} else {
if((state_val_64255 === (2))){
var inst_64227 = (state_64254[(8)]);
var inst_64233 = (inst_64227 < n);
var state_64254__$1 = state_64254;
if(cljs.core.truth_(inst_64233)){
var statearr_64306_66289 = state_64254__$1;
(statearr_64306_66289[(1)] = (4));

} else {
var statearr_64310_66294 = state_64254__$1;
(statearr_64310_66294[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64255 === (11))){
var inst_64227 = (state_64254[(8)]);
var inst_64241 = (state_64254[(2)]);
var inst_64242 = (inst_64227 + (1));
var inst_64227__$1 = inst_64242;
var state_64254__$1 = (function (){var statearr_64311 = state_64254;
(statearr_64311[(10)] = inst_64241);

(statearr_64311[(8)] = inst_64227__$1);

return statearr_64311;
})();
var statearr_64314_66307 = state_64254__$1;
(statearr_64314_66307[(2)] = null);

(statearr_64314_66307[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64255 === (9))){
var state_64254__$1 = state_64254;
var statearr_64319_66313 = state_64254__$1;
(statearr_64319_66313[(2)] = null);

(statearr_64319_66313[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64255 === (5))){
var state_64254__$1 = state_64254;
var statearr_64323_66319 = state_64254__$1;
(statearr_64323_66319[(2)] = null);

(statearr_64323_66319[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64255 === (10))){
var inst_64246 = (state_64254[(2)]);
var state_64254__$1 = state_64254;
var statearr_64324_66327 = state_64254__$1;
(statearr_64324_66327[(2)] = inst_64246);

(statearr_64324_66327[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64255 === (8))){
var inst_64236 = (state_64254[(7)]);
var state_64254__$1 = state_64254;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64254__$1,(11),out,inst_64236);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_64332 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_64332[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_64332[(1)] = (1));

return statearr_64332;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_64254){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_64254);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e64335){if((e64335 instanceof Object)){
var ex__61709__auto__ = e64335;
var statearr_64336_66349 = state_64254;
(statearr_64336_66349[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_64254);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e64335;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66350 = state_64254;
state_64254 = G__66350;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_64254){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_64254);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_64342 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_64342[(6)] = c__61983__auto___66246);

return statearr_64342;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async64347 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async64347 = (function (f,ch,meta64348){
this.f = f;
this.ch = ch;
this.meta64348 = meta64348;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_64349,meta64348__$1){
var self__ = this;
var _64349__$1 = this;
return (new cljs.core.async.t_cljs$core$async64347(self__.f,self__.ch,meta64348__$1));
}));

(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_64349){
var self__ = this;
var _64349__$1 = this;
return self__.meta64348;
}));

(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async64354 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async64354 = (function (f,ch,meta64348,_,fn1,meta64355){
this.f = f;
this.ch = ch;
this.meta64348 = meta64348;
this._ = _;
this.fn1 = fn1;
this.meta64355 = meta64355;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async64354.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_64356,meta64355__$1){
var self__ = this;
var _64356__$1 = this;
return (new cljs.core.async.t_cljs$core$async64354(self__.f,self__.ch,self__.meta64348,self__._,self__.fn1,meta64355__$1));
}));

(cljs.core.async.t_cljs$core$async64354.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_64356){
var self__ = this;
var _64356__$1 = this;
return self__.meta64355;
}));

(cljs.core.async.t_cljs$core$async64354.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64354.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async64354.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async64354.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__64344_SHARP_){
var G__64363 = (((p1__64344_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__64344_SHARP_) : self__.f.call(null,p1__64344_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__64363) : f1.call(null,G__64363));
});
}));

(cljs.core.async.t_cljs$core$async64354.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta64348","meta64348",-1932694986,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async64347","cljs.core.async/t_cljs$core$async64347",134854595,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta64355","meta64355",-1784889121,null)], null);
}));

(cljs.core.async.t_cljs$core$async64354.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async64354.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async64354");

(cljs.core.async.t_cljs$core$async64354.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async64354");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async64354.
 */
cljs.core.async.__GT_t_cljs$core$async64354 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async64354(f__$1,ch__$1,meta64348__$1,___$2,fn1__$1,meta64355){
return (new cljs.core.async.t_cljs$core$async64354(f__$1,ch__$1,meta64348__$1,___$2,fn1__$1,meta64355));
});

}

return (new cljs.core.async.t_cljs$core$async64354(self__.f,self__.ch,self__.meta64348,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4115__auto__ = ret;
if(cljs.core.truth_(and__4115__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4115__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__64369 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__64369) : self__.f.call(null,G__64369));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64347.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async64347.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta64348","meta64348",-1932694986,null)], null);
}));

(cljs.core.async.t_cljs$core$async64347.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async64347.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async64347");

(cljs.core.async.t_cljs$core$async64347.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async64347");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async64347.
 */
cljs.core.async.__GT_t_cljs$core$async64347 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async64347(f__$1,ch__$1,meta64348){
return (new cljs.core.async.t_cljs$core$async64347(f__$1,ch__$1,meta64348));
});

}

return (new cljs.core.async.t_cljs$core$async64347(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async64374 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async64374 = (function (f,ch,meta64375){
this.f = f;
this.ch = ch;
this.meta64375 = meta64375;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async64374.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_64376,meta64375__$1){
var self__ = this;
var _64376__$1 = this;
return (new cljs.core.async.t_cljs$core$async64374(self__.f,self__.ch,meta64375__$1));
}));

(cljs.core.async.t_cljs$core$async64374.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_64376){
var self__ = this;
var _64376__$1 = this;
return self__.meta64375;
}));

(cljs.core.async.t_cljs$core$async64374.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64374.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async64374.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64374.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async64374.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64374.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async64374.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta64375","meta64375",526205941,null)], null);
}));

(cljs.core.async.t_cljs$core$async64374.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async64374.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async64374");

(cljs.core.async.t_cljs$core$async64374.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async64374");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async64374.
 */
cljs.core.async.__GT_t_cljs$core$async64374 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async64374(f__$1,ch__$1,meta64375){
return (new cljs.core.async.t_cljs$core$async64374(f__$1,ch__$1,meta64375));
});

}

return (new cljs.core.async.t_cljs$core$async64374(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async64396 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async64396 = (function (p,ch,meta64397){
this.p = p;
this.ch = ch;
this.meta64397 = meta64397;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_64398,meta64397__$1){
var self__ = this;
var _64398__$1 = this;
return (new cljs.core.async.t_cljs$core$async64396(self__.p,self__.ch,meta64397__$1));
}));

(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_64398){
var self__ = this;
var _64398__$1 = this;
return self__.meta64397;
}));

(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async64396.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async64396.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta64397","meta64397",1128241325,null)], null);
}));

(cljs.core.async.t_cljs$core$async64396.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async64396.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async64396");

(cljs.core.async.t_cljs$core$async64396.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async64396");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async64396.
 */
cljs.core.async.__GT_t_cljs$core$async64396 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async64396(p__$1,ch__$1,meta64397){
return (new cljs.core.async.t_cljs$core$async64396(p__$1,ch__$1,meta64397));
});

}

return (new cljs.core.async.t_cljs$core$async64396(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__64420 = arguments.length;
switch (G__64420) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__61983__auto___66437 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_64455){
var state_val_64456 = (state_64455[(1)]);
if((state_val_64456 === (7))){
var inst_64451 = (state_64455[(2)]);
var state_64455__$1 = state_64455;
var statearr_64464_66438 = state_64455__$1;
(statearr_64464_66438[(2)] = inst_64451);

(statearr_64464_66438[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64456 === (1))){
var state_64455__$1 = state_64455;
var statearr_64465_66439 = state_64455__$1;
(statearr_64465_66439[(2)] = null);

(statearr_64465_66439[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64456 === (4))){
var inst_64432 = (state_64455[(7)]);
var inst_64432__$1 = (state_64455[(2)]);
var inst_64434 = (inst_64432__$1 == null);
var state_64455__$1 = (function (){var statearr_64470 = state_64455;
(statearr_64470[(7)] = inst_64432__$1);

return statearr_64470;
})();
if(cljs.core.truth_(inst_64434)){
var statearr_64471_66443 = state_64455__$1;
(statearr_64471_66443[(1)] = (5));

} else {
var statearr_64472_66444 = state_64455__$1;
(statearr_64472_66444[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64456 === (6))){
var inst_64432 = (state_64455[(7)]);
var inst_64441 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_64432) : p.call(null,inst_64432));
var state_64455__$1 = state_64455;
if(cljs.core.truth_(inst_64441)){
var statearr_64473_66449 = state_64455__$1;
(statearr_64473_66449[(1)] = (8));

} else {
var statearr_64477_66450 = state_64455__$1;
(statearr_64477_66450[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64456 === (3))){
var inst_64453 = (state_64455[(2)]);
var state_64455__$1 = state_64455;
return cljs.core.async.impl.ioc_helpers.return_chan(state_64455__$1,inst_64453);
} else {
if((state_val_64456 === (2))){
var state_64455__$1 = state_64455;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_64455__$1,(4),ch);
} else {
if((state_val_64456 === (11))){
var inst_64444 = (state_64455[(2)]);
var state_64455__$1 = state_64455;
var statearr_64486_66462 = state_64455__$1;
(statearr_64486_66462[(2)] = inst_64444);

(statearr_64486_66462[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64456 === (9))){
var state_64455__$1 = state_64455;
var statearr_64487_66463 = state_64455__$1;
(statearr_64487_66463[(2)] = null);

(statearr_64487_66463[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64456 === (5))){
var inst_64439 = cljs.core.async.close_BANG_(out);
var state_64455__$1 = state_64455;
var statearr_64488_66465 = state_64455__$1;
(statearr_64488_66465[(2)] = inst_64439);

(statearr_64488_66465[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64456 === (10))){
var inst_64447 = (state_64455[(2)]);
var state_64455__$1 = (function (){var statearr_64490 = state_64455;
(statearr_64490[(8)] = inst_64447);

return statearr_64490;
})();
var statearr_64491_66469 = state_64455__$1;
(statearr_64491_66469[(2)] = null);

(statearr_64491_66469[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64456 === (8))){
var inst_64432 = (state_64455[(7)]);
var state_64455__$1 = state_64455;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64455__$1,(11),out,inst_64432);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_64493 = [null,null,null,null,null,null,null,null,null];
(statearr_64493[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_64493[(1)] = (1));

return statearr_64493;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_64455){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_64455);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e64497){if((e64497 instanceof Object)){
var ex__61709__auto__ = e64497;
var statearr_64498_66470 = state_64455;
(statearr_64498_66470[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_64455);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e64497;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66475 = state_64455;
state_64455 = G__66475;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_64455){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_64455);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_64501 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_64501[(6)] = c__61983__auto___66437);

return statearr_64501;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__64507 = arguments.length;
switch (G__64507) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__61983__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_64587){
var state_val_64588 = (state_64587[(1)]);
if((state_val_64588 === (7))){
var inst_64580 = (state_64587[(2)]);
var state_64587__$1 = state_64587;
var statearr_64591_66477 = state_64587__$1;
(statearr_64591_66477[(2)] = inst_64580);

(statearr_64591_66477[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (20))){
var inst_64549 = (state_64587[(7)]);
var inst_64561 = (state_64587[(2)]);
var inst_64562 = cljs.core.next(inst_64549);
var inst_64535 = inst_64562;
var inst_64536 = null;
var inst_64537 = (0);
var inst_64538 = (0);
var state_64587__$1 = (function (){var statearr_64592 = state_64587;
(statearr_64592[(8)] = inst_64538);

(statearr_64592[(9)] = inst_64561);

(statearr_64592[(10)] = inst_64535);

(statearr_64592[(11)] = inst_64537);

(statearr_64592[(12)] = inst_64536);

return statearr_64592;
})();
var statearr_64594_66478 = state_64587__$1;
(statearr_64594_66478[(2)] = null);

(statearr_64594_66478[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (1))){
var state_64587__$1 = state_64587;
var statearr_64598_66481 = state_64587__$1;
(statearr_64598_66481[(2)] = null);

(statearr_64598_66481[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (4))){
var inst_64523 = (state_64587[(13)]);
var inst_64523__$1 = (state_64587[(2)]);
var inst_64525 = (inst_64523__$1 == null);
var state_64587__$1 = (function (){var statearr_64599 = state_64587;
(statearr_64599[(13)] = inst_64523__$1);

return statearr_64599;
})();
if(cljs.core.truth_(inst_64525)){
var statearr_64602_66482 = state_64587__$1;
(statearr_64602_66482[(1)] = (5));

} else {
var statearr_64603_66483 = state_64587__$1;
(statearr_64603_66483[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (15))){
var state_64587__$1 = state_64587;
var statearr_64607_66484 = state_64587__$1;
(statearr_64607_66484[(2)] = null);

(statearr_64607_66484[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (21))){
var state_64587__$1 = state_64587;
var statearr_64609_66486 = state_64587__$1;
(statearr_64609_66486[(2)] = null);

(statearr_64609_66486[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (13))){
var inst_64538 = (state_64587[(8)]);
var inst_64535 = (state_64587[(10)]);
var inst_64537 = (state_64587[(11)]);
var inst_64536 = (state_64587[(12)]);
var inst_64545 = (state_64587[(2)]);
var inst_64546 = (inst_64538 + (1));
var tmp64604 = inst_64535;
var tmp64605 = inst_64537;
var tmp64606 = inst_64536;
var inst_64535__$1 = tmp64604;
var inst_64536__$1 = tmp64606;
var inst_64537__$1 = tmp64605;
var inst_64538__$1 = inst_64546;
var state_64587__$1 = (function (){var statearr_64610 = state_64587;
(statearr_64610[(8)] = inst_64538__$1);

(statearr_64610[(10)] = inst_64535__$1);

(statearr_64610[(11)] = inst_64537__$1);

(statearr_64610[(14)] = inst_64545);

(statearr_64610[(12)] = inst_64536__$1);

return statearr_64610;
})();
var statearr_64611_66487 = state_64587__$1;
(statearr_64611_66487[(2)] = null);

(statearr_64611_66487[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (22))){
var state_64587__$1 = state_64587;
var statearr_64612_66488 = state_64587__$1;
(statearr_64612_66488[(2)] = null);

(statearr_64612_66488[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (6))){
var inst_64523 = (state_64587[(13)]);
var inst_64533 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_64523) : f.call(null,inst_64523));
var inst_64534 = cljs.core.seq(inst_64533);
var inst_64535 = inst_64534;
var inst_64536 = null;
var inst_64537 = (0);
var inst_64538 = (0);
var state_64587__$1 = (function (){var statearr_64614 = state_64587;
(statearr_64614[(8)] = inst_64538);

(statearr_64614[(10)] = inst_64535);

(statearr_64614[(11)] = inst_64537);

(statearr_64614[(12)] = inst_64536);

return statearr_64614;
})();
var statearr_64616_66491 = state_64587__$1;
(statearr_64616_66491[(2)] = null);

(statearr_64616_66491[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (17))){
var inst_64549 = (state_64587[(7)]);
var inst_64554 = cljs.core.chunk_first(inst_64549);
var inst_64555 = cljs.core.chunk_rest(inst_64549);
var inst_64556 = cljs.core.count(inst_64554);
var inst_64535 = inst_64555;
var inst_64536 = inst_64554;
var inst_64537 = inst_64556;
var inst_64538 = (0);
var state_64587__$1 = (function (){var statearr_64620 = state_64587;
(statearr_64620[(8)] = inst_64538);

(statearr_64620[(10)] = inst_64535);

(statearr_64620[(11)] = inst_64537);

(statearr_64620[(12)] = inst_64536);

return statearr_64620;
})();
var statearr_64621_66493 = state_64587__$1;
(statearr_64621_66493[(2)] = null);

(statearr_64621_66493[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (3))){
var inst_64582 = (state_64587[(2)]);
var state_64587__$1 = state_64587;
return cljs.core.async.impl.ioc_helpers.return_chan(state_64587__$1,inst_64582);
} else {
if((state_val_64588 === (12))){
var inst_64570 = (state_64587[(2)]);
var state_64587__$1 = state_64587;
var statearr_64622_66495 = state_64587__$1;
(statearr_64622_66495[(2)] = inst_64570);

(statearr_64622_66495[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (2))){
var state_64587__$1 = state_64587;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_64587__$1,(4),in$);
} else {
if((state_val_64588 === (23))){
var inst_64578 = (state_64587[(2)]);
var state_64587__$1 = state_64587;
var statearr_64624_66500 = state_64587__$1;
(statearr_64624_66500[(2)] = inst_64578);

(statearr_64624_66500[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (19))){
var inst_64565 = (state_64587[(2)]);
var state_64587__$1 = state_64587;
var statearr_64625_66502 = state_64587__$1;
(statearr_64625_66502[(2)] = inst_64565);

(statearr_64625_66502[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (11))){
var inst_64549 = (state_64587[(7)]);
var inst_64535 = (state_64587[(10)]);
var inst_64549__$1 = cljs.core.seq(inst_64535);
var state_64587__$1 = (function (){var statearr_64626 = state_64587;
(statearr_64626[(7)] = inst_64549__$1);

return statearr_64626;
})();
if(inst_64549__$1){
var statearr_64627_66503 = state_64587__$1;
(statearr_64627_66503[(1)] = (14));

} else {
var statearr_64628_66504 = state_64587__$1;
(statearr_64628_66504[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (9))){
var inst_64572 = (state_64587[(2)]);
var inst_64573 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_64587__$1 = (function (){var statearr_64635 = state_64587;
(statearr_64635[(15)] = inst_64572);

return statearr_64635;
})();
if(cljs.core.truth_(inst_64573)){
var statearr_64640_66506 = state_64587__$1;
(statearr_64640_66506[(1)] = (21));

} else {
var statearr_64641_66507 = state_64587__$1;
(statearr_64641_66507[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (5))){
var inst_64527 = cljs.core.async.close_BANG_(out);
var state_64587__$1 = state_64587;
var statearr_64642_66512 = state_64587__$1;
(statearr_64642_66512[(2)] = inst_64527);

(statearr_64642_66512[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (14))){
var inst_64549 = (state_64587[(7)]);
var inst_64552 = cljs.core.chunked_seq_QMARK_(inst_64549);
var state_64587__$1 = state_64587;
if(inst_64552){
var statearr_64649_66513 = state_64587__$1;
(statearr_64649_66513[(1)] = (17));

} else {
var statearr_64653_66514 = state_64587__$1;
(statearr_64653_66514[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (16))){
var inst_64568 = (state_64587[(2)]);
var state_64587__$1 = state_64587;
var statearr_64660_66515 = state_64587__$1;
(statearr_64660_66515[(2)] = inst_64568);

(statearr_64660_66515[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64588 === (10))){
var inst_64538 = (state_64587[(8)]);
var inst_64536 = (state_64587[(12)]);
var inst_64543 = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(inst_64536,inst_64538);
var state_64587__$1 = state_64587;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64587__$1,(13),out,inst_64543);
} else {
if((state_val_64588 === (18))){
var inst_64549 = (state_64587[(7)]);
var inst_64559 = cljs.core.first(inst_64549);
var state_64587__$1 = state_64587;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64587__$1,(20),out,inst_64559);
} else {
if((state_val_64588 === (8))){
var inst_64538 = (state_64587[(8)]);
var inst_64537 = (state_64587[(11)]);
var inst_64540 = (inst_64538 < inst_64537);
var inst_64541 = inst_64540;
var state_64587__$1 = state_64587;
if(cljs.core.truth_(inst_64541)){
var statearr_64673_66516 = state_64587__$1;
(statearr_64673_66516[(1)] = (10));

} else {
var statearr_64674_66518 = state_64587__$1;
(statearr_64674_66518[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__61706__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__61706__auto____0 = (function (){
var statearr_64675 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_64675[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__61706__auto__);

(statearr_64675[(1)] = (1));

return statearr_64675;
});
var cljs$core$async$mapcat_STAR__$_state_machine__61706__auto____1 = (function (state_64587){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_64587);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e64676){if((e64676 instanceof Object)){
var ex__61709__auto__ = e64676;
var statearr_64677_66519 = state_64587;
(statearr_64677_66519[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_64587);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e64676;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66522 = state_64587;
state_64587 = G__66522;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__61706__auto__ = function(state_64587){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__61706__auto____1.call(this,state_64587);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__61706__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__61706__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_64685 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_64685[(6)] = c__61983__auto__);

return statearr_64685;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));

return c__61983__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__64688 = arguments.length;
switch (G__64688) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__64693 = arguments.length;
switch (G__64693) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__64703 = arguments.length;
switch (G__64703) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__61983__auto___66542 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_64730){
var state_val_64731 = (state_64730[(1)]);
if((state_val_64731 === (7))){
var inst_64725 = (state_64730[(2)]);
var state_64730__$1 = state_64730;
var statearr_64739_66543 = state_64730__$1;
(statearr_64739_66543[(2)] = inst_64725);

(statearr_64739_66543[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64731 === (1))){
var inst_64707 = null;
var state_64730__$1 = (function (){var statearr_64741 = state_64730;
(statearr_64741[(7)] = inst_64707);

return statearr_64741;
})();
var statearr_64745_66544 = state_64730__$1;
(statearr_64745_66544[(2)] = null);

(statearr_64745_66544[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64731 === (4))){
var inst_64710 = (state_64730[(8)]);
var inst_64710__$1 = (state_64730[(2)]);
var inst_64711 = (inst_64710__$1 == null);
var inst_64712 = cljs.core.not(inst_64711);
var state_64730__$1 = (function (){var statearr_64751 = state_64730;
(statearr_64751[(8)] = inst_64710__$1);

return statearr_64751;
})();
if(inst_64712){
var statearr_64752_66545 = state_64730__$1;
(statearr_64752_66545[(1)] = (5));

} else {
var statearr_64753_66550 = state_64730__$1;
(statearr_64753_66550[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64731 === (6))){
var state_64730__$1 = state_64730;
var statearr_64754_66554 = state_64730__$1;
(statearr_64754_66554[(2)] = null);

(statearr_64754_66554[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64731 === (3))){
var inst_64727 = (state_64730[(2)]);
var inst_64728 = cljs.core.async.close_BANG_(out);
var state_64730__$1 = (function (){var statearr_64756 = state_64730;
(statearr_64756[(9)] = inst_64727);

return statearr_64756;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_64730__$1,inst_64728);
} else {
if((state_val_64731 === (2))){
var state_64730__$1 = state_64730;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_64730__$1,(4),ch);
} else {
if((state_val_64731 === (11))){
var inst_64710 = (state_64730[(8)]);
var inst_64719 = (state_64730[(2)]);
var inst_64707 = inst_64710;
var state_64730__$1 = (function (){var statearr_64761 = state_64730;
(statearr_64761[(10)] = inst_64719);

(statearr_64761[(7)] = inst_64707);

return statearr_64761;
})();
var statearr_64762_66569 = state_64730__$1;
(statearr_64762_66569[(2)] = null);

(statearr_64762_66569[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64731 === (9))){
var inst_64710 = (state_64730[(8)]);
var state_64730__$1 = state_64730;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64730__$1,(11),out,inst_64710);
} else {
if((state_val_64731 === (5))){
var inst_64710 = (state_64730[(8)]);
var inst_64707 = (state_64730[(7)]);
var inst_64714 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_64710,inst_64707);
var state_64730__$1 = state_64730;
if(inst_64714){
var statearr_64765_66574 = state_64730__$1;
(statearr_64765_66574[(1)] = (8));

} else {
var statearr_64768_66575 = state_64730__$1;
(statearr_64768_66575[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64731 === (10))){
var inst_64722 = (state_64730[(2)]);
var state_64730__$1 = state_64730;
var statearr_64770_66576 = state_64730__$1;
(statearr_64770_66576[(2)] = inst_64722);

(statearr_64770_66576[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64731 === (8))){
var inst_64707 = (state_64730[(7)]);
var tmp64763 = inst_64707;
var inst_64707__$1 = tmp64763;
var state_64730__$1 = (function (){var statearr_64771 = state_64730;
(statearr_64771[(7)] = inst_64707__$1);

return statearr_64771;
})();
var statearr_64774_66582 = state_64730__$1;
(statearr_64774_66582[(2)] = null);

(statearr_64774_66582[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_64778 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_64778[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_64778[(1)] = (1));

return statearr_64778;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_64730){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_64730);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e64779){if((e64779 instanceof Object)){
var ex__61709__auto__ = e64779;
var statearr_64780_66607 = state_64730;
(statearr_64780_66607[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_64730);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e64779;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66613 = state_64730;
state_64730 = G__66613;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_64730){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_64730);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_64788 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_64788[(6)] = c__61983__auto___66542);

return statearr_64788;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__64800 = arguments.length;
switch (G__64800) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__61983__auto___66650 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_64852){
var state_val_64856 = (state_64852[(1)]);
if((state_val_64856 === (7))){
var inst_64848 = (state_64852[(2)]);
var state_64852__$1 = state_64852;
var statearr_64872_66664 = state_64852__$1;
(statearr_64872_66664[(2)] = inst_64848);

(statearr_64872_66664[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (1))){
var inst_64808 = (new Array(n));
var inst_64809 = inst_64808;
var inst_64810 = (0);
var state_64852__$1 = (function (){var statearr_64880 = state_64852;
(statearr_64880[(7)] = inst_64810);

(statearr_64880[(8)] = inst_64809);

return statearr_64880;
})();
var statearr_64883_66682 = state_64852__$1;
(statearr_64883_66682[(2)] = null);

(statearr_64883_66682[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (4))){
var inst_64814 = (state_64852[(9)]);
var inst_64814__$1 = (state_64852[(2)]);
var inst_64815 = (inst_64814__$1 == null);
var inst_64816 = cljs.core.not(inst_64815);
var state_64852__$1 = (function (){var statearr_64887 = state_64852;
(statearr_64887[(9)] = inst_64814__$1);

return statearr_64887;
})();
if(inst_64816){
var statearr_64888_66694 = state_64852__$1;
(statearr_64888_66694[(1)] = (5));

} else {
var statearr_64892_66695 = state_64852__$1;
(statearr_64892_66695[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (15))){
var inst_64842 = (state_64852[(2)]);
var state_64852__$1 = state_64852;
var statearr_64893_66696 = state_64852__$1;
(statearr_64893_66696[(2)] = inst_64842);

(statearr_64893_66696[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (13))){
var state_64852__$1 = state_64852;
var statearr_64894_66697 = state_64852__$1;
(statearr_64894_66697[(2)] = null);

(statearr_64894_66697[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (6))){
var inst_64810 = (state_64852[(7)]);
var inst_64835 = (inst_64810 > (0));
var state_64852__$1 = state_64852;
if(cljs.core.truth_(inst_64835)){
var statearr_64896_66701 = state_64852__$1;
(statearr_64896_66701[(1)] = (12));

} else {
var statearr_64898_66704 = state_64852__$1;
(statearr_64898_66704[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (3))){
var inst_64850 = (state_64852[(2)]);
var state_64852__$1 = state_64852;
return cljs.core.async.impl.ioc_helpers.return_chan(state_64852__$1,inst_64850);
} else {
if((state_val_64856 === (12))){
var inst_64809 = (state_64852[(8)]);
var inst_64840 = cljs.core.vec(inst_64809);
var state_64852__$1 = state_64852;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64852__$1,(15),out,inst_64840);
} else {
if((state_val_64856 === (2))){
var state_64852__$1 = state_64852;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_64852__$1,(4),ch);
} else {
if((state_val_64856 === (11))){
var inst_64829 = (state_64852[(2)]);
var inst_64830 = (new Array(n));
var inst_64809 = inst_64830;
var inst_64810 = (0);
var state_64852__$1 = (function (){var statearr_64906 = state_64852;
(statearr_64906[(7)] = inst_64810);

(statearr_64906[(10)] = inst_64829);

(statearr_64906[(8)] = inst_64809);

return statearr_64906;
})();
var statearr_64908_66707 = state_64852__$1;
(statearr_64908_66707[(2)] = null);

(statearr_64908_66707[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (9))){
var inst_64809 = (state_64852[(8)]);
var inst_64827 = cljs.core.vec(inst_64809);
var state_64852__$1 = state_64852;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_64852__$1,(11),out,inst_64827);
} else {
if((state_val_64856 === (5))){
var inst_64810 = (state_64852[(7)]);
var inst_64814 = (state_64852[(9)]);
var inst_64820 = (state_64852[(11)]);
var inst_64809 = (state_64852[(8)]);
var inst_64819 = (inst_64809[inst_64810] = inst_64814);
var inst_64820__$1 = (inst_64810 + (1));
var inst_64822 = (inst_64820__$1 < n);
var state_64852__$1 = (function (){var statearr_64913 = state_64852;
(statearr_64913[(11)] = inst_64820__$1);

(statearr_64913[(12)] = inst_64819);

return statearr_64913;
})();
if(cljs.core.truth_(inst_64822)){
var statearr_64915_66710 = state_64852__$1;
(statearr_64915_66710[(1)] = (8));

} else {
var statearr_64916_66711 = state_64852__$1;
(statearr_64916_66711[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (14))){
var inst_64845 = (state_64852[(2)]);
var inst_64846 = cljs.core.async.close_BANG_(out);
var state_64852__$1 = (function (){var statearr_64918 = state_64852;
(statearr_64918[(13)] = inst_64845);

return statearr_64918;
})();
var statearr_64920_66715 = state_64852__$1;
(statearr_64920_66715[(2)] = inst_64846);

(statearr_64920_66715[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (10))){
var inst_64833 = (state_64852[(2)]);
var state_64852__$1 = state_64852;
var statearr_64927_66717 = state_64852__$1;
(statearr_64927_66717[(2)] = inst_64833);

(statearr_64927_66717[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_64856 === (8))){
var inst_64820 = (state_64852[(11)]);
var inst_64809 = (state_64852[(8)]);
var tmp64917 = inst_64809;
var inst_64809__$1 = tmp64917;
var inst_64810 = inst_64820;
var state_64852__$1 = (function (){var statearr_64933 = state_64852;
(statearr_64933[(7)] = inst_64810);

(statearr_64933[(8)] = inst_64809__$1);

return statearr_64933;
})();
var statearr_64934_66720 = state_64852__$1;
(statearr_64934_66720[(2)] = null);

(statearr_64934_66720[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_64937 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_64937[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_64937[(1)] = (1));

return statearr_64937;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_64852){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_64852);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e64938){if((e64938 instanceof Object)){
var ex__61709__auto__ = e64938;
var statearr_64941_66724 = state_64852;
(statearr_64941_66724[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_64852);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e64938;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66725 = state_64852;
state_64852 = G__66725;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_64852){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_64852);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_64944 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_64944[(6)] = c__61983__auto___66650);

return statearr_64944;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__64952 = arguments.length;
switch (G__64952) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__61983__auto___66730 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__61984__auto__ = (function (){var switch__61705__auto__ = (function (state_65010){
var state_val_65011 = (state_65010[(1)]);
if((state_val_65011 === (7))){
var inst_65004 = (state_65010[(2)]);
var state_65010__$1 = state_65010;
var statearr_65017_66731 = state_65010__$1;
(statearr_65017_66731[(2)] = inst_65004);

(statearr_65017_66731[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (1))){
var inst_64956 = [];
var inst_64957 = inst_64956;
var inst_64958 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_65010__$1 = (function (){var statearr_65020 = state_65010;
(statearr_65020[(7)] = inst_64957);

(statearr_65020[(8)] = inst_64958);

return statearr_65020;
})();
var statearr_65027_66732 = state_65010__$1;
(statearr_65027_66732[(2)] = null);

(statearr_65027_66732[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (4))){
var inst_64961 = (state_65010[(9)]);
var inst_64961__$1 = (state_65010[(2)]);
var inst_64962 = (inst_64961__$1 == null);
var inst_64963 = cljs.core.not(inst_64962);
var state_65010__$1 = (function (){var statearr_65034 = state_65010;
(statearr_65034[(9)] = inst_64961__$1);

return statearr_65034;
})();
if(inst_64963){
var statearr_65035_66733 = state_65010__$1;
(statearr_65035_66733[(1)] = (5));

} else {
var statearr_65036_66734 = state_65010__$1;
(statearr_65036_66734[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (15))){
var inst_64998 = (state_65010[(2)]);
var state_65010__$1 = state_65010;
var statearr_65037_66735 = state_65010__$1;
(statearr_65037_66735[(2)] = inst_64998);

(statearr_65037_66735[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (13))){
var state_65010__$1 = state_65010;
var statearr_65038_66737 = state_65010__$1;
(statearr_65038_66737[(2)] = null);

(statearr_65038_66737[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (6))){
var inst_64957 = (state_65010[(7)]);
var inst_64991 = inst_64957.length;
var inst_64992 = (inst_64991 > (0));
var state_65010__$1 = state_65010;
if(cljs.core.truth_(inst_64992)){
var statearr_65043_66742 = state_65010__$1;
(statearr_65043_66742[(1)] = (12));

} else {
var statearr_65044_66743 = state_65010__$1;
(statearr_65044_66743[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (3))){
var inst_65006 = (state_65010[(2)]);
var state_65010__$1 = state_65010;
return cljs.core.async.impl.ioc_helpers.return_chan(state_65010__$1,inst_65006);
} else {
if((state_val_65011 === (12))){
var inst_64957 = (state_65010[(7)]);
var inst_64996 = cljs.core.vec(inst_64957);
var state_65010__$1 = state_65010;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_65010__$1,(15),out,inst_64996);
} else {
if((state_val_65011 === (2))){
var state_65010__$1 = state_65010;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_65010__$1,(4),ch);
} else {
if((state_val_65011 === (11))){
var inst_64965 = (state_65010[(10)]);
var inst_64961 = (state_65010[(9)]);
var inst_64977 = (state_65010[(2)]);
var inst_64982 = [];
var inst_64985 = inst_64982.push(inst_64961);
var inst_64957 = inst_64982;
var inst_64958 = inst_64965;
var state_65010__$1 = (function (){var statearr_65056 = state_65010;
(statearr_65056[(11)] = inst_64985);

(statearr_65056[(7)] = inst_64957);

(statearr_65056[(8)] = inst_64958);

(statearr_65056[(12)] = inst_64977);

return statearr_65056;
})();
var statearr_65057_66753 = state_65010__$1;
(statearr_65057_66753[(2)] = null);

(statearr_65057_66753[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (9))){
var inst_64957 = (state_65010[(7)]);
var inst_64975 = cljs.core.vec(inst_64957);
var state_65010__$1 = state_65010;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_65010__$1,(11),out,inst_64975);
} else {
if((state_val_65011 === (5))){
var inst_64965 = (state_65010[(10)]);
var inst_64961 = (state_65010[(9)]);
var inst_64958 = (state_65010[(8)]);
var inst_64965__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_64961) : f.call(null,inst_64961));
var inst_64966 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_64965__$1,inst_64958);
var inst_64967 = cljs.core.keyword_identical_QMARK_(inst_64958,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_64968 = ((inst_64966) || (inst_64967));
var state_65010__$1 = (function (){var statearr_65062 = state_65010;
(statearr_65062[(10)] = inst_64965__$1);

return statearr_65062;
})();
if(cljs.core.truth_(inst_64968)){
var statearr_65063_66757 = state_65010__$1;
(statearr_65063_66757[(1)] = (8));

} else {
var statearr_65064_66758 = state_65010__$1;
(statearr_65064_66758[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (14))){
var inst_65001 = (state_65010[(2)]);
var inst_65002 = cljs.core.async.close_BANG_(out);
var state_65010__$1 = (function (){var statearr_65066 = state_65010;
(statearr_65066[(13)] = inst_65001);

return statearr_65066;
})();
var statearr_65068_66760 = state_65010__$1;
(statearr_65068_66760[(2)] = inst_65002);

(statearr_65068_66760[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (10))){
var inst_64988 = (state_65010[(2)]);
var state_65010__$1 = state_65010;
var statearr_65069_66761 = state_65010__$1;
(statearr_65069_66761[(2)] = inst_64988);

(statearr_65069_66761[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_65011 === (8))){
var inst_64965 = (state_65010[(10)]);
var inst_64961 = (state_65010[(9)]);
var inst_64957 = (state_65010[(7)]);
var inst_64970 = inst_64957.push(inst_64961);
var tmp65065 = inst_64957;
var inst_64957__$1 = tmp65065;
var inst_64958 = inst_64965;
var state_65010__$1 = (function (){var statearr_65073 = state_65010;
(statearr_65073[(14)] = inst_64970);

(statearr_65073[(7)] = inst_64957__$1);

(statearr_65073[(8)] = inst_64958);

return statearr_65073;
})();
var statearr_65074_66764 = state_65010__$1;
(statearr_65074_66764[(2)] = null);

(statearr_65074_66764[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__61706__auto__ = null;
var cljs$core$async$state_machine__61706__auto____0 = (function (){
var statearr_65082 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_65082[(0)] = cljs$core$async$state_machine__61706__auto__);

(statearr_65082[(1)] = (1));

return statearr_65082;
});
var cljs$core$async$state_machine__61706__auto____1 = (function (state_65010){
while(true){
var ret_value__61707__auto__ = (function (){try{while(true){
var result__61708__auto__ = switch__61705__auto__(state_65010);
if(cljs.core.keyword_identical_QMARK_(result__61708__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__61708__auto__;
}
break;
}
}catch (e65084){if((e65084 instanceof Object)){
var ex__61709__auto__ = e65084;
var statearr_65085_66766 = state_65010;
(statearr_65085_66766[(5)] = ex__61709__auto__);


cljs.core.async.impl.ioc_helpers.process_exception(state_65010);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e65084;

}
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__61707__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__66767 = state_65010;
state_65010 = G__66767;
continue;
} else {
return ret_value__61707__auto__;
}
break;
}
});
cljs$core$async$state_machine__61706__auto__ = function(state_65010){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__61706__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__61706__auto____1.call(this,state_65010);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__61706__auto____0;
cljs$core$async$state_machine__61706__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__61706__auto____1;
return cljs$core$async$state_machine__61706__auto__;
})()
})();
var state__61985__auto__ = (function (){var statearr_65086 = (f__61984__auto__.cljs$core$IFn$_invoke$arity$0 ? f__61984__auto__.cljs$core$IFn$_invoke$arity$0() : f__61984__auto__.call(null));
(statearr_65086[(6)] = c__61983__auto___66730);

return statearr_65086;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__61985__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
