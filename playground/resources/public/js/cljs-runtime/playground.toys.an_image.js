goog.provide('playground.toys.an_image');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('reagent.dom');
/**
 * Displays a single image.
 */
playground.toys.an_image.an_image = (function playground$toys$an_image$an_image(){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"display","display",242065432),"flex",new cljs.core.Keyword(null,"justify-content","justify-content",-1990475787),"center"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img","img",1442687358),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"src","src",-1651076051),"https://slatestarcodex.com/blog_images/bluetwirl.gif"], null)], null)], null);
});
goog.exportSymbol('playground.toys.an_image.an_image', playground.toys.an_image.an_image);

//# sourceMappingURL=playground.toys.an_image.js.map
