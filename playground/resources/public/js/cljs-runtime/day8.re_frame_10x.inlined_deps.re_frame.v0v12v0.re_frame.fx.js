goog.provide('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx');
goog.require('cljs.core');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.router');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.db');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.interceptor');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.interop');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.events');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers');
goog.require('day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace');
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.kind = new cljs.core.Keyword(null,"fx","fx",-1237829572);
if(cljs.core.truth_((day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1 ? day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.kinds.cljs$core$IFn$_invoke$arity$1(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.kind) : day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.kinds.call(null,day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.kind)))){
} else {
throw (new Error("Assert failed: (registrar/kinds kind)"));
}
/**
 * Register the given effect `handler` for the given `id`.
 * 
 *   `id` is keyword, often namespaced.
 *   `handler` is a side-effecting function which takes a single argument and whose return
 *   value is ignored.
 * 
 *   Example Use
 *   -----------
 * 
 *   First, registration ... associate `:effect2` with a handler.
 * 
 *   (reg-fx
 *   :effect2
 *   (fn [value]
 *      ... do something side-effect-y))
 * 
 *   Then, later, if an event handler were to return this effects map ...
 * 
 *   {...
 * :effect2  [1 2]}
 * 
 * ... then the `handler` `fn` we registered previously, using `reg-fx`, will be
 * called with an argument of `[1 2]`.
 */
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.reg_fx = (function day8$re_frame_10x$inlined_deps$re_frame$v0v12v0$re_frame$fx$reg_fx(id,handler){
return day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.register_handler(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.kind,id,handler);
});
/**
 * An interceptor whose `:after` actions the contents of `:effects`. As a result,
 *   this interceptor is Domino 3.
 * 
 *   This interceptor is silently added (by reg-event-db etc) to the front of
 *   interceptor chains for all events.
 * 
 *   For each key in `:effects` (a map), it calls the registered `effects handler`
 *   (see `reg-fx` for registration of effect handlers).
 * 
 *   So, if `:effects` was:
 *    {:dispatch  [:hello 42]
 *     :db        {...}
 *     :undo      "set flag"}
 * 
 *   it will call the registered effect handlers for each of the map's keys:
 *   `:dispatch`, `:undo` and `:db`. When calling each handler, provides the map
 *   value for that key - so in the example above the effect handler for :dispatch
 *   will be given one arg `[:hello 42]`.
 * 
 *   You cannot rely on the ordering in which effects are executed.
 */
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.do_fx = day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.interceptor.__GT_interceptor.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"id","id",-1388402092),new cljs.core.Keyword(null,"do-fx","do-fx",1194163050),new cljs.core.Keyword(null,"after","after",594996914),(function day8$re_frame_10x$inlined_deps$re_frame$v0v12v0$re_frame$fx$do_fx_after(context){
if(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace.is_trace_enabled_QMARK_()){
var _STAR_current_trace_STAR__orig_val__56560 = day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace._STAR_current_trace_STAR_;
var _STAR_current_trace_STAR__temp_val__56561 = day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace.start_trace(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op-type","op-type",-1636141668),new cljs.core.Keyword("event","do-fx","event/do-fx",1357330452)], null));
(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__temp_val__56561);

try{try{var seq__56565 = cljs.core.seq(new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__56566 = null;
var count__56567 = (0);
var i__56568 = (0);
while(true){
if((i__56568 < count__56567)){
var vec__56595 = chunk__56566.cljs$core$IIndexed$_nth$arity$2(null,i__56568);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__56595,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__56595,(1),null);
var temp__5733__auto___56766 = day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___56766)){
var effect_fn_56768 = temp__5733__auto___56766;
(effect_fn_56768.cljs$core$IFn$_invoke$arity$1 ? effect_fn_56768.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_56768.call(null,effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__56773 = seq__56565;
var G__56774 = chunk__56566;
var G__56775 = count__56567;
var G__56776 = (i__56568 + (1));
seq__56565 = G__56773;
chunk__56566 = G__56774;
count__56567 = G__56775;
i__56568 = G__56776;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__56565);
if(temp__5735__auto__){
var seq__56565__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__56565__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__56565__$1);
var G__56779 = cljs.core.chunk_rest(seq__56565__$1);
var G__56780 = c__4556__auto__;
var G__56781 = cljs.core.count(c__4556__auto__);
var G__56782 = (0);
seq__56565 = G__56779;
chunk__56566 = G__56780;
count__56567 = G__56781;
i__56568 = G__56782;
continue;
} else {
var vec__56604 = cljs.core.first(seq__56565__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__56604,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__56604,(1),null);
var temp__5733__auto___56783 = day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___56783)){
var effect_fn_56786 = temp__5733__auto___56783;
(effect_fn_56786.cljs$core$IFn$_invoke$arity$1 ? effect_fn_56786.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_56786.call(null,effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__56789 = cljs.core.next(seq__56565__$1);
var G__56790 = null;
var G__56791 = (0);
var G__56792 = (0);
seq__56565 = G__56789;
chunk__56566 = G__56790;
count__56567 = G__56791;
i__56568 = G__56792;
continue;
}
} else {
return null;
}
}
break;
}
}finally {if(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace.is_trace_enabled_QMARK_()){
var end__55887__auto___56794 = day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.interop.now();
var duration__55888__auto___56795 = (end__55887__auto___56794 - new cljs.core.Keyword(null,"start","start",-355208981).cljs$core$IFn$_invoke$arity$1(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace._STAR_current_trace_STAR_));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace.traces,cljs.core.conj,cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace._STAR_current_trace_STAR_,new cljs.core.Keyword(null,"duration","duration",1444101068),duration__55888__auto___56795,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"end","end",-268185958),day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.interop.now()], 0)));

day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace.run_tracing_callbacks_BANG_(end__55887__auto___56794);
} else {
}
}}finally {(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.trace._STAR_current_trace_STAR_ = _STAR_current_trace_STAR__orig_val__56560);
}} else {
var seq__56608 = cljs.core.seq(new cljs.core.Keyword(null,"effects","effects",-282369292).cljs$core$IFn$_invoke$arity$1(context));
var chunk__56609 = null;
var count__56610 = (0);
var i__56611 = (0);
while(true){
if((i__56611 < count__56610)){
var vec__56636 = chunk__56609.cljs$core$IIndexed$_nth$arity$2(null,i__56611);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__56636,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__56636,(1),null);
var temp__5733__auto___56799 = day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___56799)){
var effect_fn_56801 = temp__5733__auto___56799;
(effect_fn_56801.cljs$core$IFn$_invoke$arity$1 ? effect_fn_56801.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_56801.call(null,effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__56807 = seq__56608;
var G__56808 = chunk__56609;
var G__56809 = count__56610;
var G__56810 = (i__56611 + (1));
seq__56608 = G__56807;
chunk__56609 = G__56808;
count__56610 = G__56809;
i__56611 = G__56810;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__56608);
if(temp__5735__auto__){
var seq__56608__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__56608__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__56608__$1);
var G__56814 = cljs.core.chunk_rest(seq__56608__$1);
var G__56815 = c__4556__auto__;
var G__56816 = cljs.core.count(c__4556__auto__);
var G__56817 = (0);
seq__56608 = G__56814;
chunk__56609 = G__56815;
count__56610 = G__56816;
i__56611 = G__56817;
continue;
} else {
var vec__56641 = cljs.core.first(seq__56608__$1);
var effect_key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__56641,(0),null);
var effect_value = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__56641,(1),null);
var temp__5733__auto___56819 = day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.get_handler.cljs$core$IFn$_invoke$arity$3(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.kind,effect_key,false);
if(cljs.core.truth_(temp__5733__auto___56819)){
var effect_fn_56823 = temp__5733__auto___56819;
(effect_fn_56823.cljs$core$IFn$_invoke$arity$1 ? effect_fn_56823.cljs$core$IFn$_invoke$arity$1(effect_value) : effect_fn_56823.call(null,effect_value));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: no handler registered for effect:",effect_key,". Ignoring."], 0));
}


var G__56827 = cljs.core.next(seq__56608__$1);
var G__56828 = null;
var G__56829 = (0);
var G__56830 = (0);
seq__56608 = G__56827;
chunk__56609 = G__56828;
count__56610 = G__56829;
i__56611 = G__56830;
continue;
}
} else {
return null;
}
}
break;
}
}
})], 0));
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-later","dispatch-later",291951390),(function (value){
var seq__56650 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__56651 = null;
var count__56652 = (0);
var i__56653 = (0);
while(true){
if((i__56653 < count__56652)){
var map__56662 = chunk__56651.cljs$core$IIndexed$_nth$arity$2(null,i__56653);
var map__56662__$1 = (((((!((map__56662 == null))))?(((((map__56662.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__56662.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__56662):map__56662);
var effect = map__56662__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__56662__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__56662__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.interop.set_timeout_BANG_(((function (seq__56650,chunk__56651,count__56652,i__56653,map__56662,map__56662__$1,effect,ms,dispatch){
return (function (){
return day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.router.dispatch(dispatch);
});})(seq__56650,chunk__56651,count__56652,i__56653,map__56662,map__56662__$1,effect,ms,dispatch))
,ms);
}


var G__56842 = seq__56650;
var G__56843 = chunk__56651;
var G__56844 = count__56652;
var G__56845 = (i__56653 + (1));
seq__56650 = G__56842;
chunk__56651 = G__56843;
count__56652 = G__56844;
i__56653 = G__56845;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__56650);
if(temp__5735__auto__){
var seq__56650__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__56650__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__56650__$1);
var G__56853 = cljs.core.chunk_rest(seq__56650__$1);
var G__56854 = c__4556__auto__;
var G__56855 = cljs.core.count(c__4556__auto__);
var G__56856 = (0);
seq__56650 = G__56853;
chunk__56651 = G__56854;
count__56652 = G__56855;
i__56653 = G__56856;
continue;
} else {
var map__56667 = cljs.core.first(seq__56650__$1);
var map__56667__$1 = (((((!((map__56667 == null))))?(((((map__56667.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__56667.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__56667):map__56667);
var effect = map__56667__$1;
var ms = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__56667__$1,new cljs.core.Keyword(null,"ms","ms",-1152709733));
var dispatch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__56667__$1,new cljs.core.Keyword(null,"dispatch","dispatch",1319337009));
if(((cljs.core.empty_QMARK_(dispatch)) || ((!(typeof ms === 'number'))))){
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-later value:",effect], 0));
} else {
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.interop.set_timeout_BANG_(((function (seq__56650,chunk__56651,count__56652,i__56653,map__56667,map__56667__$1,effect,ms,dispatch,seq__56650__$1,temp__5735__auto__){
return (function (){
return day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.router.dispatch(dispatch);
});})(seq__56650,chunk__56651,count__56652,i__56653,map__56667,map__56667__$1,effect,ms,dispatch,seq__56650__$1,temp__5735__auto__))
,ms);
}


var G__56866 = cljs.core.next(seq__56650__$1);
var G__56867 = null;
var G__56868 = (0);
var G__56869 = (0);
seq__56650 = G__56866;
chunk__56651 = G__56867;
count__56652 = G__56868;
i__56653 = G__56869;
continue;
}
} else {
return null;
}
}
break;
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch","dispatch",1319337009),(function (value){
if((!(cljs.core.vector_QMARK_(value)))){
return day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch value. Expected a vector, but got:",value], 0));
} else {
return day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.router.dispatch(value);
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"dispatch-n","dispatch-n",-504469236),(function (value){
if((!(cljs.core.sequential_QMARK_(value)))){
return day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.loggers.console.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"error","error",-978969032),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["re-frame: ignoring bad :dispatch-n value. Expected a collection, but got:",value], 0));
} else {
var seq__56686 = cljs.core.seq(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(cljs.core.nil_QMARK_,value));
var chunk__56687 = null;
var count__56688 = (0);
var i__56689 = (0);
while(true){
if((i__56689 < count__56688)){
var event = chunk__56687.cljs$core$IIndexed$_nth$arity$2(null,i__56689);
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.router.dispatch(event);


var G__56877 = seq__56686;
var G__56878 = chunk__56687;
var G__56879 = count__56688;
var G__56880 = (i__56689 + (1));
seq__56686 = G__56877;
chunk__56687 = G__56878;
count__56688 = G__56879;
i__56689 = G__56880;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__56686);
if(temp__5735__auto__){
var seq__56686__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__56686__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__56686__$1);
var G__56896 = cljs.core.chunk_rest(seq__56686__$1);
var G__56897 = c__4556__auto__;
var G__56898 = cljs.core.count(c__4556__auto__);
var G__56899 = (0);
seq__56686 = G__56896;
chunk__56687 = G__56897;
count__56688 = G__56898;
i__56689 = G__56899;
continue;
} else {
var event = cljs.core.first(seq__56686__$1);
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.router.dispatch(event);


var G__56903 = cljs.core.next(seq__56686__$1);
var G__56904 = null;
var G__56905 = (0);
var G__56906 = (0);
seq__56686 = G__56903;
chunk__56687 = G__56904;
count__56688 = G__56905;
i__56689 = G__56906;
continue;
}
} else {
return null;
}
}
break;
}
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"deregister-event-handler","deregister-event-handler",-1096518994),(function (value){
var clear_event = cljs.core.partial.cljs$core$IFn$_invoke$arity$2(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.registrar.clear_handlers,day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.events.kind);
if(cljs.core.sequential_QMARK_(value)){
var seq__56723 = cljs.core.seq(value);
var chunk__56724 = null;
var count__56725 = (0);
var i__56726 = (0);
while(true){
if((i__56726 < count__56725)){
var event = chunk__56724.cljs$core$IIndexed$_nth$arity$2(null,i__56726);
(clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(event) : clear_event.call(null,event));


var G__56912 = seq__56723;
var G__56913 = chunk__56724;
var G__56914 = count__56725;
var G__56915 = (i__56726 + (1));
seq__56723 = G__56912;
chunk__56724 = G__56913;
count__56725 = G__56914;
i__56726 = G__56915;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__56723);
if(temp__5735__auto__){
var seq__56723__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__56723__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__56723__$1);
var G__56922 = cljs.core.chunk_rest(seq__56723__$1);
var G__56923 = c__4556__auto__;
var G__56924 = cljs.core.count(c__4556__auto__);
var G__56925 = (0);
seq__56723 = G__56922;
chunk__56724 = G__56923;
count__56725 = G__56924;
i__56726 = G__56925;
continue;
} else {
var event = cljs.core.first(seq__56723__$1);
(clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(event) : clear_event.call(null,event));


var G__56930 = cljs.core.next(seq__56723__$1);
var G__56931 = null;
var G__56932 = (0);
var G__56933 = (0);
seq__56723 = G__56930;
chunk__56724 = G__56931;
count__56725 = G__56932;
i__56726 = G__56933;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return (clear_event.cljs$core$IFn$_invoke$arity$1 ? clear_event.cljs$core$IFn$_invoke$arity$1(value) : clear_event.call(null,value));
}
}));
day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.reg_fx(new cljs.core.Keyword(null,"db","db",993250759),(function (value){
if((!((cljs.core.deref(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.db.app_db) === value)))){
return cljs.core.reset_BANG_(day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.db.app_db,value);
} else {
return null;
}
}));

//# sourceMappingURL=day8.re_frame_10x.inlined_deps.re_frame.v0v12v0.re_frame.fx.js.map
