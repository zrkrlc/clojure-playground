goog.provide('playground.blackbelt.write_belt.views');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('re_frame.core');
var module$node_modules$inspirational_quotes$index=shadow.js.require("module$node_modules$inspirational_quotes$index", {});
goog.require('playground.blackbelt.write_belt.events');
goog.require('playground.blackbelt.write_belt.subs');
playground.blackbelt.write_belt.views.nav = (function playground$blackbelt$write_belt$views$nav(){
var active_user = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"active-user","active-user",-625701621)], null)));
return (function (){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.nav","div.nav",-1805454552),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"display","display",242065432),"flex",new cljs.core.Keyword(null,"justify-content","justify-content",-1990475787),"flex-end",new cljs.core.Keyword(null,"padding","padding",1660304693),"1em"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"/write-belt",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"padding-left","padding-left",-1180879053),"1em"], null)], null),"Home"], null),(((active_user == null))?new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"/write-belt#log-in",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"padding-left","padding-left",-1180879053),"1em"], null)], null),"Log in"], null):null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),"/write-belt#dashboard",new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"padding-left","padding-left",-1180879053),"1em"], null)], null),"Dashboard"], null)], null);
});
});
playground.blackbelt.write_belt.views.task_card = (function playground$blackbelt$write_belt$views$task_card(task){
var task_id = new cljs.core.Keyword(null,"task-id","task-id",-1045480126).cljs$core$IFn$_invoke$arity$1(task);
var title = new cljs.core.Keyword(null,"title","title",636505583).cljs$core$IFn$_invoke$arity$1(task);
var subtitle = new cljs.core.Keyword(null,"subtitle","subtitle",-1614524363).cljs$core$IFn$_invoke$arity$1(task);
var description = new cljs.core.Keyword(null,"description","description",-1428560544).cljs$core$IFn$_invoke$arity$1(task);
var image_url = new cljs.core.Keyword(null,"image-url","image-url",-1064784064).cljs$core$IFn$_invoke$arity$1(task);
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"a","a",-2123407586),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"href","href",-793805698),["/write-belt?t=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(task_id)].join(''),new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"text-decoration","text-decoration",1836813207),"none",new cljs.core.Keyword(null,"color","color",1011675173),"var(--fg-normal)"], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.text-left.hover:bg-blue-100","button.text-left.hover:bg-blue-100",-1225358845),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return window.location().href().str("/write-belt?=t",task_id);
})], null),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#user-card.flex.flex-col.p-4.rounded.overflow-hidden.shadow-lg","div#user-card.flex.flex-col.p-4.rounded.overflow-hidden.shadow-lg",1347861586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"width","width",-384071477),"20em",new cljs.core.Keyword(null,"height","height",1025178622),"40em"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img.w-full","img.w-full",-1776690076),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"src","src",-1651076051),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(image_url),"?t=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(Date.now())].join('')], null)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-6.py-4","div.px-6.py-4",-1138945559),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-base.font-bold.pb-4.text-justify","p.text-base.font-bold.pb-4.text-justify",863642897),title], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-sm.text-justify","p.text-sm.text-justify",-855571593),subtitle], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs","p.text-xs",702447668),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.font-semibold","span.font-semibold",-1752897535),"ID: "], null),task_id], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-6.pb-2.text-justify.self-end.overflow-y-auto","div.px-6.pb-2.text-justify.self-end.overflow-y-auto",-906821977),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-sm","p.text-sm",-1988028746),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"hyphens","hyphens",2113533609),"auto"], null)], null),description], null)], null)], null)], null)], null);
});
});
playground.blackbelt.write_belt.views.user_card = (function playground$blackbelt$write_belt$views$user_card(user){
var user_id = new cljs.core.Keyword(null,"user-id","user-id",-206822291).cljs$core$IFn$_invoke$arity$1(user);
var first_name = new cljs.core.Keyword(null,"first-name","first-name",-1559982131).cljs$core$IFn$_invoke$arity$1(user);
var last_name = new cljs.core.Keyword(null,"last-name","last-name",-1695738974).cljs$core$IFn$_invoke$arity$1(user);
var email = new cljs.core.Keyword(null,"email","email",1415816706).cljs$core$IFn$_invoke$arity$1(user);
var date_joined = (new Date(new cljs.core.Keyword(null,"date-joined","date-joined",-206927907).cljs$core$IFn$_invoke$arity$1(user))).toString();
var image_url = new cljs.core.Keyword(null,"image-url","image-url",-1064784064).cljs$core$IFn$_invoke$arity$1(user);
return (function (){
return new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#user-card.flex.flex-col.p-4.rounded.overflow-hidden.shadow-lg","div#user-card.flex.flex-col.p-4.rounded.overflow-hidden.shadow-lg",1347861586),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"style","style",-496642736),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"width","width",-384071477),"20em"], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"img.w-full","img.w-full",-1776690076),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"src","src",-1651076051),image_url], null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-6.py-4","div.px-6.py-4",-1138945559),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h3","h3",2067611163),first_name," ",last_name], null)], null),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.px-6.pt-4.pb-2.self-end","div.px-6.pt-4.pb-2.self-end",44079187),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-xs","p.text-xs",702447668),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.font-semibold","span.font-semibold",-1752897535),"ID: "], null),user_id], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-sm","p.text-sm",-1988028746),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.font-semibold","span.font-semibold",-1752897535),"E-mail address: "], null),email], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.text-sm","p.text-sm",-1988028746),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"span.font-semibold","span.font-semibold",-1752897535),"Date joined: "], null),date_joined], null)], null)], null);
});
});
playground.blackbelt.write_belt.views.log_in = (function playground$blackbelt$write_belt$views$log_in(){
return (function (){
new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.write_belt.views.nav], null);

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"Log in"], null);
});
});
playground.blackbelt.write_belt.views.home = (function playground$blackbelt$write_belt$views$home(){
var users = cljs.core.vals(cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"users","users",-713552705)], null))));
var tasks = cljs.core.vals(cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tasks","tasks",-1754368880)], null))));
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#home","div#home",-1268728494),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#home-tasks.mx-auto.p-8","div#home-tasks.mx-auto.p-8",1564933504),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2.pb-4","h2.pb-4",-446367803),"Tasks"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex.flex-row.flex-wrap.justify-between","div.flex.flex-row.flex-wrap.justify-between",1708408089),(function (){var iter__4529__auto__ = (function playground$blackbelt$write_belt$views$home_$_iter__55062(s__55063){
return (new cljs.core.LazySeq(null,(function (){
var s__55063__$1 = s__55063;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55063__$1);
if(temp__5735__auto__){
var s__55063__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55063__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55063__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55065 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55064 = (0);
while(true){
if((i__55064 < size__4528__auto__)){
var task = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__55064);
cljs.core.chunk_append(b__55065,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.write_belt.views.task_card,task], null));

var G__55071 = (i__55064 + (1));
i__55064 = G__55071;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55065),playground$blackbelt$write_belt$views$home_$_iter__55062(cljs.core.chunk_rest(s__55063__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55065),null);
}
} else {
var task = cljs.core.first(s__55063__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.write_belt.views.task_card,task], null),playground$blackbelt$write_belt$views$home_$_iter__55062(cljs.core.rest(s__55063__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(tasks);
})()], null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div#home-users.mx-auto.p-8","div#home-users.mx-auto.p-8",431095415),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"h2.pb-4","h2.pb-4",-446367803),"Our users"], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.flex.flex-row.flex-wrap.justify-between","div.flex.flex-row.flex-wrap.justify-between",1708408089),(function (){var iter__4529__auto__ = (function playground$blackbelt$write_belt$views$home_$_iter__55066(s__55067){
return (new cljs.core.LazySeq(null,(function (){
var s__55067__$1 = s__55067;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__55067__$1);
if(temp__5735__auto__){
var s__55067__$2 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(s__55067__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__55067__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__55069 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__55068 = (0);
while(true){
if((i__55068 < size__4528__auto__)){
var user = cljs.core._nth.cljs$core$IFn$_invoke$arity$2(c__4527__auto__,i__55068);
cljs.core.chunk_append(b__55069,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.write_belt.views.user_card,user], null));

var G__55073 = (i__55068 + (1));
i__55068 = G__55073;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__55069),playground$blackbelt$write_belt$views$home_$_iter__55066(cljs.core.chunk_rest(s__55067__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__55069),null);
}
} else {
var user = cljs.core.first(s__55067__$2);
return cljs.core.cons(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.write_belt.views.user_card,user], null),playground$blackbelt$write_belt$views$home_$_iter__55066(cljs.core.rest(s__55067__$2)));
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(users);
})()], null)], null)], null);
});
});

//# sourceMappingURL=playground.blackbelt.write_belt.views.js.map
