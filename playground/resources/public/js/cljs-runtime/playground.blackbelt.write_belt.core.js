goog.provide('playground.blackbelt.write_belt.core');
goog.require('cljs.core');
goog.require('reagent.core');
goog.require('re_frame.core');
goog.require('playground.blackbelt.state');
goog.require('playground.blackbelt.write_belt.subs');
goog.require('playground.blackbelt.write_belt.events');
goog.require('playground.blackbelt.write_belt.views');
playground.blackbelt.write_belt.core.pages = (function playground$blackbelt$write_belt$core$pages(page_name){
var G__55114 = page_name;
var G__55114__$1 = (((G__55114 instanceof cljs.core.Keyword))?G__55114.fqn:null);
switch (G__55114__$1) {
case "home":
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.write_belt.views.home], null);

break;
default:
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.write_belt.views.home], null);

}
});
playground.blackbelt.write_belt.core.write_belt = (function playground$blackbelt$write_belt$core$write_belt(){
var active_nav = cljs.core.deref(re_frame.core.subscribe.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"active-nav","active-nav",-275650057)], null)));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.write-belt","div.write-belt",-1558809414),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [playground.blackbelt.write_belt.core.pages,active_nav], null)], null);
});

//# sourceMappingURL=playground.blackbelt.write_belt.core.js.map
