# How to replicate
1) Follow this guide (+shadow-cljs): https://github.com/reagent-project/reagent-template
2) Copy the package.json scripts from here: https://github.com/jacekschae/shadow-reagent 
   a) Add the script `"repl" : "shadow-cljs cljs-repl app"`
3) Start the server + REPL via the instructions below
4) In the REPL, import the core namespace via `(ns playground.core)`
5) Test the connection to the browser by opening the webpage and doing `(js/alert "Hello World")`

# How to start server
```
npm run server
npm run dev
npm run repl
```
