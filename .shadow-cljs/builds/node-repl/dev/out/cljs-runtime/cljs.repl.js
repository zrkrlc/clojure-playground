goog.provide('cljs.repl');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__33300){
var map__33301 = p__33300;
var map__33301__$1 = (((((!((map__33301 == null))))?(((((map__33301.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33301.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33301):map__33301);
var m = map__33301__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33301__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33301__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__4126__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return [(function (){var temp__5735__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__33305_33522 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__33306_33523 = null;
var count__33307_33524 = (0);
var i__33308_33525 = (0);
while(true){
if((i__33308_33525 < count__33307_33524)){
var f_33526 = chunk__33306_33523.cljs$core$IIndexed$_nth$arity$2(null,i__33308_33525);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_33526], 0));


var G__33527 = seq__33305_33522;
var G__33528 = chunk__33306_33523;
var G__33529 = count__33307_33524;
var G__33530 = (i__33308_33525 + (1));
seq__33305_33522 = G__33527;
chunk__33306_33523 = G__33528;
count__33307_33524 = G__33529;
i__33308_33525 = G__33530;
continue;
} else {
var temp__5735__auto___33531 = cljs.core.seq(seq__33305_33522);
if(temp__5735__auto___33531){
var seq__33305_33532__$1 = temp__5735__auto___33531;
if(cljs.core.chunked_seq_QMARK_(seq__33305_33532__$1)){
var c__4556__auto___33533 = cljs.core.chunk_first(seq__33305_33532__$1);
var G__33534 = cljs.core.chunk_rest(seq__33305_33532__$1);
var G__33535 = c__4556__auto___33533;
var G__33536 = cljs.core.count(c__4556__auto___33533);
var G__33537 = (0);
seq__33305_33522 = G__33534;
chunk__33306_33523 = G__33535;
count__33307_33524 = G__33536;
i__33308_33525 = G__33537;
continue;
} else {
var f_33538 = cljs.core.first(seq__33305_33532__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_33538], 0));


var G__33539 = cljs.core.next(seq__33305_33532__$1);
var G__33540 = null;
var G__33541 = (0);
var G__33542 = (0);
seq__33305_33522 = G__33539;
chunk__33306_33523 = G__33540;
count__33307_33524 = G__33541;
i__33308_33525 = G__33542;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_33543 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__4126__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_33543], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_33543)))?cljs.core.second(arglists_33543):arglists_33543)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__33311_33544 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__33312_33545 = null;
var count__33313_33546 = (0);
var i__33314_33547 = (0);
while(true){
if((i__33314_33547 < count__33313_33546)){
var vec__33328_33548 = chunk__33312_33545.cljs$core$IIndexed$_nth$arity$2(null,i__33314_33547);
var name_33549 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33328_33548,(0),null);
var map__33331_33550 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33328_33548,(1),null);
var map__33331_33551__$1 = (((((!((map__33331_33550 == null))))?(((((map__33331_33550.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33331_33550.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33331_33550):map__33331_33550);
var doc_33552 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33331_33551__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_33553 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33331_33551__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_33549], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_33553], 0));

if(cljs.core.truth_(doc_33552)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_33552], 0));
} else {
}


var G__33554 = seq__33311_33544;
var G__33555 = chunk__33312_33545;
var G__33556 = count__33313_33546;
var G__33557 = (i__33314_33547 + (1));
seq__33311_33544 = G__33554;
chunk__33312_33545 = G__33555;
count__33313_33546 = G__33556;
i__33314_33547 = G__33557;
continue;
} else {
var temp__5735__auto___33558 = cljs.core.seq(seq__33311_33544);
if(temp__5735__auto___33558){
var seq__33311_33559__$1 = temp__5735__auto___33558;
if(cljs.core.chunked_seq_QMARK_(seq__33311_33559__$1)){
var c__4556__auto___33560 = cljs.core.chunk_first(seq__33311_33559__$1);
var G__33561 = cljs.core.chunk_rest(seq__33311_33559__$1);
var G__33562 = c__4556__auto___33560;
var G__33563 = cljs.core.count(c__4556__auto___33560);
var G__33564 = (0);
seq__33311_33544 = G__33561;
chunk__33312_33545 = G__33562;
count__33313_33546 = G__33563;
i__33314_33547 = G__33564;
continue;
} else {
var vec__33333_33565 = cljs.core.first(seq__33311_33559__$1);
var name_33566 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33333_33565,(0),null);
var map__33336_33567 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33333_33565,(1),null);
var map__33336_33568__$1 = (((((!((map__33336_33567 == null))))?(((((map__33336_33567.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33336_33567.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33336_33567):map__33336_33567);
var doc_33569 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33336_33568__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_33570 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33336_33568__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_33566], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_33570], 0));

if(cljs.core.truth_(doc_33569)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_33569], 0));
} else {
}


var G__33572 = cljs.core.next(seq__33311_33559__$1);
var G__33573 = null;
var G__33574 = (0);
var G__33575 = (0);
seq__33311_33544 = G__33572;
chunk__33312_33545 = G__33573;
count__33313_33546 = G__33574;
i__33314_33547 = G__33575;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5735__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5735__auto__)){
var fnspec = temp__5735__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__33340 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__33341 = null;
var count__33342 = (0);
var i__33343 = (0);
while(true){
if((i__33343 < count__33342)){
var role = chunk__33341.cljs$core$IIndexed$_nth$arity$2(null,i__33343);
var temp__5735__auto___33576__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___33576__$1)){
var spec_33577 = temp__5735__auto___33576__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_33577)], 0));
} else {
}


var G__33578 = seq__33340;
var G__33579 = chunk__33341;
var G__33580 = count__33342;
var G__33581 = (i__33343 + (1));
seq__33340 = G__33578;
chunk__33341 = G__33579;
count__33342 = G__33580;
i__33343 = G__33581;
continue;
} else {
var temp__5735__auto____$1 = cljs.core.seq(seq__33340);
if(temp__5735__auto____$1){
var seq__33340__$1 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__33340__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__33340__$1);
var G__33582 = cljs.core.chunk_rest(seq__33340__$1);
var G__33583 = c__4556__auto__;
var G__33584 = cljs.core.count(c__4556__auto__);
var G__33585 = (0);
seq__33340 = G__33582;
chunk__33341 = G__33583;
count__33342 = G__33584;
i__33343 = G__33585;
continue;
} else {
var role = cljs.core.first(seq__33340__$1);
var temp__5735__auto___33586__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5735__auto___33586__$2)){
var spec_33587 = temp__5735__auto___33586__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_33587)], 0));
} else {
}


var G__33588 = cljs.core.next(seq__33340__$1);
var G__33589 = null;
var G__33590 = (0);
var G__33591 = (0);
seq__33340 = G__33588;
chunk__33341 = G__33589;
count__33342 = G__33590;
i__33343 = G__33591;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
var base = (function (t){
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),(((t instanceof cljs.core.ExceptionInfo))?new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null):(((t instanceof Error))?cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("js",t.name):null
))], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(t);
if(cljs.core.truth_(temp__5735__auto__)){
var msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"message","message",-406056002),msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(t);
if(cljs.core.truth_(temp__5735__auto__)){
var ed = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),ed], null);
} else {
return null;
}
})()], 0));
});
var via = (function (){var via = cljs.core.PersistentVector.EMPTY;
var t = o;
while(true){
if(cljs.core.truth_(t)){
var G__33592 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(via,t);
var G__33593 = cljs.core.ex_cause(t);
via = G__33592;
t = G__33593;
continue;
} else {
return via;
}
break;
}
})();
var root = cljs.core.peek(via);
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"via","via",-1904457336),cljs.core.vec(cljs.core.map.cljs$core$IFn$_invoke$arity$2(base,via)),new cljs.core.Keyword(null,"trace","trace",-1082747415),null], null),(function (){var temp__5735__auto__ = cljs.core.ex_message(root);
if(cljs.core.truth_(temp__5735__auto__)){
var root_msg = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cause","cause",231901252),root_msg], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = cljs.core.ex_data(root);
if(cljs.core.truth_(temp__5735__auto__)){
var data = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"data","data",-232669377),data], null);
} else {
return null;
}
})(),(function (){var temp__5735__auto__ = new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358).cljs$core$IFn$_invoke$arity$1(cljs.core.ex_data(o));
if(cljs.core.truth_(temp__5735__auto__)){
var phase = temp__5735__auto__;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"phase","phase",575722892),phase], null);
} else {
return null;
}
})()], 0));
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__33415 = datafied_throwable;
var map__33415__$1 = (((((!((map__33415 == null))))?(((((map__33415.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33415.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33415):map__33415);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33415__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33415__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__33415__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__33416 = cljs.core.last(via);
var map__33416__$1 = (((((!((map__33416 == null))))?(((((map__33416.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33416.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33416):map__33416);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33416__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33416__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33416__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__33417 = data;
var map__33417__$1 = (((((!((map__33417 == null))))?(((((map__33417.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33417.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33417):map__33417);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33417__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33417__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33417__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__33418 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__33418__$1 = (((((!((map__33418 == null))))?(((((map__33418.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33418.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33418):map__33418);
var top_data = map__33418__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33418__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__33449 = phase;
var G__33449__$1 = (((G__33449 instanceof cljs.core.Keyword))?G__33449.fqn:null);
switch (G__33449__$1) {
case "read-source":
var map__33451 = data;
var map__33451__$1 = (((((!((map__33451 == null))))?(((((map__33451.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33451.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33451):map__33451);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33451__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33451__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__33453 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__33453__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33453,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__33453);
var G__33453__$2 = (cljs.core.truth_((function (){var fexpr__33455 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__33455.cljs$core$IFn$_invoke$arity$1 ? fexpr__33455.cljs$core$IFn$_invoke$arity$1(source) : fexpr__33455.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__33453__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__33453__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33453__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__33453__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__33456 = top_data;
var G__33456__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33456,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__33456);
var G__33456__$2 = (cljs.core.truth_((function (){var fexpr__33457 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__33457.cljs$core$IFn$_invoke$arity$1 ? fexpr__33457.cljs$core$IFn$_invoke$arity$1(source) : fexpr__33457.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__33456__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__33456__$1);
var G__33456__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33456__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__33456__$2);
var G__33456__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33456__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__33456__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33456__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__33456__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__33458 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33458,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33458,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33458,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33458,(3),null);
var G__33461 = top_data;
var G__33461__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33461,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__33461);
var G__33461__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33461__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__33461__$1);
var G__33461__$3 = (cljs.core.truth_((function (){var and__4115__auto__ = source__$1;
if(cljs.core.truth_(and__4115__auto__)){
return method;
} else {
return and__4115__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33461__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__33461__$2);
var G__33461__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33461__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__33461__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33461__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__33461__$4;
}

break;
case "execution":
var vec__33463 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33463,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33463,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33463,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__33463,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__33407_SHARP_){
var or__4126__auto__ = (p1__33407_SHARP_ == null);
if(or__4126__auto__){
return or__4126__auto__;
} else {
var fexpr__33468 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__33468.cljs$core$IFn$_invoke$arity$1 ? fexpr__33468.cljs$core$IFn$_invoke$arity$1(p1__33407_SHARP_) : fexpr__33468.call(null,p1__33407_SHARP_));
}
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__4126__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return line;
}
})();
var G__33469 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__33469__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33469,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__33469);
var G__33469__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33469__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__33469__$1);
var G__33469__$3 = (cljs.core.truth_((function (){var or__4126__auto__ = fn;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
var and__4115__auto__ = source__$1;
if(cljs.core.truth_(and__4115__auto__)){
return method;
} else {
return and__4115__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33469__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__4126__auto__ = fn;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__33469__$2);
var G__33469__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33469__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__33469__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__33469__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__33469__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__33449__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__33474){
var map__33475 = p__33474;
var map__33475__$1 = (((((!((map__33475 == null))))?(((((map__33475.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__33475.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__33475):map__33475);
var triage_data = map__33475__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33475__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33475__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33475__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33475__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33475__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33475__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33475__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__33475__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4126__auto__ = source;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__4126__auto__ = line;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__4126__auto__ = class$;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__33477 = phase;
var G__33477__$1 = (((G__33477 instanceof cljs.core.Keyword))?G__33477.fqn:null);
switch (G__33477__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__33478 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__33479 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__33480 = loc;
var G__33481 = (cljs.core.truth_(spec)?(function (){var sb__4667__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__33482_33598 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__33483_33599 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__33484_33600 = true;
var _STAR_print_fn_STAR__temp_val__33485_33601 = (function (x__4668__auto__){
return sb__4667__auto__.append(x__4668__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__33484_33600);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__33485_33601);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__33471_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__33471_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__33483_33599);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__33482_33598);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4667__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__33478,G__33479,G__33480,G__33481) : format.call(null,G__33478,G__33479,G__33480,G__33481));

break;
case "macroexpansion":
var G__33486 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__33487 = cause_type;
var G__33488 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__33489 = loc;
var G__33490 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__33486,G__33487,G__33488,G__33489,G__33490) : format.call(null,G__33486,G__33487,G__33488,G__33489,G__33490));

break;
case "compile-syntax-check":
var G__33491 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__33492 = cause_type;
var G__33493 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__33494 = loc;
var G__33495 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__33491,G__33492,G__33493,G__33494,G__33495) : format.call(null,G__33491,G__33492,G__33493,G__33494,G__33495));

break;
case "compilation":
var G__33498 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__33499 = cause_type;
var G__33500 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__33501 = loc;
var G__33502 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__33498,G__33499,G__33500,G__33501,G__33502) : format.call(null,G__33498,G__33499,G__33500,G__33501,G__33502));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__33505 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__33506 = symbol;
var G__33507 = loc;
var G__33508 = (function (){var sb__4667__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__33509_33606 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__33510_33607 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__33511_33608 = true;
var _STAR_print_fn_STAR__temp_val__33512_33609 = (function (x__4668__auto__){
return sb__4667__auto__.append(x__4668__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__33511_33608);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__33512_33609);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__33472_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__33472_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__33510_33607);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__33509_33606);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__4667__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__33505,G__33506,G__33507,G__33508) : format.call(null,G__33505,G__33506,G__33507,G__33508));
} else {
var G__33517 = "Execution error%s at %s(%s).\n%s\n";
var G__33518 = cause_type;
var G__33519 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__33520 = loc;
var G__33521 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__33517,G__33518,G__33519,G__33520,G__33521) : format.call(null,G__33517,G__33518,G__33519,G__33520,G__33521));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__33477__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
