goog.provide('shadow.cljs.devtools.client.node');
shadow.cljs.devtools.client.node.node_eval = (function shadow$cljs$devtools$client$node$node_eval(p__31714){
var map__31719 = p__31714;
var map__31719__$1 = (((((!((map__31719 == null))))?(((((map__31719.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31719.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31719):map__31719);
var msg = map__31719__$1;
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31719__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var source_map_json = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31719__$1,new cljs.core.Keyword(null,"source-map-json","source-map-json",-299460036));
var result = SHADOW_NODE_EVAL(js,source_map_json);
return result;
});
shadow.cljs.devtools.client.node.is_loaded_QMARK_ = (function shadow$cljs$devtools$client$node$is_loaded_QMARK_(src){
return goog.object.get(SHADOW_IMPORTED,src) === true;
});
shadow.cljs.devtools.client.node.closure_import = (function shadow$cljs$devtools$client$node$closure_import(src){
if(typeof src === 'string'){
} else {
throw (new Error("Assert failed: (string? src)"));
}

return SHADOW_IMPORT(src);
});
shadow.cljs.devtools.client.node.handle_build_complete = (function shadow$cljs$devtools$client$node$handle_build_complete(runtime,p__31726){
var map__31727 = p__31726;
var map__31727__$1 = (((((!((map__31727 == null))))?(((((map__31727.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31727.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31727):map__31727);
var msg = map__31727__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31727__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31727__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var map__31729 = info;
var map__31729__$1 = (((((!((map__31729 == null))))?(((((map__31729.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31729.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31729):map__31729);
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31729__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var compiled = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31729__$1,new cljs.core.Keyword(null,"compiled","compiled",850043082));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31729__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(((shadow.cljs.devtools.client.env.autoload) && (((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))))){
var files_to_require = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"output-name","output-name",-1769107767),cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (p__31739){
var map__31740 = p__31739;
var map__31740__$1 = (((((!((map__31740 == null))))?(((((map__31740.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31740.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31740):map__31740);
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31740__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31740__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
return ((cljs.core.contains_QMARK_(compiled,resource_id)) || (cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"always-load","always-load",66405637).cljs$core$IFn$_invoke$arity$1(reload_info),ns)));
}),cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__31742){
var map__31743 = p__31742;
var map__31743__$1 = (((((!((map__31743 == null))))?(((((map__31743.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31743.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31743):map__31743);
var ns = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31743__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
return cljs.core.contains_QMARK_(new cljs.core.Keyword(null,"never-load","never-load",1300896819).cljs$core$IFn$_invoke$arity$1(reload_info),ns);
}),sources))));
if(cljs.core.seq(files_to_require)){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$2(msg,(function (){
var seq__31745 = cljs.core.seq(files_to_require);
var chunk__31746 = null;
var count__31747 = (0);
var i__31748 = (0);
while(true){
if((i__31748 < count__31747)){
var src = chunk__31746.cljs$core$IIndexed$_nth$arity$2(null,i__31748);
shadow.cljs.devtools.client.env.before_load_src(src);

shadow.cljs.devtools.client.node.closure_import(src);


var G__31847 = seq__31745;
var G__31848 = chunk__31746;
var G__31849 = count__31747;
var G__31850 = (i__31748 + (1));
seq__31745 = G__31847;
chunk__31746 = G__31848;
count__31747 = G__31849;
i__31748 = G__31850;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__31745);
if(temp__5735__auto__){
var seq__31745__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__31745__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__31745__$1);
var G__31851 = cljs.core.chunk_rest(seq__31745__$1);
var G__31852 = c__4556__auto__;
var G__31853 = cljs.core.count(c__4556__auto__);
var G__31854 = (0);
seq__31745 = G__31851;
chunk__31746 = G__31852;
count__31747 = G__31853;
i__31748 = G__31854;
continue;
} else {
var src = cljs.core.first(seq__31745__$1);
shadow.cljs.devtools.client.env.before_load_src(src);

shadow.cljs.devtools.client.node.closure_import(src);


var G__31855 = cljs.core.next(seq__31745__$1);
var G__31856 = null;
var G__31857 = (0);
var G__31858 = (0);
seq__31745 = G__31855;
chunk__31746 = G__31856;
count__31747 = G__31857;
i__31748 = G__31858;
continue;
}
} else {
return null;
}
}
break;
}
}));
} else {
return null;
}
} else {
return null;
}
});
shadow.cljs.devtools.client.node.client_info = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"host","host",-1558485167),new cljs.core.Keyword(null,"node","node",581201198),new cljs.core.Keyword(null,"desc","desc",2093485764),["Node ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(process.version)].join('')], null);
shadow.cljs.devtools.client.node.start = (function shadow$cljs$devtools$client$node$start(runtime){
var ws_url = shadow.cljs.devtools.client.env.get_ws_relay_url();
var socket = (new shadow.js.shim.module$ws(ws_url,({"rejectUnauthorized": false})));
var ws_active_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
socket.on("message",(function (data){
if(cljs.core.truth_(cljs.core.deref(ws_active_ref))){
return shadow.cljs.devtools.client.shared.remote_msg(runtime,data);
} else {
return null;
}
}));

socket.on("open",(function (e){
if(cljs.core.truth_(cljs.core.deref(ws_active_ref))){
return shadow.cljs.devtools.client.shared.remote_open(runtime,e);
} else {
return null;
}
}));

socket.on("close",(function (e){
if(cljs.core.truth_(cljs.core.deref(ws_active_ref))){
return shadow.cljs.devtools.client.shared.remote_close(runtime,e);
} else {
return null;
}
}));

socket.on("error",(function (e){
if(cljs.core.truth_(cljs.core.deref(ws_active_ref))){
return shadow.cljs.devtools.client.shared.remote_error(runtime,e);
} else {
return null;
}
}));

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"socket","socket",59137063),socket,new cljs.core.Keyword(null,"ws-active-ref","ws-active-ref",804496391),ws_active_ref], null);
});
shadow.cljs.devtools.client.node.send = (function shadow$cljs$devtools$client$node$send(p__31762,msg){
var map__31763 = p__31762;
var map__31763__$1 = (((((!((map__31763 == null))))?(((((map__31763.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31763.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31763):map__31763);
var socket = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31763__$1,new cljs.core.Keyword(null,"socket","socket",59137063));
return socket.send(msg);
});
shadow.cljs.devtools.client.node.stop = (function shadow$cljs$devtools$client$node$stop(p__31765){
var map__31766 = p__31765;
var map__31766__$1 = (((((!((map__31766 == null))))?(((((map__31766.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31766.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31766):map__31766);
var socket = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31766__$1,new cljs.core.Keyword(null,"socket","socket",59137063));
var ws_active_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31766__$1,new cljs.core.Keyword(null,"ws-active-ref","ws-active-ref",804496391));
cljs.core.reset_BANG_(ws_active_ref,false);

return socket.close();
});
if((shadow.cljs.devtools.client.env.worker_client_id > (0))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return SHADOW_NODE_EVAL(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$2 = (function (this$,msg){
var this$__$1 = this;
return shadow.cljs.devtools.client.node.node_eval(msg);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__31773,done,error){
var map__31774 = p__31773;
var map__31774__$1 = (((((!((map__31774 == null))))?(((((map__31774.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31774.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31774):map__31774);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31774__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
try{var seq__31777_31875 = cljs.core.seq(repl_sources);
var chunk__31779_31876 = null;
var count__31780_31877 = (0);
var i__31781_31878 = (0);
while(true){
if((i__31781_31878 < count__31780_31877)){
var map__31791_31879 = chunk__31779_31876.cljs$core$IIndexed$_nth$arity$2(null,i__31781_31878);
var map__31791_31880__$1 = (((((!((map__31791_31879 == null))))?(((((map__31791_31879.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31791_31879.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31791_31879):map__31791_31879);
var src_31881 = map__31791_31880__$1;
var output_name_31882 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31791_31880__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
if((!(shadow.cljs.devtools.client.node.is_loaded_QMARK_(output_name_31882)))){
shadow.cljs.devtools.client.node.closure_import(output_name_31882);


var G__31887 = seq__31777_31875;
var G__31888 = chunk__31779_31876;
var G__31889 = count__31780_31877;
var G__31890 = (i__31781_31878 + (1));
seq__31777_31875 = G__31887;
chunk__31779_31876 = G__31888;
count__31780_31877 = G__31889;
i__31781_31878 = G__31890;
continue;
} else {
var G__31891 = seq__31777_31875;
var G__31892 = chunk__31779_31876;
var G__31893 = count__31780_31877;
var G__31894 = (i__31781_31878 + (1));
seq__31777_31875 = G__31891;
chunk__31779_31876 = G__31892;
count__31780_31877 = G__31893;
i__31781_31878 = G__31894;
continue;
}
} else {
var temp__5735__auto___31895 = cljs.core.seq(seq__31777_31875);
if(temp__5735__auto___31895){
var seq__31777_31896__$1 = temp__5735__auto___31895;
if(cljs.core.chunked_seq_QMARK_(seq__31777_31896__$1)){
var c__4556__auto___31897 = cljs.core.chunk_first(seq__31777_31896__$1);
var G__31898 = cljs.core.chunk_rest(seq__31777_31896__$1);
var G__31899 = c__4556__auto___31897;
var G__31900 = cljs.core.count(c__4556__auto___31897);
var G__31901 = (0);
seq__31777_31875 = G__31898;
chunk__31779_31876 = G__31899;
count__31780_31877 = G__31900;
i__31781_31878 = G__31901;
continue;
} else {
var map__31793_31904 = cljs.core.first(seq__31777_31896__$1);
var map__31793_31905__$1 = (((((!((map__31793_31904 == null))))?(((((map__31793_31904.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31793_31904.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31793_31904):map__31793_31904);
var src_31906 = map__31793_31905__$1;
var output_name_31907 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31793_31905__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
if((!(shadow.cljs.devtools.client.node.is_loaded_QMARK_(output_name_31907)))){
shadow.cljs.devtools.client.node.closure_import(output_name_31907);


var G__31909 = cljs.core.next(seq__31777_31896__$1);
var G__31910 = null;
var G__31911 = (0);
var G__31912 = (0);
seq__31777_31875 = G__31909;
chunk__31779_31876 = G__31910;
count__31780_31877 = G__31911;
i__31781_31878 = G__31912;
continue;
} else {
var G__31913 = cljs.core.next(seq__31777_31896__$1);
var G__31914 = null;
var G__31915 = (0);
var G__31916 = (0);
seq__31777_31875 = G__31913;
chunk__31779_31876 = G__31914;
count__31780_31877 = G__31915;
i__31781_31878 = G__31916;
continue;
}
}
} else {
}
}
break;
}

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}catch (e31776){var e = e31776;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(e) : error.call(null,e));
}}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (this$,p__31795,done,error){
var map__31796 = p__31795;
var map__31796__$1 = (((((!((map__31796 == null))))?(((((map__31796.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31796.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31796):map__31796);
var msg = map__31796__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31796__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31796__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var this$__$1 = this;
try{var seq__31799_31921 = cljs.core.seq(sources);
var chunk__31800_31922 = null;
var count__31801_31923 = (0);
var i__31802_31924 = (0);
while(true){
if((i__31802_31924 < count__31801_31923)){
var map__31817_31927 = chunk__31800_31922.cljs$core$IIndexed$_nth$arity$2(null,i__31802_31924);
var map__31817_31928__$1 = (((((!((map__31817_31927 == null))))?(((((map__31817_31927.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31817_31927.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31817_31927):map__31817_31927);
var src_31929 = map__31817_31928__$1;
var provides_31930 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31817_31928__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var output_name_31931 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31817_31928__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
if(cljs.core.truth_((function (){var or__4126__auto__ = (!(shadow.cljs.devtools.client.node.is_loaded_QMARK_(output_name_31931)));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return cljs.core.some(reload_namespaces,provides_31930);
}
})())){
shadow.cljs.devtools.client.node.closure_import(output_name_31931);
} else {
}


var G__31937 = seq__31799_31921;
var G__31938 = chunk__31800_31922;
var G__31939 = count__31801_31923;
var G__31940 = (i__31802_31924 + (1));
seq__31799_31921 = G__31937;
chunk__31800_31922 = G__31938;
count__31801_31923 = G__31939;
i__31802_31924 = G__31940;
continue;
} else {
var temp__5735__auto___31941 = cljs.core.seq(seq__31799_31921);
if(temp__5735__auto___31941){
var seq__31799_31942__$1 = temp__5735__auto___31941;
if(cljs.core.chunked_seq_QMARK_(seq__31799_31942__$1)){
var c__4556__auto___31943 = cljs.core.chunk_first(seq__31799_31942__$1);
var G__31944 = cljs.core.chunk_rest(seq__31799_31942__$1);
var G__31945 = c__4556__auto___31943;
var G__31946 = cljs.core.count(c__4556__auto___31943);
var G__31947 = (0);
seq__31799_31921 = G__31944;
chunk__31800_31922 = G__31945;
count__31801_31923 = G__31946;
i__31802_31924 = G__31947;
continue;
} else {
var map__31820_31948 = cljs.core.first(seq__31799_31942__$1);
var map__31820_31949__$1 = (((((!((map__31820_31948 == null))))?(((((map__31820_31948.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31820_31948.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31820_31948):map__31820_31948);
var src_31950 = map__31820_31949__$1;
var provides_31951 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31820_31949__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var output_name_31952 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31820_31949__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
if(cljs.core.truth_((function (){var or__4126__auto__ = (!(shadow.cljs.devtools.client.node.is_loaded_QMARK_(output_name_31952)));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return cljs.core.some(reload_namespaces,provides_31951);
}
})())){
shadow.cljs.devtools.client.node.closure_import(output_name_31952);
} else {
}


var G__31955 = cljs.core.next(seq__31799_31942__$1);
var G__31956 = null;
var G__31957 = (0);
var G__31958 = (0);
seq__31799_31921 = G__31955;
chunk__31800_31922 = G__31956;
count__31801_31923 = G__31957;
i__31802_31924 = G__31958;
continue;
}
} else {
}
}
break;
}

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}catch (e31798){var e = e31798;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(e) : error.call(null,e));
}}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.node","client","shadow.cljs.devtools.client.node/client",1327452098),cljs.core.PersistentHashSet.EMPTY,(function (p__31822){
var map__31823 = p__31822;
var map__31823__$1 = (((((!((map__31823 == null))))?(((((map__31823.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31823.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31823):map__31823);
var env = map__31823__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31823__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.node","client","shadow.cljs.devtools.client.node/client",1327452098),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
shadow.cljs.devtools.client.env.patch_goog_BANG_();

return console.log(["shadow-cljs - #",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (){
return console.warn("The shadow-cljs Websocket was disconnected.");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
return console.error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.node.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__31825){
var map__31826 = p__31825;
var map__31826__$1 = (((((!((map__31826 == null))))?(((((map__31826.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31826.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31826):map__31826);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31826__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31826__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
return console.warn("shadow-cljs - The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
return console.warn("shadow-cljs - A new watch for this build was started, restart of this process required!");
} else {
return null;

}
}
})], null)], null));

return svc;
}),(function (p__31830){
var map__31831 = p__31830;
var map__31831__$1 = (((((!((map__31831 == null))))?(((((map__31831.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__31831.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__31831):map__31831);
var svc = map__31831__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__31831__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.node","client","shadow.cljs.devtools.client.node/client",1327452098));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.node.client_info,shadow.cljs.devtools.client.node.start,shadow.cljs.devtools.client.node.send,shadow.cljs.devtools.client.node.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.node.js.map
