goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___43790 = arguments.length;
var i__4737__auto___43791 = (0);
while(true){
if((i__4737__auto___43791 < len__4736__auto___43790)){
args__4742__auto__.push((arguments[i__4737__auto___43791]));

var G__43792 = (i__4737__auto___43791 + (1));
i__4737__auto___43791 = G__43792;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq43642){
var G__43643 = cljs.core.first(seq43642);
var seq43642__$1 = cljs.core.next(seq43642);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__43643,seq43642__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__43650 = cljs.core.seq(sources);
var chunk__43651 = null;
var count__43652 = (0);
var i__43653 = (0);
while(true){
if((i__43653 < count__43652)){
var map__43660 = chunk__43651.cljs$core$IIndexed$_nth$arity$2(null,i__43653);
var map__43660__$1 = (((((!((map__43660 == null))))?(((((map__43660.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43660.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43660):map__43660);
var src = map__43660__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43660__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43660__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43660__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43660__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e43665){var e_43793 = e43665;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_43793);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_43793.message)].join('')));
}

var G__43794 = seq__43650;
var G__43795 = chunk__43651;
var G__43796 = count__43652;
var G__43797 = (i__43653 + (1));
seq__43650 = G__43794;
chunk__43651 = G__43795;
count__43652 = G__43796;
i__43653 = G__43797;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__43650);
if(temp__5735__auto__){
var seq__43650__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__43650__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__43650__$1);
var G__43798 = cljs.core.chunk_rest(seq__43650__$1);
var G__43799 = c__4556__auto__;
var G__43800 = cljs.core.count(c__4556__auto__);
var G__43801 = (0);
seq__43650 = G__43798;
chunk__43651 = G__43799;
count__43652 = G__43800;
i__43653 = G__43801;
continue;
} else {
var map__43666 = cljs.core.first(seq__43650__$1);
var map__43666__$1 = (((((!((map__43666 == null))))?(((((map__43666.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43666.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43666):map__43666);
var src = map__43666__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43666__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43666__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43666__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43666__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e43668){var e_43802 = e43668;
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_43802);

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_43802.message)].join('')));
}

var G__43803 = cljs.core.next(seq__43650__$1);
var G__43804 = null;
var G__43805 = (0);
var G__43806 = (0);
seq__43650 = G__43803;
chunk__43651 = G__43804;
count__43652 = G__43805;
i__43653 = G__43806;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__43671 = cljs.core.seq(js_requires);
var chunk__43672 = null;
var count__43673 = (0);
var i__43674 = (0);
while(true){
if((i__43674 < count__43673)){
var js_ns = chunk__43672.cljs$core$IIndexed$_nth$arity$2(null,i__43674);
var require_str_43807 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_43807);


var G__43808 = seq__43671;
var G__43809 = chunk__43672;
var G__43810 = count__43673;
var G__43811 = (i__43674 + (1));
seq__43671 = G__43808;
chunk__43672 = G__43809;
count__43673 = G__43810;
i__43674 = G__43811;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__43671);
if(temp__5735__auto__){
var seq__43671__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__43671__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__43671__$1);
var G__43812 = cljs.core.chunk_rest(seq__43671__$1);
var G__43813 = c__4556__auto__;
var G__43814 = cljs.core.count(c__4556__auto__);
var G__43815 = (0);
seq__43671 = G__43812;
chunk__43672 = G__43813;
count__43673 = G__43814;
i__43674 = G__43815;
continue;
} else {
var js_ns = cljs.core.first(seq__43671__$1);
var require_str_43816 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_43816);


var G__43817 = cljs.core.next(seq__43671__$1);
var G__43818 = null;
var G__43819 = (0);
var G__43820 = (0);
seq__43671 = G__43817;
chunk__43672 = G__43818;
count__43673 = G__43819;
i__43674 = G__43820;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__43676){
var map__43677 = p__43676;
var map__43677__$1 = (((((!((map__43677 == null))))?(((((map__43677.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43677.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43677):map__43677);
var msg = map__43677__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43677__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43677__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__4529__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__43679(s__43680){
return (new cljs.core.LazySeq(null,(function (){
var s__43680__$1 = s__43680;
while(true){
var temp__5735__auto__ = cljs.core.seq(s__43680__$1);
if(temp__5735__auto__){
var xs__6292__auto__ = temp__5735__auto__;
var map__43685 = cljs.core.first(xs__6292__auto__);
var map__43685__$1 = (((((!((map__43685 == null))))?(((((map__43685.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43685.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43685):map__43685);
var src = map__43685__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43685__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43685__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__4525__auto__ = ((function (s__43680__$1,map__43685,map__43685__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__43677,map__43677__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__43679_$_iter__43681(s__43682){
return (new cljs.core.LazySeq(null,((function (s__43680__$1,map__43685,map__43685__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__43677,map__43677__$1,msg,info,reload_info){
return (function (){
var s__43682__$1 = s__43682;
while(true){
var temp__5735__auto____$1 = cljs.core.seq(s__43682__$1);
if(temp__5735__auto____$1){
var s__43682__$2 = temp__5735__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__43682__$2)){
var c__4527__auto__ = cljs.core.chunk_first(s__43682__$2);
var size__4528__auto__ = cljs.core.count(c__4527__auto__);
var b__43684 = cljs.core.chunk_buffer(size__4528__auto__);
if((function (){var i__43683 = (0);
while(true){
if((i__43683 < size__4528__auto__)){
var warning = cljs.core._nth(c__4527__auto__,i__43683);
cljs.core.chunk_append(b__43684,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__43821 = (i__43683 + (1));
i__43683 = G__43821;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__43684),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__43679_$_iter__43681(cljs.core.chunk_rest(s__43682__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__43684),null);
}
} else {
var warning = cljs.core.first(s__43682__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__43679_$_iter__43681(cljs.core.rest(s__43682__$2)));
}
} else {
return null;
}
break;
}
});})(s__43680__$1,map__43685,map__43685__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__43677,map__43677__$1,msg,info,reload_info))
,null,null));
});})(s__43680__$1,map__43685,map__43685__$1,src,resource_name,warnings,xs__6292__auto__,temp__5735__auto__,map__43677,map__43677__$1,msg,info,reload_info))
;
var fs__4526__auto__ = cljs.core.seq(iterys__4525__auto__(warnings));
if(fs__4526__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__4526__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__43679(cljs.core.rest(s__43680__$1)));
} else {
var G__43822 = cljs.core.rest(s__43680__$1);
s__43680__$1 = G__43822;
continue;
}
} else {
var G__43823 = cljs.core.rest(s__43680__$1);
s__43680__$1 = G__43823;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__4529__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
var seq__43687_43824 = cljs.core.seq(warnings);
var chunk__43688_43825 = null;
var count__43689_43826 = (0);
var i__43690_43827 = (0);
while(true){
if((i__43690_43827 < count__43689_43826)){
var map__43697_43828 = chunk__43688_43825.cljs$core$IIndexed$_nth$arity$2(null,i__43690_43827);
var map__43697_43829__$1 = (((((!((map__43697_43828 == null))))?(((((map__43697_43828.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43697_43828.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43697_43828):map__43697_43828);
var w_43830 = map__43697_43829__$1;
var msg_43831__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43697_43829__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_43832 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43697_43829__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_43833 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43697_43829__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_43834 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43697_43829__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_43834)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_43832),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_43833),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_43831__$1)].join(''));


var G__43835 = seq__43687_43824;
var G__43836 = chunk__43688_43825;
var G__43837 = count__43689_43826;
var G__43838 = (i__43690_43827 + (1));
seq__43687_43824 = G__43835;
chunk__43688_43825 = G__43836;
count__43689_43826 = G__43837;
i__43690_43827 = G__43838;
continue;
} else {
var temp__5735__auto___43839 = cljs.core.seq(seq__43687_43824);
if(temp__5735__auto___43839){
var seq__43687_43840__$1 = temp__5735__auto___43839;
if(cljs.core.chunked_seq_QMARK_(seq__43687_43840__$1)){
var c__4556__auto___43841 = cljs.core.chunk_first(seq__43687_43840__$1);
var G__43842 = cljs.core.chunk_rest(seq__43687_43840__$1);
var G__43843 = c__4556__auto___43841;
var G__43844 = cljs.core.count(c__4556__auto___43841);
var G__43845 = (0);
seq__43687_43824 = G__43842;
chunk__43688_43825 = G__43843;
count__43689_43826 = G__43844;
i__43690_43827 = G__43845;
continue;
} else {
var map__43699_43846 = cljs.core.first(seq__43687_43840__$1);
var map__43699_43847__$1 = (((((!((map__43699_43846 == null))))?(((((map__43699_43846.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43699_43846.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43699_43846):map__43699_43846);
var w_43848 = map__43699_43847__$1;
var msg_43849__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43699_43847__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_43850 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43699_43847__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_43851 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43699_43847__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_43852 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43699_43847__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_43852)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_43850),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_43851),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_43849__$1)].join(''));


var G__43853 = cljs.core.next(seq__43687_43840__$1);
var G__43854 = null;
var G__43855 = (0);
var G__43856 = (0);
seq__43687_43824 = G__43853;
chunk__43688_43825 = G__43854;
count__43689_43826 = G__43855;
i__43690_43827 = G__43856;
continue;
}
} else {
}
}
break;
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__43675_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__43675_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$)){
return new$;
} else {
return false;
}
} else {
return false;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__43701){
var map__43702 = p__43701;
var map__43702__$1 = (((((!((map__43702 == null))))?(((((map__43702.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43702.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43702):map__43702);
var msg = map__43702__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43702__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var seq__43704 = cljs.core.seq(updates);
var chunk__43706 = null;
var count__43707 = (0);
var i__43708 = (0);
while(true){
if((i__43708 < count__43707)){
var path = chunk__43706.cljs$core$IIndexed$_nth$arity$2(null,i__43708);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__43734_43857 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__43737_43858 = null;
var count__43738_43859 = (0);
var i__43739_43860 = (0);
while(true){
if((i__43739_43860 < count__43738_43859)){
var node_43861 = chunk__43737_43858.cljs$core$IIndexed$_nth$arity$2(null,i__43739_43860);
var path_match_43862 = shadow.cljs.devtools.client.browser.match_paths(node_43861.getAttribute("href"),path);
if(cljs.core.truth_(path_match_43862)){
var new_link_43863 = (function (){var G__43744 = node_43861.cloneNode(true);
G__43744.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_43862),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__43744;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_43862], 0));

goog.dom.insertSiblingAfter(new_link_43863,node_43861);

goog.dom.removeNode(node_43861);


var G__43864 = seq__43734_43857;
var G__43865 = chunk__43737_43858;
var G__43866 = count__43738_43859;
var G__43867 = (i__43739_43860 + (1));
seq__43734_43857 = G__43864;
chunk__43737_43858 = G__43865;
count__43738_43859 = G__43866;
i__43739_43860 = G__43867;
continue;
} else {
var G__43868 = seq__43734_43857;
var G__43869 = chunk__43737_43858;
var G__43870 = count__43738_43859;
var G__43871 = (i__43739_43860 + (1));
seq__43734_43857 = G__43868;
chunk__43737_43858 = G__43869;
count__43738_43859 = G__43870;
i__43739_43860 = G__43871;
continue;
}
} else {
var temp__5735__auto___43872 = cljs.core.seq(seq__43734_43857);
if(temp__5735__auto___43872){
var seq__43734_43873__$1 = temp__5735__auto___43872;
if(cljs.core.chunked_seq_QMARK_(seq__43734_43873__$1)){
var c__4556__auto___43874 = cljs.core.chunk_first(seq__43734_43873__$1);
var G__43875 = cljs.core.chunk_rest(seq__43734_43873__$1);
var G__43876 = c__4556__auto___43874;
var G__43877 = cljs.core.count(c__4556__auto___43874);
var G__43878 = (0);
seq__43734_43857 = G__43875;
chunk__43737_43858 = G__43876;
count__43738_43859 = G__43877;
i__43739_43860 = G__43878;
continue;
} else {
var node_43879 = cljs.core.first(seq__43734_43873__$1);
var path_match_43880 = shadow.cljs.devtools.client.browser.match_paths(node_43879.getAttribute("href"),path);
if(cljs.core.truth_(path_match_43880)){
var new_link_43881 = (function (){var G__43745 = node_43879.cloneNode(true);
G__43745.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_43880),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__43745;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_43880], 0));

goog.dom.insertSiblingAfter(new_link_43881,node_43879);

goog.dom.removeNode(node_43879);


var G__43882 = cljs.core.next(seq__43734_43873__$1);
var G__43883 = null;
var G__43884 = (0);
var G__43885 = (0);
seq__43734_43857 = G__43882;
chunk__43737_43858 = G__43883;
count__43738_43859 = G__43884;
i__43739_43860 = G__43885;
continue;
} else {
var G__43886 = cljs.core.next(seq__43734_43873__$1);
var G__43887 = null;
var G__43888 = (0);
var G__43889 = (0);
seq__43734_43857 = G__43886;
chunk__43737_43858 = G__43887;
count__43738_43859 = G__43888;
i__43739_43860 = G__43889;
continue;
}
}
} else {
}
}
break;
}


var G__43890 = seq__43704;
var G__43891 = chunk__43706;
var G__43892 = count__43707;
var G__43893 = (i__43708 + (1));
seq__43704 = G__43890;
chunk__43706 = G__43891;
count__43707 = G__43892;
i__43708 = G__43893;
continue;
} else {
var G__43894 = seq__43704;
var G__43895 = chunk__43706;
var G__43896 = count__43707;
var G__43897 = (i__43708 + (1));
seq__43704 = G__43894;
chunk__43706 = G__43895;
count__43707 = G__43896;
i__43708 = G__43897;
continue;
}
} else {
var temp__5735__auto__ = cljs.core.seq(seq__43704);
if(temp__5735__auto__){
var seq__43704__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__43704__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__43704__$1);
var G__43898 = cljs.core.chunk_rest(seq__43704__$1);
var G__43899 = c__4556__auto__;
var G__43900 = cljs.core.count(c__4556__auto__);
var G__43901 = (0);
seq__43704 = G__43898;
chunk__43706 = G__43899;
count__43707 = G__43900;
i__43708 = G__43901;
continue;
} else {
var path = cljs.core.first(seq__43704__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__43746_43902 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__43749_43903 = null;
var count__43750_43904 = (0);
var i__43751_43905 = (0);
while(true){
if((i__43751_43905 < count__43750_43904)){
var node_43906 = chunk__43749_43903.cljs$core$IIndexed$_nth$arity$2(null,i__43751_43905);
var path_match_43907 = shadow.cljs.devtools.client.browser.match_paths(node_43906.getAttribute("href"),path);
if(cljs.core.truth_(path_match_43907)){
var new_link_43908 = (function (){var G__43756 = node_43906.cloneNode(true);
G__43756.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_43907),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__43756;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_43907], 0));

goog.dom.insertSiblingAfter(new_link_43908,node_43906);

goog.dom.removeNode(node_43906);


var G__43909 = seq__43746_43902;
var G__43910 = chunk__43749_43903;
var G__43911 = count__43750_43904;
var G__43912 = (i__43751_43905 + (1));
seq__43746_43902 = G__43909;
chunk__43749_43903 = G__43910;
count__43750_43904 = G__43911;
i__43751_43905 = G__43912;
continue;
} else {
var G__43913 = seq__43746_43902;
var G__43914 = chunk__43749_43903;
var G__43915 = count__43750_43904;
var G__43916 = (i__43751_43905 + (1));
seq__43746_43902 = G__43913;
chunk__43749_43903 = G__43914;
count__43750_43904 = G__43915;
i__43751_43905 = G__43916;
continue;
}
} else {
var temp__5735__auto___43917__$1 = cljs.core.seq(seq__43746_43902);
if(temp__5735__auto___43917__$1){
var seq__43746_43918__$1 = temp__5735__auto___43917__$1;
if(cljs.core.chunked_seq_QMARK_(seq__43746_43918__$1)){
var c__4556__auto___43919 = cljs.core.chunk_first(seq__43746_43918__$1);
var G__43920 = cljs.core.chunk_rest(seq__43746_43918__$1);
var G__43921 = c__4556__auto___43919;
var G__43922 = cljs.core.count(c__4556__auto___43919);
var G__43923 = (0);
seq__43746_43902 = G__43920;
chunk__43749_43903 = G__43921;
count__43750_43904 = G__43922;
i__43751_43905 = G__43923;
continue;
} else {
var node_43924 = cljs.core.first(seq__43746_43918__$1);
var path_match_43925 = shadow.cljs.devtools.client.browser.match_paths(node_43924.getAttribute("href"),path);
if(cljs.core.truth_(path_match_43925)){
var new_link_43926 = (function (){var G__43757 = node_43924.cloneNode(true);
G__43757.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_43925),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__43757;
})();
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_43925], 0));

goog.dom.insertSiblingAfter(new_link_43926,node_43924);

goog.dom.removeNode(node_43924);


var G__43927 = cljs.core.next(seq__43746_43918__$1);
var G__43928 = null;
var G__43929 = (0);
var G__43930 = (0);
seq__43746_43902 = G__43927;
chunk__43749_43903 = G__43928;
count__43750_43904 = G__43929;
i__43751_43905 = G__43930;
continue;
} else {
var G__43931 = cljs.core.next(seq__43746_43918__$1);
var G__43932 = null;
var G__43933 = (0);
var G__43934 = (0);
seq__43746_43902 = G__43931;
chunk__43749_43903 = G__43932;
count__43750_43904 = G__43933;
i__43751_43905 = G__43934;
continue;
}
}
} else {
}
}
break;
}


var G__43935 = cljs.core.next(seq__43704__$1);
var G__43936 = null;
var G__43937 = (0);
var G__43938 = (0);
seq__43704 = G__43935;
chunk__43706 = G__43936;
count__43707 = G__43937;
i__43708 = G__43938;
continue;
} else {
var G__43939 = cljs.core.next(seq__43704__$1);
var G__43940 = null;
var G__43941 = (0);
var G__43942 = (0);
seq__43704 = G__43939;
chunk__43706 = G__43940;
count__43707 = G__43941;
i__43708 = G__43942;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.repl_init = (function shadow$cljs$devtools$client$browser$repl_init(runtime,p__43758){
var map__43759 = p__43758;
var map__43759__$1 = (((((!((map__43759 == null))))?(((((map__43759.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43759.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43759):map__43759);
var repl_state = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43759__$1,new cljs.core.Keyword(null,"repl-state","repl-state",-1733780387));
return shadow.cljs.devtools.client.shared.load_sources(runtime,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535).cljs$core$IFn$_invoke$arity$1(repl_state))),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return shadow.cljs.devtools.client.browser.devtools_msg("ready!");
}));
});
shadow.cljs.devtools.client.browser.client_info = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null);
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$2 = (function (this$,p__43761){
var map__43762 = p__43761;
var map__43762__$1 = (((((!((map__43762 == null))))?(((((map__43762.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43762.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43762):map__43762);
var _ = map__43762__$1;
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43762__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__43764,done,error){
var map__43765 = p__43764;
var map__43765__$1 = (((((!((map__43765 == null))))?(((((map__43765.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43765.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43765):map__43765);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43765__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__43767,done,error){
var map__43768 = p__43767;
var map__43768__$1 = (((((!((map__43768 == null))))?(((((map__43768.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43768.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43768):map__43768);
var msg = map__43768__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43768__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43768__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43768__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__43770){
var map__43771 = p__43770;
var map__43771__$1 = (((((!((map__43771 == null))))?(((((map__43771.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43771.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43771):map__43771);
var src = map__43771__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43771__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__4115__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__4115__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__43773 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__43773) : done.call(null,G__43773));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__43774){
var map__43775 = p__43774;
var map__43775__$1 = (((((!((map__43775 == null))))?(((((map__43775.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43775.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43775):map__43775);
var msg__$1 = map__43775__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43775__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e43777){var ex = e43777;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__43778){
var map__43779 = p__43778;
var map__43779__$1 = (((((!((map__43779 == null))))?(((((map__43779.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43779.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43779):map__43779);
var env = map__43779__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43779__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-runtime-init","cljs-runtime-init",1305890232),(function (msg){
return shadow.cljs.devtools.client.browser.repl_init(runtime,msg);
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (p__43781){
var map__43782 = p__43781;
var map__43782__$1 = (((((!((map__43782 == null))))?(((((map__43782.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43782.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43782):map__43782);
var msg = map__43782__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43782__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__43784){
var map__43785 = p__43784;
var map__43785__$1 = (((((!((map__43785 == null))))?(((((map__43785.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43785.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43785):map__43785);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43785__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43785__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__43787){
var map__43788 = p__43787;
var map__43788__$1 = (((((!((map__43788 == null))))?(((((map__43788.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__43788.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__43788):map__43788);
var svc = map__43788__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__43788__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
