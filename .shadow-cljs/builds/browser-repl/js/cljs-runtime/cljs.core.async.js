goog.provide('cljs.core.async');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__37646 = arguments.length;
switch (G__37646) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async37650 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async37650 = (function (f,blockable,meta37651){
this.f = f;
this.blockable = blockable;
this.meta37651 = meta37651;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async37650.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_37652,meta37651__$1){
var self__ = this;
var _37652__$1 = this;
return (new cljs.core.async.t_cljs$core$async37650(self__.f,self__.blockable,meta37651__$1));
}));

(cljs.core.async.t_cljs$core$async37650.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_37652){
var self__ = this;
var _37652__$1 = this;
return self__.meta37651;
}));

(cljs.core.async.t_cljs$core$async37650.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async37650.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async37650.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async37650.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async37650.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta37651","meta37651",330655790,null)], null);
}));

(cljs.core.async.t_cljs$core$async37650.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async37650.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async37650");

(cljs.core.async.t_cljs$core$async37650.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async37650");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async37650.
 */
cljs.core.async.__GT_t_cljs$core$async37650 = (function cljs$core$async$__GT_t_cljs$core$async37650(f__$1,blockable__$1,meta37651){
return (new cljs.core.async.t_cljs$core$async37650(f__$1,blockable__$1,meta37651));
});

}

return (new cljs.core.async.t_cljs$core$async37650(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__37683 = arguments.length;
switch (G__37683) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__37701 = arguments.length;
switch (G__37701) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__37705 = arguments.length;
switch (G__37705) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_41071 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_41071) : fn1.call(null,val_41071));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_41071) : fn1.call(null,val_41071));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__37723 = arguments.length;
switch (G__37723) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5733__auto__)){
var ret = temp__5733__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5733__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5733__auto__)){
var retb = temp__5733__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__4613__auto___41111 = n;
var x_41112 = (0);
while(true){
if((x_41112 < n__4613__auto___41111)){
(a[x_41112] = x_41112);

var G__41113 = (x_41112 + (1));
x_41112 = G__41113;
continue;
} else {
}
break;
}

goog.array.shuffle(a);

return a;
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async37724 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async37724 = (function (flag,meta37725){
this.flag = flag;
this.meta37725 = meta37725;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async37724.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_37726,meta37725__$1){
var self__ = this;
var _37726__$1 = this;
return (new cljs.core.async.t_cljs$core$async37724(self__.flag,meta37725__$1));
}));

(cljs.core.async.t_cljs$core$async37724.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_37726){
var self__ = this;
var _37726__$1 = this;
return self__.meta37725;
}));

(cljs.core.async.t_cljs$core$async37724.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async37724.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async37724.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async37724.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async37724.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta37725","meta37725",660957084,null)], null);
}));

(cljs.core.async.t_cljs$core$async37724.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async37724.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async37724");

(cljs.core.async.t_cljs$core$async37724.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async37724");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async37724.
 */
cljs.core.async.__GT_t_cljs$core$async37724 = (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async37724(flag__$1,meta37725){
return (new cljs.core.async.t_cljs$core$async37724(flag__$1,meta37725));
});

}

return (new cljs.core.async.t_cljs$core$async37724(flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async37732 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async37732 = (function (flag,cb,meta37733){
this.flag = flag;
this.cb = cb;
this.meta37733 = meta37733;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async37732.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_37734,meta37733__$1){
var self__ = this;
var _37734__$1 = this;
return (new cljs.core.async.t_cljs$core$async37732(self__.flag,self__.cb,meta37733__$1));
}));

(cljs.core.async.t_cljs$core$async37732.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_37734){
var self__ = this;
var _37734__$1 = this;
return self__.meta37733;
}));

(cljs.core.async.t_cljs$core$async37732.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async37732.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async37732.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async37732.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async37732.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta37733","meta37733",-1468187910,null)], null);
}));

(cljs.core.async.t_cljs$core$async37732.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async37732.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async37732");

(cljs.core.async.t_cljs$core$async37732.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async37732");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async37732.
 */
cljs.core.async.__GT_t_cljs$core$async37732 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async37732(flag__$1,cb__$1,meta37733){
return (new cljs.core.async.t_cljs$core$async37732(flag__$1,cb__$1,meta37733));
});

}

return (new cljs.core.async.t_cljs$core$async37732(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__37753_SHARP_){
var G__37756 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__37753_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__37756) : fret.call(null,G__37756));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__37754_SHARP_){
var G__37757 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__37754_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__37757) : fret.call(null,G__37757));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__4126__auto__ = wport;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return port;
}
})()], null));
} else {
var G__41129 = (i + (1));
i = G__41129;
continue;
}
} else {
return null;
}
break;
}
})();
var or__4126__auto__ = ret;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5735__auto__ = (function (){var and__4115__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__4115__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__4115__auto__;
}
})();
if(cljs.core.truth_(temp__5735__auto__)){
var got = temp__5735__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___41133 = arguments.length;
var i__4737__auto___41134 = (0);
while(true){
if((i__4737__auto___41134 < len__4736__auto___41133)){
args__4742__auto__.push((arguments[i__4737__auto___41134]));

var G__41136 = (i__4737__auto___41134 + (1));
i__4737__auto___41134 = G__41136;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__37773){
var map__37774 = p__37773;
var map__37774__$1 = (((((!((map__37774 == null))))?(((((map__37774.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__37774.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__37774):map__37774);
var opts = map__37774__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq37771){
var G__37772 = cljs.core.first(seq37771);
var seq37771__$1 = cljs.core.next(seq37771);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__37772,seq37771__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__37786 = arguments.length;
switch (G__37786) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__26226__auto___41150 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_37843){
var state_val_37849 = (state_37843[(1)]);
if((state_val_37849 === (7))){
var inst_37836 = (state_37843[(2)]);
var state_37843__$1 = state_37843;
var statearr_37860_41151 = state_37843__$1;
(statearr_37860_41151[(2)] = inst_37836);

(statearr_37860_41151[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (1))){
var state_37843__$1 = state_37843;
var statearr_37861_41152 = state_37843__$1;
(statearr_37861_41152[(2)] = null);

(statearr_37861_41152[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (4))){
var inst_37812 = (state_37843[(7)]);
var inst_37812__$1 = (state_37843[(2)]);
var inst_37813 = (inst_37812__$1 == null);
var state_37843__$1 = (function (){var statearr_37864 = state_37843;
(statearr_37864[(7)] = inst_37812__$1);

return statearr_37864;
})();
if(cljs.core.truth_(inst_37813)){
var statearr_37865_41153 = state_37843__$1;
(statearr_37865_41153[(1)] = (5));

} else {
var statearr_37867_41154 = state_37843__$1;
(statearr_37867_41154[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (13))){
var state_37843__$1 = state_37843;
var statearr_37871_41156 = state_37843__$1;
(statearr_37871_41156[(2)] = null);

(statearr_37871_41156[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (6))){
var inst_37812 = (state_37843[(7)]);
var state_37843__$1 = state_37843;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_37843__$1,(11),to,inst_37812);
} else {
if((state_val_37849 === (3))){
var inst_37840 = (state_37843[(2)]);
var state_37843__$1 = state_37843;
return cljs.core.async.impl.ioc_helpers.return_chan(state_37843__$1,inst_37840);
} else {
if((state_val_37849 === (12))){
var state_37843__$1 = state_37843;
var statearr_37873_41162 = state_37843__$1;
(statearr_37873_41162[(2)] = null);

(statearr_37873_41162[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (2))){
var state_37843__$1 = state_37843;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_37843__$1,(4),from);
} else {
if((state_val_37849 === (11))){
var inst_37825 = (state_37843[(2)]);
var state_37843__$1 = state_37843;
if(cljs.core.truth_(inst_37825)){
var statearr_37876_41163 = state_37843__$1;
(statearr_37876_41163[(1)] = (12));

} else {
var statearr_37879_41164 = state_37843__$1;
(statearr_37879_41164[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (9))){
var state_37843__$1 = state_37843;
var statearr_37880_41165 = state_37843__$1;
(statearr_37880_41165[(2)] = null);

(statearr_37880_41165[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (5))){
var state_37843__$1 = state_37843;
if(cljs.core.truth_(close_QMARK_)){
var statearr_37881_41166 = state_37843__$1;
(statearr_37881_41166[(1)] = (8));

} else {
var statearr_37882_41167 = state_37843__$1;
(statearr_37882_41167[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (14))){
var inst_37834 = (state_37843[(2)]);
var state_37843__$1 = state_37843;
var statearr_37883_41168 = state_37843__$1;
(statearr_37883_41168[(2)] = inst_37834);

(statearr_37883_41168[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (10))){
var inst_37822 = (state_37843[(2)]);
var state_37843__$1 = state_37843;
var statearr_37884_41169 = state_37843__$1;
(statearr_37884_41169[(2)] = inst_37822);

(statearr_37884_41169[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37849 === (8))){
var inst_37819 = cljs.core.async.close_BANG_(to);
var state_37843__$1 = state_37843;
var statearr_37885_41171 = state_37843__$1;
(statearr_37885_41171[(2)] = inst_37819);

(statearr_37885_41171[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_37886 = [null,null,null,null,null,null,null,null];
(statearr_37886[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_37886[(1)] = (1));

return statearr_37886;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_37843){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_37843);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e37889){var ex__26072__auto__ = e37889;
var statearr_37890_41182 = state_37843;
(statearr_37890_41182[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_37843[(4)]))){
var statearr_37893_41184 = state_37843;
(statearr_37893_41184[(1)] = cljs.core.first((state_37843[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41191 = state_37843;
state_37843 = G__41191;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_37843){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_37843);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_37894 = f__26227__auto__();
(statearr_37894[(6)] = c__26226__auto___41150);

return statearr_37894;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process = (function (p__37904){
var vec__37905 = p__37904;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__37905,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__37905,(1),null);
var job = vec__37905;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__26226__auto___41202 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_37918){
var state_val_37919 = (state_37918[(1)]);
if((state_val_37919 === (1))){
var state_37918__$1 = state_37918;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_37918__$1,(2),res,v);
} else {
if((state_val_37919 === (2))){
var inst_37913 = (state_37918[(2)]);
var inst_37914 = cljs.core.async.close_BANG_(res);
var state_37918__$1 = (function (){var statearr_37925 = state_37918;
(statearr_37925[(7)] = inst_37913);

return statearr_37925;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_37918__$1,inst_37914);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0 = (function (){
var statearr_37928 = [null,null,null,null,null,null,null,null];
(statearr_37928[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__);

(statearr_37928[(1)] = (1));

return statearr_37928;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1 = (function (state_37918){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_37918);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e37930){var ex__26072__auto__ = e37930;
var statearr_37935_41209 = state_37918;
(statearr_37935_41209[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_37918[(4)]))){
var statearr_37937_41211 = state_37918;
(statearr_37937_41211[(1)] = cljs.core.first((state_37918[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41212 = state_37918;
state_37918 = G__41212;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = function(state_37918){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1.call(this,state_37918);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_37939 = f__26227__auto__();
(statearr_37939[(6)] = c__26226__auto___41202);

return statearr_37939;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__37947){
var vec__37950 = p__37947;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__37950,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__37950,(1),null);
var job = vec__37950;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__4613__auto___41216 = n;
var __41217 = (0);
while(true){
if((__41217 < n__4613__auto___41216)){
var G__37958_41218 = type;
var G__37958_41219__$1 = (((G__37958_41218 instanceof cljs.core.Keyword))?G__37958_41218.fqn:null);
switch (G__37958_41219__$1) {
case "compute":
var c__26226__auto___41221 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__41217,c__26226__auto___41221,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async){
return (function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = ((function (__41217,c__26226__auto___41221,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async){
return (function (state_37976){
var state_val_37977 = (state_37976[(1)]);
if((state_val_37977 === (1))){
var state_37976__$1 = state_37976;
var statearr_37981_41225 = state_37976__$1;
(statearr_37981_41225[(2)] = null);

(statearr_37981_41225[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37977 === (2))){
var state_37976__$1 = state_37976;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_37976__$1,(4),jobs);
} else {
if((state_val_37977 === (3))){
var inst_37974 = (state_37976[(2)]);
var state_37976__$1 = state_37976;
return cljs.core.async.impl.ioc_helpers.return_chan(state_37976__$1,inst_37974);
} else {
if((state_val_37977 === (4))){
var inst_37966 = (state_37976[(2)]);
var inst_37967 = process(inst_37966);
var state_37976__$1 = state_37976;
if(cljs.core.truth_(inst_37967)){
var statearr_37985_41228 = state_37976__$1;
(statearr_37985_41228[(1)] = (5));

} else {
var statearr_37986_41229 = state_37976__$1;
(statearr_37986_41229[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37977 === (5))){
var state_37976__$1 = state_37976;
var statearr_37987_41230 = state_37976__$1;
(statearr_37987_41230[(2)] = null);

(statearr_37987_41230[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37977 === (6))){
var state_37976__$1 = state_37976;
var statearr_37992_41231 = state_37976__$1;
(statearr_37992_41231[(2)] = null);

(statearr_37992_41231[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_37977 === (7))){
var inst_37972 = (state_37976[(2)]);
var state_37976__$1 = state_37976;
var statearr_37993_41232 = state_37976__$1;
(statearr_37993_41232[(2)] = inst_37972);

(statearr_37993_41232[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__41217,c__26226__auto___41221,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async))
;
return ((function (__41217,switch__26068__auto__,c__26226__auto___41221,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0 = (function (){
var statearr_37995 = [null,null,null,null,null,null,null];
(statearr_37995[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__);

(statearr_37995[(1)] = (1));

return statearr_37995;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1 = (function (state_37976){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_37976);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e37997){var ex__26072__auto__ = e37997;
var statearr_37998_41239 = state_37976;
(statearr_37998_41239[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_37976[(4)]))){
var statearr_37999_41240 = state_37976;
(statearr_37999_41240[(1)] = cljs.core.first((state_37976[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41241 = state_37976;
state_37976 = G__41241;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = function(state_37976){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1.call(this,state_37976);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__;
})()
;})(__41217,switch__26068__auto__,c__26226__auto___41221,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async))
})();
var state__26228__auto__ = (function (){var statearr_38000 = f__26227__auto__();
(statearr_38000[(6)] = c__26226__auto___41221);

return statearr_38000;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
});})(__41217,c__26226__auto___41221,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async))
);


break;
case "async":
var c__26226__auto___41243 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__41217,c__26226__auto___41243,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async){
return (function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = ((function (__41217,c__26226__auto___41243,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async){
return (function (state_38014){
var state_val_38015 = (state_38014[(1)]);
if((state_val_38015 === (1))){
var state_38014__$1 = state_38014;
var statearr_38017_41246 = state_38014__$1;
(statearr_38017_41246[(2)] = null);

(statearr_38017_41246[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38015 === (2))){
var state_38014__$1 = state_38014;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38014__$1,(4),jobs);
} else {
if((state_val_38015 === (3))){
var inst_38012 = (state_38014[(2)]);
var state_38014__$1 = state_38014;
return cljs.core.async.impl.ioc_helpers.return_chan(state_38014__$1,inst_38012);
} else {
if((state_val_38015 === (4))){
var inst_38003 = (state_38014[(2)]);
var inst_38005 = async(inst_38003);
var state_38014__$1 = state_38014;
if(cljs.core.truth_(inst_38005)){
var statearr_38020_41250 = state_38014__$1;
(statearr_38020_41250[(1)] = (5));

} else {
var statearr_38022_41251 = state_38014__$1;
(statearr_38022_41251[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38015 === (5))){
var state_38014__$1 = state_38014;
var statearr_38023_41252 = state_38014__$1;
(statearr_38023_41252[(2)] = null);

(statearr_38023_41252[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38015 === (6))){
var state_38014__$1 = state_38014;
var statearr_38027_41253 = state_38014__$1;
(statearr_38027_41253[(2)] = null);

(statearr_38027_41253[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38015 === (7))){
var inst_38010 = (state_38014[(2)]);
var state_38014__$1 = state_38014;
var statearr_38028_41254 = state_38014__$1;
(statearr_38028_41254[(2)] = inst_38010);

(statearr_38028_41254[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__41217,c__26226__auto___41243,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async))
;
return ((function (__41217,switch__26068__auto__,c__26226__auto___41243,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0 = (function (){
var statearr_38031 = [null,null,null,null,null,null,null];
(statearr_38031[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__);

(statearr_38031[(1)] = (1));

return statearr_38031;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1 = (function (state_38014){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_38014);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e38033){var ex__26072__auto__ = e38033;
var statearr_38034_41258 = state_38014;
(statearr_38034_41258[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_38014[(4)]))){
var statearr_38035_41259 = state_38014;
(statearr_38035_41259[(1)] = cljs.core.first((state_38014[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41260 = state_38014;
state_38014 = G__41260;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = function(state_38014){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1.call(this,state_38014);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__;
})()
;})(__41217,switch__26068__auto__,c__26226__auto___41243,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async))
})();
var state__26228__auto__ = (function (){var statearr_38042 = f__26227__auto__();
(statearr_38042[(6)] = c__26226__auto___41243);

return statearr_38042;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
});})(__41217,c__26226__auto___41243,G__37958_41218,G__37958_41219__$1,n__4613__auto___41216,jobs,results,process,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__37958_41219__$1)].join('')));

}

var G__41263 = (__41217 + (1));
__41217 = G__41263;
continue;
} else {
}
break;
}

var c__26226__auto___41265 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_38067){
var state_val_38068 = (state_38067[(1)]);
if((state_val_38068 === (7))){
var inst_38063 = (state_38067[(2)]);
var state_38067__$1 = state_38067;
var statearr_38080_41268 = state_38067__$1;
(statearr_38080_41268[(2)] = inst_38063);

(statearr_38080_41268[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38068 === (1))){
var state_38067__$1 = state_38067;
var statearr_38081_41272 = state_38067__$1;
(statearr_38081_41272[(2)] = null);

(statearr_38081_41272[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38068 === (4))){
var inst_38045 = (state_38067[(7)]);
var inst_38045__$1 = (state_38067[(2)]);
var inst_38047 = (inst_38045__$1 == null);
var state_38067__$1 = (function (){var statearr_38084 = state_38067;
(statearr_38084[(7)] = inst_38045__$1);

return statearr_38084;
})();
if(cljs.core.truth_(inst_38047)){
var statearr_38085_41275 = state_38067__$1;
(statearr_38085_41275[(1)] = (5));

} else {
var statearr_38086_41277 = state_38067__$1;
(statearr_38086_41277[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38068 === (6))){
var inst_38051 = (state_38067[(8)]);
var inst_38045 = (state_38067[(7)]);
var inst_38051__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_38054 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_38055 = [inst_38045,inst_38051__$1];
var inst_38056 = (new cljs.core.PersistentVector(null,2,(5),inst_38054,inst_38055,null));
var state_38067__$1 = (function (){var statearr_38089 = state_38067;
(statearr_38089[(8)] = inst_38051__$1);

return statearr_38089;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_38067__$1,(8),jobs,inst_38056);
} else {
if((state_val_38068 === (3))){
var inst_38065 = (state_38067[(2)]);
var state_38067__$1 = state_38067;
return cljs.core.async.impl.ioc_helpers.return_chan(state_38067__$1,inst_38065);
} else {
if((state_val_38068 === (2))){
var state_38067__$1 = state_38067;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38067__$1,(4),from);
} else {
if((state_val_38068 === (9))){
var inst_38060 = (state_38067[(2)]);
var state_38067__$1 = (function (){var statearr_38091 = state_38067;
(statearr_38091[(9)] = inst_38060);

return statearr_38091;
})();
var statearr_38092_41286 = state_38067__$1;
(statearr_38092_41286[(2)] = null);

(statearr_38092_41286[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38068 === (5))){
var inst_38049 = cljs.core.async.close_BANG_(jobs);
var state_38067__$1 = state_38067;
var statearr_38094_41290 = state_38067__$1;
(statearr_38094_41290[(2)] = inst_38049);

(statearr_38094_41290[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38068 === (8))){
var inst_38051 = (state_38067[(8)]);
var inst_38058 = (state_38067[(2)]);
var state_38067__$1 = (function (){var statearr_38095 = state_38067;
(statearr_38095[(10)] = inst_38058);

return statearr_38095;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_38067__$1,(9),results,inst_38051);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0 = (function (){
var statearr_38097 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_38097[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__);

(statearr_38097[(1)] = (1));

return statearr_38097;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1 = (function (state_38067){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_38067);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e38098){var ex__26072__auto__ = e38098;
var statearr_38099_41302 = state_38067;
(statearr_38099_41302[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_38067[(4)]))){
var statearr_38100_41305 = state_38067;
(statearr_38100_41305[(1)] = cljs.core.first((state_38067[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41307 = state_38067;
state_38067 = G__41307;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = function(state_38067){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1.call(this,state_38067);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_38103 = f__26227__auto__();
(statearr_38103[(6)] = c__26226__auto___41265);

return statearr_38103;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


var c__26226__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_38151){
var state_val_38152 = (state_38151[(1)]);
if((state_val_38152 === (7))){
var inst_38146 = (state_38151[(2)]);
var state_38151__$1 = state_38151;
var statearr_38157_41312 = state_38151__$1;
(statearr_38157_41312[(2)] = inst_38146);

(statearr_38157_41312[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (20))){
var state_38151__$1 = state_38151;
var statearr_38159_41315 = state_38151__$1;
(statearr_38159_41315[(2)] = null);

(statearr_38159_41315[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (1))){
var state_38151__$1 = state_38151;
var statearr_38163_41316 = state_38151__$1;
(statearr_38163_41316[(2)] = null);

(statearr_38163_41316[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (4))){
var inst_38108 = (state_38151[(7)]);
var inst_38108__$1 = (state_38151[(2)]);
var inst_38110 = (inst_38108__$1 == null);
var state_38151__$1 = (function (){var statearr_38166 = state_38151;
(statearr_38166[(7)] = inst_38108__$1);

return statearr_38166;
})();
if(cljs.core.truth_(inst_38110)){
var statearr_38167_41325 = state_38151__$1;
(statearr_38167_41325[(1)] = (5));

} else {
var statearr_38169_41326 = state_38151__$1;
(statearr_38169_41326[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (15))){
var inst_38125 = (state_38151[(8)]);
var state_38151__$1 = state_38151;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_38151__$1,(18),to,inst_38125);
} else {
if((state_val_38152 === (21))){
var inst_38141 = (state_38151[(2)]);
var state_38151__$1 = state_38151;
var statearr_38175_41327 = state_38151__$1;
(statearr_38175_41327[(2)] = inst_38141);

(statearr_38175_41327[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (13))){
var inst_38143 = (state_38151[(2)]);
var state_38151__$1 = (function (){var statearr_38179 = state_38151;
(statearr_38179[(9)] = inst_38143);

return statearr_38179;
})();
var statearr_38186_41331 = state_38151__$1;
(statearr_38186_41331[(2)] = null);

(statearr_38186_41331[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (6))){
var inst_38108 = (state_38151[(7)]);
var state_38151__$1 = state_38151;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38151__$1,(11),inst_38108);
} else {
if((state_val_38152 === (17))){
var inst_38135 = (state_38151[(2)]);
var state_38151__$1 = state_38151;
if(cljs.core.truth_(inst_38135)){
var statearr_38189_41334 = state_38151__$1;
(statearr_38189_41334[(1)] = (19));

} else {
var statearr_38190_41339 = state_38151__$1;
(statearr_38190_41339[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (3))){
var inst_38148 = (state_38151[(2)]);
var state_38151__$1 = state_38151;
return cljs.core.async.impl.ioc_helpers.return_chan(state_38151__$1,inst_38148);
} else {
if((state_val_38152 === (12))){
var inst_38121 = (state_38151[(10)]);
var state_38151__$1 = state_38151;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38151__$1,(14),inst_38121);
} else {
if((state_val_38152 === (2))){
var state_38151__$1 = state_38151;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38151__$1,(4),results);
} else {
if((state_val_38152 === (19))){
var state_38151__$1 = state_38151;
var statearr_38195_41341 = state_38151__$1;
(statearr_38195_41341[(2)] = null);

(statearr_38195_41341[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (11))){
var inst_38121 = (state_38151[(2)]);
var state_38151__$1 = (function (){var statearr_38196 = state_38151;
(statearr_38196[(10)] = inst_38121);

return statearr_38196;
})();
var statearr_38197_41349 = state_38151__$1;
(statearr_38197_41349[(2)] = null);

(statearr_38197_41349[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (9))){
var state_38151__$1 = state_38151;
var statearr_38199_41350 = state_38151__$1;
(statearr_38199_41350[(2)] = null);

(statearr_38199_41350[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (5))){
var state_38151__$1 = state_38151;
if(cljs.core.truth_(close_QMARK_)){
var statearr_38200_41351 = state_38151__$1;
(statearr_38200_41351[(1)] = (8));

} else {
var statearr_38203_41352 = state_38151__$1;
(statearr_38203_41352[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (14))){
var inst_38125 = (state_38151[(8)]);
var inst_38125__$1 = (state_38151[(2)]);
var inst_38128 = (inst_38125__$1 == null);
var inst_38129 = cljs.core.not(inst_38128);
var state_38151__$1 = (function (){var statearr_38205 = state_38151;
(statearr_38205[(8)] = inst_38125__$1);

return statearr_38205;
})();
if(inst_38129){
var statearr_38206_41356 = state_38151__$1;
(statearr_38206_41356[(1)] = (15));

} else {
var statearr_38207_41358 = state_38151__$1;
(statearr_38207_41358[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (16))){
var state_38151__$1 = state_38151;
var statearr_38208_41359 = state_38151__$1;
(statearr_38208_41359[(2)] = false);

(statearr_38208_41359[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (10))){
var inst_38116 = (state_38151[(2)]);
var state_38151__$1 = state_38151;
var statearr_38210_41361 = state_38151__$1;
(statearr_38210_41361[(2)] = inst_38116);

(statearr_38210_41361[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (18))){
var inst_38132 = (state_38151[(2)]);
var state_38151__$1 = state_38151;
var statearr_38211_41367 = state_38151__$1;
(statearr_38211_41367[(2)] = inst_38132);

(statearr_38211_41367[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38152 === (8))){
var inst_38113 = cljs.core.async.close_BANG_(to);
var state_38151__$1 = state_38151;
var statearr_38212_41369 = state_38151__$1;
(statearr_38212_41369[(2)] = inst_38113);

(statearr_38212_41369[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0 = (function (){
var statearr_38214 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_38214[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__);

(statearr_38214[(1)] = (1));

return statearr_38214;
});
var cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1 = (function (state_38151){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_38151);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e38216){var ex__26072__auto__ = e38216;
var statearr_38218_41370 = state_38151;
(statearr_38218_41370[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_38151[(4)]))){
var statearr_38219_41371 = state_38151;
(statearr_38219_41371[(1)] = cljs.core.first((state_38151[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41372 = state_38151;
state_38151 = G__41372;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__ = function(state_38151){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1.call(this,state_38151);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__26069__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_38222 = f__26227__auto__();
(statearr_38222[(6)] = c__26226__auto__);

return statearr_38222;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));

return c__26226__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__38234 = arguments.length;
switch (G__38234) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__38242 = arguments.length;
switch (G__38242) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__38271 = arguments.length;
switch (G__38271) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__26226__auto___41383 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_38308){
var state_val_38309 = (state_38308[(1)]);
if((state_val_38309 === (7))){
var inst_38303 = (state_38308[(2)]);
var state_38308__$1 = state_38308;
var statearr_38311_41385 = state_38308__$1;
(statearr_38311_41385[(2)] = inst_38303);

(statearr_38311_41385[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (1))){
var state_38308__$1 = state_38308;
var statearr_38313_41387 = state_38308__$1;
(statearr_38313_41387[(2)] = null);

(statearr_38313_41387[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (4))){
var inst_38279 = (state_38308[(7)]);
var inst_38279__$1 = (state_38308[(2)]);
var inst_38281 = (inst_38279__$1 == null);
var state_38308__$1 = (function (){var statearr_38322 = state_38308;
(statearr_38322[(7)] = inst_38279__$1);

return statearr_38322;
})();
if(cljs.core.truth_(inst_38281)){
var statearr_38323_41388 = state_38308__$1;
(statearr_38323_41388[(1)] = (5));

} else {
var statearr_38325_41389 = state_38308__$1;
(statearr_38325_41389[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (13))){
var state_38308__$1 = state_38308;
var statearr_38326_41391 = state_38308__$1;
(statearr_38326_41391[(2)] = null);

(statearr_38326_41391[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (6))){
var inst_38279 = (state_38308[(7)]);
var inst_38289 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_38279) : p.call(null,inst_38279));
var state_38308__$1 = state_38308;
if(cljs.core.truth_(inst_38289)){
var statearr_38331_41393 = state_38308__$1;
(statearr_38331_41393[(1)] = (9));

} else {
var statearr_38333_41395 = state_38308__$1;
(statearr_38333_41395[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (3))){
var inst_38306 = (state_38308[(2)]);
var state_38308__$1 = state_38308;
return cljs.core.async.impl.ioc_helpers.return_chan(state_38308__$1,inst_38306);
} else {
if((state_val_38309 === (12))){
var state_38308__$1 = state_38308;
var statearr_38335_41401 = state_38308__$1;
(statearr_38335_41401[(2)] = null);

(statearr_38335_41401[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (2))){
var state_38308__$1 = state_38308;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38308__$1,(4),ch);
} else {
if((state_val_38309 === (11))){
var inst_38279 = (state_38308[(7)]);
var inst_38294 = (state_38308[(2)]);
var state_38308__$1 = state_38308;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_38308__$1,(8),inst_38294,inst_38279);
} else {
if((state_val_38309 === (9))){
var state_38308__$1 = state_38308;
var statearr_38338_41403 = state_38308__$1;
(statearr_38338_41403[(2)] = tc);

(statearr_38338_41403[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (5))){
var inst_38286 = cljs.core.async.close_BANG_(tc);
var inst_38287 = cljs.core.async.close_BANG_(fc);
var state_38308__$1 = (function (){var statearr_38339 = state_38308;
(statearr_38339[(8)] = inst_38286);

return statearr_38339;
})();
var statearr_38340_41407 = state_38308__$1;
(statearr_38340_41407[(2)] = inst_38287);

(statearr_38340_41407[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (14))){
var inst_38301 = (state_38308[(2)]);
var state_38308__$1 = state_38308;
var statearr_38341_41408 = state_38308__$1;
(statearr_38341_41408[(2)] = inst_38301);

(statearr_38341_41408[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (10))){
var state_38308__$1 = state_38308;
var statearr_38343_41411 = state_38308__$1;
(statearr_38343_41411[(2)] = fc);

(statearr_38343_41411[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38309 === (8))){
var inst_38296 = (state_38308[(2)]);
var state_38308__$1 = state_38308;
if(cljs.core.truth_(inst_38296)){
var statearr_38346_41412 = state_38308__$1;
(statearr_38346_41412[(1)] = (12));

} else {
var statearr_38347_41414 = state_38308__$1;
(statearr_38347_41414[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_38352 = [null,null,null,null,null,null,null,null,null];
(statearr_38352[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_38352[(1)] = (1));

return statearr_38352;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_38308){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_38308);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e38356){var ex__26072__auto__ = e38356;
var statearr_38357_41421 = state_38308;
(statearr_38357_41421[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_38308[(4)]))){
var statearr_38358_41422 = state_38308;
(statearr_38358_41422[(1)] = cljs.core.first((state_38308[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41423 = state_38308;
state_38308 = G__41423;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_38308){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_38308);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_38360 = f__26227__auto__();
(statearr_38360[(6)] = c__26226__auto___41383);

return statearr_38360;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__26226__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_38392){
var state_val_38393 = (state_38392[(1)]);
if((state_val_38393 === (7))){
var inst_38388 = (state_38392[(2)]);
var state_38392__$1 = state_38392;
var statearr_38401_41427 = state_38392__$1;
(statearr_38401_41427[(2)] = inst_38388);

(statearr_38401_41427[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38393 === (1))){
var inst_38364 = init;
var inst_38365 = inst_38364;
var state_38392__$1 = (function (){var statearr_38405 = state_38392;
(statearr_38405[(7)] = inst_38365);

return statearr_38405;
})();
var statearr_38408_41429 = state_38392__$1;
(statearr_38408_41429[(2)] = null);

(statearr_38408_41429[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38393 === (4))){
var inst_38369 = (state_38392[(8)]);
var inst_38369__$1 = (state_38392[(2)]);
var inst_38370 = (inst_38369__$1 == null);
var state_38392__$1 = (function (){var statearr_38411 = state_38392;
(statearr_38411[(8)] = inst_38369__$1);

return statearr_38411;
})();
if(cljs.core.truth_(inst_38370)){
var statearr_38414_41433 = state_38392__$1;
(statearr_38414_41433[(1)] = (5));

} else {
var statearr_38418_41438 = state_38392__$1;
(statearr_38418_41438[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38393 === (6))){
var inst_38365 = (state_38392[(7)]);
var inst_38369 = (state_38392[(8)]);
var inst_38374 = (state_38392[(9)]);
var inst_38374__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_38365,inst_38369) : f.call(null,inst_38365,inst_38369));
var inst_38376 = cljs.core.reduced_QMARK_(inst_38374__$1);
var state_38392__$1 = (function (){var statearr_38424 = state_38392;
(statearr_38424[(9)] = inst_38374__$1);

return statearr_38424;
})();
if(inst_38376){
var statearr_38428_41444 = state_38392__$1;
(statearr_38428_41444[(1)] = (8));

} else {
var statearr_38429_41446 = state_38392__$1;
(statearr_38429_41446[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38393 === (3))){
var inst_38390 = (state_38392[(2)]);
var state_38392__$1 = state_38392;
return cljs.core.async.impl.ioc_helpers.return_chan(state_38392__$1,inst_38390);
} else {
if((state_val_38393 === (2))){
var state_38392__$1 = state_38392;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38392__$1,(4),ch);
} else {
if((state_val_38393 === (9))){
var inst_38374 = (state_38392[(9)]);
var inst_38365 = inst_38374;
var state_38392__$1 = (function (){var statearr_38433 = state_38392;
(statearr_38433[(7)] = inst_38365);

return statearr_38433;
})();
var statearr_38435_41451 = state_38392__$1;
(statearr_38435_41451[(2)] = null);

(statearr_38435_41451[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38393 === (5))){
var inst_38365 = (state_38392[(7)]);
var state_38392__$1 = state_38392;
var statearr_38438_41452 = state_38392__$1;
(statearr_38438_41452[(2)] = inst_38365);

(statearr_38438_41452[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38393 === (10))){
var inst_38386 = (state_38392[(2)]);
var state_38392__$1 = state_38392;
var statearr_38441_41454 = state_38392__$1;
(statearr_38441_41454[(2)] = inst_38386);

(statearr_38441_41454[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38393 === (8))){
var inst_38374 = (state_38392[(9)]);
var inst_38382 = cljs.core.deref(inst_38374);
var state_38392__$1 = state_38392;
var statearr_38448_41459 = state_38392__$1;
(statearr_38448_41459[(2)] = inst_38382);

(statearr_38448_41459[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__26069__auto__ = null;
var cljs$core$async$reduce_$_state_machine__26069__auto____0 = (function (){
var statearr_38459 = [null,null,null,null,null,null,null,null,null,null];
(statearr_38459[(0)] = cljs$core$async$reduce_$_state_machine__26069__auto__);

(statearr_38459[(1)] = (1));

return statearr_38459;
});
var cljs$core$async$reduce_$_state_machine__26069__auto____1 = (function (state_38392){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_38392);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e38463){var ex__26072__auto__ = e38463;
var statearr_38464_41476 = state_38392;
(statearr_38464_41476[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_38392[(4)]))){
var statearr_38467_41477 = state_38392;
(statearr_38467_41477[(1)] = cljs.core.first((state_38392[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41478 = state_38392;
state_38392 = G__41478;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__26069__auto__ = function(state_38392){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__26069__auto____1.call(this,state_38392);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__26069__auto____0;
cljs$core$async$reduce_$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__26069__auto____1;
return cljs$core$async$reduce_$_state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_38479 = f__26227__auto__();
(statearr_38479[(6)] = c__26226__auto__);

return statearr_38479;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));

return c__26226__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__26226__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_38496){
var state_val_38497 = (state_38496[(1)]);
if((state_val_38497 === (1))){
var inst_38491 = cljs.core.async.reduce(f__$1,init,ch);
var state_38496__$1 = state_38496;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38496__$1,(2),inst_38491);
} else {
if((state_val_38497 === (2))){
var inst_38493 = (state_38496[(2)]);
var inst_38494 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_38493) : f__$1.call(null,inst_38493));
var state_38496__$1 = state_38496;
return cljs.core.async.impl.ioc_helpers.return_chan(state_38496__$1,inst_38494);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__26069__auto__ = null;
var cljs$core$async$transduce_$_state_machine__26069__auto____0 = (function (){
var statearr_38514 = [null,null,null,null,null,null,null];
(statearr_38514[(0)] = cljs$core$async$transduce_$_state_machine__26069__auto__);

(statearr_38514[(1)] = (1));

return statearr_38514;
});
var cljs$core$async$transduce_$_state_machine__26069__auto____1 = (function (state_38496){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_38496);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e38516){var ex__26072__auto__ = e38516;
var statearr_38517_41492 = state_38496;
(statearr_38517_41492[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_38496[(4)]))){
var statearr_38518_41493 = state_38496;
(statearr_38518_41493[(1)] = cljs.core.first((state_38496[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41494 = state_38496;
state_38496 = G__41494;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__26069__auto__ = function(state_38496){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__26069__auto____1.call(this,state_38496);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__26069__auto____0;
cljs$core$async$transduce_$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__26069__auto____1;
return cljs$core$async$transduce_$_state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_38520 = f__26227__auto__();
(statearr_38520[(6)] = c__26226__auto__);

return statearr_38520;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));

return c__26226__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__38528 = arguments.length;
switch (G__38528) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__26226__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_38563){
var state_val_38564 = (state_38563[(1)]);
if((state_val_38564 === (7))){
var inst_38538 = (state_38563[(2)]);
var state_38563__$1 = state_38563;
var statearr_38570_41505 = state_38563__$1;
(statearr_38570_41505[(2)] = inst_38538);

(statearr_38570_41505[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (1))){
var inst_38532 = cljs.core.seq(coll);
var inst_38533 = inst_38532;
var state_38563__$1 = (function (){var statearr_38571 = state_38563;
(statearr_38571[(7)] = inst_38533);

return statearr_38571;
})();
var statearr_38572_41506 = state_38563__$1;
(statearr_38572_41506[(2)] = null);

(statearr_38572_41506[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (4))){
var inst_38533 = (state_38563[(7)]);
var inst_38536 = cljs.core.first(inst_38533);
var state_38563__$1 = state_38563;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_38563__$1,(7),ch,inst_38536);
} else {
if((state_val_38564 === (13))){
var inst_38554 = (state_38563[(2)]);
var state_38563__$1 = state_38563;
var statearr_38577_41507 = state_38563__$1;
(statearr_38577_41507[(2)] = inst_38554);

(statearr_38577_41507[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (6))){
var inst_38541 = (state_38563[(2)]);
var state_38563__$1 = state_38563;
if(cljs.core.truth_(inst_38541)){
var statearr_38579_41508 = state_38563__$1;
(statearr_38579_41508[(1)] = (8));

} else {
var statearr_38580_41509 = state_38563__$1;
(statearr_38580_41509[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (3))){
var inst_38558 = (state_38563[(2)]);
var state_38563__$1 = state_38563;
return cljs.core.async.impl.ioc_helpers.return_chan(state_38563__$1,inst_38558);
} else {
if((state_val_38564 === (12))){
var state_38563__$1 = state_38563;
var statearr_38581_41515 = state_38563__$1;
(statearr_38581_41515[(2)] = null);

(statearr_38581_41515[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (2))){
var inst_38533 = (state_38563[(7)]);
var state_38563__$1 = state_38563;
if(cljs.core.truth_(inst_38533)){
var statearr_38582_41521 = state_38563__$1;
(statearr_38582_41521[(1)] = (4));

} else {
var statearr_38583_41522 = state_38563__$1;
(statearr_38583_41522[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (11))){
var inst_38551 = cljs.core.async.close_BANG_(ch);
var state_38563__$1 = state_38563;
var statearr_38584_41526 = state_38563__$1;
(statearr_38584_41526[(2)] = inst_38551);

(statearr_38584_41526[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (9))){
var state_38563__$1 = state_38563;
if(cljs.core.truth_(close_QMARK_)){
var statearr_38587_41530 = state_38563__$1;
(statearr_38587_41530[(1)] = (11));

} else {
var statearr_38588_41531 = state_38563__$1;
(statearr_38588_41531[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (5))){
var inst_38533 = (state_38563[(7)]);
var state_38563__$1 = state_38563;
var statearr_38589_41538 = state_38563__$1;
(statearr_38589_41538[(2)] = inst_38533);

(statearr_38589_41538[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (10))){
var inst_38556 = (state_38563[(2)]);
var state_38563__$1 = state_38563;
var statearr_38590_41541 = state_38563__$1;
(statearr_38590_41541[(2)] = inst_38556);

(statearr_38590_41541[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38564 === (8))){
var inst_38533 = (state_38563[(7)]);
var inst_38543 = cljs.core.next(inst_38533);
var inst_38533__$1 = inst_38543;
var state_38563__$1 = (function (){var statearr_38591 = state_38563;
(statearr_38591[(7)] = inst_38533__$1);

return statearr_38591;
})();
var statearr_38592_41543 = state_38563__$1;
(statearr_38592_41543[(2)] = null);

(statearr_38592_41543[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_38597 = [null,null,null,null,null,null,null,null];
(statearr_38597[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_38597[(1)] = (1));

return statearr_38597;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_38563){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_38563);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e38598){var ex__26072__auto__ = e38598;
var statearr_38599_41552 = state_38563;
(statearr_38599_41552[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_38563[(4)]))){
var statearr_38600_41553 = state_38563;
(statearr_38600_41553[(1)] = cljs.core.first((state_38563[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41558 = state_38563;
state_38563 = G__41558;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_38563){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_38563);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_38603 = f__26227__auto__();
(statearr_38603[(6)] = c__26226__auto__);

return statearr_38603;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));

return c__26226__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__38606 = arguments.length;
switch (G__38606) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_41566 = (function (_){
var x__4428__auto__ = (((_ == null))?null:_);
var m__4429__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4429__auto__.call(null,_));
} else {
var m__4426__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__4426__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_41566(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_41576 = (function (m,ch,close_QMARK_){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4429__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__4426__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_41576(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_41581 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_41581(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_41582 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_41582(m);
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async38654 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async38654 = (function (ch,cs,meta38655){
this.ch = ch;
this.cs = cs;
this.meta38655 = meta38655;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async38654.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_38656,meta38655__$1){
var self__ = this;
var _38656__$1 = this;
return (new cljs.core.async.t_cljs$core$async38654(self__.ch,self__.cs,meta38655__$1));
}));

(cljs.core.async.t_cljs$core$async38654.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_38656){
var self__ = this;
var _38656__$1 = this;
return self__.meta38655;
}));

(cljs.core.async.t_cljs$core$async38654.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async38654.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async38654.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async38654.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async38654.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async38654.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async38654.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta38655","meta38655",-364363093,null)], null);
}));

(cljs.core.async.t_cljs$core$async38654.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async38654.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async38654");

(cljs.core.async.t_cljs$core$async38654.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async38654");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async38654.
 */
cljs.core.async.__GT_t_cljs$core$async38654 = (function cljs$core$async$mult_$___GT_t_cljs$core$async38654(ch__$1,cs__$1,meta38655){
return (new cljs.core.async.t_cljs$core$async38654(ch__$1,cs__$1,meta38655));
});

}

return (new cljs.core.async.t_cljs$core$async38654(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__26226__auto___41597 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_38865){
var state_val_38866 = (state_38865[(1)]);
if((state_val_38866 === (7))){
var inst_38855 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_38872_41606 = state_38865__$1;
(statearr_38872_41606[(2)] = inst_38855);

(statearr_38872_41606[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (20))){
var inst_38749 = (state_38865[(7)]);
var inst_38764 = cljs.core.first(inst_38749);
var inst_38765 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_38764,(0),null);
var inst_38766 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_38764,(1),null);
var state_38865__$1 = (function (){var statearr_38875 = state_38865;
(statearr_38875[(8)] = inst_38765);

return statearr_38875;
})();
if(cljs.core.truth_(inst_38766)){
var statearr_38876_41616 = state_38865__$1;
(statearr_38876_41616[(1)] = (22));

} else {
var statearr_38877_41617 = state_38865__$1;
(statearr_38877_41617[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (27))){
var inst_38710 = (state_38865[(9)]);
var inst_38796 = (state_38865[(10)]);
var inst_38804 = (state_38865[(11)]);
var inst_38798 = (state_38865[(12)]);
var inst_38804__$1 = cljs.core._nth(inst_38796,inst_38798);
var inst_38805 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_38804__$1,inst_38710,done);
var state_38865__$1 = (function (){var statearr_38880 = state_38865;
(statearr_38880[(11)] = inst_38804__$1);

return statearr_38880;
})();
if(cljs.core.truth_(inst_38805)){
var statearr_38882_41619 = state_38865__$1;
(statearr_38882_41619[(1)] = (30));

} else {
var statearr_38883_41620 = state_38865__$1;
(statearr_38883_41620[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (1))){
var state_38865__$1 = state_38865;
var statearr_38886_41621 = state_38865__$1;
(statearr_38886_41621[(2)] = null);

(statearr_38886_41621[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (24))){
var inst_38749 = (state_38865[(7)]);
var inst_38771 = (state_38865[(2)]);
var inst_38772 = cljs.core.next(inst_38749);
var inst_38725 = inst_38772;
var inst_38726 = null;
var inst_38727 = (0);
var inst_38728 = (0);
var state_38865__$1 = (function (){var statearr_38889 = state_38865;
(statearr_38889[(13)] = inst_38728);

(statearr_38889[(14)] = inst_38727);

(statearr_38889[(15)] = inst_38726);

(statearr_38889[(16)] = inst_38725);

(statearr_38889[(17)] = inst_38771);

return statearr_38889;
})();
var statearr_38891_41627 = state_38865__$1;
(statearr_38891_41627[(2)] = null);

(statearr_38891_41627[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (39))){
var state_38865__$1 = state_38865;
var statearr_38900_41629 = state_38865__$1;
(statearr_38900_41629[(2)] = null);

(statearr_38900_41629[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (4))){
var inst_38710 = (state_38865[(9)]);
var inst_38710__$1 = (state_38865[(2)]);
var inst_38713 = (inst_38710__$1 == null);
var state_38865__$1 = (function (){var statearr_38906 = state_38865;
(statearr_38906[(9)] = inst_38710__$1);

return statearr_38906;
})();
if(cljs.core.truth_(inst_38713)){
var statearr_38907_41633 = state_38865__$1;
(statearr_38907_41633[(1)] = (5));

} else {
var statearr_38908_41634 = state_38865__$1;
(statearr_38908_41634[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (15))){
var inst_38728 = (state_38865[(13)]);
var inst_38727 = (state_38865[(14)]);
var inst_38726 = (state_38865[(15)]);
var inst_38725 = (state_38865[(16)]);
var inst_38745 = (state_38865[(2)]);
var inst_38746 = (inst_38728 + (1));
var tmp38897 = inst_38727;
var tmp38898 = inst_38726;
var tmp38899 = inst_38725;
var inst_38725__$1 = tmp38899;
var inst_38726__$1 = tmp38898;
var inst_38727__$1 = tmp38897;
var inst_38728__$1 = inst_38746;
var state_38865__$1 = (function (){var statearr_38912 = state_38865;
(statearr_38912[(13)] = inst_38728__$1);

(statearr_38912[(14)] = inst_38727__$1);

(statearr_38912[(18)] = inst_38745);

(statearr_38912[(15)] = inst_38726__$1);

(statearr_38912[(16)] = inst_38725__$1);

return statearr_38912;
})();
var statearr_38918_41639 = state_38865__$1;
(statearr_38918_41639[(2)] = null);

(statearr_38918_41639[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (21))){
var inst_38775 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_38928_41642 = state_38865__$1;
(statearr_38928_41642[(2)] = inst_38775);

(statearr_38928_41642[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (31))){
var inst_38804 = (state_38865[(11)]);
var inst_38809 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_38804);
var state_38865__$1 = state_38865;
var statearr_38930_41646 = state_38865__$1;
(statearr_38930_41646[(2)] = inst_38809);

(statearr_38930_41646[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (32))){
var inst_38797 = (state_38865[(19)]);
var inst_38796 = (state_38865[(10)]);
var inst_38798 = (state_38865[(12)]);
var inst_38795 = (state_38865[(20)]);
var inst_38811 = (state_38865[(2)]);
var inst_38812 = (inst_38798 + (1));
var tmp38923 = inst_38797;
var tmp38924 = inst_38796;
var tmp38925 = inst_38795;
var inst_38795__$1 = tmp38925;
var inst_38796__$1 = tmp38924;
var inst_38797__$1 = tmp38923;
var inst_38798__$1 = inst_38812;
var state_38865__$1 = (function (){var statearr_38935 = state_38865;
(statearr_38935[(19)] = inst_38797__$1);

(statearr_38935[(10)] = inst_38796__$1);

(statearr_38935[(21)] = inst_38811);

(statearr_38935[(12)] = inst_38798__$1);

(statearr_38935[(20)] = inst_38795__$1);

return statearr_38935;
})();
var statearr_38936_41653 = state_38865__$1;
(statearr_38936_41653[(2)] = null);

(statearr_38936_41653[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (40))){
var inst_38828 = (state_38865[(22)]);
var inst_38832 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_38828);
var state_38865__$1 = state_38865;
var statearr_38937_41657 = state_38865__$1;
(statearr_38937_41657[(2)] = inst_38832);

(statearr_38937_41657[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (33))){
var inst_38815 = (state_38865[(23)]);
var inst_38818 = cljs.core.chunked_seq_QMARK_(inst_38815);
var state_38865__$1 = state_38865;
if(inst_38818){
var statearr_38938_41661 = state_38865__$1;
(statearr_38938_41661[(1)] = (36));

} else {
var statearr_38939_41662 = state_38865__$1;
(statearr_38939_41662[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (13))){
var inst_38737 = (state_38865[(24)]);
var inst_38742 = cljs.core.async.close_BANG_(inst_38737);
var state_38865__$1 = state_38865;
var statearr_38941_41663 = state_38865__$1;
(statearr_38941_41663[(2)] = inst_38742);

(statearr_38941_41663[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (22))){
var inst_38765 = (state_38865[(8)]);
var inst_38768 = cljs.core.async.close_BANG_(inst_38765);
var state_38865__$1 = state_38865;
var statearr_38943_41665 = state_38865__$1;
(statearr_38943_41665[(2)] = inst_38768);

(statearr_38943_41665[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (36))){
var inst_38815 = (state_38865[(23)]);
var inst_38823 = cljs.core.chunk_first(inst_38815);
var inst_38824 = cljs.core.chunk_rest(inst_38815);
var inst_38825 = cljs.core.count(inst_38823);
var inst_38795 = inst_38824;
var inst_38796 = inst_38823;
var inst_38797 = inst_38825;
var inst_38798 = (0);
var state_38865__$1 = (function (){var statearr_38946 = state_38865;
(statearr_38946[(19)] = inst_38797);

(statearr_38946[(10)] = inst_38796);

(statearr_38946[(12)] = inst_38798);

(statearr_38946[(20)] = inst_38795);

return statearr_38946;
})();
var statearr_38947_41668 = state_38865__$1;
(statearr_38947_41668[(2)] = null);

(statearr_38947_41668[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (41))){
var inst_38815 = (state_38865[(23)]);
var inst_38834 = (state_38865[(2)]);
var inst_38835 = cljs.core.next(inst_38815);
var inst_38795 = inst_38835;
var inst_38796 = null;
var inst_38797 = (0);
var inst_38798 = (0);
var state_38865__$1 = (function (){var statearr_38953 = state_38865;
(statearr_38953[(19)] = inst_38797);

(statearr_38953[(10)] = inst_38796);

(statearr_38953[(12)] = inst_38798);

(statearr_38953[(25)] = inst_38834);

(statearr_38953[(20)] = inst_38795);

return statearr_38953;
})();
var statearr_38956_41672 = state_38865__$1;
(statearr_38956_41672[(2)] = null);

(statearr_38956_41672[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (43))){
var state_38865__$1 = state_38865;
var statearr_38957_41673 = state_38865__$1;
(statearr_38957_41673[(2)] = null);

(statearr_38957_41673[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (29))){
var inst_38843 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_38964_41675 = state_38865__$1;
(statearr_38964_41675[(2)] = inst_38843);

(statearr_38964_41675[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (44))){
var inst_38852 = (state_38865[(2)]);
var state_38865__$1 = (function (){var statearr_38979 = state_38865;
(statearr_38979[(26)] = inst_38852);

return statearr_38979;
})();
var statearr_38987_41676 = state_38865__$1;
(statearr_38987_41676[(2)] = null);

(statearr_38987_41676[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (6))){
var inst_38786 = (state_38865[(27)]);
var inst_38785 = cljs.core.deref(cs);
var inst_38786__$1 = cljs.core.keys(inst_38785);
var inst_38787 = cljs.core.count(inst_38786__$1);
var inst_38788 = cljs.core.reset_BANG_(dctr,inst_38787);
var inst_38794 = cljs.core.seq(inst_38786__$1);
var inst_38795 = inst_38794;
var inst_38796 = null;
var inst_38797 = (0);
var inst_38798 = (0);
var state_38865__$1 = (function (){var statearr_39015 = state_38865;
(statearr_39015[(19)] = inst_38797);

(statearr_39015[(27)] = inst_38786__$1);

(statearr_39015[(28)] = inst_38788);

(statearr_39015[(10)] = inst_38796);

(statearr_39015[(12)] = inst_38798);

(statearr_39015[(20)] = inst_38795);

return statearr_39015;
})();
var statearr_39021_41679 = state_38865__$1;
(statearr_39021_41679[(2)] = null);

(statearr_39021_41679[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (28))){
var inst_38815 = (state_38865[(23)]);
var inst_38795 = (state_38865[(20)]);
var inst_38815__$1 = cljs.core.seq(inst_38795);
var state_38865__$1 = (function (){var statearr_39029 = state_38865;
(statearr_39029[(23)] = inst_38815__$1);

return statearr_39029;
})();
if(inst_38815__$1){
var statearr_39034_41683 = state_38865__$1;
(statearr_39034_41683[(1)] = (33));

} else {
var statearr_39037_41687 = state_38865__$1;
(statearr_39037_41687[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (25))){
var inst_38797 = (state_38865[(19)]);
var inst_38798 = (state_38865[(12)]);
var inst_38801 = (inst_38798 < inst_38797);
var inst_38802 = inst_38801;
var state_38865__$1 = state_38865;
if(cljs.core.truth_(inst_38802)){
var statearr_39052_41688 = state_38865__$1;
(statearr_39052_41688[(1)] = (27));

} else {
var statearr_39053_41693 = state_38865__$1;
(statearr_39053_41693[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (34))){
var state_38865__$1 = state_38865;
var statearr_39058_41697 = state_38865__$1;
(statearr_39058_41697[(2)] = null);

(statearr_39058_41697[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (17))){
var state_38865__$1 = state_38865;
var statearr_39059_41699 = state_38865__$1;
(statearr_39059_41699[(2)] = null);

(statearr_39059_41699[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (3))){
var inst_38857 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
return cljs.core.async.impl.ioc_helpers.return_chan(state_38865__$1,inst_38857);
} else {
if((state_val_38866 === (12))){
var inst_38780 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_39070_41701 = state_38865__$1;
(statearr_39070_41701[(2)] = inst_38780);

(statearr_39070_41701[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (2))){
var state_38865__$1 = state_38865;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38865__$1,(4),ch);
} else {
if((state_val_38866 === (23))){
var state_38865__$1 = state_38865;
var statearr_39078_41702 = state_38865__$1;
(statearr_39078_41702[(2)] = null);

(statearr_39078_41702[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (35))){
var inst_38841 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_39082_41703 = state_38865__$1;
(statearr_39082_41703[(2)] = inst_38841);

(statearr_39082_41703[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (19))){
var inst_38749 = (state_38865[(7)]);
var inst_38753 = cljs.core.chunk_first(inst_38749);
var inst_38754 = cljs.core.chunk_rest(inst_38749);
var inst_38756 = cljs.core.count(inst_38753);
var inst_38725 = inst_38754;
var inst_38726 = inst_38753;
var inst_38727 = inst_38756;
var inst_38728 = (0);
var state_38865__$1 = (function (){var statearr_39093 = state_38865;
(statearr_39093[(13)] = inst_38728);

(statearr_39093[(14)] = inst_38727);

(statearr_39093[(15)] = inst_38726);

(statearr_39093[(16)] = inst_38725);

return statearr_39093;
})();
var statearr_39096_41715 = state_38865__$1;
(statearr_39096_41715[(2)] = null);

(statearr_39096_41715[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (11))){
var inst_38749 = (state_38865[(7)]);
var inst_38725 = (state_38865[(16)]);
var inst_38749__$1 = cljs.core.seq(inst_38725);
var state_38865__$1 = (function (){var statearr_39103 = state_38865;
(statearr_39103[(7)] = inst_38749__$1);

return statearr_39103;
})();
if(inst_38749__$1){
var statearr_39104_41717 = state_38865__$1;
(statearr_39104_41717[(1)] = (16));

} else {
var statearr_39106_41719 = state_38865__$1;
(statearr_39106_41719[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (9))){
var inst_38782 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_39117_41723 = state_38865__$1;
(statearr_39117_41723[(2)] = inst_38782);

(statearr_39117_41723[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (5))){
var inst_38720 = cljs.core.deref(cs);
var inst_38721 = cljs.core.seq(inst_38720);
var inst_38725 = inst_38721;
var inst_38726 = null;
var inst_38727 = (0);
var inst_38728 = (0);
var state_38865__$1 = (function (){var statearr_39120 = state_38865;
(statearr_39120[(13)] = inst_38728);

(statearr_39120[(14)] = inst_38727);

(statearr_39120[(15)] = inst_38726);

(statearr_39120[(16)] = inst_38725);

return statearr_39120;
})();
var statearr_39125_41732 = state_38865__$1;
(statearr_39125_41732[(2)] = null);

(statearr_39125_41732[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (14))){
var state_38865__$1 = state_38865;
var statearr_39133_41735 = state_38865__$1;
(statearr_39133_41735[(2)] = null);

(statearr_39133_41735[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (45))){
var inst_38849 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_39139_41736 = state_38865__$1;
(statearr_39139_41736[(2)] = inst_38849);

(statearr_39139_41736[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (26))){
var inst_38786 = (state_38865[(27)]);
var inst_38845 = (state_38865[(2)]);
var inst_38846 = cljs.core.seq(inst_38786);
var state_38865__$1 = (function (){var statearr_39144 = state_38865;
(statearr_39144[(29)] = inst_38845);

return statearr_39144;
})();
if(inst_38846){
var statearr_39145_41738 = state_38865__$1;
(statearr_39145_41738[(1)] = (42));

} else {
var statearr_39147_41742 = state_38865__$1;
(statearr_39147_41742[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (16))){
var inst_38749 = (state_38865[(7)]);
var inst_38751 = cljs.core.chunked_seq_QMARK_(inst_38749);
var state_38865__$1 = state_38865;
if(inst_38751){
var statearr_39148_41745 = state_38865__$1;
(statearr_39148_41745[(1)] = (19));

} else {
var statearr_39149_41746 = state_38865__$1;
(statearr_39149_41746[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (38))){
var inst_38838 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_39150_41747 = state_38865__$1;
(statearr_39150_41747[(2)] = inst_38838);

(statearr_39150_41747[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (30))){
var state_38865__$1 = state_38865;
var statearr_39151_41749 = state_38865__$1;
(statearr_39151_41749[(2)] = null);

(statearr_39151_41749[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (10))){
var inst_38728 = (state_38865[(13)]);
var inst_38726 = (state_38865[(15)]);
var inst_38736 = cljs.core._nth(inst_38726,inst_38728);
var inst_38737 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_38736,(0),null);
var inst_38738 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_38736,(1),null);
var state_38865__$1 = (function (){var statearr_39153 = state_38865;
(statearr_39153[(24)] = inst_38737);

return statearr_39153;
})();
if(cljs.core.truth_(inst_38738)){
var statearr_39156_41759 = state_38865__$1;
(statearr_39156_41759[(1)] = (13));

} else {
var statearr_39158_41760 = state_38865__$1;
(statearr_39158_41760[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (18))){
var inst_38778 = (state_38865[(2)]);
var state_38865__$1 = state_38865;
var statearr_39159_41770 = state_38865__$1;
(statearr_39159_41770[(2)] = inst_38778);

(statearr_39159_41770[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (42))){
var state_38865__$1 = state_38865;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_38865__$1,(45),dchan);
} else {
if((state_val_38866 === (37))){
var inst_38710 = (state_38865[(9)]);
var inst_38815 = (state_38865[(23)]);
var inst_38828 = (state_38865[(22)]);
var inst_38828__$1 = cljs.core.first(inst_38815);
var inst_38829 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_38828__$1,inst_38710,done);
var state_38865__$1 = (function (){var statearr_39160 = state_38865;
(statearr_39160[(22)] = inst_38828__$1);

return statearr_39160;
})();
if(cljs.core.truth_(inst_38829)){
var statearr_39161_41777 = state_38865__$1;
(statearr_39161_41777[(1)] = (39));

} else {
var statearr_39162_41778 = state_38865__$1;
(statearr_39162_41778[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_38866 === (8))){
var inst_38728 = (state_38865[(13)]);
var inst_38727 = (state_38865[(14)]);
var inst_38730 = (inst_38728 < inst_38727);
var inst_38731 = inst_38730;
var state_38865__$1 = state_38865;
if(cljs.core.truth_(inst_38731)){
var statearr_39164_41787 = state_38865__$1;
(statearr_39164_41787[(1)] = (10));

} else {
var statearr_39166_41788 = state_38865__$1;
(statearr_39166_41788[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__26069__auto__ = null;
var cljs$core$async$mult_$_state_machine__26069__auto____0 = (function (){
var statearr_39167 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_39167[(0)] = cljs$core$async$mult_$_state_machine__26069__auto__);

(statearr_39167[(1)] = (1));

return statearr_39167;
});
var cljs$core$async$mult_$_state_machine__26069__auto____1 = (function (state_38865){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_38865);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e39173){var ex__26072__auto__ = e39173;
var statearr_39174_41798 = state_38865;
(statearr_39174_41798[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_38865[(4)]))){
var statearr_39175_41803 = state_38865;
(statearr_39175_41803[(1)] = cljs.core.first((state_38865[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41804 = state_38865;
state_38865 = G__41804;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__26069__auto__ = function(state_38865){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__26069__auto____1.call(this,state_38865);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__26069__auto____0;
cljs$core$async$mult_$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__26069__auto____1;
return cljs$core$async$mult_$_state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_39176 = f__26227__auto__();
(statearr_39176[(6)] = c__26226__auto___41597);

return statearr_39176;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__39181 = arguments.length;
switch (G__39181) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_41815 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_41815(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_41819 = (function (m,ch){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4429__auto__.call(null,m,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__4426__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_41819(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_41822 = (function (m){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4429__auto__.call(null,m));
} else {
var m__4426__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__4426__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_41822(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_41824 = (function (m,state_map){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4429__auto__.call(null,m,state_map));
} else {
var m__4426__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__4426__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_41824(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_41827 = (function (m,mode){
var x__4428__auto__ = (((m == null))?null:m);
var m__4429__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4429__auto__.call(null,m,mode));
} else {
var m__4426__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__4426__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_41827(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___41832 = arguments.length;
var i__4737__auto___41833 = (0);
while(true){
if((i__4737__auto___41833 < len__4736__auto___41832)){
args__4742__auto__.push((arguments[i__4737__auto___41833]));

var G__41834 = (i__4737__auto___41833 + (1));
i__4737__auto___41833 = G__41834;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__39213){
var map__39214 = p__39213;
var map__39214__$1 = (((((!((map__39214 == null))))?(((((map__39214.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__39214.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__39214):map__39214);
var opts = map__39214__$1;
var statearr_39218_41835 = state;
(statearr_39218_41835[(1)] = cont_block);


var temp__5735__auto__ = cljs.core.async.do_alts((function (val){
var statearr_39221_41840 = state;
(statearr_39221_41840[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5735__auto__)){
var cb = temp__5735__auto__;
var statearr_39224_41841 = state;
(statearr_39224_41841[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq39204){
var G__39205 = cljs.core.first(seq39204);
var seq39204__$1 = cljs.core.next(seq39204);
var G__39206 = cljs.core.first(seq39204__$1);
var seq39204__$2 = cljs.core.next(seq39204__$1);
var G__39207 = cljs.core.first(seq39204__$2);
var seq39204__$3 = cljs.core.next(seq39204__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__39205,G__39206,G__39207,seq39204__$3);
}));

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async39230 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async39230 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta39231){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta39231 = meta39231;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_39232,meta39231__$1){
var self__ = this;
var _39232__$1 = this;
return (new cljs.core.async.t_cljs$core$async39230(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta39231__$1));
}));

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_39232){
var self__ = this;
var _39232__$1 = this;
return self__.meta39231;
}));

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async39230.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async39230.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta39231","meta39231",434986038,null)], null);
}));

(cljs.core.async.t_cljs$core$async39230.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async39230.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async39230");

(cljs.core.async.t_cljs$core$async39230.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async39230");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async39230.
 */
cljs.core.async.__GT_t_cljs$core$async39230 = (function cljs$core$async$mix_$___GT_t_cljs$core$async39230(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta39231){
return (new cljs.core.async.t_cljs$core$async39230(change__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta39231));
});

}

return (new cljs.core.async.t_cljs$core$async39230(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__26226__auto___41865 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_39355){
var state_val_39356 = (state_39355[(1)]);
if((state_val_39356 === (7))){
var inst_39264 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
var statearr_39361_41866 = state_39355__$1;
(statearr_39361_41866[(2)] = inst_39264);

(statearr_39361_41866[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (20))){
var inst_39276 = (state_39355[(7)]);
var state_39355__$1 = state_39355;
var statearr_39362_41867 = state_39355__$1;
(statearr_39362_41867[(2)] = inst_39276);

(statearr_39362_41867[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (27))){
var state_39355__$1 = state_39355;
var statearr_39363_41868 = state_39355__$1;
(statearr_39363_41868[(2)] = null);

(statearr_39363_41868[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (1))){
var inst_39249 = (state_39355[(8)]);
var inst_39249__$1 = calc_state();
var inst_39251 = (inst_39249__$1 == null);
var inst_39252 = cljs.core.not(inst_39251);
var state_39355__$1 = (function (){var statearr_39364 = state_39355;
(statearr_39364[(8)] = inst_39249__$1);

return statearr_39364;
})();
if(inst_39252){
var statearr_39365_41869 = state_39355__$1;
(statearr_39365_41869[(1)] = (2));

} else {
var statearr_39366_41870 = state_39355__$1;
(statearr_39366_41870[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (24))){
var inst_39312 = (state_39355[(9)]);
var inst_39328 = (state_39355[(10)]);
var inst_39302 = (state_39355[(11)]);
var inst_39328__$1 = (inst_39302.cljs$core$IFn$_invoke$arity$1 ? inst_39302.cljs$core$IFn$_invoke$arity$1(inst_39312) : inst_39302.call(null,inst_39312));
var state_39355__$1 = (function (){var statearr_39368 = state_39355;
(statearr_39368[(10)] = inst_39328__$1);

return statearr_39368;
})();
if(cljs.core.truth_(inst_39328__$1)){
var statearr_39369_41871 = state_39355__$1;
(statearr_39369_41871[(1)] = (29));

} else {
var statearr_39370_41872 = state_39355__$1;
(statearr_39370_41872[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (4))){
var inst_39267 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
if(cljs.core.truth_(inst_39267)){
var statearr_39371_41873 = state_39355__$1;
(statearr_39371_41873[(1)] = (8));

} else {
var statearr_39372_41874 = state_39355__$1;
(statearr_39372_41874[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (15))){
var inst_39295 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
if(cljs.core.truth_(inst_39295)){
var statearr_39373_41875 = state_39355__$1;
(statearr_39373_41875[(1)] = (19));

} else {
var statearr_39374_41876 = state_39355__$1;
(statearr_39374_41876[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (21))){
var inst_39300 = (state_39355[(12)]);
var inst_39300__$1 = (state_39355[(2)]);
var inst_39302 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_39300__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_39303 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_39300__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_39305 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_39300__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_39355__$1 = (function (){var statearr_39375 = state_39355;
(statearr_39375[(13)] = inst_39303);

(statearr_39375[(12)] = inst_39300__$1);

(statearr_39375[(11)] = inst_39302);

return statearr_39375;
})();
return cljs.core.async.ioc_alts_BANG_(state_39355__$1,(22),inst_39305);
} else {
if((state_val_39356 === (31))){
var inst_39337 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
if(cljs.core.truth_(inst_39337)){
var statearr_39380_41881 = state_39355__$1;
(statearr_39380_41881[(1)] = (32));

} else {
var statearr_39381_41883 = state_39355__$1;
(statearr_39381_41883[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (32))){
var inst_39311 = (state_39355[(14)]);
var state_39355__$1 = state_39355;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_39355__$1,(35),out,inst_39311);
} else {
if((state_val_39356 === (33))){
var inst_39300 = (state_39355[(12)]);
var inst_39276 = inst_39300;
var state_39355__$1 = (function (){var statearr_39384 = state_39355;
(statearr_39384[(7)] = inst_39276);

return statearr_39384;
})();
var statearr_39385_41885 = state_39355__$1;
(statearr_39385_41885[(2)] = null);

(statearr_39385_41885[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (13))){
var inst_39276 = (state_39355[(7)]);
var inst_39284 = inst_39276.cljs$lang$protocol_mask$partition0$;
var inst_39285 = (inst_39284 & (64));
var inst_39286 = inst_39276.cljs$core$ISeq$;
var inst_39287 = (cljs.core.PROTOCOL_SENTINEL === inst_39286);
var inst_39288 = ((inst_39285) || (inst_39287));
var state_39355__$1 = state_39355;
if(cljs.core.truth_(inst_39288)){
var statearr_39390_41886 = state_39355__$1;
(statearr_39390_41886[(1)] = (16));

} else {
var statearr_39391_41887 = state_39355__$1;
(statearr_39391_41887[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (22))){
var inst_39312 = (state_39355[(9)]);
var inst_39311 = (state_39355[(14)]);
var inst_39310 = (state_39355[(2)]);
var inst_39311__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_39310,(0),null);
var inst_39312__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_39310,(1),null);
var inst_39313 = (inst_39311__$1 == null);
var inst_39314 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_39312__$1,change);
var inst_39315 = ((inst_39313) || (inst_39314));
var state_39355__$1 = (function (){var statearr_39392 = state_39355;
(statearr_39392[(9)] = inst_39312__$1);

(statearr_39392[(14)] = inst_39311__$1);

return statearr_39392;
})();
if(cljs.core.truth_(inst_39315)){
var statearr_39393_41891 = state_39355__$1;
(statearr_39393_41891[(1)] = (23));

} else {
var statearr_39394_41892 = state_39355__$1;
(statearr_39394_41892[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (36))){
var inst_39300 = (state_39355[(12)]);
var inst_39276 = inst_39300;
var state_39355__$1 = (function (){var statearr_39395 = state_39355;
(statearr_39395[(7)] = inst_39276);

return statearr_39395;
})();
var statearr_39396_41894 = state_39355__$1;
(statearr_39396_41894[(2)] = null);

(statearr_39396_41894[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (29))){
var inst_39328 = (state_39355[(10)]);
var state_39355__$1 = state_39355;
var statearr_39397_41896 = state_39355__$1;
(statearr_39397_41896[(2)] = inst_39328);

(statearr_39397_41896[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (6))){
var state_39355__$1 = state_39355;
var statearr_39398_41897 = state_39355__$1;
(statearr_39398_41897[(2)] = false);

(statearr_39398_41897[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (28))){
var inst_39323 = (state_39355[(2)]);
var inst_39325 = calc_state();
var inst_39276 = inst_39325;
var state_39355__$1 = (function (){var statearr_39399 = state_39355;
(statearr_39399[(15)] = inst_39323);

(statearr_39399[(7)] = inst_39276);

return statearr_39399;
})();
var statearr_39400_41898 = state_39355__$1;
(statearr_39400_41898[(2)] = null);

(statearr_39400_41898[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (25))){
var inst_39351 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
var statearr_39401_41900 = state_39355__$1;
(statearr_39401_41900[(2)] = inst_39351);

(statearr_39401_41900[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (34))){
var inst_39349 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
var statearr_39403_41901 = state_39355__$1;
(statearr_39403_41901[(2)] = inst_39349);

(statearr_39403_41901[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (17))){
var state_39355__$1 = state_39355;
var statearr_39405_41902 = state_39355__$1;
(statearr_39405_41902[(2)] = false);

(statearr_39405_41902[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (3))){
var state_39355__$1 = state_39355;
var statearr_39406_41904 = state_39355__$1;
(statearr_39406_41904[(2)] = false);

(statearr_39406_41904[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (12))){
var inst_39353 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
return cljs.core.async.impl.ioc_helpers.return_chan(state_39355__$1,inst_39353);
} else {
if((state_val_39356 === (2))){
var inst_39249 = (state_39355[(8)]);
var inst_39255 = inst_39249.cljs$lang$protocol_mask$partition0$;
var inst_39256 = (inst_39255 & (64));
var inst_39257 = inst_39249.cljs$core$ISeq$;
var inst_39258 = (cljs.core.PROTOCOL_SENTINEL === inst_39257);
var inst_39259 = ((inst_39256) || (inst_39258));
var state_39355__$1 = state_39355;
if(cljs.core.truth_(inst_39259)){
var statearr_39407_41906 = state_39355__$1;
(statearr_39407_41906[(1)] = (5));

} else {
var statearr_39408_41907 = state_39355__$1;
(statearr_39408_41907[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (23))){
var inst_39311 = (state_39355[(14)]);
var inst_39318 = (inst_39311 == null);
var state_39355__$1 = state_39355;
if(cljs.core.truth_(inst_39318)){
var statearr_39417_41908 = state_39355__$1;
(statearr_39417_41908[(1)] = (26));

} else {
var statearr_39418_41909 = state_39355__$1;
(statearr_39418_41909[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (35))){
var inst_39340 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
if(cljs.core.truth_(inst_39340)){
var statearr_39425_41910 = state_39355__$1;
(statearr_39425_41910[(1)] = (36));

} else {
var statearr_39426_41911 = state_39355__$1;
(statearr_39426_41911[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (19))){
var inst_39276 = (state_39355[(7)]);
var inst_39297 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_39276);
var state_39355__$1 = state_39355;
var statearr_39427_41913 = state_39355__$1;
(statearr_39427_41913[(2)] = inst_39297);

(statearr_39427_41913[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (11))){
var inst_39276 = (state_39355[(7)]);
var inst_39281 = (inst_39276 == null);
var inst_39282 = cljs.core.not(inst_39281);
var state_39355__$1 = state_39355;
if(inst_39282){
var statearr_39428_41917 = state_39355__$1;
(statearr_39428_41917[(1)] = (13));

} else {
var statearr_39431_41918 = state_39355__$1;
(statearr_39431_41918[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (9))){
var inst_39249 = (state_39355[(8)]);
var state_39355__$1 = state_39355;
var statearr_39432_41919 = state_39355__$1;
(statearr_39432_41919[(2)] = inst_39249);

(statearr_39432_41919[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (5))){
var state_39355__$1 = state_39355;
var statearr_39433_41924 = state_39355__$1;
(statearr_39433_41924[(2)] = true);

(statearr_39433_41924[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (14))){
var state_39355__$1 = state_39355;
var statearr_39434_41925 = state_39355__$1;
(statearr_39434_41925[(2)] = false);

(statearr_39434_41925[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (26))){
var inst_39312 = (state_39355[(9)]);
var inst_39320 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_39312);
var state_39355__$1 = state_39355;
var statearr_39435_41927 = state_39355__$1;
(statearr_39435_41927[(2)] = inst_39320);

(statearr_39435_41927[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (16))){
var state_39355__$1 = state_39355;
var statearr_39437_41932 = state_39355__$1;
(statearr_39437_41932[(2)] = true);

(statearr_39437_41932[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (38))){
var inst_39345 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
var statearr_39438_41935 = state_39355__$1;
(statearr_39438_41935[(2)] = inst_39345);

(statearr_39438_41935[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (30))){
var inst_39303 = (state_39355[(13)]);
var inst_39312 = (state_39355[(9)]);
var inst_39302 = (state_39355[(11)]);
var inst_39332 = cljs.core.empty_QMARK_(inst_39302);
var inst_39333 = (inst_39303.cljs$core$IFn$_invoke$arity$1 ? inst_39303.cljs$core$IFn$_invoke$arity$1(inst_39312) : inst_39303.call(null,inst_39312));
var inst_39334 = cljs.core.not(inst_39333);
var inst_39335 = ((inst_39332) && (inst_39334));
var state_39355__$1 = state_39355;
var statearr_39439_41936 = state_39355__$1;
(statearr_39439_41936[(2)] = inst_39335);

(statearr_39439_41936[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (10))){
var inst_39249 = (state_39355[(8)]);
var inst_39272 = (state_39355[(2)]);
var inst_39273 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_39272,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_39274 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_39272,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_39275 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_39272,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_39276 = inst_39249;
var state_39355__$1 = (function (){var statearr_39444 = state_39355;
(statearr_39444[(7)] = inst_39276);

(statearr_39444[(16)] = inst_39273);

(statearr_39444[(17)] = inst_39274);

(statearr_39444[(18)] = inst_39275);

return statearr_39444;
})();
var statearr_39445_41944 = state_39355__$1;
(statearr_39445_41944[(2)] = null);

(statearr_39445_41944[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (18))){
var inst_39292 = (state_39355[(2)]);
var state_39355__$1 = state_39355;
var statearr_39446_41945 = state_39355__$1;
(statearr_39446_41945[(2)] = inst_39292);

(statearr_39446_41945[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (37))){
var state_39355__$1 = state_39355;
var statearr_39447_41950 = state_39355__$1;
(statearr_39447_41950[(2)] = null);

(statearr_39447_41950[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39356 === (8))){
var inst_39249 = (state_39355[(8)]);
var inst_39269 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_39249);
var state_39355__$1 = state_39355;
var statearr_39448_41952 = state_39355__$1;
(statearr_39448_41952[(2)] = inst_39269);

(statearr_39448_41952[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__26069__auto__ = null;
var cljs$core$async$mix_$_state_machine__26069__auto____0 = (function (){
var statearr_39457 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_39457[(0)] = cljs$core$async$mix_$_state_machine__26069__auto__);

(statearr_39457[(1)] = (1));

return statearr_39457;
});
var cljs$core$async$mix_$_state_machine__26069__auto____1 = (function (state_39355){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_39355);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e39459){var ex__26072__auto__ = e39459;
var statearr_39460_41967 = state_39355;
(statearr_39460_41967[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_39355[(4)]))){
var statearr_39461_41973 = state_39355;
(statearr_39461_41973[(1)] = cljs.core.first((state_39355[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__41977 = state_39355;
state_39355 = G__41977;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__26069__auto__ = function(state_39355){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__26069__auto____1.call(this,state_39355);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__26069__auto____0;
cljs$core$async$mix_$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__26069__auto____1;
return cljs$core$async$mix_$_state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_39462 = f__26227__auto__();
(statearr_39462[(6)] = c__26226__auto___41865);

return statearr_39462;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_42001 = (function (p,v,ch,close_QMARK_){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4429__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__4426__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$4 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__4426__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_42001(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_42007 = (function (p,v,ch){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4429__auto__.call(null,p,v,ch));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$3 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__4426__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_42007(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_42015 = (function() {
var G__42016 = null;
var G__42016__1 = (function (p){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4429__auto__.call(null,p));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__4426__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__42016__2 = (function (p,v){
var x__4428__auto__ = (((p == null))?null:p);
var m__4429__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4429__auto__.call(null,p,v));
} else {
var m__4426__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$2 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__4426__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__42016 = function(p,v){
switch(arguments.length){
case 1:
return G__42016__1.call(this,p);
case 2:
return G__42016__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__42016.cljs$core$IFn$_invoke$arity$1 = G__42016__1;
G__42016.cljs$core$IFn$_invoke$arity$2 = G__42016__2;
return G__42016;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__39527 = arguments.length;
switch (G__39527) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_42015(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_42015(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__39540 = arguments.length;
switch (G__39540) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__4126__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__39536_SHARP_){
if(cljs.core.truth_((p1__39536_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__39536_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__39536_SHARP_.call(null,topic)))){
return p1__39536_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__39536_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async39547 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async39547 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta39548){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta39548 = meta39548;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_39549,meta39548__$1){
var self__ = this;
var _39549__$1 = this;
return (new cljs.core.async.t_cljs$core$async39547(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta39548__$1));
}));

(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_39549){
var self__ = this;
var _39549__$1 = this;
return self__.meta39548;
}));

(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5735__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5735__auto__)){
var m = temp__5735__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async39547.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async39547.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta39548","meta39548",-1652482651,null)], null);
}));

(cljs.core.async.t_cljs$core$async39547.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async39547.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async39547");

(cljs.core.async.t_cljs$core$async39547.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async39547");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async39547.
 */
cljs.core.async.__GT_t_cljs$core$async39547 = (function cljs$core$async$__GT_t_cljs$core$async39547(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta39548){
return (new cljs.core.async.t_cljs$core$async39547(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta39548));
});

}

return (new cljs.core.async.t_cljs$core$async39547(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__26226__auto___42031 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_39638){
var state_val_39639 = (state_39638[(1)]);
if((state_val_39639 === (7))){
var inst_39634 = (state_39638[(2)]);
var state_39638__$1 = state_39638;
var statearr_39646_42032 = state_39638__$1;
(statearr_39646_42032[(2)] = inst_39634);

(statearr_39646_42032[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (20))){
var state_39638__$1 = state_39638;
var statearr_39654_42034 = state_39638__$1;
(statearr_39654_42034[(2)] = null);

(statearr_39654_42034[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (1))){
var state_39638__$1 = state_39638;
var statearr_39655_42037 = state_39638__$1;
(statearr_39655_42037[(2)] = null);

(statearr_39655_42037[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (24))){
var inst_39617 = (state_39638[(7)]);
var inst_39626 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_39617);
var state_39638__$1 = state_39638;
var statearr_39666_42040 = state_39638__$1;
(statearr_39666_42040[(2)] = inst_39626);

(statearr_39666_42040[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (4))){
var inst_39563 = (state_39638[(8)]);
var inst_39563__$1 = (state_39638[(2)]);
var inst_39564 = (inst_39563__$1 == null);
var state_39638__$1 = (function (){var statearr_39688 = state_39638;
(statearr_39688[(8)] = inst_39563__$1);

return statearr_39688;
})();
if(cljs.core.truth_(inst_39564)){
var statearr_39691_42043 = state_39638__$1;
(statearr_39691_42043[(1)] = (5));

} else {
var statearr_39692_42044 = state_39638__$1;
(statearr_39692_42044[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (15))){
var inst_39611 = (state_39638[(2)]);
var state_39638__$1 = state_39638;
var statearr_39693_42045 = state_39638__$1;
(statearr_39693_42045[(2)] = inst_39611);

(statearr_39693_42045[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (21))){
var inst_39631 = (state_39638[(2)]);
var state_39638__$1 = (function (){var statearr_39694 = state_39638;
(statearr_39694[(9)] = inst_39631);

return statearr_39694;
})();
var statearr_39695_42047 = state_39638__$1;
(statearr_39695_42047[(2)] = null);

(statearr_39695_42047[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (13))){
var inst_39591 = (state_39638[(10)]);
var inst_39595 = cljs.core.chunked_seq_QMARK_(inst_39591);
var state_39638__$1 = state_39638;
if(inst_39595){
var statearr_39696_42052 = state_39638__$1;
(statearr_39696_42052[(1)] = (16));

} else {
var statearr_39697_42053 = state_39638__$1;
(statearr_39697_42053[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (22))){
var inst_39623 = (state_39638[(2)]);
var state_39638__$1 = state_39638;
if(cljs.core.truth_(inst_39623)){
var statearr_39698_42054 = state_39638__$1;
(statearr_39698_42054[(1)] = (23));

} else {
var statearr_39706_42055 = state_39638__$1;
(statearr_39706_42055[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (6))){
var inst_39619 = (state_39638[(11)]);
var inst_39563 = (state_39638[(8)]);
var inst_39617 = (state_39638[(7)]);
var inst_39617__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_39563) : topic_fn.call(null,inst_39563));
var inst_39618 = cljs.core.deref(mults);
var inst_39619__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_39618,inst_39617__$1);
var state_39638__$1 = (function (){var statearr_39713 = state_39638;
(statearr_39713[(11)] = inst_39619__$1);

(statearr_39713[(7)] = inst_39617__$1);

return statearr_39713;
})();
if(cljs.core.truth_(inst_39619__$1)){
var statearr_39714_42062 = state_39638__$1;
(statearr_39714_42062[(1)] = (19));

} else {
var statearr_39715_42064 = state_39638__$1;
(statearr_39715_42064[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (25))){
var inst_39628 = (state_39638[(2)]);
var state_39638__$1 = state_39638;
var statearr_39716_42066 = state_39638__$1;
(statearr_39716_42066[(2)] = inst_39628);

(statearr_39716_42066[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (17))){
var inst_39591 = (state_39638[(10)]);
var inst_39602 = cljs.core.first(inst_39591);
var inst_39603 = cljs.core.async.muxch_STAR_(inst_39602);
var inst_39604 = cljs.core.async.close_BANG_(inst_39603);
var inst_39605 = cljs.core.next(inst_39591);
var inst_39573 = inst_39605;
var inst_39574 = null;
var inst_39575 = (0);
var inst_39576 = (0);
var state_39638__$1 = (function (){var statearr_39718 = state_39638;
(statearr_39718[(12)] = inst_39576);

(statearr_39718[(13)] = inst_39573);

(statearr_39718[(14)] = inst_39575);

(statearr_39718[(15)] = inst_39604);

(statearr_39718[(16)] = inst_39574);

return statearr_39718;
})();
var statearr_39719_42070 = state_39638__$1;
(statearr_39719_42070[(2)] = null);

(statearr_39719_42070[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (3))){
var inst_39636 = (state_39638[(2)]);
var state_39638__$1 = state_39638;
return cljs.core.async.impl.ioc_helpers.return_chan(state_39638__$1,inst_39636);
} else {
if((state_val_39639 === (12))){
var inst_39613 = (state_39638[(2)]);
var state_39638__$1 = state_39638;
var statearr_39726_42071 = state_39638__$1;
(statearr_39726_42071[(2)] = inst_39613);

(statearr_39726_42071[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (2))){
var state_39638__$1 = state_39638;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_39638__$1,(4),ch);
} else {
if((state_val_39639 === (23))){
var state_39638__$1 = state_39638;
var statearr_39727_42075 = state_39638__$1;
(statearr_39727_42075[(2)] = null);

(statearr_39727_42075[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (19))){
var inst_39619 = (state_39638[(11)]);
var inst_39563 = (state_39638[(8)]);
var inst_39621 = cljs.core.async.muxch_STAR_(inst_39619);
var state_39638__$1 = state_39638;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_39638__$1,(22),inst_39621,inst_39563);
} else {
if((state_val_39639 === (11))){
var inst_39573 = (state_39638[(13)]);
var inst_39591 = (state_39638[(10)]);
var inst_39591__$1 = cljs.core.seq(inst_39573);
var state_39638__$1 = (function (){var statearr_39728 = state_39638;
(statearr_39728[(10)] = inst_39591__$1);

return statearr_39728;
})();
if(inst_39591__$1){
var statearr_39730_42081 = state_39638__$1;
(statearr_39730_42081[(1)] = (13));

} else {
var statearr_39731_42082 = state_39638__$1;
(statearr_39731_42082[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (9))){
var inst_39615 = (state_39638[(2)]);
var state_39638__$1 = state_39638;
var statearr_39732_42083 = state_39638__$1;
(statearr_39732_42083[(2)] = inst_39615);

(statearr_39732_42083[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (5))){
var inst_39570 = cljs.core.deref(mults);
var inst_39571 = cljs.core.vals(inst_39570);
var inst_39572 = cljs.core.seq(inst_39571);
var inst_39573 = inst_39572;
var inst_39574 = null;
var inst_39575 = (0);
var inst_39576 = (0);
var state_39638__$1 = (function (){var statearr_39734 = state_39638;
(statearr_39734[(12)] = inst_39576);

(statearr_39734[(13)] = inst_39573);

(statearr_39734[(14)] = inst_39575);

(statearr_39734[(16)] = inst_39574);

return statearr_39734;
})();
var statearr_39738_42085 = state_39638__$1;
(statearr_39738_42085[(2)] = null);

(statearr_39738_42085[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (14))){
var state_39638__$1 = state_39638;
var statearr_39746_42086 = state_39638__$1;
(statearr_39746_42086[(2)] = null);

(statearr_39746_42086[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (16))){
var inst_39591 = (state_39638[(10)]);
var inst_39597 = cljs.core.chunk_first(inst_39591);
var inst_39598 = cljs.core.chunk_rest(inst_39591);
var inst_39599 = cljs.core.count(inst_39597);
var inst_39573 = inst_39598;
var inst_39574 = inst_39597;
var inst_39575 = inst_39599;
var inst_39576 = (0);
var state_39638__$1 = (function (){var statearr_39754 = state_39638;
(statearr_39754[(12)] = inst_39576);

(statearr_39754[(13)] = inst_39573);

(statearr_39754[(14)] = inst_39575);

(statearr_39754[(16)] = inst_39574);

return statearr_39754;
})();
var statearr_39755_42089 = state_39638__$1;
(statearr_39755_42089[(2)] = null);

(statearr_39755_42089[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (10))){
var inst_39576 = (state_39638[(12)]);
var inst_39573 = (state_39638[(13)]);
var inst_39575 = (state_39638[(14)]);
var inst_39574 = (state_39638[(16)]);
var inst_39583 = cljs.core._nth(inst_39574,inst_39576);
var inst_39586 = cljs.core.async.muxch_STAR_(inst_39583);
var inst_39587 = cljs.core.async.close_BANG_(inst_39586);
var inst_39588 = (inst_39576 + (1));
var tmp39739 = inst_39573;
var tmp39740 = inst_39575;
var tmp39741 = inst_39574;
var inst_39573__$1 = tmp39739;
var inst_39574__$1 = tmp39741;
var inst_39575__$1 = tmp39740;
var inst_39576__$1 = inst_39588;
var state_39638__$1 = (function (){var statearr_39763 = state_39638;
(statearr_39763[(12)] = inst_39576__$1);

(statearr_39763[(13)] = inst_39573__$1);

(statearr_39763[(14)] = inst_39575__$1);

(statearr_39763[(17)] = inst_39587);

(statearr_39763[(16)] = inst_39574__$1);

return statearr_39763;
})();
var statearr_39764_42092 = state_39638__$1;
(statearr_39764_42092[(2)] = null);

(statearr_39764_42092[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (18))){
var inst_39608 = (state_39638[(2)]);
var state_39638__$1 = state_39638;
var statearr_39765_42094 = state_39638__$1;
(statearr_39765_42094[(2)] = inst_39608);

(statearr_39765_42094[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39639 === (8))){
var inst_39576 = (state_39638[(12)]);
var inst_39575 = (state_39638[(14)]);
var inst_39578 = (inst_39576 < inst_39575);
var inst_39579 = inst_39578;
var state_39638__$1 = state_39638;
if(cljs.core.truth_(inst_39579)){
var statearr_39769_42096 = state_39638__$1;
(statearr_39769_42096[(1)] = (10));

} else {
var statearr_39770_42097 = state_39638__$1;
(statearr_39770_42097[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_39771 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_39771[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_39771[(1)] = (1));

return statearr_39771;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_39638){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_39638);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e39776){var ex__26072__auto__ = e39776;
var statearr_39777_42100 = state_39638;
(statearr_39777_42100[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_39638[(4)]))){
var statearr_39778_42101 = state_39638;
(statearr_39778_42101[(1)] = cljs.core.first((state_39638[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42102 = state_39638;
state_39638 = G__42102;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_39638){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_39638);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_39780 = f__26227__auto__();
(statearr_39780[(6)] = c__26226__auto___42031);

return statearr_39780;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__39785 = arguments.length;
switch (G__39785) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__39794 = arguments.length;
switch (G__39794) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__39805 = arguments.length;
switch (G__39805) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
var c__26226__auto___42117 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_39867){
var state_val_39868 = (state_39867[(1)]);
if((state_val_39868 === (7))){
var state_39867__$1 = state_39867;
var statearr_39869_42121 = state_39867__$1;
(statearr_39869_42121[(2)] = null);

(statearr_39869_42121[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (1))){
var state_39867__$1 = state_39867;
var statearr_39870_42122 = state_39867__$1;
(statearr_39870_42122[(2)] = null);

(statearr_39870_42122[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (4))){
var inst_39819 = (state_39867[(7)]);
var inst_39818 = (state_39867[(8)]);
var inst_39821 = (inst_39819 < inst_39818);
var state_39867__$1 = state_39867;
if(cljs.core.truth_(inst_39821)){
var statearr_39871_42124 = state_39867__$1;
(statearr_39871_42124[(1)] = (6));

} else {
var statearr_39872_42128 = state_39867__$1;
(statearr_39872_42128[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (15))){
var inst_39853 = (state_39867[(9)]);
var inst_39858 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_39853);
var state_39867__$1 = state_39867;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_39867__$1,(17),out,inst_39858);
} else {
if((state_val_39868 === (13))){
var inst_39853 = (state_39867[(9)]);
var inst_39853__$1 = (state_39867[(2)]);
var inst_39854 = cljs.core.some(cljs.core.nil_QMARK_,inst_39853__$1);
var state_39867__$1 = (function (){var statearr_39878 = state_39867;
(statearr_39878[(9)] = inst_39853__$1);

return statearr_39878;
})();
if(cljs.core.truth_(inst_39854)){
var statearr_39879_42135 = state_39867__$1;
(statearr_39879_42135[(1)] = (14));

} else {
var statearr_39880_42136 = state_39867__$1;
(statearr_39880_42136[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (6))){
var state_39867__$1 = state_39867;
var statearr_39885_42137 = state_39867__$1;
(statearr_39885_42137[(2)] = null);

(statearr_39885_42137[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (17))){
var inst_39860 = (state_39867[(2)]);
var state_39867__$1 = (function (){var statearr_39891 = state_39867;
(statearr_39891[(10)] = inst_39860);

return statearr_39891;
})();
var statearr_39892_42138 = state_39867__$1;
(statearr_39892_42138[(2)] = null);

(statearr_39892_42138[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (3))){
var inst_39865 = (state_39867[(2)]);
var state_39867__$1 = state_39867;
return cljs.core.async.impl.ioc_helpers.return_chan(state_39867__$1,inst_39865);
} else {
if((state_val_39868 === (12))){
var _ = (function (){var statearr_39893 = state_39867;
(statearr_39893[(4)] = cljs.core.rest((state_39867[(4)])));

return statearr_39893;
})();
var state_39867__$1 = state_39867;
var ex39889 = (state_39867__$1[(2)]);
var statearr_39894_42143 = state_39867__$1;
(statearr_39894_42143[(5)] = ex39889);


if((ex39889 instanceof Object)){
var statearr_39895_42144 = state_39867__$1;
(statearr_39895_42144[(1)] = (11));

(statearr_39895_42144[(5)] = null);

} else {
throw ex39889;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (2))){
var inst_39815 = cljs.core.reset_BANG_(dctr,cnt);
var inst_39818 = cnt;
var inst_39819 = (0);
var state_39867__$1 = (function (){var statearr_39897 = state_39867;
(statearr_39897[(7)] = inst_39819);

(statearr_39897[(8)] = inst_39818);

(statearr_39897[(11)] = inst_39815);

return statearr_39897;
})();
var statearr_39898_42149 = state_39867__$1;
(statearr_39898_42149[(2)] = null);

(statearr_39898_42149[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (11))){
var inst_39828 = (state_39867[(2)]);
var inst_39829 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_39867__$1 = (function (){var statearr_39900 = state_39867;
(statearr_39900[(12)] = inst_39828);

return statearr_39900;
})();
var statearr_39901_42150 = state_39867__$1;
(statearr_39901_42150[(2)] = inst_39829);

(statearr_39901_42150[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (9))){
var inst_39819 = (state_39867[(7)]);
var _ = (function (){var statearr_39902 = state_39867;
(statearr_39902[(4)] = cljs.core.cons((12),(state_39867[(4)])));

return statearr_39902;
})();
var inst_39838 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_39819) : chs__$1.call(null,inst_39819));
var inst_39839 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_39819) : done.call(null,inst_39819));
var inst_39840 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_39838,inst_39839);
var ___$1 = (function (){var statearr_39905 = state_39867;
(statearr_39905[(4)] = cljs.core.rest((state_39867[(4)])));

return statearr_39905;
})();
var state_39867__$1 = state_39867;
var statearr_39906_42160 = state_39867__$1;
(statearr_39906_42160[(2)] = inst_39840);

(statearr_39906_42160[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (5))){
var inst_39850 = (state_39867[(2)]);
var state_39867__$1 = (function (){var statearr_39907 = state_39867;
(statearr_39907[(13)] = inst_39850);

return statearr_39907;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_39867__$1,(13),dchan);
} else {
if((state_val_39868 === (14))){
var inst_39856 = cljs.core.async.close_BANG_(out);
var state_39867__$1 = state_39867;
var statearr_39908_42161 = state_39867__$1;
(statearr_39908_42161[(2)] = inst_39856);

(statearr_39908_42161[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (16))){
var inst_39863 = (state_39867[(2)]);
var state_39867__$1 = state_39867;
var statearr_39909_42163 = state_39867__$1;
(statearr_39909_42163[(2)] = inst_39863);

(statearr_39909_42163[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (10))){
var inst_39819 = (state_39867[(7)]);
var inst_39843 = (state_39867[(2)]);
var inst_39844 = (inst_39819 + (1));
var inst_39819__$1 = inst_39844;
var state_39867__$1 = (function (){var statearr_39910 = state_39867;
(statearr_39910[(7)] = inst_39819__$1);

(statearr_39910[(14)] = inst_39843);

return statearr_39910;
})();
var statearr_39911_42166 = state_39867__$1;
(statearr_39911_42166[(2)] = null);

(statearr_39911_42166[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39868 === (8))){
var inst_39848 = (state_39867[(2)]);
var state_39867__$1 = state_39867;
var statearr_39912_42168 = state_39867__$1;
(statearr_39912_42168[(2)] = inst_39848);

(statearr_39912_42168[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_39913 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_39913[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_39913[(1)] = (1));

return statearr_39913;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_39867){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_39867);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e39914){var ex__26072__auto__ = e39914;
var statearr_39915_42173 = state_39867;
(statearr_39915_42173[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_39867[(4)]))){
var statearr_39916_42180 = state_39867;
(statearr_39916_42180[(1)] = cljs.core.first((state_39867[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42182 = state_39867;
state_39867 = G__42182;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_39867){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_39867);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_39921 = f__26227__auto__();
(statearr_39921[(6)] = c__26226__auto___42117);

return statearr_39921;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__39943 = arguments.length;
switch (G__39943) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__26226__auto___42193 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_39992){
var state_val_39993 = (state_39992[(1)]);
if((state_val_39993 === (7))){
var inst_39971 = (state_39992[(7)]);
var inst_39969 = (state_39992[(8)]);
var inst_39969__$1 = (state_39992[(2)]);
var inst_39971__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_39969__$1,(0),null);
var inst_39972 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_39969__$1,(1),null);
var inst_39973 = (inst_39971__$1 == null);
var state_39992__$1 = (function (){var statearr_40002 = state_39992;
(statearr_40002[(7)] = inst_39971__$1);

(statearr_40002[(9)] = inst_39972);

(statearr_40002[(8)] = inst_39969__$1);

return statearr_40002;
})();
if(cljs.core.truth_(inst_39973)){
var statearr_40003_42195 = state_39992__$1;
(statearr_40003_42195[(1)] = (8));

} else {
var statearr_40004_42196 = state_39992__$1;
(statearr_40004_42196[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39993 === (1))){
var inst_39959 = cljs.core.vec(chs);
var inst_39960 = inst_39959;
var state_39992__$1 = (function (){var statearr_40010 = state_39992;
(statearr_40010[(10)] = inst_39960);

return statearr_40010;
})();
var statearr_40011_42198 = state_39992__$1;
(statearr_40011_42198[(2)] = null);

(statearr_40011_42198[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39993 === (4))){
var inst_39960 = (state_39992[(10)]);
var state_39992__$1 = state_39992;
return cljs.core.async.ioc_alts_BANG_(state_39992__$1,(7),inst_39960);
} else {
if((state_val_39993 === (6))){
var inst_39988 = (state_39992[(2)]);
var state_39992__$1 = state_39992;
var statearr_40020_42200 = state_39992__$1;
(statearr_40020_42200[(2)] = inst_39988);

(statearr_40020_42200[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39993 === (3))){
var inst_39990 = (state_39992[(2)]);
var state_39992__$1 = state_39992;
return cljs.core.async.impl.ioc_helpers.return_chan(state_39992__$1,inst_39990);
} else {
if((state_val_39993 === (2))){
var inst_39960 = (state_39992[(10)]);
var inst_39962 = cljs.core.count(inst_39960);
var inst_39963 = (inst_39962 > (0));
var state_39992__$1 = state_39992;
if(cljs.core.truth_(inst_39963)){
var statearr_40022_42201 = state_39992__$1;
(statearr_40022_42201[(1)] = (4));

} else {
var statearr_40023_42202 = state_39992__$1;
(statearr_40023_42202[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39993 === (11))){
var inst_39960 = (state_39992[(10)]);
var inst_39981 = (state_39992[(2)]);
var tmp40021 = inst_39960;
var inst_39960__$1 = tmp40021;
var state_39992__$1 = (function (){var statearr_40025 = state_39992;
(statearr_40025[(11)] = inst_39981);

(statearr_40025[(10)] = inst_39960__$1);

return statearr_40025;
})();
var statearr_40026_42208 = state_39992__$1;
(statearr_40026_42208[(2)] = null);

(statearr_40026_42208[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39993 === (9))){
var inst_39971 = (state_39992[(7)]);
var state_39992__$1 = state_39992;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_39992__$1,(11),out,inst_39971);
} else {
if((state_val_39993 === (5))){
var inst_39986 = cljs.core.async.close_BANG_(out);
var state_39992__$1 = state_39992;
var statearr_40051_42209 = state_39992__$1;
(statearr_40051_42209[(2)] = inst_39986);

(statearr_40051_42209[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39993 === (10))){
var inst_39984 = (state_39992[(2)]);
var state_39992__$1 = state_39992;
var statearr_40058_42210 = state_39992__$1;
(statearr_40058_42210[(2)] = inst_39984);

(statearr_40058_42210[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_39993 === (8))){
var inst_39971 = (state_39992[(7)]);
var inst_39972 = (state_39992[(9)]);
var inst_39969 = (state_39992[(8)]);
var inst_39960 = (state_39992[(10)]);
var inst_39976 = (function (){var cs = inst_39960;
var vec__39965 = inst_39969;
var v = inst_39971;
var c = inst_39972;
return (function (p1__39932_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__39932_SHARP_);
});
})();
var inst_39977 = cljs.core.filterv(inst_39976,inst_39960);
var inst_39960__$1 = inst_39977;
var state_39992__$1 = (function (){var statearr_40062 = state_39992;
(statearr_40062[(10)] = inst_39960__$1);

return statearr_40062;
})();
var statearr_40064_42217 = state_39992__$1;
(statearr_40064_42217[(2)] = null);

(statearr_40064_42217[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_40069 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_40069[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_40069[(1)] = (1));

return statearr_40069;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_39992){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_39992);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e40072){var ex__26072__auto__ = e40072;
var statearr_40074_42218 = state_39992;
(statearr_40074_42218[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_39992[(4)]))){
var statearr_40075_42219 = state_39992;
(statearr_40075_42219[(1)] = cljs.core.first((state_39992[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42220 = state_39992;
state_39992 = G__42220;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_39992){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_39992);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_40077 = f__26227__auto__();
(statearr_40077[(6)] = c__26226__auto___42193);

return statearr_40077;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__40102 = arguments.length;
switch (G__40102) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__26226__auto___42222 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_40159){
var state_val_40161 = (state_40159[(1)]);
if((state_val_40161 === (7))){
var inst_40128 = (state_40159[(7)]);
var inst_40128__$1 = (state_40159[(2)]);
var inst_40130 = (inst_40128__$1 == null);
var inst_40131 = cljs.core.not(inst_40130);
var state_40159__$1 = (function (){var statearr_40164 = state_40159;
(statearr_40164[(7)] = inst_40128__$1);

return statearr_40164;
})();
if(inst_40131){
var statearr_40165_42223 = state_40159__$1;
(statearr_40165_42223[(1)] = (8));

} else {
var statearr_40166_42226 = state_40159__$1;
(statearr_40166_42226[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40161 === (1))){
var inst_40117 = (0);
var state_40159__$1 = (function (){var statearr_40167 = state_40159;
(statearr_40167[(8)] = inst_40117);

return statearr_40167;
})();
var statearr_40168_42228 = state_40159__$1;
(statearr_40168_42228[(2)] = null);

(statearr_40168_42228[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40161 === (4))){
var state_40159__$1 = state_40159;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_40159__$1,(7),ch);
} else {
if((state_val_40161 === (6))){
var inst_40149 = (state_40159[(2)]);
var state_40159__$1 = state_40159;
var statearr_40177_42231 = state_40159__$1;
(statearr_40177_42231[(2)] = inst_40149);

(statearr_40177_42231[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40161 === (3))){
var inst_40151 = (state_40159[(2)]);
var inst_40152 = cljs.core.async.close_BANG_(out);
var state_40159__$1 = (function (){var statearr_40183 = state_40159;
(statearr_40183[(9)] = inst_40151);

return statearr_40183;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_40159__$1,inst_40152);
} else {
if((state_val_40161 === (2))){
var inst_40117 = (state_40159[(8)]);
var inst_40124 = (inst_40117 < n);
var state_40159__$1 = state_40159;
if(cljs.core.truth_(inst_40124)){
var statearr_40191_42238 = state_40159__$1;
(statearr_40191_42238[(1)] = (4));

} else {
var statearr_40193_42239 = state_40159__$1;
(statearr_40193_42239[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40161 === (11))){
var inst_40117 = (state_40159[(8)]);
var inst_40135 = (state_40159[(2)]);
var inst_40141 = (inst_40117 + (1));
var inst_40117__$1 = inst_40141;
var state_40159__$1 = (function (){var statearr_40198 = state_40159;
(statearr_40198[(8)] = inst_40117__$1);

(statearr_40198[(10)] = inst_40135);

return statearr_40198;
})();
var statearr_40199_42240 = state_40159__$1;
(statearr_40199_42240[(2)] = null);

(statearr_40199_42240[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40161 === (9))){
var state_40159__$1 = state_40159;
var statearr_40200_42241 = state_40159__$1;
(statearr_40200_42241[(2)] = null);

(statearr_40200_42241[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40161 === (5))){
var state_40159__$1 = state_40159;
var statearr_40202_42242 = state_40159__$1;
(statearr_40202_42242[(2)] = null);

(statearr_40202_42242[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40161 === (10))){
var inst_40146 = (state_40159[(2)]);
var state_40159__$1 = state_40159;
var statearr_40203_42245 = state_40159__$1;
(statearr_40203_42245[(2)] = inst_40146);

(statearr_40203_42245[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40161 === (8))){
var inst_40128 = (state_40159[(7)]);
var state_40159__$1 = state_40159;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40159__$1,(11),out,inst_40128);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_40204 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_40204[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_40204[(1)] = (1));

return statearr_40204;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_40159){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_40159);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e40205){var ex__26072__auto__ = e40205;
var statearr_40206_42247 = state_40159;
(statearr_40206_42247[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_40159[(4)]))){
var statearr_40207_42248 = state_40159;
(statearr_40207_42248[(1)] = cljs.core.first((state_40159[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42255 = state_40159;
state_40159 = G__42255;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_40159){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_40159);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_40209 = f__26227__auto__();
(statearr_40209[(6)] = c__26226__auto___42222);

return statearr_40209;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async40216 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async40216 = (function (f,ch,meta40217){
this.f = f;
this.ch = ch;
this.meta40217 = meta40217;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_40218,meta40217__$1){
var self__ = this;
var _40218__$1 = this;
return (new cljs.core.async.t_cljs$core$async40216(self__.f,self__.ch,meta40217__$1));
}));

(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_40218){
var self__ = this;
var _40218__$1 = this;
return self__.meta40217;
}));

(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(function (){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async40240 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async40240 = (function (f,ch,meta40217,_,fn1,meta40242){
this.f = f;
this.ch = ch;
this.meta40217 = meta40217;
this._ = _;
this.fn1 = fn1;
this.meta40242 = meta40242;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async40240.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_40243,meta40242__$1){
var self__ = this;
var _40243__$1 = this;
return (new cljs.core.async.t_cljs$core$async40240(self__.f,self__.ch,self__.meta40217,self__._,self__.fn1,meta40242__$1));
}));

(cljs.core.async.t_cljs$core$async40240.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_40243){
var self__ = this;
var _40243__$1 = this;
return self__.meta40242;
}));

(cljs.core.async.t_cljs$core$async40240.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40240.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async40240.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async40240.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__40214_SHARP_){
var G__40259 = (((p1__40214_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__40214_SHARP_) : self__.f.call(null,p1__40214_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__40259) : f1.call(null,G__40259));
});
}));

(cljs.core.async.t_cljs$core$async40240.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta40217","meta40217",-807431549,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async40216","cljs.core.async/t_cljs$core$async40216",1534528380,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta40242","meta40242",-437761283,null)], null);
}));

(cljs.core.async.t_cljs$core$async40240.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async40240.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async40240");

(cljs.core.async.t_cljs$core$async40240.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async40240");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async40240.
 */
cljs.core.async.__GT_t_cljs$core$async40240 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async40240(f__$1,ch__$1,meta40217__$1,___$2,fn1__$1,meta40242){
return (new cljs.core.async.t_cljs$core$async40240(f__$1,ch__$1,meta40217__$1,___$2,fn1__$1,meta40242));
});

}

return (new cljs.core.async.t_cljs$core$async40240(self__.f,self__.ch,self__.meta40217,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__4115__auto__ = ret;
if(cljs.core.truth_(and__4115__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__4115__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__40266 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__40266) : self__.f.call(null,G__40266));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40216.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async40216.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta40217","meta40217",-807431549,null)], null);
}));

(cljs.core.async.t_cljs$core$async40216.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async40216.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async40216");

(cljs.core.async.t_cljs$core$async40216.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async40216");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async40216.
 */
cljs.core.async.__GT_t_cljs$core$async40216 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async40216(f__$1,ch__$1,meta40217){
return (new cljs.core.async.t_cljs$core$async40216(f__$1,ch__$1,meta40217));
});

}

return (new cljs.core.async.t_cljs$core$async40216(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async40269 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async40269 = (function (f,ch,meta40270){
this.f = f;
this.ch = ch;
this.meta40270 = meta40270;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async40269.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_40271,meta40270__$1){
var self__ = this;
var _40271__$1 = this;
return (new cljs.core.async.t_cljs$core$async40269(self__.f,self__.ch,meta40270__$1));
}));

(cljs.core.async.t_cljs$core$async40269.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_40271){
var self__ = this;
var _40271__$1 = this;
return self__.meta40270;
}));

(cljs.core.async.t_cljs$core$async40269.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40269.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async40269.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40269.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async40269.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40269.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async40269.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta40270","meta40270",2077633474,null)], null);
}));

(cljs.core.async.t_cljs$core$async40269.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async40269.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async40269");

(cljs.core.async.t_cljs$core$async40269.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async40269");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async40269.
 */
cljs.core.async.__GT_t_cljs$core$async40269 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async40269(f__$1,ch__$1,meta40270){
return (new cljs.core.async.t_cljs$core$async40269(f__$1,ch__$1,meta40270));
});

}

return (new cljs.core.async.t_cljs$core$async40269(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if((typeof cljs !== 'undefined') && (typeof cljs.core !== 'undefined') && (typeof cljs.core.async !== 'undefined') && (typeof cljs.core.async.t_cljs$core$async40276 !== 'undefined')){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async40276 = (function (p,ch,meta40277){
this.p = p;
this.ch = ch;
this.meta40277 = meta40277;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_40278,meta40277__$1){
var self__ = this;
var _40278__$1 = this;
return (new cljs.core.async.t_cljs$core$async40276(self__.p,self__.ch,meta40277__$1));
}));

(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_40278){
var self__ = this;
var _40278__$1 = this;
return self__.meta40277;
}));

(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async40276.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async40276.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta40277","meta40277",446553313,null)], null);
}));

(cljs.core.async.t_cljs$core$async40276.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async40276.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async40276");

(cljs.core.async.t_cljs$core$async40276.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"cljs.core.async/t_cljs$core$async40276");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async40276.
 */
cljs.core.async.__GT_t_cljs$core$async40276 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async40276(p__$1,ch__$1,meta40277){
return (new cljs.core.async.t_cljs$core$async40276(p__$1,ch__$1,meta40277));
});

}

return (new cljs.core.async.t_cljs$core$async40276(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__40287 = arguments.length;
switch (G__40287) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__26226__auto___42271 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_40313){
var state_val_40314 = (state_40313[(1)]);
if((state_val_40314 === (7))){
var inst_40309 = (state_40313[(2)]);
var state_40313__$1 = state_40313;
var statearr_40322_42273 = state_40313__$1;
(statearr_40322_42273[(2)] = inst_40309);

(statearr_40322_42273[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40314 === (1))){
var state_40313__$1 = state_40313;
var statearr_40323_42276 = state_40313__$1;
(statearr_40323_42276[(2)] = null);

(statearr_40323_42276[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40314 === (4))){
var inst_40293 = (state_40313[(7)]);
var inst_40293__$1 = (state_40313[(2)]);
var inst_40294 = (inst_40293__$1 == null);
var state_40313__$1 = (function (){var statearr_40324 = state_40313;
(statearr_40324[(7)] = inst_40293__$1);

return statearr_40324;
})();
if(cljs.core.truth_(inst_40294)){
var statearr_40326_42277 = state_40313__$1;
(statearr_40326_42277[(1)] = (5));

} else {
var statearr_40328_42278 = state_40313__$1;
(statearr_40328_42278[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40314 === (6))){
var inst_40293 = (state_40313[(7)]);
var inst_40299 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_40293) : p.call(null,inst_40293));
var state_40313__$1 = state_40313;
if(cljs.core.truth_(inst_40299)){
var statearr_40330_42282 = state_40313__$1;
(statearr_40330_42282[(1)] = (8));

} else {
var statearr_40331_42283 = state_40313__$1;
(statearr_40331_42283[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40314 === (3))){
var inst_40311 = (state_40313[(2)]);
var state_40313__$1 = state_40313;
return cljs.core.async.impl.ioc_helpers.return_chan(state_40313__$1,inst_40311);
} else {
if((state_val_40314 === (2))){
var state_40313__$1 = state_40313;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_40313__$1,(4),ch);
} else {
if((state_val_40314 === (11))){
var inst_40302 = (state_40313[(2)]);
var state_40313__$1 = state_40313;
var statearr_40334_42284 = state_40313__$1;
(statearr_40334_42284[(2)] = inst_40302);

(statearr_40334_42284[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40314 === (9))){
var state_40313__$1 = state_40313;
var statearr_40337_42285 = state_40313__$1;
(statearr_40337_42285[(2)] = null);

(statearr_40337_42285[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40314 === (5))){
var inst_40297 = cljs.core.async.close_BANG_(out);
var state_40313__$1 = state_40313;
var statearr_40338_42288 = state_40313__$1;
(statearr_40338_42288[(2)] = inst_40297);

(statearr_40338_42288[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40314 === (10))){
var inst_40306 = (state_40313[(2)]);
var state_40313__$1 = (function (){var statearr_40339 = state_40313;
(statearr_40339[(8)] = inst_40306);

return statearr_40339;
})();
var statearr_40340_42292 = state_40313__$1;
(statearr_40340_42292[(2)] = null);

(statearr_40340_42292[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40314 === (8))){
var inst_40293 = (state_40313[(7)]);
var state_40313__$1 = state_40313;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40313__$1,(11),out,inst_40293);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_40342 = [null,null,null,null,null,null,null,null,null];
(statearr_40342[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_40342[(1)] = (1));

return statearr_40342;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_40313){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_40313);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e40347){var ex__26072__auto__ = e40347;
var statearr_40348_42293 = state_40313;
(statearr_40348_42293[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_40313[(4)]))){
var statearr_40350_42294 = state_40313;
(statearr_40350_42294[(1)] = cljs.core.first((state_40313[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42295 = state_40313;
state_40313 = G__42295;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_40313){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_40313);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_40351 = f__26227__auto__();
(statearr_40351[(6)] = c__26226__auto___42271);

return statearr_40351;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__40354 = arguments.length;
switch (G__40354) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__26226__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_40424){
var state_val_40425 = (state_40424[(1)]);
if((state_val_40425 === (7))){
var inst_40420 = (state_40424[(2)]);
var state_40424__$1 = state_40424;
var statearr_40427_42299 = state_40424__$1;
(statearr_40427_42299[(2)] = inst_40420);

(statearr_40427_42299[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (20))){
var inst_40389 = (state_40424[(7)]);
var inst_40401 = (state_40424[(2)]);
var inst_40402 = cljs.core.next(inst_40389);
var inst_40373 = inst_40402;
var inst_40374 = null;
var inst_40375 = (0);
var inst_40376 = (0);
var state_40424__$1 = (function (){var statearr_40428 = state_40424;
(statearr_40428[(8)] = inst_40373);

(statearr_40428[(9)] = inst_40401);

(statearr_40428[(10)] = inst_40376);

(statearr_40428[(11)] = inst_40374);

(statearr_40428[(12)] = inst_40375);

return statearr_40428;
})();
var statearr_40429_42302 = state_40424__$1;
(statearr_40429_42302[(2)] = null);

(statearr_40429_42302[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (1))){
var state_40424__$1 = state_40424;
var statearr_40431_42303 = state_40424__$1;
(statearr_40431_42303[(2)] = null);

(statearr_40431_42303[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (4))){
var inst_40360 = (state_40424[(13)]);
var inst_40360__$1 = (state_40424[(2)]);
var inst_40361 = (inst_40360__$1 == null);
var state_40424__$1 = (function (){var statearr_40434 = state_40424;
(statearr_40434[(13)] = inst_40360__$1);

return statearr_40434;
})();
if(cljs.core.truth_(inst_40361)){
var statearr_40435_42313 = state_40424__$1;
(statearr_40435_42313[(1)] = (5));

} else {
var statearr_40436_42314 = state_40424__$1;
(statearr_40436_42314[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (15))){
var state_40424__$1 = state_40424;
var statearr_40440_42315 = state_40424__$1;
(statearr_40440_42315[(2)] = null);

(statearr_40440_42315[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (21))){
var state_40424__$1 = state_40424;
var statearr_40441_42316 = state_40424__$1;
(statearr_40441_42316[(2)] = null);

(statearr_40441_42316[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (13))){
var inst_40373 = (state_40424[(8)]);
var inst_40376 = (state_40424[(10)]);
var inst_40374 = (state_40424[(11)]);
var inst_40375 = (state_40424[(12)]);
var inst_40385 = (state_40424[(2)]);
var inst_40386 = (inst_40376 + (1));
var tmp40437 = inst_40373;
var tmp40438 = inst_40374;
var tmp40439 = inst_40375;
var inst_40373__$1 = tmp40437;
var inst_40374__$1 = tmp40438;
var inst_40375__$1 = tmp40439;
var inst_40376__$1 = inst_40386;
var state_40424__$1 = (function (){var statearr_40442 = state_40424;
(statearr_40442[(14)] = inst_40385);

(statearr_40442[(8)] = inst_40373__$1);

(statearr_40442[(10)] = inst_40376__$1);

(statearr_40442[(11)] = inst_40374__$1);

(statearr_40442[(12)] = inst_40375__$1);

return statearr_40442;
})();
var statearr_40443_42321 = state_40424__$1;
(statearr_40443_42321[(2)] = null);

(statearr_40443_42321[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (22))){
var state_40424__$1 = state_40424;
var statearr_40444_42322 = state_40424__$1;
(statearr_40444_42322[(2)] = null);

(statearr_40444_42322[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (6))){
var inst_40360 = (state_40424[(13)]);
var inst_40370 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_40360) : f.call(null,inst_40360));
var inst_40371 = cljs.core.seq(inst_40370);
var inst_40373 = inst_40371;
var inst_40374 = null;
var inst_40375 = (0);
var inst_40376 = (0);
var state_40424__$1 = (function (){var statearr_40445 = state_40424;
(statearr_40445[(8)] = inst_40373);

(statearr_40445[(10)] = inst_40376);

(statearr_40445[(11)] = inst_40374);

(statearr_40445[(12)] = inst_40375);

return statearr_40445;
})();
var statearr_40446_42329 = state_40424__$1;
(statearr_40446_42329[(2)] = null);

(statearr_40446_42329[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (17))){
var inst_40389 = (state_40424[(7)]);
var inst_40394 = cljs.core.chunk_first(inst_40389);
var inst_40395 = cljs.core.chunk_rest(inst_40389);
var inst_40396 = cljs.core.count(inst_40394);
var inst_40373 = inst_40395;
var inst_40374 = inst_40394;
var inst_40375 = inst_40396;
var inst_40376 = (0);
var state_40424__$1 = (function (){var statearr_40449 = state_40424;
(statearr_40449[(8)] = inst_40373);

(statearr_40449[(10)] = inst_40376);

(statearr_40449[(11)] = inst_40374);

(statearr_40449[(12)] = inst_40375);

return statearr_40449;
})();
var statearr_40450_42334 = state_40424__$1;
(statearr_40450_42334[(2)] = null);

(statearr_40450_42334[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (3))){
var inst_40422 = (state_40424[(2)]);
var state_40424__$1 = state_40424;
return cljs.core.async.impl.ioc_helpers.return_chan(state_40424__$1,inst_40422);
} else {
if((state_val_40425 === (12))){
var inst_40410 = (state_40424[(2)]);
var state_40424__$1 = state_40424;
var statearr_40455_42336 = state_40424__$1;
(statearr_40455_42336[(2)] = inst_40410);

(statearr_40455_42336[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (2))){
var state_40424__$1 = state_40424;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_40424__$1,(4),in$);
} else {
if((state_val_40425 === (23))){
var inst_40418 = (state_40424[(2)]);
var state_40424__$1 = state_40424;
var statearr_40456_42337 = state_40424__$1;
(statearr_40456_42337[(2)] = inst_40418);

(statearr_40456_42337[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (19))){
var inst_40405 = (state_40424[(2)]);
var state_40424__$1 = state_40424;
var statearr_40459_42338 = state_40424__$1;
(statearr_40459_42338[(2)] = inst_40405);

(statearr_40459_42338[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (11))){
var inst_40373 = (state_40424[(8)]);
var inst_40389 = (state_40424[(7)]);
var inst_40389__$1 = cljs.core.seq(inst_40373);
var state_40424__$1 = (function (){var statearr_40467 = state_40424;
(statearr_40467[(7)] = inst_40389__$1);

return statearr_40467;
})();
if(inst_40389__$1){
var statearr_40469_42339 = state_40424__$1;
(statearr_40469_42339[(1)] = (14));

} else {
var statearr_40474_42340 = state_40424__$1;
(statearr_40474_42340[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (9))){
var inst_40412 = (state_40424[(2)]);
var inst_40413 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_40424__$1 = (function (){var statearr_40475 = state_40424;
(statearr_40475[(15)] = inst_40412);

return statearr_40475;
})();
if(cljs.core.truth_(inst_40413)){
var statearr_40477_42341 = state_40424__$1;
(statearr_40477_42341[(1)] = (21));

} else {
var statearr_40479_42342 = state_40424__$1;
(statearr_40479_42342[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (5))){
var inst_40364 = cljs.core.async.close_BANG_(out);
var state_40424__$1 = state_40424;
var statearr_40484_42343 = state_40424__$1;
(statearr_40484_42343[(2)] = inst_40364);

(statearr_40484_42343[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (14))){
var inst_40389 = (state_40424[(7)]);
var inst_40392 = cljs.core.chunked_seq_QMARK_(inst_40389);
var state_40424__$1 = state_40424;
if(inst_40392){
var statearr_40485_42344 = state_40424__$1;
(statearr_40485_42344[(1)] = (17));

} else {
var statearr_40487_42346 = state_40424__$1;
(statearr_40487_42346[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (16))){
var inst_40408 = (state_40424[(2)]);
var state_40424__$1 = state_40424;
var statearr_40489_42347 = state_40424__$1;
(statearr_40489_42347[(2)] = inst_40408);

(statearr_40489_42347[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40425 === (10))){
var inst_40376 = (state_40424[(10)]);
var inst_40374 = (state_40424[(11)]);
var inst_40383 = cljs.core._nth(inst_40374,inst_40376);
var state_40424__$1 = state_40424;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40424__$1,(13),out,inst_40383);
} else {
if((state_val_40425 === (18))){
var inst_40389 = (state_40424[(7)]);
var inst_40399 = cljs.core.first(inst_40389);
var state_40424__$1 = state_40424;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40424__$1,(20),out,inst_40399);
} else {
if((state_val_40425 === (8))){
var inst_40376 = (state_40424[(10)]);
var inst_40375 = (state_40424[(12)]);
var inst_40379 = (inst_40376 < inst_40375);
var inst_40380 = inst_40379;
var state_40424__$1 = state_40424;
if(cljs.core.truth_(inst_40380)){
var statearr_40498_42351 = state_40424__$1;
(statearr_40498_42351[(1)] = (10));

} else {
var statearr_40499_42352 = state_40424__$1;
(statearr_40499_42352[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__26069__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__26069__auto____0 = (function (){
var statearr_40504 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_40504[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__26069__auto__);

(statearr_40504[(1)] = (1));

return statearr_40504;
});
var cljs$core$async$mapcat_STAR__$_state_machine__26069__auto____1 = (function (state_40424){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_40424);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e40509){var ex__26072__auto__ = e40509;
var statearr_40511_42355 = state_40424;
(statearr_40511_42355[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_40424[(4)]))){
var statearr_40515_42357 = state_40424;
(statearr_40515_42357[(1)] = cljs.core.first((state_40424[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42358 = state_40424;
state_40424 = G__42358;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__26069__auto__ = function(state_40424){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__26069__auto____1.call(this,state_40424);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__26069__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__26069__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_40521 = f__26227__auto__();
(statearr_40521[(6)] = c__26226__auto__);

return statearr_40521;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));

return c__26226__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__40527 = arguments.length;
switch (G__40527) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__40542 = arguments.length;
switch (G__40542) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__40555 = arguments.length;
switch (G__40555) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__26226__auto___42367 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_40594){
var state_val_40595 = (state_40594[(1)]);
if((state_val_40595 === (7))){
var inst_40589 = (state_40594[(2)]);
var state_40594__$1 = state_40594;
var statearr_40599_42368 = state_40594__$1;
(statearr_40599_42368[(2)] = inst_40589);

(statearr_40599_42368[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40595 === (1))){
var inst_40567 = null;
var state_40594__$1 = (function (){var statearr_40602 = state_40594;
(statearr_40602[(7)] = inst_40567);

return statearr_40602;
})();
var statearr_40603_42370 = state_40594__$1;
(statearr_40603_42370[(2)] = null);

(statearr_40603_42370[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40595 === (4))){
var inst_40574 = (state_40594[(8)]);
var inst_40574__$1 = (state_40594[(2)]);
var inst_40575 = (inst_40574__$1 == null);
var inst_40576 = cljs.core.not(inst_40575);
var state_40594__$1 = (function (){var statearr_40606 = state_40594;
(statearr_40606[(8)] = inst_40574__$1);

return statearr_40606;
})();
if(inst_40576){
var statearr_40609_42371 = state_40594__$1;
(statearr_40609_42371[(1)] = (5));

} else {
var statearr_40610_42373 = state_40594__$1;
(statearr_40610_42373[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40595 === (6))){
var state_40594__$1 = state_40594;
var statearr_40612_42374 = state_40594__$1;
(statearr_40612_42374[(2)] = null);

(statearr_40612_42374[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40595 === (3))){
var inst_40591 = (state_40594[(2)]);
var inst_40592 = cljs.core.async.close_BANG_(out);
var state_40594__$1 = (function (){var statearr_40615 = state_40594;
(statearr_40615[(9)] = inst_40591);

return statearr_40615;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_40594__$1,inst_40592);
} else {
if((state_val_40595 === (2))){
var state_40594__$1 = state_40594;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_40594__$1,(4),ch);
} else {
if((state_val_40595 === (11))){
var inst_40574 = (state_40594[(8)]);
var inst_40583 = (state_40594[(2)]);
var inst_40567 = inst_40574;
var state_40594__$1 = (function (){var statearr_40624 = state_40594;
(statearr_40624[(10)] = inst_40583);

(statearr_40624[(7)] = inst_40567);

return statearr_40624;
})();
var statearr_40625_42378 = state_40594__$1;
(statearr_40625_42378[(2)] = null);

(statearr_40625_42378[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40595 === (9))){
var inst_40574 = (state_40594[(8)]);
var state_40594__$1 = state_40594;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40594__$1,(11),out,inst_40574);
} else {
if((state_val_40595 === (5))){
var inst_40574 = (state_40594[(8)]);
var inst_40567 = (state_40594[(7)]);
var inst_40578 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_40574,inst_40567);
var state_40594__$1 = state_40594;
if(inst_40578){
var statearr_40633_42384 = state_40594__$1;
(statearr_40633_42384[(1)] = (8));

} else {
var statearr_40635_42385 = state_40594__$1;
(statearr_40635_42385[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40595 === (10))){
var inst_40586 = (state_40594[(2)]);
var state_40594__$1 = state_40594;
var statearr_40637_42387 = state_40594__$1;
(statearr_40637_42387[(2)] = inst_40586);

(statearr_40637_42387[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40595 === (8))){
var inst_40567 = (state_40594[(7)]);
var tmp40628 = inst_40567;
var inst_40567__$1 = tmp40628;
var state_40594__$1 = (function (){var statearr_40645 = state_40594;
(statearr_40645[(7)] = inst_40567__$1);

return statearr_40645;
})();
var statearr_40647_42390 = state_40594__$1;
(statearr_40647_42390[(2)] = null);

(statearr_40647_42390[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_40659 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_40659[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_40659[(1)] = (1));

return statearr_40659;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_40594){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_40594);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e40660){var ex__26072__auto__ = e40660;
var statearr_40661_42392 = state_40594;
(statearr_40661_42392[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_40594[(4)]))){
var statearr_40668_42393 = state_40594;
(statearr_40668_42393[(1)] = cljs.core.first((state_40594[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42394 = state_40594;
state_40594 = G__42394;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_40594){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_40594);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_40672 = f__26227__auto__();
(statearr_40672[(6)] = c__26226__auto___42367);

return statearr_40672;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__40690 = arguments.length;
switch (G__40690) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__26226__auto___42410 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_40738){
var state_val_40739 = (state_40738[(1)]);
if((state_val_40739 === (7))){
var inst_40734 = (state_40738[(2)]);
var state_40738__$1 = state_40738;
var statearr_40748_42415 = state_40738__$1;
(statearr_40748_42415[(2)] = inst_40734);

(statearr_40748_42415[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (1))){
var inst_40697 = (new Array(n));
var inst_40698 = inst_40697;
var inst_40699 = (0);
var state_40738__$1 = (function (){var statearr_40751 = state_40738;
(statearr_40751[(7)] = inst_40699);

(statearr_40751[(8)] = inst_40698);

return statearr_40751;
})();
var statearr_40755_42421 = state_40738__$1;
(statearr_40755_42421[(2)] = null);

(statearr_40755_42421[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (4))){
var inst_40702 = (state_40738[(9)]);
var inst_40702__$1 = (state_40738[(2)]);
var inst_40703 = (inst_40702__$1 == null);
var inst_40704 = cljs.core.not(inst_40703);
var state_40738__$1 = (function (){var statearr_40761 = state_40738;
(statearr_40761[(9)] = inst_40702__$1);

return statearr_40761;
})();
if(inst_40704){
var statearr_40764_42422 = state_40738__$1;
(statearr_40764_42422[(1)] = (5));

} else {
var statearr_40768_42423 = state_40738__$1;
(statearr_40768_42423[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (15))){
var inst_40728 = (state_40738[(2)]);
var state_40738__$1 = state_40738;
var statearr_40771_42425 = state_40738__$1;
(statearr_40771_42425[(2)] = inst_40728);

(statearr_40771_42425[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (13))){
var state_40738__$1 = state_40738;
var statearr_40777_42426 = state_40738__$1;
(statearr_40777_42426[(2)] = null);

(statearr_40777_42426[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (6))){
var inst_40699 = (state_40738[(7)]);
var inst_40724 = (inst_40699 > (0));
var state_40738__$1 = state_40738;
if(cljs.core.truth_(inst_40724)){
var statearr_40779_42431 = state_40738__$1;
(statearr_40779_42431[(1)] = (12));

} else {
var statearr_40782_42432 = state_40738__$1;
(statearr_40782_42432[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (3))){
var inst_40736 = (state_40738[(2)]);
var state_40738__$1 = state_40738;
return cljs.core.async.impl.ioc_helpers.return_chan(state_40738__$1,inst_40736);
} else {
if((state_val_40739 === (12))){
var inst_40698 = (state_40738[(8)]);
var inst_40726 = cljs.core.vec(inst_40698);
var state_40738__$1 = state_40738;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40738__$1,(15),out,inst_40726);
} else {
if((state_val_40739 === (2))){
var state_40738__$1 = state_40738;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_40738__$1,(4),ch);
} else {
if((state_val_40739 === (11))){
var inst_40718 = (state_40738[(2)]);
var inst_40719 = (new Array(n));
var inst_40698 = inst_40719;
var inst_40699 = (0);
var state_40738__$1 = (function (){var statearr_40794 = state_40738;
(statearr_40794[(7)] = inst_40699);

(statearr_40794[(8)] = inst_40698);

(statearr_40794[(10)] = inst_40718);

return statearr_40794;
})();
var statearr_40799_42443 = state_40738__$1;
(statearr_40799_42443[(2)] = null);

(statearr_40799_42443[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (9))){
var inst_40698 = (state_40738[(8)]);
var inst_40716 = cljs.core.vec(inst_40698);
var state_40738__$1 = state_40738;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40738__$1,(11),out,inst_40716);
} else {
if((state_val_40739 === (5))){
var inst_40699 = (state_40738[(7)]);
var inst_40709 = (state_40738[(11)]);
var inst_40702 = (state_40738[(9)]);
var inst_40698 = (state_40738[(8)]);
var inst_40707 = (inst_40698[inst_40699] = inst_40702);
var inst_40709__$1 = (inst_40699 + (1));
var inst_40711 = (inst_40709__$1 < n);
var state_40738__$1 = (function (){var statearr_40803 = state_40738;
(statearr_40803[(11)] = inst_40709__$1);

(statearr_40803[(12)] = inst_40707);

return statearr_40803;
})();
if(cljs.core.truth_(inst_40711)){
var statearr_40804_42446 = state_40738__$1;
(statearr_40804_42446[(1)] = (8));

} else {
var statearr_40805_42447 = state_40738__$1;
(statearr_40805_42447[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (14))){
var inst_40731 = (state_40738[(2)]);
var inst_40732 = cljs.core.async.close_BANG_(out);
var state_40738__$1 = (function (){var statearr_40811 = state_40738;
(statearr_40811[(13)] = inst_40731);

return statearr_40811;
})();
var statearr_40813_42449 = state_40738__$1;
(statearr_40813_42449[(2)] = inst_40732);

(statearr_40813_42449[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (10))){
var inst_40722 = (state_40738[(2)]);
var state_40738__$1 = state_40738;
var statearr_40817_42450 = state_40738__$1;
(statearr_40817_42450[(2)] = inst_40722);

(statearr_40817_42450[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40739 === (8))){
var inst_40709 = (state_40738[(11)]);
var inst_40698 = (state_40738[(8)]);
var tmp40810 = inst_40698;
var inst_40698__$1 = tmp40810;
var inst_40699 = inst_40709;
var state_40738__$1 = (function (){var statearr_40823 = state_40738;
(statearr_40823[(7)] = inst_40699);

(statearr_40823[(8)] = inst_40698__$1);

return statearr_40823;
})();
var statearr_40825_42456 = state_40738__$1;
(statearr_40825_42456[(2)] = null);

(statearr_40825_42456[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_40829 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_40829[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_40829[(1)] = (1));

return statearr_40829;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_40738){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_40738);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e40831){var ex__26072__auto__ = e40831;
var statearr_40832_42461 = state_40738;
(statearr_40832_42461[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_40738[(4)]))){
var statearr_40833_42466 = state_40738;
(statearr_40833_42466[(1)] = cljs.core.first((state_40738[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42467 = state_40738;
state_40738 = G__42467;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_40738){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_40738);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_40838 = f__26227__auto__();
(statearr_40838[(6)] = c__26226__auto___42410);

return statearr_40838;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__40850 = arguments.length;
switch (G__40850) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__26226__auto___42480 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_40921){
var state_val_40922 = (state_40921[(1)]);
if((state_val_40922 === (7))){
var inst_40915 = (state_40921[(2)]);
var state_40921__$1 = state_40921;
var statearr_40932_42481 = state_40921__$1;
(statearr_40932_42481[(2)] = inst_40915);

(statearr_40932_42481[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (1))){
var inst_40872 = [];
var inst_40874 = inst_40872;
var inst_40875 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_40921__$1 = (function (){var statearr_40937 = state_40921;
(statearr_40937[(7)] = inst_40874);

(statearr_40937[(8)] = inst_40875);

return statearr_40937;
})();
var statearr_40939_42486 = state_40921__$1;
(statearr_40939_42486[(2)] = null);

(statearr_40939_42486[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (4))){
var inst_40879 = (state_40921[(9)]);
var inst_40879__$1 = (state_40921[(2)]);
var inst_40880 = (inst_40879__$1 == null);
var inst_40881 = cljs.core.not(inst_40880);
var state_40921__$1 = (function (){var statearr_40945 = state_40921;
(statearr_40945[(9)] = inst_40879__$1);

return statearr_40945;
})();
if(inst_40881){
var statearr_40948_42490 = state_40921__$1;
(statearr_40948_42490[(1)] = (5));

} else {
var statearr_40949_42491 = state_40921__$1;
(statearr_40949_42491[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (15))){
var inst_40909 = (state_40921[(2)]);
var state_40921__$1 = state_40921;
var statearr_40950_42492 = state_40921__$1;
(statearr_40950_42492[(2)] = inst_40909);

(statearr_40950_42492[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (13))){
var state_40921__$1 = state_40921;
var statearr_40955_42493 = state_40921__$1;
(statearr_40955_42493[(2)] = null);

(statearr_40955_42493[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (6))){
var inst_40874 = (state_40921[(7)]);
var inst_40903 = inst_40874.length;
var inst_40904 = (inst_40903 > (0));
var state_40921__$1 = state_40921;
if(cljs.core.truth_(inst_40904)){
var statearr_40957_42494 = state_40921__$1;
(statearr_40957_42494[(1)] = (12));

} else {
var statearr_40958_42495 = state_40921__$1;
(statearr_40958_42495[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (3))){
var inst_40918 = (state_40921[(2)]);
var state_40921__$1 = state_40921;
return cljs.core.async.impl.ioc_helpers.return_chan(state_40921__$1,inst_40918);
} else {
if((state_val_40922 === (12))){
var inst_40874 = (state_40921[(7)]);
var inst_40907 = cljs.core.vec(inst_40874);
var state_40921__$1 = state_40921;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40921__$1,(15),out,inst_40907);
} else {
if((state_val_40922 === (2))){
var state_40921__$1 = state_40921;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_40921__$1,(4),ch);
} else {
if((state_val_40922 === (11))){
var inst_40883 = (state_40921[(10)]);
var inst_40879 = (state_40921[(9)]);
var inst_40896 = (state_40921[(2)]);
var inst_40897 = [];
var inst_40898 = inst_40897.push(inst_40879);
var inst_40874 = inst_40897;
var inst_40875 = inst_40883;
var state_40921__$1 = (function (){var statearr_40974 = state_40921;
(statearr_40974[(11)] = inst_40896);

(statearr_40974[(7)] = inst_40874);

(statearr_40974[(8)] = inst_40875);

(statearr_40974[(12)] = inst_40898);

return statearr_40974;
})();
var statearr_40977_42496 = state_40921__$1;
(statearr_40977_42496[(2)] = null);

(statearr_40977_42496[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (9))){
var inst_40874 = (state_40921[(7)]);
var inst_40894 = cljs.core.vec(inst_40874);
var state_40921__$1 = state_40921;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_40921__$1,(11),out,inst_40894);
} else {
if((state_val_40922 === (5))){
var inst_40875 = (state_40921[(8)]);
var inst_40883 = (state_40921[(10)]);
var inst_40879 = (state_40921[(9)]);
var inst_40883__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_40879) : f.call(null,inst_40879));
var inst_40886 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_40883__$1,inst_40875);
var inst_40887 = cljs.core.keyword_identical_QMARK_(inst_40875,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_40888 = ((inst_40886) || (inst_40887));
var state_40921__$1 = (function (){var statearr_40984 = state_40921;
(statearr_40984[(10)] = inst_40883__$1);

return statearr_40984;
})();
if(cljs.core.truth_(inst_40888)){
var statearr_40985_42498 = state_40921__$1;
(statearr_40985_42498[(1)] = (8));

} else {
var statearr_40987_42499 = state_40921__$1;
(statearr_40987_42499[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (14))){
var inst_40912 = (state_40921[(2)]);
var inst_40913 = cljs.core.async.close_BANG_(out);
var state_40921__$1 = (function (){var statearr_40992 = state_40921;
(statearr_40992[(13)] = inst_40912);

return statearr_40992;
})();
var statearr_40994_42500 = state_40921__$1;
(statearr_40994_42500[(2)] = inst_40913);

(statearr_40994_42500[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (10))){
var inst_40901 = (state_40921[(2)]);
var state_40921__$1 = state_40921;
var statearr_40996_42502 = state_40921__$1;
(statearr_40996_42502[(2)] = inst_40901);

(statearr_40996_42502[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_40922 === (8))){
var inst_40874 = (state_40921[(7)]);
var inst_40883 = (state_40921[(10)]);
var inst_40879 = (state_40921[(9)]);
var inst_40891 = inst_40874.push(inst_40879);
var tmp40990 = inst_40874;
var inst_40874__$1 = tmp40990;
var inst_40875 = inst_40883;
var state_40921__$1 = (function (){var statearr_41001 = state_40921;
(statearr_41001[(14)] = inst_40891);

(statearr_41001[(7)] = inst_40874__$1);

(statearr_41001[(8)] = inst_40875);

return statearr_41001;
})();
var statearr_41005_42504 = state_40921__$1;
(statearr_41005_42504[(2)] = null);

(statearr_41005_42504[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__26069__auto__ = null;
var cljs$core$async$state_machine__26069__auto____0 = (function (){
var statearr_41010 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_41010[(0)] = cljs$core$async$state_machine__26069__auto__);

(statearr_41010[(1)] = (1));

return statearr_41010;
});
var cljs$core$async$state_machine__26069__auto____1 = (function (state_40921){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_40921);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e41014){var ex__26072__auto__ = e41014;
var statearr_41015_42505 = state_40921;
(statearr_41015_42505[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_40921[(4)]))){
var statearr_41016_42506 = state_40921;
(statearr_41016_42506[(1)] = cljs.core.first((state_40921[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42507 = state_40921;
state_40921 = G__42507;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
cljs$core$async$state_machine__26069__auto__ = function(state_40921){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__26069__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__26069__auto____1.call(this,state_40921);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__26069__auto____0;
cljs$core$async$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__26069__auto____1;
return cljs$core$async$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_41018 = f__26227__auto__();
(statearr_41018[(6)] = c__26226__auto___42480);

return statearr_41018;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
