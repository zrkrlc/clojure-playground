goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_42353 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_dom[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_42353(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_42356 = (function (this$){
var x__4428__auto__ = (((this$ == null))?null:this$);
var m__4429__auto__ = (shadow.dom._to_svg[goog.typeOf(x__4428__auto__)]);
if((!((m__4429__auto__ == null)))){
return (m__4429__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4429__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4429__auto__.call(null,this$));
} else {
var m__4426__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__4426__auto__ == null)))){
return (m__4426__auto__.cljs$core$IFn$_invoke$arity$1 ? m__4426__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__4426__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_42356(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__41074 = coll;
var G__41075 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__41074,G__41075) : shadow.dom.lazy_native_coll_seq.call(null,G__41074,G__41075));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__4126__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__4369__auto__,writer__4370__auto__,opt__4371__auto__){
return cljs.core._write(writer__4370__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__41127 = arguments.length;
switch (G__41127) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__41135 = arguments.length;
switch (G__41135) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__41145 = arguments.length;
switch (G__41145) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__41158 = arguments.length;
switch (G__41158) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__41172 = arguments.length;
switch (G__41172) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__41207 = arguments.length;
switch (G__41207) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e41223){if((e41223 instanceof Object)){
var e = e41223;
return console.log("didnt support attachEvent",el,e);
} else {
throw e41223;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__4126__auto__ = (!((typeof document !== 'undefined')));
if(or__4126__auto__){
return or__4126__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__41235 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__41236 = null;
var count__41237 = (0);
var i__41238 = (0);
while(true){
if((i__41238 < count__41237)){
var el = chunk__41236.cljs$core$IIndexed$_nth$arity$2(null,i__41238);
var handler_42424__$1 = ((function (seq__41235,chunk__41236,count__41237,i__41238,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__41235,chunk__41236,count__41237,i__41238,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_42424__$1);


var G__42427 = seq__41235;
var G__42428 = chunk__41236;
var G__42429 = count__41237;
var G__42430 = (i__41238 + (1));
seq__41235 = G__42427;
chunk__41236 = G__42428;
count__41237 = G__42429;
i__41238 = G__42430;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__41235);
if(temp__5735__auto__){
var seq__41235__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__41235__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__41235__$1);
var G__42433 = cljs.core.chunk_rest(seq__41235__$1);
var G__42434 = c__4556__auto__;
var G__42435 = cljs.core.count(c__4556__auto__);
var G__42436 = (0);
seq__41235 = G__42433;
chunk__41236 = G__42434;
count__41237 = G__42435;
i__41238 = G__42436;
continue;
} else {
var el = cljs.core.first(seq__41235__$1);
var handler_42437__$1 = ((function (seq__41235,chunk__41236,count__41237,i__41238,el,seq__41235__$1,temp__5735__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__41235,chunk__41236,count__41237,i__41238,el,seq__41235__$1,temp__5735__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_42437__$1);


var G__42438 = cljs.core.next(seq__41235__$1);
var G__42439 = null;
var G__42440 = (0);
var G__42441 = (0);
seq__41235 = G__42438;
chunk__41236 = G__42439;
count__41237 = G__42440;
i__41238 = G__42441;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__41269 = arguments.length;
switch (G__41269) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__41282 = cljs.core.seq(events);
var chunk__41283 = null;
var count__41284 = (0);
var i__41285 = (0);
while(true){
if((i__41285 < count__41284)){
var vec__41299 = chunk__41283.cljs$core$IIndexed$_nth$arity$2(null,i__41285);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41299,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41299,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__42452 = seq__41282;
var G__42453 = chunk__41283;
var G__42454 = count__41284;
var G__42455 = (i__41285 + (1));
seq__41282 = G__42452;
chunk__41283 = G__42453;
count__41284 = G__42454;
i__41285 = G__42455;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__41282);
if(temp__5735__auto__){
var seq__41282__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__41282__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__41282__$1);
var G__42457 = cljs.core.chunk_rest(seq__41282__$1);
var G__42458 = c__4556__auto__;
var G__42459 = cljs.core.count(c__4556__auto__);
var G__42460 = (0);
seq__41282 = G__42457;
chunk__41283 = G__42458;
count__41284 = G__42459;
i__41285 = G__42460;
continue;
} else {
var vec__41308 = cljs.core.first(seq__41282__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41308,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41308,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__42462 = cljs.core.next(seq__41282__$1);
var G__42463 = null;
var G__42464 = (0);
var G__42465 = (0);
seq__41282 = G__42462;
chunk__41283 = G__42463;
count__41284 = G__42464;
i__41285 = G__42465;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__41317 = cljs.core.seq(styles);
var chunk__41318 = null;
var count__41319 = (0);
var i__41320 = (0);
while(true){
if((i__41320 < count__41319)){
var vec__41335 = chunk__41318.cljs$core$IIndexed$_nth$arity$2(null,i__41320);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41335,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41335,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__42470 = seq__41317;
var G__42471 = chunk__41318;
var G__42472 = count__41319;
var G__42473 = (i__41320 + (1));
seq__41317 = G__42470;
chunk__41318 = G__42471;
count__41319 = G__42472;
i__41320 = G__42473;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__41317);
if(temp__5735__auto__){
var seq__41317__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__41317__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__41317__$1);
var G__42476 = cljs.core.chunk_rest(seq__41317__$1);
var G__42477 = c__4556__auto__;
var G__42478 = cljs.core.count(c__4556__auto__);
var G__42479 = (0);
seq__41317 = G__42476;
chunk__41318 = G__42477;
count__41319 = G__42478;
i__41320 = G__42479;
continue;
} else {
var vec__41345 = cljs.core.first(seq__41317__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41345,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41345,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__42482 = cljs.core.next(seq__41317__$1);
var G__42483 = null;
var G__42484 = (0);
var G__42485 = (0);
seq__41317 = G__42482;
chunk__41318 = G__42483;
count__41319 = G__42484;
i__41320 = G__42485;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__41357_42487 = key;
var G__41357_42488__$1 = (((G__41357_42487 instanceof cljs.core.Keyword))?G__41357_42487.fqn:null);
switch (G__41357_42488__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_42497 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__4126__auto__ = goog.string.startsWith(ks_42497,"data-");
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return goog.string.startsWith(ks_42497,"aria-");
}
})())){
el.setAttribute(ks_42497,value);
} else {
(el[ks_42497] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__41396){
var map__41397 = p__41396;
var map__41397__$1 = (((((!((map__41397 == null))))?(((((map__41397.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__41397.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__41397):map__41397);
var props = map__41397__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__41397__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__41404 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41404,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41404,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41404,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__41409 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__41409,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__41409;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__41419 = arguments.length;
switch (G__41419) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5735__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5735__auto__)){
var n = temp__5735__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__41436){
var vec__41439 = p__41436;
var seq__41440 = cljs.core.seq(vec__41439);
var first__41441 = cljs.core.first(seq__41440);
var seq__41440__$1 = cljs.core.next(seq__41440);
var nn = first__41441;
var first__41441__$1 = cljs.core.first(seq__41440__$1);
var seq__41440__$2 = cljs.core.next(seq__41440__$1);
var np = first__41441__$1;
var nc = seq__41440__$2;
var node = vec__41439;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__41447 = nn;
var G__41448 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__41447,G__41448) : create_fn.call(null,G__41447,G__41448));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__41449 = nn;
var G__41450 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__41449,G__41450) : create_fn.call(null,G__41449,G__41450));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__41455 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41455,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41455,(1),null);
var seq__41461_42516 = cljs.core.seq(node_children);
var chunk__41462_42517 = null;
var count__41463_42518 = (0);
var i__41464_42519 = (0);
while(true){
if((i__41464_42519 < count__41463_42518)){
var child_struct_42520 = chunk__41462_42517.cljs$core$IIndexed$_nth$arity$2(null,i__41464_42519);
var children_42521 = shadow.dom.dom_node(child_struct_42520);
if(cljs.core.seq_QMARK_(children_42521)){
var seq__41532_42522 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_42521));
var chunk__41534_42523 = null;
var count__41535_42524 = (0);
var i__41536_42525 = (0);
while(true){
if((i__41536_42525 < count__41535_42524)){
var child_42526 = chunk__41534_42523.cljs$core$IIndexed$_nth$arity$2(null,i__41536_42525);
if(cljs.core.truth_(child_42526)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_42526);


var G__42527 = seq__41532_42522;
var G__42528 = chunk__41534_42523;
var G__42529 = count__41535_42524;
var G__42530 = (i__41536_42525 + (1));
seq__41532_42522 = G__42527;
chunk__41534_42523 = G__42528;
count__41535_42524 = G__42529;
i__41536_42525 = G__42530;
continue;
} else {
var G__42533 = seq__41532_42522;
var G__42534 = chunk__41534_42523;
var G__42535 = count__41535_42524;
var G__42536 = (i__41536_42525 + (1));
seq__41532_42522 = G__42533;
chunk__41534_42523 = G__42534;
count__41535_42524 = G__42535;
i__41536_42525 = G__42536;
continue;
}
} else {
var temp__5735__auto___42538 = cljs.core.seq(seq__41532_42522);
if(temp__5735__auto___42538){
var seq__41532_42539__$1 = temp__5735__auto___42538;
if(cljs.core.chunked_seq_QMARK_(seq__41532_42539__$1)){
var c__4556__auto___42540 = cljs.core.chunk_first(seq__41532_42539__$1);
var G__42541 = cljs.core.chunk_rest(seq__41532_42539__$1);
var G__42542 = c__4556__auto___42540;
var G__42543 = cljs.core.count(c__4556__auto___42540);
var G__42544 = (0);
seq__41532_42522 = G__42541;
chunk__41534_42523 = G__42542;
count__41535_42524 = G__42543;
i__41536_42525 = G__42544;
continue;
} else {
var child_42545 = cljs.core.first(seq__41532_42539__$1);
if(cljs.core.truth_(child_42545)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_42545);


var G__42546 = cljs.core.next(seq__41532_42539__$1);
var G__42547 = null;
var G__42548 = (0);
var G__42549 = (0);
seq__41532_42522 = G__42546;
chunk__41534_42523 = G__42547;
count__41535_42524 = G__42548;
i__41536_42525 = G__42549;
continue;
} else {
var G__42551 = cljs.core.next(seq__41532_42539__$1);
var G__42552 = null;
var G__42553 = (0);
var G__42554 = (0);
seq__41532_42522 = G__42551;
chunk__41534_42523 = G__42552;
count__41535_42524 = G__42553;
i__41536_42525 = G__42554;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_42521);
}


var G__42556 = seq__41461_42516;
var G__42557 = chunk__41462_42517;
var G__42558 = count__41463_42518;
var G__42559 = (i__41464_42519 + (1));
seq__41461_42516 = G__42556;
chunk__41462_42517 = G__42557;
count__41463_42518 = G__42558;
i__41464_42519 = G__42559;
continue;
} else {
var temp__5735__auto___42561 = cljs.core.seq(seq__41461_42516);
if(temp__5735__auto___42561){
var seq__41461_42562__$1 = temp__5735__auto___42561;
if(cljs.core.chunked_seq_QMARK_(seq__41461_42562__$1)){
var c__4556__auto___42563 = cljs.core.chunk_first(seq__41461_42562__$1);
var G__42564 = cljs.core.chunk_rest(seq__41461_42562__$1);
var G__42565 = c__4556__auto___42563;
var G__42566 = cljs.core.count(c__4556__auto___42563);
var G__42567 = (0);
seq__41461_42516 = G__42564;
chunk__41462_42517 = G__42565;
count__41463_42518 = G__42566;
i__41464_42519 = G__42567;
continue;
} else {
var child_struct_42568 = cljs.core.first(seq__41461_42562__$1);
var children_42569 = shadow.dom.dom_node(child_struct_42568);
if(cljs.core.seq_QMARK_(children_42569)){
var seq__41567_42570 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_42569));
var chunk__41569_42571 = null;
var count__41570_42572 = (0);
var i__41571_42573 = (0);
while(true){
if((i__41571_42573 < count__41570_42572)){
var child_42574 = chunk__41569_42571.cljs$core$IIndexed$_nth$arity$2(null,i__41571_42573);
if(cljs.core.truth_(child_42574)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_42574);


var G__42575 = seq__41567_42570;
var G__42576 = chunk__41569_42571;
var G__42577 = count__41570_42572;
var G__42578 = (i__41571_42573 + (1));
seq__41567_42570 = G__42575;
chunk__41569_42571 = G__42576;
count__41570_42572 = G__42577;
i__41571_42573 = G__42578;
continue;
} else {
var G__42579 = seq__41567_42570;
var G__42580 = chunk__41569_42571;
var G__42581 = count__41570_42572;
var G__42582 = (i__41571_42573 + (1));
seq__41567_42570 = G__42579;
chunk__41569_42571 = G__42580;
count__41570_42572 = G__42581;
i__41571_42573 = G__42582;
continue;
}
} else {
var temp__5735__auto___42583__$1 = cljs.core.seq(seq__41567_42570);
if(temp__5735__auto___42583__$1){
var seq__41567_42584__$1 = temp__5735__auto___42583__$1;
if(cljs.core.chunked_seq_QMARK_(seq__41567_42584__$1)){
var c__4556__auto___42585 = cljs.core.chunk_first(seq__41567_42584__$1);
var G__42586 = cljs.core.chunk_rest(seq__41567_42584__$1);
var G__42587 = c__4556__auto___42585;
var G__42588 = cljs.core.count(c__4556__auto___42585);
var G__42589 = (0);
seq__41567_42570 = G__42586;
chunk__41569_42571 = G__42587;
count__41570_42572 = G__42588;
i__41571_42573 = G__42589;
continue;
} else {
var child_42590 = cljs.core.first(seq__41567_42584__$1);
if(cljs.core.truth_(child_42590)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_42590);


var G__42591 = cljs.core.next(seq__41567_42584__$1);
var G__42592 = null;
var G__42593 = (0);
var G__42594 = (0);
seq__41567_42570 = G__42591;
chunk__41569_42571 = G__42592;
count__41570_42572 = G__42593;
i__41571_42573 = G__42594;
continue;
} else {
var G__42595 = cljs.core.next(seq__41567_42584__$1);
var G__42596 = null;
var G__42597 = (0);
var G__42598 = (0);
seq__41567_42570 = G__42595;
chunk__41569_42571 = G__42596;
count__41570_42572 = G__42597;
i__41571_42573 = G__42598;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_42569);
}


var G__42599 = cljs.core.next(seq__41461_42562__$1);
var G__42600 = null;
var G__42601 = (0);
var G__42602 = (0);
seq__41461_42516 = G__42599;
chunk__41462_42517 = G__42600;
count__41463_42518 = G__42601;
i__41464_42519 = G__42602;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__41609 = cljs.core.seq(node);
var chunk__41610 = null;
var count__41611 = (0);
var i__41612 = (0);
while(true){
if((i__41612 < count__41611)){
var n = chunk__41610.cljs$core$IIndexed$_nth$arity$2(null,i__41612);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__42610 = seq__41609;
var G__42611 = chunk__41610;
var G__42612 = count__41611;
var G__42613 = (i__41612 + (1));
seq__41609 = G__42610;
chunk__41610 = G__42611;
count__41611 = G__42612;
i__41612 = G__42613;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__41609);
if(temp__5735__auto__){
var seq__41609__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__41609__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__41609__$1);
var G__42614 = cljs.core.chunk_rest(seq__41609__$1);
var G__42615 = c__4556__auto__;
var G__42616 = cljs.core.count(c__4556__auto__);
var G__42617 = (0);
seq__41609 = G__42614;
chunk__41610 = G__42615;
count__41611 = G__42616;
i__41612 = G__42617;
continue;
} else {
var n = cljs.core.first(seq__41609__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__42618 = cljs.core.next(seq__41609__$1);
var G__42619 = null;
var G__42620 = (0);
var G__42621 = (0);
seq__41609 = G__42618;
chunk__41610 = G__42619;
count__41611 = G__42620;
i__41612 = G__42621;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__41640 = arguments.length;
switch (G__41640) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__41658 = arguments.length;
switch (G__41658) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__41678 = arguments.length;
switch (G__41678) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__4126__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__4742__auto__ = [];
var len__4736__auto___42631 = arguments.length;
var i__4737__auto___42632 = (0);
while(true){
if((i__4737__auto___42632 < len__4736__auto___42631)){
args__4742__auto__.push((arguments[i__4737__auto___42632]));

var G__42633 = (i__4737__auto___42632 + (1));
i__4737__auto___42632 = G__42633;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__41725_42637 = cljs.core.seq(nodes);
var chunk__41726_42638 = null;
var count__41727_42639 = (0);
var i__41728_42640 = (0);
while(true){
if((i__41728_42640 < count__41727_42639)){
var node_42641 = chunk__41726_42638.cljs$core$IIndexed$_nth$arity$2(null,i__41728_42640);
fragment.appendChild(shadow.dom._to_dom(node_42641));


var G__42642 = seq__41725_42637;
var G__42643 = chunk__41726_42638;
var G__42644 = count__41727_42639;
var G__42645 = (i__41728_42640 + (1));
seq__41725_42637 = G__42642;
chunk__41726_42638 = G__42643;
count__41727_42639 = G__42644;
i__41728_42640 = G__42645;
continue;
} else {
var temp__5735__auto___42646 = cljs.core.seq(seq__41725_42637);
if(temp__5735__auto___42646){
var seq__41725_42647__$1 = temp__5735__auto___42646;
if(cljs.core.chunked_seq_QMARK_(seq__41725_42647__$1)){
var c__4556__auto___42648 = cljs.core.chunk_first(seq__41725_42647__$1);
var G__42649 = cljs.core.chunk_rest(seq__41725_42647__$1);
var G__42650 = c__4556__auto___42648;
var G__42651 = cljs.core.count(c__4556__auto___42648);
var G__42652 = (0);
seq__41725_42637 = G__42649;
chunk__41726_42638 = G__42650;
count__41727_42639 = G__42651;
i__41728_42640 = G__42652;
continue;
} else {
var node_42653 = cljs.core.first(seq__41725_42647__$1);
fragment.appendChild(shadow.dom._to_dom(node_42653));


var G__42654 = cljs.core.next(seq__41725_42647__$1);
var G__42655 = null;
var G__42656 = (0);
var G__42657 = (0);
seq__41725_42637 = G__42654;
chunk__41726_42638 = G__42655;
count__41727_42639 = G__42656;
i__41728_42640 = G__42657;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq41716){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq41716));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__41755_42658 = cljs.core.seq(scripts);
var chunk__41756_42659 = null;
var count__41757_42660 = (0);
var i__41758_42661 = (0);
while(true){
if((i__41758_42661 < count__41757_42660)){
var vec__41784_42662 = chunk__41756_42659.cljs$core$IIndexed$_nth$arity$2(null,i__41758_42661);
var script_tag_42663 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41784_42662,(0),null);
var script_body_42664 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41784_42662,(1),null);
eval(script_body_42664);


var G__42665 = seq__41755_42658;
var G__42666 = chunk__41756_42659;
var G__42667 = count__41757_42660;
var G__42668 = (i__41758_42661 + (1));
seq__41755_42658 = G__42665;
chunk__41756_42659 = G__42666;
count__41757_42660 = G__42667;
i__41758_42661 = G__42668;
continue;
} else {
var temp__5735__auto___42669 = cljs.core.seq(seq__41755_42658);
if(temp__5735__auto___42669){
var seq__41755_42673__$1 = temp__5735__auto___42669;
if(cljs.core.chunked_seq_QMARK_(seq__41755_42673__$1)){
var c__4556__auto___42674 = cljs.core.chunk_first(seq__41755_42673__$1);
var G__42675 = cljs.core.chunk_rest(seq__41755_42673__$1);
var G__42676 = c__4556__auto___42674;
var G__42677 = cljs.core.count(c__4556__auto___42674);
var G__42678 = (0);
seq__41755_42658 = G__42675;
chunk__41756_42659 = G__42676;
count__41757_42660 = G__42677;
i__41758_42661 = G__42678;
continue;
} else {
var vec__41792_42679 = cljs.core.first(seq__41755_42673__$1);
var script_tag_42680 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41792_42679,(0),null);
var script_body_42681 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41792_42679,(1),null);
eval(script_body_42681);


var G__42682 = cljs.core.next(seq__41755_42673__$1);
var G__42683 = null;
var G__42684 = (0);
var G__42685 = (0);
seq__41755_42658 = G__42682;
chunk__41756_42659 = G__42683;
count__41757_42660 = G__42684;
i__41758_42661 = G__42685;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__41799){
var vec__41800 = p__41799;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41800,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41800,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__41818 = arguments.length;
switch (G__41818) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__41828 = cljs.core.seq(style_keys);
var chunk__41829 = null;
var count__41830 = (0);
var i__41831 = (0);
while(true){
if((i__41831 < count__41830)){
var it = chunk__41829.cljs$core$IIndexed$_nth$arity$2(null,i__41831);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__42690 = seq__41828;
var G__42691 = chunk__41829;
var G__42692 = count__41830;
var G__42693 = (i__41831 + (1));
seq__41828 = G__42690;
chunk__41829 = G__42691;
count__41830 = G__42692;
i__41831 = G__42693;
continue;
} else {
var temp__5735__auto__ = cljs.core.seq(seq__41828);
if(temp__5735__auto__){
var seq__41828__$1 = temp__5735__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__41828__$1)){
var c__4556__auto__ = cljs.core.chunk_first(seq__41828__$1);
var G__42694 = cljs.core.chunk_rest(seq__41828__$1);
var G__42695 = c__4556__auto__;
var G__42696 = cljs.core.count(c__4556__auto__);
var G__42697 = (0);
seq__41828 = G__42694;
chunk__41829 = G__42695;
count__41830 = G__42696;
i__41831 = G__42697;
continue;
} else {
var it = cljs.core.first(seq__41828__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__42698 = cljs.core.next(seq__41828__$1);
var G__42699 = null;
var G__42700 = (0);
var G__42701 = (0);
seq__41828 = G__42698;
chunk__41829 = G__42699;
count__41830 = G__42700;
i__41831 = G__42701;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k41848,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__41856 = k41848;
var G__41856__$1 = (((G__41856 instanceof cljs.core.Keyword))?G__41856.fqn:null);
switch (G__41856__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k41848,else__4383__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__41857){
var vec__41858 = p__41857;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41858,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41858,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Coordinate{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__41847){
var self__ = this;
var G__41847__$1 = this;
return (new cljs.core.RecordIter((0),G__41847__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (coll__4377__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
})(this__4376__auto____$1);
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this41849,other41850){
var self__ = this;
var this41849__$1 = this;
return (((!((other41850 == null)))) && ((this41849__$1.constructor === other41850.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this41849__$1.x,other41850.x)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this41849__$1.y,other41850.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this41849__$1.__extmap,other41850.__extmap)));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__41847){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__41888 = cljs.core.keyword_identical_QMARK_;
var expr__41889 = k__4388__auto__;
if(cljs.core.truth_((pred__41888.cljs$core$IFn$_invoke$arity$2 ? pred__41888.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__41889) : pred__41888.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__41889)))){
return (new shadow.dom.Coordinate(G__41847,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__41888.cljs$core$IFn$_invoke$arity$2 ? pred__41888.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__41889) : pred__41888.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__41889)))){
return (new shadow.dom.Coordinate(self__.x,G__41847,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__41847),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__41847){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__41847,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4386__auto__,(0)),cljs.core._nth(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__41855){
var extmap__4419__auto__ = (function (){var G__41912 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__41855,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__41855)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__41912);
} else {
return G__41912;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__41855),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__41855),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__4380__auto__,k__4381__auto__){
var self__ = this;
var this__4380__auto____$1 = this;
return this__4380__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__4381__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__4382__auto__,k41929,else__4383__auto__){
var self__ = this;
var this__4382__auto____$1 = this;
var G__41942 = k41929;
var G__41942__$1 = (((G__41942 instanceof cljs.core.Keyword))?G__41942.fqn:null);
switch (G__41942__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k41929,else__4383__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__4399__auto__,f__4400__auto__,init__4401__auto__){
var self__ = this;
var this__4399__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__4402__auto__,p__41946){
var vec__41947 = p__41946;
var k__4403__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41947,(0),null);
var v__4404__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__41947,(1),null);
return (f__4400__auto__.cljs$core$IFn$_invoke$arity$3 ? f__4400__auto__.cljs$core$IFn$_invoke$arity$3(ret__4402__auto__,k__4403__auto__,v__4404__auto__) : f__4400__auto__.call(null,ret__4402__auto__,k__4403__auto__,v__4404__auto__));
}),init__4401__auto__,this__4399__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__4394__auto__,writer__4395__auto__,opts__4396__auto__){
var self__ = this;
var this__4394__auto____$1 = this;
var pr_pair__4397__auto__ = (function (keyval__4398__auto__){
return cljs.core.pr_sequential_writer(writer__4395__auto__,cljs.core.pr_writer,""," ","",opts__4396__auto__,keyval__4398__auto__);
});
return cljs.core.pr_sequential_writer(writer__4395__auto__,pr_pair__4397__auto__,"#shadow.dom.Size{",", ","}",opts__4396__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__41928){
var self__ = this;
var G__41928__$1 = this;
return (new cljs.core.RecordIter((0),G__41928__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__4378__auto__){
var self__ = this;
var this__4378__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__4375__auto__){
var self__ = this;
var this__4375__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__4384__auto__){
var self__ = this;
var this__4384__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__4376__auto__){
var self__ = this;
var this__4376__auto____$1 = this;
var h__4238__auto__ = self__.__hash;
if((!((h__4238__auto__ == null)))){
return h__4238__auto__;
} else {
var h__4238__auto____$1 = (function (coll__4377__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__4377__auto__));
})(this__4376__auto____$1);
(self__.__hash = h__4238__auto____$1);

return h__4238__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this41930,other41931){
var self__ = this;
var this41930__$1 = this;
return (((!((other41931 == null)))) && ((this41930__$1.constructor === other41931.constructor)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this41930__$1.w,other41931.w)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this41930__$1.h,other41931.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this41930__$1.__extmap,other41931.__extmap)));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__4389__auto__,k__4390__auto__){
var self__ = this;
var this__4389__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__4390__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__4389__auto____$1),self__.__meta),k__4390__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__4390__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__4387__auto__,k__4388__auto__,G__41928){
var self__ = this;
var this__4387__auto____$1 = this;
var pred__42011 = cljs.core.keyword_identical_QMARK_;
var expr__42012 = k__4388__auto__;
if(cljs.core.truth_((pred__42011.cljs$core$IFn$_invoke$arity$2 ? pred__42011.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__42012) : pred__42011.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__42012)))){
return (new shadow.dom.Size(G__41928,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__42011.cljs$core$IFn$_invoke$arity$2 ? pred__42011.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__42012) : pred__42011.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__42012)))){
return (new shadow.dom.Size(self__.w,G__41928,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__4388__auto__,G__41928),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__4392__auto__){
var self__ = this;
var this__4392__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__4379__auto__,G__41928){
var self__ = this;
var this__4379__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__41928,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__4385__auto__,entry__4386__auto__){
var self__ = this;
var this__4385__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__4386__auto__)){
return this__4385__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__4386__auto__,(0)),cljs.core._nth(entry__4386__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__4385__auto____$1,entry__4386__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__4423__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__4423__auto__,writer__4424__auto__){
return cljs.core._write(writer__4424__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__41934){
var extmap__4419__auto__ = (function (){var G__42022 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__41934,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__41934)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__42022);
} else {
return G__42022;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__41934),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__41934),null,cljs.core.not_empty(extmap__4419__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__4610__auto__ = opts;
var l__4611__auto__ = a__4610__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__4611__auto__)){
var G__42724 = (i + (1));
var G__42725 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__42724;
ret = G__42725;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__42058){
var vec__42059 = p__42058;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42059,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42059,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__42069 = arguments.length;
switch (G__42069) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5733__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5733__auto__)){
var child = temp__5733__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__42731 = ps;
var G__42732 = (i + (1));
el__$1 = G__42731;
i = G__42732;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__42118 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42118,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42118,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42118,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__42123_42733 = cljs.core.seq(props);
var chunk__42125_42734 = null;
var count__42126_42735 = (0);
var i__42127_42736 = (0);
while(true){
if((i__42127_42736 < count__42126_42735)){
var vec__42146_42737 = chunk__42125_42734.cljs$core$IIndexed$_nth$arity$2(null,i__42127_42736);
var k_42738 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42146_42737,(0),null);
var v_42739 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42146_42737,(1),null);
el.setAttributeNS((function (){var temp__5735__auto__ = cljs.core.namespace(k_42738);
if(cljs.core.truth_(temp__5735__auto__)){
var ns = temp__5735__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_42738),v_42739);


var G__42740 = seq__42123_42733;
var G__42741 = chunk__42125_42734;
var G__42742 = count__42126_42735;
var G__42743 = (i__42127_42736 + (1));
seq__42123_42733 = G__42740;
chunk__42125_42734 = G__42741;
count__42126_42735 = G__42742;
i__42127_42736 = G__42743;
continue;
} else {
var temp__5735__auto___42744 = cljs.core.seq(seq__42123_42733);
if(temp__5735__auto___42744){
var seq__42123_42745__$1 = temp__5735__auto___42744;
if(cljs.core.chunked_seq_QMARK_(seq__42123_42745__$1)){
var c__4556__auto___42746 = cljs.core.chunk_first(seq__42123_42745__$1);
var G__42747 = cljs.core.chunk_rest(seq__42123_42745__$1);
var G__42748 = c__4556__auto___42746;
var G__42749 = cljs.core.count(c__4556__auto___42746);
var G__42750 = (0);
seq__42123_42733 = G__42747;
chunk__42125_42734 = G__42748;
count__42126_42735 = G__42749;
i__42127_42736 = G__42750;
continue;
} else {
var vec__42155_42751 = cljs.core.first(seq__42123_42745__$1);
var k_42752 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42155_42751,(0),null);
var v_42753 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42155_42751,(1),null);
el.setAttributeNS((function (){var temp__5735__auto____$1 = cljs.core.namespace(k_42752);
if(cljs.core.truth_(temp__5735__auto____$1)){
var ns = temp__5735__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_42752),v_42753);


var G__42754 = cljs.core.next(seq__42123_42745__$1);
var G__42755 = null;
var G__42756 = (0);
var G__42757 = (0);
seq__42123_42733 = G__42754;
chunk__42125_42734 = G__42755;
count__42126_42735 = G__42756;
i__42127_42736 = G__42757;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__42170 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42170,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__42170,(1),null);
var seq__42174_42758 = cljs.core.seq(node_children);
var chunk__42176_42759 = null;
var count__42177_42760 = (0);
var i__42178_42761 = (0);
while(true){
if((i__42178_42761 < count__42177_42760)){
var child_struct_42762 = chunk__42176_42759.cljs$core$IIndexed$_nth$arity$2(null,i__42178_42761);
if((!((child_struct_42762 == null)))){
if(typeof child_struct_42762 === 'string'){
var text_42765 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_42765),child_struct_42762].join(''));
} else {
var children_42766 = shadow.dom.svg_node(child_struct_42762);
if(cljs.core.seq_QMARK_(children_42766)){
var seq__42232_42767 = cljs.core.seq(children_42766);
var chunk__42234_42768 = null;
var count__42235_42769 = (0);
var i__42236_42770 = (0);
while(true){
if((i__42236_42770 < count__42235_42769)){
var child_42771 = chunk__42234_42768.cljs$core$IIndexed$_nth$arity$2(null,i__42236_42770);
if(cljs.core.truth_(child_42771)){
node.appendChild(child_42771);


var G__42772 = seq__42232_42767;
var G__42773 = chunk__42234_42768;
var G__42774 = count__42235_42769;
var G__42775 = (i__42236_42770 + (1));
seq__42232_42767 = G__42772;
chunk__42234_42768 = G__42773;
count__42235_42769 = G__42774;
i__42236_42770 = G__42775;
continue;
} else {
var G__42776 = seq__42232_42767;
var G__42777 = chunk__42234_42768;
var G__42778 = count__42235_42769;
var G__42779 = (i__42236_42770 + (1));
seq__42232_42767 = G__42776;
chunk__42234_42768 = G__42777;
count__42235_42769 = G__42778;
i__42236_42770 = G__42779;
continue;
}
} else {
var temp__5735__auto___42780 = cljs.core.seq(seq__42232_42767);
if(temp__5735__auto___42780){
var seq__42232_42781__$1 = temp__5735__auto___42780;
if(cljs.core.chunked_seq_QMARK_(seq__42232_42781__$1)){
var c__4556__auto___42782 = cljs.core.chunk_first(seq__42232_42781__$1);
var G__42783 = cljs.core.chunk_rest(seq__42232_42781__$1);
var G__42784 = c__4556__auto___42782;
var G__42785 = cljs.core.count(c__4556__auto___42782);
var G__42786 = (0);
seq__42232_42767 = G__42783;
chunk__42234_42768 = G__42784;
count__42235_42769 = G__42785;
i__42236_42770 = G__42786;
continue;
} else {
var child_42787 = cljs.core.first(seq__42232_42781__$1);
if(cljs.core.truth_(child_42787)){
node.appendChild(child_42787);


var G__42788 = cljs.core.next(seq__42232_42781__$1);
var G__42789 = null;
var G__42790 = (0);
var G__42791 = (0);
seq__42232_42767 = G__42788;
chunk__42234_42768 = G__42789;
count__42235_42769 = G__42790;
i__42236_42770 = G__42791;
continue;
} else {
var G__42792 = cljs.core.next(seq__42232_42781__$1);
var G__42793 = null;
var G__42794 = (0);
var G__42795 = (0);
seq__42232_42767 = G__42792;
chunk__42234_42768 = G__42793;
count__42235_42769 = G__42794;
i__42236_42770 = G__42795;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_42766);
}
}


var G__42796 = seq__42174_42758;
var G__42797 = chunk__42176_42759;
var G__42798 = count__42177_42760;
var G__42799 = (i__42178_42761 + (1));
seq__42174_42758 = G__42796;
chunk__42176_42759 = G__42797;
count__42177_42760 = G__42798;
i__42178_42761 = G__42799;
continue;
} else {
var G__42800 = seq__42174_42758;
var G__42801 = chunk__42176_42759;
var G__42802 = count__42177_42760;
var G__42803 = (i__42178_42761 + (1));
seq__42174_42758 = G__42800;
chunk__42176_42759 = G__42801;
count__42177_42760 = G__42802;
i__42178_42761 = G__42803;
continue;
}
} else {
var temp__5735__auto___42804 = cljs.core.seq(seq__42174_42758);
if(temp__5735__auto___42804){
var seq__42174_42805__$1 = temp__5735__auto___42804;
if(cljs.core.chunked_seq_QMARK_(seq__42174_42805__$1)){
var c__4556__auto___42806 = cljs.core.chunk_first(seq__42174_42805__$1);
var G__42807 = cljs.core.chunk_rest(seq__42174_42805__$1);
var G__42808 = c__4556__auto___42806;
var G__42809 = cljs.core.count(c__4556__auto___42806);
var G__42810 = (0);
seq__42174_42758 = G__42807;
chunk__42176_42759 = G__42808;
count__42177_42760 = G__42809;
i__42178_42761 = G__42810;
continue;
} else {
var child_struct_42812 = cljs.core.first(seq__42174_42805__$1);
if((!((child_struct_42812 == null)))){
if(typeof child_struct_42812 === 'string'){
var text_42813 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_42813),child_struct_42812].join(''));
} else {
var children_42814 = shadow.dom.svg_node(child_struct_42812);
if(cljs.core.seq_QMARK_(children_42814)){
var seq__42249_42815 = cljs.core.seq(children_42814);
var chunk__42251_42816 = null;
var count__42252_42817 = (0);
var i__42253_42818 = (0);
while(true){
if((i__42253_42818 < count__42252_42817)){
var child_42819 = chunk__42251_42816.cljs$core$IIndexed$_nth$arity$2(null,i__42253_42818);
if(cljs.core.truth_(child_42819)){
node.appendChild(child_42819);


var G__42820 = seq__42249_42815;
var G__42821 = chunk__42251_42816;
var G__42822 = count__42252_42817;
var G__42823 = (i__42253_42818 + (1));
seq__42249_42815 = G__42820;
chunk__42251_42816 = G__42821;
count__42252_42817 = G__42822;
i__42253_42818 = G__42823;
continue;
} else {
var G__42824 = seq__42249_42815;
var G__42825 = chunk__42251_42816;
var G__42826 = count__42252_42817;
var G__42827 = (i__42253_42818 + (1));
seq__42249_42815 = G__42824;
chunk__42251_42816 = G__42825;
count__42252_42817 = G__42826;
i__42253_42818 = G__42827;
continue;
}
} else {
var temp__5735__auto___42828__$1 = cljs.core.seq(seq__42249_42815);
if(temp__5735__auto___42828__$1){
var seq__42249_42829__$1 = temp__5735__auto___42828__$1;
if(cljs.core.chunked_seq_QMARK_(seq__42249_42829__$1)){
var c__4556__auto___42830 = cljs.core.chunk_first(seq__42249_42829__$1);
var G__42831 = cljs.core.chunk_rest(seq__42249_42829__$1);
var G__42832 = c__4556__auto___42830;
var G__42833 = cljs.core.count(c__4556__auto___42830);
var G__42834 = (0);
seq__42249_42815 = G__42831;
chunk__42251_42816 = G__42832;
count__42252_42817 = G__42833;
i__42253_42818 = G__42834;
continue;
} else {
var child_42835 = cljs.core.first(seq__42249_42829__$1);
if(cljs.core.truth_(child_42835)){
node.appendChild(child_42835);


var G__42836 = cljs.core.next(seq__42249_42829__$1);
var G__42837 = null;
var G__42838 = (0);
var G__42839 = (0);
seq__42249_42815 = G__42836;
chunk__42251_42816 = G__42837;
count__42252_42817 = G__42838;
i__42253_42818 = G__42839;
continue;
} else {
var G__42840 = cljs.core.next(seq__42249_42829__$1);
var G__42841 = null;
var G__42842 = (0);
var G__42843 = (0);
seq__42249_42815 = G__42840;
chunk__42251_42816 = G__42841;
count__42252_42817 = G__42842;
i__42253_42818 = G__42843;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_42814);
}
}


var G__42844 = cljs.core.next(seq__42174_42805__$1);
var G__42845 = null;
var G__42846 = (0);
var G__42847 = (0);
seq__42174_42758 = G__42844;
chunk__42176_42759 = G__42845;
count__42177_42760 = G__42846;
i__42178_42761 = G__42847;
continue;
} else {
var G__42848 = cljs.core.next(seq__42174_42805__$1);
var G__42849 = null;
var G__42850 = (0);
var G__42851 = (0);
seq__42174_42758 = G__42848;
chunk__42176_42759 = G__42849;
count__42177_42760 = G__42850;
i__42178_42761 = G__42851;
continue;
}
}
} else {
}
}
break;
}

return node;
});
goog.object.set(shadow.dom.SVGElement,"string",true);

goog.object.set(shadow.dom._to_svg,"string",(function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

goog.object.set(shadow.dom.SVGElement,"null",true);

goog.object.set(shadow.dom._to_svg,"null",(function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__4742__auto__ = [];
var len__4736__auto___42852 = arguments.length;
var i__4737__auto___42853 = (0);
while(true){
if((i__4737__auto___42853 < len__4736__auto___42852)){
args__4742__auto__.push((arguments[i__4737__auto___42853]));

var G__42854 = (i__4737__auto___42853 + (1));
i__4737__auto___42853 = G__42854;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((1) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4743__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq42267){
var G__42268 = cljs.core.first(seq42267);
var seq42267__$1 = cljs.core.next(seq42267);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__42268,seq42267__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__42291 = arguments.length;
switch (G__42291) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__4115__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__4115__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__4115__auto__;
}
})())){
var c__26226__auto___42859 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__26227__auto__ = (function (){var switch__26068__auto__ = (function (state_42309){
var state_val_42310 = (state_42309[(1)]);
if((state_val_42310 === (1))){
var state_42309__$1 = state_42309;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_42309__$1,(2),once_or_cleanup);
} else {
if((state_val_42310 === (2))){
var inst_42305 = (state_42309[(2)]);
var inst_42306 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_42309__$1 = (function (){var statearr_42320 = state_42309;
(statearr_42320[(7)] = inst_42305);

return statearr_42320;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_42309__$1,inst_42306);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__26069__auto__ = null;
var shadow$dom$state_machine__26069__auto____0 = (function (){
var statearr_42325 = [null,null,null,null,null,null,null,null];
(statearr_42325[(0)] = shadow$dom$state_machine__26069__auto__);

(statearr_42325[(1)] = (1));

return statearr_42325;
});
var shadow$dom$state_machine__26069__auto____1 = (function (state_42309){
while(true){
var ret_value__26070__auto__ = (function (){try{while(true){
var result__26071__auto__ = switch__26068__auto__(state_42309);
if(cljs.core.keyword_identical_QMARK_(result__26071__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__26071__auto__;
}
break;
}
}catch (e42327){var ex__26072__auto__ = e42327;
var statearr_42328_42864 = state_42309;
(statearr_42328_42864[(2)] = ex__26072__auto__);


if(cljs.core.seq((state_42309[(4)]))){
var statearr_42330_42865 = state_42309;
(statearr_42330_42865[(1)] = cljs.core.first((state_42309[(4)])));

} else {
throw ex__26072__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__26070__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__42866 = state_42309;
state_42309 = G__42866;
continue;
} else {
return ret_value__26070__auto__;
}
break;
}
});
shadow$dom$state_machine__26069__auto__ = function(state_42309){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__26069__auto____0.call(this);
case 1:
return shadow$dom$state_machine__26069__auto____1.call(this,state_42309);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__26069__auto____0;
shadow$dom$state_machine__26069__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__26069__auto____1;
return shadow$dom$state_machine__26069__auto__;
})()
})();
var state__26228__auto__ = (function (){var statearr_42335 = f__26227__auto__();
(statearr_42335[(6)] = c__26226__auto___42859);

return statearr_42335;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__26228__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
